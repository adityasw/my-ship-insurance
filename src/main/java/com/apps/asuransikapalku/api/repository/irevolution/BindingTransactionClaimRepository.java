package com.apps.asuransikapalku.api.repository.irevolution;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionClaim;

@Repository
public interface BindingTransactionClaimRepository extends JpaRepository<BindingTransactionClaim, Long> {

}
