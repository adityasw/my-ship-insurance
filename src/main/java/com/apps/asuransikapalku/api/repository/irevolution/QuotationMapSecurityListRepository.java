package com.apps.asuransikapalku.api.repository.irevolution;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.irevolution.QuotationMapSecurityList;

@Repository
public interface QuotationMapSecurityListRepository extends JpaRepository<QuotationMapSecurityList, Long> {

}
