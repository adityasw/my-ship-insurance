package com.apps.asuransikapalku.api.repository.claimfinancing;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinance;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceDocument;

@Repository
public interface ClaimFinanceDocumentRepository
		extends JpaRepository<ClaimFinanceDocument, Long>, JpaSpecificationExecutor<ClaimFinanceDocument> {
	List<ClaimFinanceDocument> findByClaimFinance(ClaimFinance claimFinance);
}
