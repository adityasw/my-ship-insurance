package com.apps.asuransikapalku.api.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.user.SupplierDetail;
import com.apps.asuransikapalku.api.model.entity.user.SupplierDetailPk;

@Repository
public interface SupplierDetailRepository extends JpaRepository<SupplierDetail, SupplierDetailPk> {
	@Query("select sd from SupplierDetail sd where sd.supplierDetailPk.idSupplier = ?1")
	SupplierDetail findByIdSupplier(Long idSupplier);
	
}
