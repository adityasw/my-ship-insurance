package com.apps.asuransikapalku.api.repository.view;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.view.ViewClaimFinanceChat;

@Repository
public interface ViewClaimFinanceChatRepository
		extends JpaRepository<ViewClaimFinanceChat, Long>, JpaSpecificationExecutor<ViewClaimFinanceChat> {
	List<ViewClaimFinanceChat> findByIdClaimFinance(Long idClaimFinance);
}
