package com.apps.asuransikapalku.api.repository.user;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.user.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long>, JpaSpecificationExecutor<Admin> {
	Optional<Admin> findById(Long id);
	
	Admin findByEmail(String email);
	
	List<Admin> findByIdIn(List<Long> ids);
	
	@Query("select count(a) from Admin a join User u on u.admin.id = a.id where upper(u.username) = ?1")
	int getCountByUsername(String username);
	
	@Query("select count(a) from Admin a where a.email = ?1")
	int getCountByEmail(String email);
}
