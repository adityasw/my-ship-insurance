package com.apps.asuransikapalku.api.repository.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.user.SupplierClient;
import com.apps.asuransikapalku.api.model.entity.user.SupplierDetailPk;

@Repository
public interface SupplierClientRepository extends JpaRepository<SupplierClient, SupplierDetailPk> {
	@Query("select sc from SupplierClient sc where sc.supplierDetailPk.idSupplier = ?1")
	List<SupplierClient> findByIdSupplier(Long idSupplier);
	List<SupplierClient> findBySupplierDetailPk(SupplierDetailPk supplierDetailPk);
}
