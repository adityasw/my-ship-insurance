package com.apps.asuransikapalku.api.repository.claimfinancing;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinance;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceOffering;

@Repository
public interface ClaimFinanceOfferingRepository
		extends JpaRepository<ClaimFinanceOffering, Long>, JpaSpecificationExecutor<ClaimFinanceOffering> {
	List<ClaimFinanceOffering> findByClaimFinance(ClaimFinance claimFinance);
	
	@Query("select cfo.claimFinance.id from ClaimFinanceOffering cfo where cfo.supplier.id = ?1")
	List<Long> getClaimFinanceIdBySupplierId(Long supplierId);
}
