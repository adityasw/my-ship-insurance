package com.apps.asuransikapalku.api.repository.occupation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.occupation.Occupation;
import com.apps.asuransikapalku.api.model.entity.occupation.OccupationMap;

@Repository
public interface OccupationMapRepository extends JpaRepository<OccupationMap, Long> {
	OccupationMap findByOccupation(Occupation occupation);
}
