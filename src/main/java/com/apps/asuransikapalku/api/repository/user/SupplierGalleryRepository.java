package com.apps.asuransikapalku.api.repository.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.user.SupplierDetailPk;
import com.apps.asuransikapalku.api.model.entity.user.SupplierGallery;

@Repository
public interface SupplierGalleryRepository extends JpaRepository<SupplierGallery, SupplierDetailPk> {
	@Query("select sg from SupplierGallery sg where sg.supplierDetailPk.idSupplier = ?1")
	List<SupplierGallery> findByIdSupplier(Long idSupplier);
	List<SupplierGallery> findBySupplierDetailPk(SupplierDetailPk supplierDetailPk);
}
