package com.apps.asuransikapalku.api.repository.ship;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.ship.ShipType;

@Repository
public interface ShipTypeRepository extends JpaRepository<ShipType, Long>, JpaSpecificationExecutor<ShipType> {
	Optional<ShipType> findById(Long id);
	
	ShipType findByCode(String code);
	
	List<ShipType> findByIdIn(List<Long> ids);
	
	List<ShipType> findByIsActive(boolean isActive);
	
	@Query("select count(st) from ShipType st where upper(st.code) = ?1")
	Integer getCountByCode(String code);
	
	@Query("select count(st) from ShipType st where upper(st.name) = ?1")
	Integer getCountByName(String name);
	
	@Query("select max(st.id) from ShipType st")
	Integer getMaxId();
}
