package com.apps.asuransikapalku.api.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.user.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findByToken(String token);

	User findByUsername(String username);
	
	User findByUsernameAndPasswordAndIsActive(String username, String password, boolean isActive);

	User findByUsernameAndIsActive(String username, boolean isActive);
}
