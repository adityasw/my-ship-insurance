package com.apps.asuransikapalku.api.repository.claimfinancing;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceChat;

@Repository
public interface ClaimFinanceChatRepository
		extends JpaRepository<ClaimFinanceChat, Long>, JpaSpecificationExecutor<ClaimFinanceChat> {
	@Query("select cfc from ClaimFinanceChat cfc where cfc.idClaimFinance = ?1 and (cfc.sender = ?2 or cfc.receiver = ?3) order by cfc.id asc")
	List<ClaimFinanceChat> findByIdClaimFinanceAndSenderAndReceiverOrderByIdAsc(Long idClaimFinance, String sender,
			String receiver);
}
