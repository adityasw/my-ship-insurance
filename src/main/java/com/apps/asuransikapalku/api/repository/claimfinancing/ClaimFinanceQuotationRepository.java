package com.apps.asuransikapalku.api.repository.claimfinancing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceQuotation;

@Repository
public interface ClaimFinanceQuotationRepository
		extends JpaRepository<ClaimFinanceQuotation, Long>, JpaSpecificationExecutor<ClaimFinanceQuotation> {

}
