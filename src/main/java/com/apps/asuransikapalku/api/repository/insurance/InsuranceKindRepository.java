package com.apps.asuransikapalku.api.repository.insurance;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceKind;
import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceType;

@Repository
public interface InsuranceKindRepository extends JpaRepository<InsuranceKind, Long>, JpaSpecificationExecutor<InsuranceKind> {
	Optional<InsuranceKind> findById(Long id);
	
	InsuranceKind findByCode(String code);
	
	List<InsuranceKind> findByIdIn(List<Long> ids);
	
	List<InsuranceKind> findByInsuranceTypeAndIsActive(InsuranceType insuranceType, boolean isActive);
	
	@Query("select count(ik) from InsuranceKind ik where upper(ik.code) = ?1")
	Integer getCountByCode(String code);
	
	@Query("select count(ik) from InsuranceKind ik where upper(ik.name) = ?1")
	Integer getCountByName(String name);
	
	@Query("select max(ik.id) from InsuranceKind ik")
	Integer getMaxId();
}
