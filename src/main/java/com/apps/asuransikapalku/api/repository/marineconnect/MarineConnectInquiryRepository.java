package com.apps.asuransikapalku.api.repository.marineconnect;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectInquiry;

@Repository
public interface MarineConnectInquiryRepository extends JpaRepository<MarineConnectInquiry, Long> {
	Optional<MarineConnectInquiry> findById(Long id);
}
