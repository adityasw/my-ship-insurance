package com.apps.asuransikapalku.api.repository.master;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.master.ClassificationBureau;

@Repository
public interface ClassifiacationBureauRepository extends JpaRepository<ClassificationBureau, Long>, JpaSpecificationExecutor<ClassificationBureau>{
	Optional<ClassificationBureau> findById(Long id);
	
	@Query("select count(cb) from ClassificationBureau cb where upper(cb.name) = ?1")
	Integer getCountByName(String name);
}
