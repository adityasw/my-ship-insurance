package com.apps.asuransikapalku.api.repository.master;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.master.City;

@Repository
public interface CityRepository extends JpaRepository<City, String> {
	Optional<City> findById(String id);

	@Query("select c from City c where c.province.id = ?1")
	List<City> findByProvinceId(String id);
}
