package com.apps.asuransikapalku.api.repository.ship;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.ship.LossRecord;

@Repository
public interface LossRecordRepository extends JpaRepository<LossRecord, Long> {

}
