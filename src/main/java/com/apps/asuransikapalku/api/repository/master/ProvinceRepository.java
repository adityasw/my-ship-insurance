package com.apps.asuransikapalku.api.repository.master;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.master.Province;

@Repository
public interface ProvinceRepository extends JpaRepository<Province, String> {
	Optional<Province> findById(String id);
}
