package com.apps.asuransikapalku.api.repository.view;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.view.ViewProjectTc;

@Repository
public interface ViewProjectTcRepository extends JpaRepository<ViewProjectTc, Long>, JpaSpecificationExecutor<ViewProjectTc> {
	List<ViewProjectTc> findByIdBinding(Long id);
}
