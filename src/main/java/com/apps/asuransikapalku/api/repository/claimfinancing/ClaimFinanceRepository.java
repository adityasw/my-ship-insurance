package com.apps.asuransikapalku.api.repository.claimfinancing;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinance;

@Repository
public interface ClaimFinanceRepository extends JpaRepository<ClaimFinance, Long> {
	Optional<ClaimFinance> findById(Long id);
}
