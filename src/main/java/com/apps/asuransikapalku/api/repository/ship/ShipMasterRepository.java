package com.apps.asuransikapalku.api.repository.ship;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.ship.ShipMaster;

@Repository
public interface ShipMasterRepository extends JpaRepository<ShipMaster, Long>, JpaSpecificationExecutor<ShipMaster> {
	Optional<ShipMaster> findById(Long id);
	
	List<ShipMaster> findByIdIn(List<Long> ids);
}
