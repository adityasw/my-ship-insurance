package com.apps.asuransikapalku.api.repository.irevolution;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransaction;

@Repository
public interface BindingTransactionRepository extends JpaRepository<BindingTransaction, Long>, JpaSpecificationExecutor<BindingTransaction> {
	Optional<BindingTransaction> findById(Long id);
	
	@Query("select count(bt) from BindingTransaction bt where bt.shipMaster.id = ?1")
	Integer getCountByShipMasterId(Long id);
}
