package com.apps.asuransikapalku.api.repository.view;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.dto.view.ViewChat;

@Repository
public interface ViewChatRepository extends JpaRepository<ViewChat, Long> {
	List<ViewChat> findByIdReferenceAndIsProjectAndIsSupply(Long idReference, boolean isProject, boolean isSupply);
}
