package com.apps.asuransikapalku.api.repository.marineconnect;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectInvoice;

@Repository
public interface MarineConnectInvoiceRepository extends JpaRepository<MarineConnectInvoice, Long> {
	@Query("select mci from MarineConnectInvoice mci where mci.marineConnect.id = ?1")
	MarineConnectInvoice findByIdMarineConnect(Long idMarineConnect);
}
