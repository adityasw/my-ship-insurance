package com.apps.asuransikapalku.api.repository.irevolution;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransaction;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionHistory;

@Repository
public interface BindingTransactionHistoryRepository
		extends JpaRepository<BindingTransactionHistory, Long>, JpaSpecificationExecutor<BindingTransactionHistory> {
	Optional<BindingTransactionHistory> findById(Long id);

	List<BindingTransactionHistory> findByBindingTransaction(BindingTransaction bindingTransaction);
	
	List<BindingTransactionHistory> findByBindingTransactionAndStatus(BindingTransaction bindingTransaction, String status);
}
