package com.apps.asuransikapalku.api.repository.claimfinancing;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinance;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceMapFinancer;

@Repository
public interface ClaimFinanceMapFinancerRepository
		extends JpaRepository<ClaimFinanceMapFinancer, Long>, JpaSpecificationExecutor<ClaimFinanceMapFinancer> {
	List<ClaimFinanceMapFinancer> findByClaimFinanceOrderByIdAsc(ClaimFinance claimFinance);
	
	@Modifying
	@Query("delete from ClaimFinanceMapFinancer cfmf where cfmf.supplier.id not in (?1)")
	void deleteBatch(List<Long> ids);
}
