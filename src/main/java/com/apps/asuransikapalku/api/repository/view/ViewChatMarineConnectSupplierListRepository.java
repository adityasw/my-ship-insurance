package com.apps.asuransikapalku.api.repository.view;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.view.ViewChatMarineConnectSupplierList;

@Repository
public interface ViewChatMarineConnectSupplierListRepository extends JpaRepository<ViewChatMarineConnectSupplierList, Long> {
	List<ViewChatMarineConnectSupplierList> findByReceiver(String receiver);
}
