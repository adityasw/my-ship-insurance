package com.apps.asuransikapalku.api.repository.claimfinancing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceTracking;

@Repository
public interface ClaimFinanceTrackingRepository
		extends JpaRepository<ClaimFinanceTracking, Long>, JpaSpecificationExecutor<ClaimFinanceTracking> {

}
