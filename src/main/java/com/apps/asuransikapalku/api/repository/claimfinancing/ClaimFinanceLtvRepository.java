package com.apps.asuransikapalku.api.repository.claimfinancing;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceLtv;

@Repository
public interface ClaimFinanceLtvRepository
		extends JpaRepository<ClaimFinanceLtv, Long>, JpaSpecificationExecutor<ClaimFinanceLtv> {
	@Query("select cfl from ClaimFinanceLtv cfl where cfl.claimFinance.id = ?1")
	List<ClaimFinanceLtv> findByIdClaimFinance(Long idClaimFinance);
}
