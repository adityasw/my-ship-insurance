package com.apps.asuransikapalku.api.repository.marineconnect;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.marineconnect.ChatMarineConnect;

@Repository
public interface ChatMarineConnectRepository extends JpaRepository<ChatMarineConnect, Long> {
	public List<ChatMarineConnect> findBySenderAndReceiverOrderByIdAsc(String sender, String receiver);
	
	@Query("select c from ChatMarineConnect c where (c.sender = ?1 or c.receiver = ?1) and (c.sender = ?2 or c.receiver = ?2) order by c.id asc")
	public List<ChatMarineConnect> findBySenderReceiver(String sender, String receiver);
	
	@Modifying
	@Query("update ChatMarineConnect c set c.isReadSupply = true where (c.sender = ?1 or c.receiver = ?1) and (c.sender = ?2 or c.receiver = ?2)")
	void updateStatusRead(String sender, String receiver);
}
