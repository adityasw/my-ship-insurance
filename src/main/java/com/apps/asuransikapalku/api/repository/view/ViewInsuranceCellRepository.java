package com.apps.asuransikapalku.api.repository.view;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.view.ViewInsuranceCell;

@Repository
public interface ViewInsuranceCellRepository extends JpaRepository<ViewInsuranceCell, Long>, JpaSpecificationExecutor<ViewInsuranceCell> {

}
