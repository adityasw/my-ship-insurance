package com.apps.asuransikapalku.api.repository.claimfinancing;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinance;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceComission;

@Repository
public interface ClaimFinanceComissionRepository
		extends JpaRepository<ClaimFinanceComission, Long>, JpaSpecificationExecutor<ClaimFinanceComission> {
	List<ClaimFinanceComission> findByClaimFinanceAndUsernameOrderByIdAsc(ClaimFinance claimFinance, String username);
}
