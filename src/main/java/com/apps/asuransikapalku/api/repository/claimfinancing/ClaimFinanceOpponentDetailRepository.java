package com.apps.asuransikapalku.api.repository.claimfinancing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceOpponentDetail;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceOpponentDetailPk;

@Repository
public interface ClaimFinanceOpponentDetailRepository extends JpaRepository<ClaimFinanceOpponentDetail, ClaimFinanceOpponentDetailPk> {
	ClaimFinanceOpponentDetail findByPk(ClaimFinanceOpponentDetailPk pk);
	
	@Query("select d from ClaimFinanceOpponentDetail d where d.pk.idClaimFinance = ?1")
	ClaimFinanceOpponentDetail findByIdClaimFinance(Long idClaimFinance);
}
