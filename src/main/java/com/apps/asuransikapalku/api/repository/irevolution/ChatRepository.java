package com.apps.asuransikapalku.api.repository.irevolution;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.irevolution.Chat;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {
	List<Chat> findByIdReferenceAndIsProjectOrderByIdAsc(Long idReference, boolean isProject);
	
	List<Chat> findByIdReferenceAndIsProjectAndIsSupplyOrderByIdAsc(Long idReference, boolean isProject, boolean isSupply);
	
	@Query("select c from Chat c where (c.sender = ?1 or c.receiver = ?1) and c.isSupply = ?2 and c.idReference is null order by id asc")
	List<Chat> findBySenderAndIsSupply(String sender, boolean isSupply);
	
	@Query("select c from Chat c where c.idReference = ?1 and c.isProject = ?2 and c.isSupply = ?3 and (c.sender = ?4 or c.receiver = ?5) order by id asc")
	List<Chat> findByIdReferenceAndIsProjectAndIsSupplyAndSenderAndReceiverOrderByIdAsc(Long idReference, boolean isProject, boolean isSupply, String sender, String receiver);

	List<Chat> findByCodeOrderByIdAsc(String code);
	
	@Query("select distinct(c.code) from Chat c where c.idReference = ?1 and c.isProject = ?2 and c.isSupply = ?3")
	String findCodeByIdReferenceAndIsProjectAndIsSupply(Long idReference, boolean isProject, boolean isSupply);

	@Query("select c.code from Chat c where c.sender = ?1 and c.idReference = ?2 and c.isProject = ?3")
	String findCodeBySenderAndIdReferenceAndIsProject(String sender, Long idReference, boolean isProject);
	
	@Modifying
	@Query("update Chat c set c.isRead = true where c.code = ?1")
	void updateReadFlagByCode(String code);
}
