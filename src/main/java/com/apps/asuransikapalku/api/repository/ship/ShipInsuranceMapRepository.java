package com.apps.asuransikapalku.api.repository.ship;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.ship.ShipInsuranceMap;

@Repository
public interface ShipInsuranceMapRepository
		extends JpaRepository<ShipInsuranceMap, Long>, JpaSpecificationExecutor<ShipInsuranceMap> {
	@Modifying
	@Query("delete from ShipInsuranceMap sim where sim.insuranceMaster.id not in (:insuranceIds) and sim.shipMaster.id = :idShip")
	void deleteMap(@Param("insuranceIds") List<Long> insuranceIds, @Param("idShip") Long idShip);
}
