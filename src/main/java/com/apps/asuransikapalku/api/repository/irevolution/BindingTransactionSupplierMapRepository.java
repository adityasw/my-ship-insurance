package com.apps.asuransikapalku.api.repository.irevolution;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransaction;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionSupplierMap;

@Repository
public interface BindingTransactionSupplierMapRepository extends JpaRepository<BindingTransactionSupplierMap, Long>,
		JpaSpecificationExecutor<BindingTransactionSupplierMap> {
	Optional<BindingTransactionSupplierMap> findById(Long id);

	List<BindingTransactionSupplierMap> findByBindingTransaction(BindingTransaction bindingTransaction);
	
	@Query("select bts from BindingTransactionSupplierMap bts where bts.bindingTransaction.id = ?1 and bts.supplier.id = ?2")
	List<BindingTransactionSupplierMap> findByIdBindingTransactionAndSupplier(Long idBinding, Long idSupplier);
	
	@Modifying
	@Query("delete from BindingTransactionSupplierMap btsm where btsm.insuranceMaster.id not in (:insuranceIds) and btsm.bindingTransaction.id = :idBinding")
	void deleteMap(@Param("insuranceIds") List<Long> insuranceIds, @Param("idBinding") Long idBinding);
	
	@Modifying
	@Query("delete from BindingTransactionSupplierMap btsm where btsm.supplier.id not in (:supplierIds) and btsm.bindingTransaction.id = :idBinding")
	void deleteMapBySupplierIds(@Param("supplierIds") List<Long> supplierIds, @Param("idBinding") Long idBinding);
	
	@Query("select btsm.bid from BindingTransactionSupplierMap btsm where btsm.bindingTransaction.id = ?1 and btsm.supplier.id = ?2")
	Boolean isBid(Long idBinding, Long idSupplier);
	
	@Query("select btsm.supplier.id from BindingTransactionSupplierMap btsm where btsm.bindingTransaction.id = ?1 and btsm.bid = true")
	List<Long> getListSupplierIdByBindingId(Long id);
}
