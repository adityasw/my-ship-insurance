package com.apps.asuransikapalku.api.repository.marineconnect;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectQuotation;

@Repository
public interface MarineConnectQuotationRepository extends JpaRepository<MarineConnectQuotation, Long> {
	@Query("select mcq from MarineConnectQuotation mcq where mcq.marineConnect.id = ?1")
	MarineConnectQuotation findByIdMarineConnect(Long id);
}
