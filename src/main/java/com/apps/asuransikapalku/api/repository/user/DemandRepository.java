package com.apps.asuransikapalku.api.repository.user;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.user.Demand;

@Repository
public interface DemandRepository extends JpaRepository<Demand, Long>, JpaSpecificationExecutor<Demand>{
	
	Optional<Demand> findById(Long id);
	
	Demand findByEmail(String email);
	
	List<Demand> findByIdIn(List<Long> id);
	
	@Query("select count(d) from Demand d where d.email = ?1")
	Integer getCountByEmail(String email);

}
