package com.apps.asuransikapalku.api.repository.marineconnect;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectInquiry;
import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectUploadTc;

@Repository
public interface MarineConnectUploadTcRepository extends JpaRepository<MarineConnectUploadTc, Long> {
	@Query("select mtc from MarineConnectUploadTc mtc where mtc.marineConnect.id = ?1 order by mtc.id asc")
	List<MarineConnectUploadTc> findByMarineConnectId(Long id);
	
	List<MarineConnectUploadTc> findByMarineConnectOrderByIdAsc(MarineConnectInquiry marineConnect);
	
	List<MarineConnectUploadTc> findByMarineConnectAndIsSupplyOrderByIdAsc(MarineConnectInquiry marineConnect, boolean isSupply);
}
