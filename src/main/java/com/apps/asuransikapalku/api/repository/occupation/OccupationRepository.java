package com.apps.asuransikapalku.api.repository.occupation;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.occupation.Occupation;
import com.apps.asuransikapalku.api.model.entity.occupation.OccupationType;

@Repository
public interface OccupationRepository extends JpaRepository<Occupation, Long>, JpaSpecificationExecutor<Occupation> {
	Optional<Occupation> findById(Long id);
	
	Occupation findByCode(String code);
	
	List<Occupation> findByIdIn(List<Long> ids);
	
	List<Occupation> findByIsActive(boolean isActive);
	
	List<Occupation> findByOccupationType(OccupationType occupationType);
	
	@Query("select count(o) from Occupation o where upper(o.code) = ?1")
	Integer getCountByCode(String code);
	
	@Query("select count(o) from Occupation o where upper(o.name) = ?1 and o.occupationType.id = ?2")
	Integer getCountByNameAndTypeId(String name, Long id);
	
	@Query("select max(o.id) from Occupation o")
	Integer getMaxId();
}
