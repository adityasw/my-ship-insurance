package com.apps.asuransikapalku.api.repository.view;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.view.ViewInsuranceDem;

@Repository
public interface ViewInsuranceDemRepository
		extends JpaRepository<ViewInsuranceDem, Long>, JpaSpecificationExecutor<ViewInsuranceDem> {

}
