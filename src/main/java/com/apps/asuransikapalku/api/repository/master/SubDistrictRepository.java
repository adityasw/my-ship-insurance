package com.apps.asuransikapalku.api.repository.master;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.master.SubDistrict;

@Repository
public interface SubDistrictRepository extends JpaRepository<SubDistrict, String> {
	Optional<SubDistrict> findById(String id);

	@Query("select sd from SubDistrict sd where sd.city.id = ?1")
	List<SubDistrict> findByCityId(String id);
}
