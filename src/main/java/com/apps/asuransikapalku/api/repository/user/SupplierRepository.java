package com.apps.asuransikapalku.api.repository.user;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.user.Supplier;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long>, JpaSpecificationExecutor<Supplier> {
	
	Optional<Supplier> findById(Long id);
	
	Supplier findByEmail(String email);
	
	List<Supplier> findByIdIn(List<Long> ids);
	
	@Query("select count(s) from Supplier s where s.email = ?1")
	Integer getCountByEmail(String email);
	
	@Query("select s from Supplier s where s.occupation.code = ?1")
	List<Supplier> findByOccupationCode(String occupationCode);
}
