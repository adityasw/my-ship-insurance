package com.apps.asuransikapalku.api.repository.user;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.user.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, JpaSpecificationExecutor<Role> {
	Optional<Role> findById(Long id);
	
	Role findByNameIgnoreCase(String name);
	
	List<Role> findByIdIn(List<Long> ids);
	
	List<Role> findByIsActive(boolean isActive);
	
	@Query("select count(r) from Role r where upper(r.name) = ?1")
	Integer getCountByName(String name);
}
