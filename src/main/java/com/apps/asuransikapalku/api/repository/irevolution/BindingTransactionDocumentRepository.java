package com.apps.asuransikapalku.api.repository.irevolution;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionDocument;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionSupplierMap;

@Repository
public interface BindingTransactionDocumentRepository
		extends JpaRepository<BindingTransactionDocument, Long>, JpaSpecificationExecutor<BindingTransactionDocument> {
	Optional<BindingTransactionDocument> findById(Long id);

	List<BindingTransactionDocument> findByBindingTransactionSupplierMap(
			BindingTransactionSupplierMap bindingTransactionSupplierMap);

	@Modifying
	@Query(value = "delete from trn_binding_transaction_doc where id_binding_tran_supp_map in ( "
			+ "select btsm.id from trn_binding_supplier_map btsm "
			+ "join trn_binding b on btsm.id_binding_transaction = b.id "
			+ "where btsm.id_supplier not in (:supplierIds) "
			+ "and b.id = :idBinding) ", nativeQuery = true)
	void deleteMapBySupplierIds(@Param("supplierIds") List<Long> supplierIds, @Param("idBinding") Long idBinding);
	
	@Query("select btd.bindingTransactionSupplierMap.supplier.id from BindingTransactionDocument btd where btd.bindingTransactionSupplierMap.bindingTransaction.id = ?1")
	List<Long> getListSupplierIdByBindingId(Long id);
}
