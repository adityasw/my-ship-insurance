package com.apps.asuransikapalku.api.repository.insurance;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceMaster;

@Repository
public interface InsuranceMasterRepository extends JpaRepository<InsuranceMaster, Long>, JpaSpecificationExecutor<InsuranceMaster> {
	Optional<InsuranceMaster> findById(Long id);
	
	List<InsuranceMaster> findByIdIn(List<Long> ids);
	
	@Query("select count(im) from InsuranceMaster im where upper(im.name) = ?1")
	Integer getCountByName(String name);
}
