package com.apps.asuransikapalku.api.repository.insurance;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceType;

@Repository
public interface InsuranceTypeRepository extends JpaRepository<InsuranceType, Long>, JpaSpecificationExecutor<InsuranceType> {
	Optional<InsuranceType> findById(Long id);
	
	InsuranceType findByCode(String code);
	
	List<InsuranceType> findByIdIn(List<Long> ids);
	
	List<InsuranceType> findByIsActive(boolean isActive);
	
	@Query("select count(it) from InsuranceType it where upper(it.code) = ?1")
	Integer getCountByCode(String code);
	
	@Query("select count(it) from InsuranceType it where upper(it.name) = ?1")
	Integer getCountByName(String name);
	
	@Query("select max(it.id) from InsuranceType it")
	Integer getMaxId();
}
