package com.apps.asuransikapalku.api.repository.user;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceMaster;
import com.apps.asuransikapalku.api.model.entity.user.SupplierInsuranceMap;

@Repository
public interface SupplierInsuranceMapRepository extends JpaRepository<SupplierInsuranceMap, Long>, JpaSpecificationExecutor<SupplierInsuranceMap> {
	Optional<SupplierInsuranceMap> findById(Long id);
	
	@Query("select sim from SupplierInsuranceMap sim where sim.supplier.id = ?1")
	public List<SupplierInsuranceMap> findByIdSupplierId(Long id);
	
	@Query("select sim from SupplierInsuranceMap sim where sim.insuranceMaster.id = :idInsurance and sim.supplier.id in (:idSuppliers)")
	public List<SupplierInsuranceMap> supplierInsuranceMaps(@Param("idInsurance") Long idInsurance, @Param("idSuppliers") List<Long> idSuppliers);
	
	public List<SupplierInsuranceMap> findByInsuranceMaster(InsuranceMaster insuranceMaster);
}
