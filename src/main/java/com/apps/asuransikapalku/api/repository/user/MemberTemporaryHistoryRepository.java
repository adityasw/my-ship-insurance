package com.apps.asuransikapalku.api.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.user.MemberTemporaryHistory;

@Repository
public interface MemberTemporaryHistoryRepository extends JpaRepository<MemberTemporaryHistory, Long> {

}
