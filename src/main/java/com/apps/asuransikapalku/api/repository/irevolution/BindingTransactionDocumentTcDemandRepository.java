package com.apps.asuransikapalku.api.repository.irevolution;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransaction;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionDocumentTcDemand;

@Repository
public interface BindingTransactionDocumentTcDemandRepository extends JpaRepository<BindingTransactionDocumentTcDemand, Long> {
	List<BindingTransactionDocumentTcDemand> findByBindingTransactionOrderByIdDesc(BindingTransaction bindingTransaction);
}
