package com.apps.asuransikapalku.api.repository.user;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.user.MemberTemporary;

@Repository
public interface MemberTemporaryRepository extends JpaRepository<MemberTemporary, Long>, JpaSpecificationExecutor<MemberTemporary> {
	Optional<MemberTemporary> findById(Long id);
	
	MemberTemporary findByEmail(String email);
	
	List<MemberTemporary> findByIdIn(List<Long> id);
	
	@Query("select count(mt) from MemberTemporary mt where mt.email = ?1 and status <> 'REJ'")
	Integer getCountByEmail(String email);
}
