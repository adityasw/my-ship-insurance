package com.apps.asuransikapalku.api.repository.claimfinancing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceActiveInsuranceDetail;

@Repository
public interface ClaimFinanceActiveInsuranceDetailRepository extends JpaRepository<ClaimFinanceActiveInsuranceDetail, Long> {

}
