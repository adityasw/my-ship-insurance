package com.apps.asuransikapalku.api.repository.occupation;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.occupation.OccupationType;

@Repository
public interface OccupationTypeRepository extends JpaRepository<OccupationType, Long>, JpaSpecificationExecutor<OccupationType> {
	Optional<OccupationType> findById(Long id);
	
	List<OccupationType> findByIsActive(boolean isActive);
	
	@Query("select count(ot) from OccupationType ot where upper(ot.name) = ?1")
	Integer getCountByName(String name);
}
