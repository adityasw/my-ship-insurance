package com.apps.asuransikapalku.api.repository.master;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.master.EmailLog;

@Repository
public interface EmailLogRepository extends JpaRepository<EmailLog, Long> {

}
