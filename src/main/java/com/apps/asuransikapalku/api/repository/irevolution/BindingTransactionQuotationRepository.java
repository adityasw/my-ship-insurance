package com.apps.asuransikapalku.api.repository.irevolution;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionQuotation;

@Repository
public interface BindingTransactionQuotationRepository extends JpaRepository<BindingTransactionQuotation, Long> {
	Optional<BindingTransactionQuotation> findById(Long id);
}
