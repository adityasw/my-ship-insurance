package com.apps.asuransikapalku.api.repository.view;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.view.ViewInsuranceSupRej;

@Repository
public interface ViewInsuranceSupRejRepository extends JpaRepository<ViewInsuranceSupRej, Long>, JpaSpecificationExecutor<ViewInsuranceSupRej> {

}
