package com.apps.asuransikapalku.api.repository.view;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.view.ViewSupplierPage;

@Repository
public interface ViewSupplierPageRepository
		extends JpaRepository<ViewSupplierPage, Long>, JpaSpecificationExecutor<ViewSupplierPage> {

}
