package com.apps.asuransikapalku.api.repository.marineconnect;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectWishList;
import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectWishListPk;

@Repository
public interface MarineConnectWishListRepository extends JpaRepository<MarineConnectWishList, MarineConnectWishListPk> {
	@Query("select mc from MarineConnectWishList mc where mc.demand.id = ?1 and mc.supplier.id = ?2")
	MarineConnectWishList findByIdDemandAndIdSupplier(Long idDemand, Long idSupplier);
}
