package com.apps.asuransikapalku.api.configs;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.service.UserService;
import com.apps.asuransikapalku.api.service.impl.UserDetailServiceImpl;

import io.jsonwebtoken.ExpiredJwtException;

@Component
public class RequestFilterConfig extends OncePerRequestFilter {
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestFilterConfig.class);
	private UserDetailServiceImpl userDetailServiceJwat;
	private TokenJwtConfig tokenJwtConfig;
	private UserService userService;
	
	private User user;
	
	@Autowired
	public void support(UserDetailServiceImpl userDetailServiceImpl, TokenJwtConfig tokenJwtConfig
			, UserService userService) {
		this.userDetailServiceJwat = userDetailServiceImpl;
		this.tokenJwtConfig = tokenJwtConfig;
		this.userService = userService;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		final String requestTokenHeader = request.getHeader("Authorization");
		LOGGER.info("Token : {}", requestTokenHeader);
		String username = null;
		String jwtToken = null;
		// JWT Token is in the form "Bearer token". Remove Bearer word and get
		// only the Token
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			jwtToken = requestTokenHeader.substring(7);
			try {
				user = userService.findByToken(jwtToken);
				username = tokenJwtConfig.getUsernameFromToken(jwtToken);
			} catch (IllegalArgumentException e) {
				resetStatus();
				LOGGER.error("Unable to get JWT Token");
			} catch (ExpiredJwtException e) {
				resetStatus();
				LOGGER.error("JWT Token has expired");
			}
		} else {
			logger.warn("JWT Token does not begin with Bearer String");
		}

		// Once we get the token validate it.
		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

			UserDetails userDetails = this.userDetailServiceJwat.loadUserByUsername(username);
			
			// if token is valid configure Spring Security to manually set
			// authentication
			if (null != user && Boolean.TRUE.equals(user.getIsLogin())
					&& tokenJwtConfig.validateToken(jwtToken, userDetails)) {

				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				usernamePasswordAuthenticationToken
						.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				// After setting the Authentication in the context, we specify
				// that the current user is authenticated. So it passes the
				// Spring Security Configurations successfully.
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			} else {
				resetStatus();
			}
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
	    response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
	    response.setHeader("Access-Control-Allow-Credentials", "true");
	    response.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With, remember-me");
	    response.setHeader("Access-Control-Expose-Headers", "Content-Length, Authorization");
		filterChain.doFilter(request, response);
	}
	
	private void resetStatus() {
		if (null != user) {
			user.setIsLogin(Boolean.FALSE);
			user.setToken(null);
			userService.save(user);
		}
	}

}
