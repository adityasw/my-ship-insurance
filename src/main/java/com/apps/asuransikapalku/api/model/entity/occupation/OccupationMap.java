package com.apps.asuransikapalku.api.model.entity.occupation;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "mst_occupation_map")
@NoArgsConstructor
@AllArgsConstructor
public class OccupationMap {
	@Id
	@SequenceGenerator(name = "mst_occupation_map_seq", sequenceName = "mst_occupation_map_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_occupation_map_seq")
	private Long id;
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_occupation", referencedColumnName = "id")
	private Occupation occupation;
	@Column(name = "is_revolutions")
	private Boolean isIrevolution;
	@Column(name = "is_claim_solution")
	private Boolean isClaimSolution;
	@Column(name = "is_claim_financing")
	private Boolean isClaimFinancing;
	@Column(name = "is_marine_connect")
	private Boolean isMarineConnect;
}
