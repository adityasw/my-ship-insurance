package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UpdateStatusOfferingDto {
	private Long id;
	@JsonProperty("id-claim-finance")
	private Long idClaimFinance;
	@JsonProperty("is-receive")
	private Boolean isReceive;
	@JsonProperty("is-reject-all")
	private Boolean isRejectAll;
}
