package com.apps.asuransikapalku.api.model.entity.marineconnect;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.entity.user.Demand;
import com.apps.asuransikapalku.api.model.entity.user.Supplier;

import lombok.Data;

@Entity
@Data
@Table(name = "marine_connect_wish_list")
public class MarineConnectWishList {
	@Id
	@SequenceGenerator(name = "marine_connect_wish_list_seq", sequenceName = "marine_connect_wish_list_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "marine_connect_wish_list_seq")
	private Long id;
	@ManyToOne
	@JoinColumn(name = "id_demand", referencedColumnName = "id")
	private Demand demand;
	@ManyToOne
	@JoinColumn(name = "id_supplier", referencedColumnName = "id")
	private Supplier supplier;
	private Boolean flag;
}
