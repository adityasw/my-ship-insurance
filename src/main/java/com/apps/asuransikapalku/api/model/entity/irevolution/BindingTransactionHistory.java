package com.apps.asuransikapalku.api.model.entity.irevolution;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Entity
@Data
@Table(name = "trn_binding_transaction_history")
public class BindingTransactionHistory {
	@Id
	@SequenceGenerator(name = "trn_binding_transaction_history_seq", sequenceName = "trn_binding_transaction_history_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trn_binding_transaction_history_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_binding_transaction", referencedColumnName = "id")
	private BindingTransaction bindingTransaction;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy HH:mm:ss.SSS'Z'", timezone="GMT")
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "history_date")
	private Date historyDate;
	private String status;
	@Column(columnDefinition="TEXT")
	private String description;
}
