package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import java.util.Date;
import java.util.List;

import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceKindDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipTypeDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClaimFinancingDto {
	
	@Data
	public static class SoaDto {
		private Long id;
		private String filename;
		private String file;
	}
	
	@Data
	public static class ActiveInsuranceDetail {
		private Long id;
		@JsonProperty("insurance-name")
		private String insuranceName;
		@JsonProperty("insurance-kind")
		private InsuranceKindDto insuranceKindDto;
		private String file;
	}
	
	@Data
	public static class OpponentDetail {
		@JsonProperty("object-type")
		private String objectType;
		@JsonProperty("object-name")
		private String objectName;
		@JsonProperty("owner-name")
		private String ownerName;
		@JsonProperty("nature-damage-to-our-vessel")
		private String natureDamageToOurVessel;
		@JsonProperty("nature-damage-to-third-parties")
		private String natureDamageToThirdParties;
		@JsonProperty("claim-estimate-to-our-vessel")
		private String claimEstimateToOurVessel;
		@JsonProperty("claim-estimate-to-third-parties")
		private String claimEstimateToThirdParties;
	}
	
	private Long id;
	@JsonProperty("id-demand")
	private Long idDemand;
	@JsonProperty("username-demand")
	private String usernameDemand;
	@JsonProperty("company-name")
	private String companyName;
	@JsonProperty("vessel-name")
	private String vesselName;
	@JsonProperty("vessel-type")
	private ShipTypeDto vesselType;
	@JsonProperty("vessel-type-other")
	private String vesselTypeOther;
	@JsonProperty("casuality")
	private String casuality;
	@JsonProperty("date-of-loss")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "Asia/Jakarta")
	private Date dateOfLoss;
	@JsonProperty("files")
	List<SoaDto> soas;
	@JsonProperty("location-of-loss")
	private String locationOfLoss;
	
	@JsonProperty("is-other-vessel-type")
	private Boolean isOtherVesselType;
	
	private String status;
	
	@JsonProperty("master-name")
	private String masterName;
	@JsonProperty("master-phone")
	private String masterPhone;
	@JsonProperty("master-email")
	private String masterEmail;
	
	@JsonProperty("owner-pic-name")
	private String ownerPicName;
	@JsonProperty("owner-pic-phone")
	private String ownerPicPhone;
	@JsonProperty("owner-pic-email")
	private String ownerPicEmail;
	
	@JsonProperty("active-insurance-details")
	private List<ActiveInsuranceDetail> activeInsuranceDetails;
	
	@JsonProperty("vessel-current-condition")
	private String vesselCurrentCondition;
	@JsonProperty("owner-next-course-of-action")
	private String ownerNextCourseOfAction;
	
	@JsonProperty("opponent-detail")
	private OpponentDetail opponentDetail;
}
