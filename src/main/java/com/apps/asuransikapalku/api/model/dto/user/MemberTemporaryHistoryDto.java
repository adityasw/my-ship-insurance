package com.apps.asuransikapalku.api.model.dto.user;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MemberTemporaryHistoryDto {
	private Long id;
	private String status;
	@JsonProperty("status-description")
	private String statusDescription;
	private String description;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy' 'HH:mm:ss", timezone = "Asia/Jakarta")
	@JsonProperty("history-date")
	private Date historyDate;
}
