package com.apps.asuransikapalku.api.model.entity.claimfinancing;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "claim_finance_chat")
public class ClaimFinanceChat {
	@Id
	@SequenceGenerator(name = "claim_finance_chat_seq", sequenceName = "claim_finance_chat_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "claim_finance_chat_seq")
	private Long id;
	@Column(name = "id_claim_finance")
	private Long idClaimFinance;
	private String sender;
	private String receiver;
	@Column(columnDefinition = "TEXT")
	private String content;
	@Column(name = "send_time")
	private Date sendTime;
	@Column(name = "is_read_admin")
	private Boolean isReadAdmin;
	@Column(name = "is_read_demand")
	private Boolean isReadDemand;
	@Column(name = "is_read_supply")
	private Boolean isReadSupply;
	@Column(name = "is_supply")
	private Boolean isSupply;
}
