package com.apps.asuransikapalku.api.model.dto.marineconnect;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MarineConnectQuotationDto {
	@JsonProperty("id-marine-connect")
	private Long idMarineConnect;
	private Long id;
	@JsonProperty("quoted-price")
	private String quotedPrice;
	@JsonProperty("payment-type")
	private String paymentType;
	private String description;
	@JsonProperty("scope-of-work")
	private String scopeOfWork;
	private String deliverle;
	@JsonProperty("estimate-time-completion")
	private String estimateTimeCompletion;
	@JsonProperty("negotiate-price")
	private String negotiatePrice;
	@JsonProperty("negotiate-description")
	private String negotiateDescription;
	@JsonProperty("final-price")
	private String finalPrice;
	@JsonProperty("final_description")
	private String finalDescription;
}
