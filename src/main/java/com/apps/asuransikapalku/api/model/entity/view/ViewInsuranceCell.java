package com.apps.asuransikapalku.api.model.entity.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "v_insurance_cell")
public class ViewInsuranceCell {
	@Id
	private Long id;
	@Column(name = "id_supplier")
	private Long idSupplier;
	@Column(name = "vessel_name")
	private String vesselName;
	@Column(name = "vessel_type")
	private String vesselType;
	@Column(name = "insurance_type")
	private String insuranceType;
	@Column(name = "insurance_kind")
	private String insuranceKind;
	private String status;
}
