package com.apps.asuransikapalku.api.model.entity.user;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "mst_supplier_gallery")
public class SupplierGallery {
	@EmbeddedId
	private SupplierDetailPk supplierDetailPk = new SupplierDetailPk();
	@ManyToOne
	@JoinColumn(name = "id_supplier", insertable = false, updatable = false)
	private Supplier supplier;
	private String file;
}
