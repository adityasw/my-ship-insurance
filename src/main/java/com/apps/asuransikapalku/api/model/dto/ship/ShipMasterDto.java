package com.apps.asuransikapalku.api.model.dto.ship;

import java.util.List;

import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceKindDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.ChatDto;
import com.apps.asuransikapalku.api.model.dto.master.ClassifiacationBureauDto;
import com.apps.asuransikapalku.api.model.dto.user.DemandDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ShipMasterDto {
	private Long id;
	private DemandDto demand;
	private String name;
	@JsonProperty("company-name")
	private String companyName;
	@JsonProperty("company-address")
	private String companyAddress;
	@JsonProperty("coverage-name")
	private String coverageName;
	@JsonProperty("coverage-address")
	private String covarageAddress;
	@JsonProperty("letter-address")
	private String letterAddress;
	@JsonProperty("ship-type")
	private ShipTypeDto shipType;
	@JsonProperty("is-other-ship-type")
	private Boolean isOtherShipType;
	@JsonProperty("other-ship-type")
	private String otherShipType;
	@JsonProperty("trading-area")
	private String tradingArea;
	
	private Double size = 0D;
	@JsonProperty("production-year")
	private Integer productionYear;
	private Integer age;
	@JsonProperty("coverage-value") 
	private String coverageValue;
	@JsonProperty("classification-bureau")
	private ClassifiacationBureauDto classificationBureau;
	
	@JsonProperty("status-approval")
	private String statusApproval;
	@JsonProperty("is-public")
	private Boolean isPublic;
	@JsonProperty("is-tnc")
	private Boolean isTnc;
	
	@JsonProperty("insurance-kind")
	private InsuranceKindDto insuranceKindDto;
	
	@JsonProperty("req-replacement-flag")
	private Boolean reqReplacementFlag;
	
		
	@JsonProperty("loss-records")
	private List<LossRecordDto> lossRecords;
	@JsonProperty("insurance-ongoing")
	private List<InsuranceOnGoingDto> insuranceOnGoings;
	@JsonProperty("insurances") 
	private List<ShipInsuranceMapDto> insuranceMapDtos;
	private List<ChatDto> chats;
}
