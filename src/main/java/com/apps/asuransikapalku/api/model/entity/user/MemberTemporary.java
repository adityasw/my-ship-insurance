package com.apps.asuransikapalku.api.model.entity.user;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.apps.asuransikapalku.api.model.Common;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "member_temporary")
@EqualsAndHashCode(callSuper = true)
public class MemberTemporary extends Common {
	@Id
	@SequenceGenerator(name = "member_temporary_seq", sequenceName = "member_temporary_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "member_temporary_seq")
	private Long id;
	private String name;
	private String email;
	private String occupation;
	private String position;
	@Column(name = "insurance_needs", columnDefinition = "TEXT")
	private String insuranceNeeds;
	@Column(name = "company_name")
	private String companyName;
	@Column(name = "phone_number")
	private String phoneNumber;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "schedule_meet")
	private Date scheduleMeet;
	private String status;
	@OneToMany(mappedBy = "memberTemporary", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<MemberTemporaryHistory> memberTemporaryHistories;
}
