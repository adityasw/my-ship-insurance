package com.apps.asuransikapalku.api.model.entity.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "v_project")
public class ViewProject {
	@Id
	@Column(name = "id_project")
	private Long id;
	@Column(name = "code_project")
	private String code;
	@Column(name = "id_demand")
	private Long idDemand;
	private String username;
	@Column(name = "vessel_name")
	private String vesselName;
	private String status;
	@Column(name = "status_description")
	private String statusDescription;
	@Column(name = "insurance_type")
	private String insuranceType;
	
}
