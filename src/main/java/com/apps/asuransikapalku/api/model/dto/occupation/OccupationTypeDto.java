package com.apps.asuransikapalku.api.model.dto.occupation;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class OccupationTypeDto {
	private Long id;
	private String name;
	private String description;
	@JsonProperty("is-active")
	private Boolean isActive;
}
