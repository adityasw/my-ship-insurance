package com.apps.asuransikapalku.api.model.dto.user;

import java.util.List;

import com.apps.asuransikapalku.api.model.dto.occupation.OccupationDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MemberTemporaryDto {
	private Long id;
	private String name;
	private String email;
	private OccupationDto occupation;
	private String position;
	@JsonProperty("insurance-needs")
	private String insuranceNeeds;
	@JsonProperty("company-name")
	private String companyName;
	@JsonProperty("phone-number")
	private String phoneNumber;
	@JsonProperty("schedule-meet")
	private String scheduleMeet;
	private String status;
	@JsonProperty("occupation-from-register")
	private String occupationFromRegister;
	private List<MemberTemporaryHistoryDto> histories;
}
