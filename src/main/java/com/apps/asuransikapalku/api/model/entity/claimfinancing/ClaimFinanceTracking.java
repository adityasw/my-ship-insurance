package com.apps.asuransikapalku.api.model.entity.claimfinancing;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
@Table(name = "claim_finance_tracking")
public class ClaimFinanceTracking {
	@Id
	@SequenceGenerator(name = "claim_finance_tracking_seq", sequenceName = "claim_finance_tracking_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "claim_finance_tracking_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name= "id_claim_finance", referencedColumnName = "id")
	private ClaimFinance claimFinance;
	private String username;
	private String items;
	@Column(name = "vendor_name")
	private String vendorName;
	@Column(name = "vendor_type")
	private String vendorType;
	private String file;
	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date")
	private Date paymentDate;
	@Column(name = "total_invoice")
	private String totalInvoice;
	@Column(name = "payment_to_vendor")
	private String paymentToVendor;
}
