package com.apps.asuransikapalku.api.model.dto.marineconnect;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MarineConnectUploadTcDto {
	private Long id;
	@JsonProperty("marine-connect-id")
	private Long marineConnectId;
	@JsonProperty("file-name")
	private String fileName;
	private String file;
	private String description;
	private String to;
	@JsonProperty("is-supply")
	private Boolean isSupply;
}
