package com.apps.asuransikapalku.api.model.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class SupplierDetailPk implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6402192971464599719L;
	@Column(name = "id_supplier")
	private Long idSupplier;
	private String code;
}
