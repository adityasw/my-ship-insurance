package com.apps.asuransikapalku.api.model.dto.view;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ViewProjectDto {
	private Long id;
	@JsonProperty("code-project")
	private String code;
	@JsonProperty("id-demand")
	private Long idDemand;
	private String username;
	@JsonProperty("vessel-name")
	private String vesselName;
	private String status;
	@JsonProperty("status-description")
	private String statusDescription;
	@JsonProperty("insurance-type")
	private String insuranceType;
	
}
