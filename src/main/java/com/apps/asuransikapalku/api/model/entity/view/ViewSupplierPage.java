package com.apps.asuransikapalku.api.model.entity.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "v_supplier_page")
public class ViewSupplierPage {
	@Id
	private Long id;
	@Column(name = "id_supplier")
	private Long idSupplier;
	private String avatar;
	private String name;
	@Column(name = "id_location")
	private String idLocation;
	@Column(name = "location_name")
	private String locationName;
	@Column(name = "id_occupation")
	private Long idOccupation;
	@Column(name = "occupation_name")
	private String occupationName;
	@Column(name = "response_time")
	private String responseTime;
	@Column(name = "insurance_needs")
	private String insuranceNeeds;
	@Column(name = "id_demand")
	private Long idDemand;
	private Boolean flag;
}
