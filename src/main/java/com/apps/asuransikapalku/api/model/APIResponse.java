package com.apps.asuransikapalku.api.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class APIResponse<T> {
	private int status;
	private List<String> message;
    private T data;
    
    public APIResponse() {
		List<String> str = new ArrayList<>();
		str.add(HttpStatus.OK.getReasonPhrase());
		this.message = str;
		this.status = HttpStatus.OK.value();
	}
    
	public APIResponse(int status, String message) {
		List<String> str = new ArrayList<>();
		str.add(message);
		this.message = str;
		this.status = status;
	}
	
	public APIResponse(int status, List<String> message) {
		this.message = message;
		this.status = status;
	}
}