package com.apps.asuransikapalku.api.model.entity.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.Common;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "mst_classification_bureau")
@EqualsAndHashCode(callSuper=true)
public class ClassificationBureau extends Common{
	@Id
	@SequenceGenerator(name = "mst_classification_bureau_seq", sequenceName = "mst_classification_bureau_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_classification_bureau_seq")
	private Long id;
	private String name;
}
