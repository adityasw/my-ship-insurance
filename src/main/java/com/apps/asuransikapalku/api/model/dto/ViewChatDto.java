package com.apps.asuransikapalku.api.model.dto;

import java.util.Date;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ViewChatDto {
	@JsonProperty("full-name")
	private String fullName;
	private String code;
	private String username;
	@Column(name = "send-time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MMM-yyyy' 'HH:mm:ss", timezone = "Asia/Jakarta")
	private Date sendTime;
	private String content;
	private Long total;
}
