package com.apps.asuransikapalku.api.model.entity.marineconnect;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
@Table(name = "marine_connect_doc_tc")
public class MarineConnectUploadTc {
	@Id
	@SequenceGenerator(name = "marine_connect_doc_tc_seq", sequenceName = "marine_connect_doc_tc_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "marine_connect_doc_tc_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name= "id_marine_connect", referencedColumnName = "id")
	private MarineConnectInquiry marineConnect;
	private String type;
	@Temporal(TemporalType.DATE)
    @Column(name = "upload_date")
	private Date uploadDate;
	@Column(name = "file_name")
	private String fileName;
	private String file;
	@Column(columnDefinition = "TEXT")
	private String description;
	private String uploader;
	private String target;
	@Column(name = "is_supply")
	private Boolean isSupply;
}
