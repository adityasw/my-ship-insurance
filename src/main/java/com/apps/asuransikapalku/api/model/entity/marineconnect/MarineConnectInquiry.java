package com.apps.asuransikapalku.api.model.entity.marineconnect;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.Common;
import com.apps.asuransikapalku.api.model.entity.user.Demand;
import com.apps.asuransikapalku.api.model.entity.user.Supplier;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "marine_connect_inq")
@EqualsAndHashCode(callSuper=true)
public class MarineConnectInquiry extends Common {
	@Id
	@SequenceGenerator(name = "marine_connect_inq_seq", sequenceName = "marine_connect_inq_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "marine_connect_inq_seq")
	private Long id;
	@ManyToOne
	@JoinColumn(name = "id_supplier", referencedColumnName = "id")
	private Supplier supplier;
	@ManyToOne
	@JoinColumn(name = "id_demand", referencedColumnName = "id")
	private Demand demand;
	@Column(name = "project_type")
	private String projectType;
	@Column(name = "object_kind")
	private String objectKind;
	private Double size;
	private String information;
	private String status;
	@OneToOne(mappedBy = "marineConnect", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, orphanRemoval = false)
	private MarineConnectQuotation marineConnectQuotation;
}
