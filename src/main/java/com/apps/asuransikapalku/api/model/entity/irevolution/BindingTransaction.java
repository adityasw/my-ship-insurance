package com.apps.asuransikapalku.api.model.entity.irevolution;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.Common;
import com.apps.asuransikapalku.api.model.entity.ship.ShipMaster;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "trn_binding")
@EqualsAndHashCode(callSuper = true)
public class BindingTransaction extends Common {
	@Id
	@SequenceGenerator(name = "trn_binding_seq", sequenceName = "trn_binding_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trn_binding_seq")
	private Long id;
	private String code;
	private String username;
	@OneToOne
	@JoinColumn(name = "id_ship", referencedColumnName = "id")
	private ShipMaster shipMaster;
	private String status;
	@Column(name = "is_public")
	private Boolean isPublic;
	@Column(name = "is_selected")
	private Boolean isSelected;
	@OneToMany(mappedBy = "bindingTransaction", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<BindingTransactionSupplierMap> bindingTransactionSupplierMaps;
	@OneToMany(mappedBy = "bindingTransaction", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<BindingTransactionHistory> bindingTransactionHistories;
	@OneToMany(mappedBy = "bindingTransaction", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<BindingTransactionQuotation> bindingTransactionQuotations;
	@OneToMany(mappedBy = "bindingTransaction", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<BindingTransactionDocumentDemand> transactionDocumentDemands;
	@OneToMany(mappedBy = "bindingTransaction", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<BindingTransactionClaim> bindingTransactionClaims;
	@OneToMany(mappedBy = "bindingTransaction", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<BindingTransactionDocumentTcDemand> bindingTransactionDocumentTcDemands;
}
