package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClaimFinanceInvoiceDto {
	private Long id;
	@JsonProperty("claim-finance-id")
	private Long claimFinanceId;
	private String username;
	@JsonProperty("detail-invoice")
	private String detailInvoice;
	private String amount;
	private String remittance;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "Asia/Jakarta")
	@JsonProperty("payment-date")
	private Date paymentDate;
	@JsonProperty("payment-status")
	private String paymentStatus;
	private String file;
	private String filename;
}
