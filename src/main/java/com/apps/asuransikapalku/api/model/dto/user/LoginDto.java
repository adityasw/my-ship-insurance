package com.apps.asuransikapalku.api.model.dto.user;

import lombok.Data;

@Data
public class LoginDto {
	private String username;
	private String password;
}
