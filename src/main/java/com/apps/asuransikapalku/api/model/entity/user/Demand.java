package com.apps.asuransikapalku.api.model.entity.user;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.Common;
import com.apps.asuransikapalku.api.model.entity.occupation.Occupation;
import com.apps.asuransikapalku.api.model.entity.ship.ShipMaster;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "mst_demand")
@EqualsAndHashCode(callSuper=true)
public class Demand extends Common{
	@Id
	@SequenceGenerator(name = "mst_demand_seq", sequenceName = "mst_demand_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_demand_seq")
	private Long id;
	private String photo;
	private String name;
	private String email;
	@OneToOne
	@JoinColumn(name = "id_occupation", referencedColumnName = "id")
	private Occupation occupation;
	private String position;
	@Column(name = "insurance_needs", columnDefinition="TEXT")
	private String insuranceNeeds;
	@Column(name = "company_name")
	private String companyName;
	@Column(name = "phone_number")
	private String phoneNumber;
	@Column(name = "is_approve")
	private Boolean isApprove = Boolean.FALSE;
	@Column(name = "is_reject")
	private Boolean isReject = Boolean.FALSE;
	private Boolean tnc = Boolean.FALSE;
	@Column(name = "demand_as")
	private String demandAs;
	@OneToOne(mappedBy = "demand", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, optional = false)
	@JsonIgnore
	private User user;
	@OneToMany(mappedBy = "demand", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, orphanRemoval = false)
	private List<ShipMaster> shipMaster;
}
