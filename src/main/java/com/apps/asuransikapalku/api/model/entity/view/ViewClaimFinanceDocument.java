package com.apps.asuransikapalku.api.model.entity.view;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "v_claim_finance_document")
public class ViewClaimFinanceDocument {
	@Id
	private Long id;
	@Column(name = "claim_finance_id")
	private Long idClaimFinance;
	@Column(name = "file_name")
	private String fileName;
	private String file;
	@Column(name = "upload_date")
	private Date uploadDate;
	private String uploader;
	@Column(name = "uploader_name")
	private String uploaderName;
	private String to;
	@Column(name = "uploader_type")
	private String uploaderType;
	@Column(name = "to_name")
	private String toName;
}
