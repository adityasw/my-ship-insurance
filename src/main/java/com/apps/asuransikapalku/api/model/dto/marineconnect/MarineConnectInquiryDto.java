package com.apps.asuransikapalku.api.model.dto.marineconnect;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MarineConnectInquiryDto {
	private Long id;
	@JsonProperty("project-type")
	private String projectType;
	@JsonProperty("object-kind")
	private String objectKind;
	private Double size;
	private String information;
	@JsonProperty("id-supplier")
	private Long idSupplier;
	@JsonProperty("email-supplier")
	private String emailSupplier;
	private String status;
}
