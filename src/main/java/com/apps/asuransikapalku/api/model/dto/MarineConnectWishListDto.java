package com.apps.asuransikapalku.api.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MarineConnectWishListDto {
	@JsonProperty("id-supplier")
	private Long idSupplier;
	private Boolean flag;
}
