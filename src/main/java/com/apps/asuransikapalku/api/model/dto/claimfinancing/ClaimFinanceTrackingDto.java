package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClaimFinanceTrackingDto {
	private Long id;
	@JsonProperty("claim-finance-id")
	private Long claimFinanceId;
	private String username;
	private String items;
	@JsonProperty("vendor-name")
	private String vendorName;
	@JsonProperty("vendor-type")
	private String vendorType;
	private String file;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "Asia/Jakarta")
	@JsonProperty("payment-date")
	private Date paymentDate;
	@JsonProperty("total-invoice")
	private String totalInvoice;
	@JsonProperty("payment-to-vendor")
	private String paymentToVendor;
}
