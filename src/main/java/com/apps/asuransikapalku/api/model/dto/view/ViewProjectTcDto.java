package com.apps.asuransikapalku.api.model.dto.view;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ViewProjectTcDto {
	private Long id;
	@JsonProperty("binding-id")
	private Long idBinding;
	@JsonProperty("file-name")
	private String fileName;
	private String file;
	@JsonProperty("upload-date")
	private Date uploadDate;
	private String uploader;
	@JsonProperty("uploader-name")
	private String uploaderName;
	private String to;
	@JsonProperty("uploader-type")
	private String uploaderType;
	private String status;
	@JsonProperty("to-name")
	private String toName;
}
