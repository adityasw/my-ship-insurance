package com.apps.asuransikapalku.api.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UpdateStatusDto {
	private Long id;
	private Boolean status;
	@JsonProperty("Status-code")
	private String statusCode;
}
