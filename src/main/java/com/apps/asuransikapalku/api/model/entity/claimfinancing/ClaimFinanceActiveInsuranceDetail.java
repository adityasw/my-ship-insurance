package com.apps.asuransikapalku.api.model.entity.claimfinancing;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceKind;

import lombok.Data;

@Entity
@Data
@Table(name = "claim_finance_active_insurance_detail")
public class ClaimFinanceActiveInsuranceDetail {
	@Id
	@SequenceGenerator(name = "claim_finance_active_insurance_detail_seq", sequenceName = "claim_finance_active_insurance_detail_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "claim_finance_active_insurance_detail_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_claim_finance", referencedColumnName = "id")
	private ClaimFinance claimFinance;
	@Column(name = "insurance_name")
	private String insuranceName;
	private String file;
	@ManyToOne
	@JoinColumn(name = "id_insurance_kind", referencedColumnName = "id")
	private InsuranceKind insuranceKind;
}
