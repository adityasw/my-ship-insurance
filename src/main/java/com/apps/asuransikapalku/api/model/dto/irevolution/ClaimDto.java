package com.apps.asuransikapalku.api.model.dto.irevolution;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClaimDto {
	private Long id;
	@JsonProperty("id-binding")
	private Long idBinding;
	private String casuality;
	private String dol;
	private String location;
	private String name;
	private List<String> soa;
	private String status;

	@JsonProperty("pic-name")
	private String picName;
	@JsonProperty("phone-number")
	private String phoneNumber;
	@JsonProperty("vessel-current-location")
	private String vesselCurrentLocation;
	@JsonProperty("owner-next-course-of-action")
	private String ownerNextCourseOfAction;
	
	@JsonProperty("object-type")
	private String objectType;
	@JsonProperty("object-name")
	private String objectName;
	@JsonProperty("owner-name")
	private String ownerName;
	@JsonProperty("nature-of-damage")
	private String natureOfDamage;
	@JsonProperty("claim-estimate")
	private String claimEstimate;
	private String tenor;
}
