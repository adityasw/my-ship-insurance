package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimFinanceComissionDto {
	private Long id;
	@JsonProperty("claim-finance-id")
	private Long claimFinanceId;
	private String username;
	@JsonProperty("detail-comission")
	private String detailComission;
	private String amount;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "Asia/Jakarta")
	@JsonProperty("due-date")
	private Date dueDate;
	private String filename;
	private String file;
	@JsonProperty("payment-status")
	private String paymentStatus;
	private String reminder;
}
