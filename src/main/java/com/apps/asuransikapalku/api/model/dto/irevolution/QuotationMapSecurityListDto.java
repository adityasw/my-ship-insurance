package com.apps.asuransikapalku.api.model.dto.irevolution;

import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceMasterDto;

import lombok.Data;

@Data
public class QuotationMapSecurityListDto {
	private Long id;
	private InsuranceMasterDto insuranceMaster;
	private String type;
	private Double percentage;
}
