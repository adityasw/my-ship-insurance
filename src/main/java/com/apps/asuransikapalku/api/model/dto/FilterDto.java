package com.apps.asuransikapalku.api.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class FilterDto {
	@Data
	public static class Paging {
		private int page;
		private int size;
	}

	@Data
	public static class Sorting {
		@JsonProperty("column-name")
		private String columnName;
		@JsonProperty("is-ascending")
		private Boolean isAscending = Boolean.TRUE;
	}
	
	@Data
	public static class MarineConnect{
		@JsonProperty("city-id")
		private String cityId;
		@JsonProperty("object-type")
		private String objectType;
	}

	private Paging paging;
	private List<Sorting> sorting;
	
	@JsonProperty("supplier-filter")
	private MarineConnect supplierFilter;

	@JsonProperty("search-by")
	private String searchBy;
	
	private String status;
	
	private String username;
	
	@JsonProperty("uploader-name")
	private String uploaderName;
	
	@JsonProperty("list-status")
	private List<String> statusList;

	@JsonProperty("is-active")
	private Boolean isActive = Boolean.FALSE;
	
	@JsonProperty("insurance-type-id")
	private Long insuranceTypeId;
	
	@JsonProperty("demand-id")
	private Long demandId;
	
	@JsonProperty("supply-id")
	private Long supplierId;
	
	@JsonProperty("ship-type-id")
	private Long shipTypeId;
	
	@JsonProperty("insurance-kind-id")
	private Long insuranceKindId;
	
	@JsonProperty("classification-bureau-id")
	private Long classificationBureauId;
	
	@JsonProperty("occupation-id")
	private Long occupationId;
	
	@JsonProperty("claim-finance-id")
	private Long claimFinanceId;
	
	@JsonProperty("wish-list-flag")
	private Boolean wishListFlag;
}
