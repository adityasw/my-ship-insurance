package com.apps.asuransikapalku.api.model.entity.master;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="mst_province")
public class Province {
	@Id
	private String id;
	private String name;
}
