package com.apps.asuransikapalku.api.model.dto.irevolution;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AddQuotationDto {
	@JsonProperty("quotations")
	private List<BindingTransactionQuotationDto> bindingTransactionQuotationDtos;
}
