package com.apps.asuransikapalku.api.model.dto.ship;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ShipTypeDto {
	private Long id;
	@JsonIgnore
	private String code;
	private String name;
	private String description;
	@JsonProperty("is-active")
	private Boolean isActive;
}
