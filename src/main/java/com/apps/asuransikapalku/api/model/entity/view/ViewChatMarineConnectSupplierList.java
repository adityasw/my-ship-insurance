package com.apps.asuransikapalku.api.model.entity.view;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "v_chat_marine_connect_supplier_list")
public class ViewChatMarineConnectSupplierList {
	@Id
	private Long id;
	private String receiver;
	private String sender;
	@Column(name = "sender_name")
	private String senderName;
	private String content;
	@Column(name = "send_time")
	private Date sendTime;
	@Column(name = "total_unread")
	private Integer totalUnread;
}
