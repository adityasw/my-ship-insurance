package com.apps.asuransikapalku.api.model.entity.user;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.apps.asuransikapalku.api.model.Common;

import lombok.Data;

@Entity
@Data
@Table(name = "member_temporary_history")
public class MemberTemporaryHistory extends Common {
	@Id
	@SequenceGenerator(name = "member_temporary_history_seq", sequenceName = "member_temporary_history_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "member_temporary_history_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_member_temporary", referencedColumnName = "id")
	private MemberTemporary memberTemporary;
	private String status;
	@Column(name = "status_description")
	private String statusDescription;
	@Column(columnDefinition="TEXT")
	private String description;
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "history_date")
	private Date historyDate;
}
