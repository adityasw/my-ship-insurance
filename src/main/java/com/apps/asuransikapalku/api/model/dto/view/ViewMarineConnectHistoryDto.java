package com.apps.asuransikapalku.api.model.dto.view;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ViewMarineConnectHistoryDto {
	private Long id;
	@JsonProperty("id-supplier")
	private Long idSupplier;
	private String avatar;
	private String name;
	@Column(name = "id_location")
	private String idLocation;
	@Column(name = "location_name")
	private String locationName;
	@JsonProperty("id-occupation")
	private Long idOccupation;
	@JsonProperty("occupation-name")
	private String occupationName;
	@JsonProperty("response-time")
	private String responseTime;
	@JsonProperty("insurance-needs")
	private String insuranceNeeds;
	private String status;
	@JsonProperty("status-description")
	private String statusDescription;
}
