package com.apps.asuransikapalku.api.model.dto.user;

import com.apps.asuransikapalku.api.model.dto.master.CityDto;
import com.apps.asuransikapalku.api.model.dto.master.ProvinceDto;
import com.apps.asuransikapalku.api.model.dto.master.SubDistrictDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AdminRegisterDto {
	@JsonProperty("first-name")
	private String firstName;
	@JsonProperty("last-name")
	private String lastName;
	private String email;
	@JsonProperty("phone-number")
	private String phoneNumber;
	private String gender;
	private String photo;
	private ProvinceDto province;
	private CityDto city;
	@JsonProperty("sub-district")
	private SubDistrictDto subDistrict;
	private Boolean tnc;
}
