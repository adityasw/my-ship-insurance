package com.apps.asuransikapalku.api.model.entity.claimfinancing;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class ClaimFinanceOpponentDetailPk implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4509833236404388994L;
	
	@Column(name = "id_claim_finance")
	private Long idClaimFinance;
}
