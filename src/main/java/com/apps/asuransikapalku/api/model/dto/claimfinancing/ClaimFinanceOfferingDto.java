package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClaimFinanceOfferingDto {
	private Long id;
	@JsonProperty("claim-finance-id")
	private Long claimFinanceId;
	private String username;
	@JsonProperty("supplier-id")
	private Long supplierId;
	private String ltv;
	@JsonProperty("annual-interest")
	private String annualInterest;
	@JsonProperty("nilai-pendanaan")
	private String nilaiPendanaan;
	@JsonProperty("is-selected")
	private Boolean isSelected;
}
