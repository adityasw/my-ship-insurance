package com.apps.asuransikapalku.api.model.entity.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "v_insurance_dem")
public class ViewInsuranceDem {
	@Id
	private Long id;
	private String username;
	@Column(name = "project_number")
	private String projectNumber;
	@Column(name = "vessel_name")
	private String vesselName;
	@Column(name = "vessel_type")
	private String vesselType;
	@Column(name = "insurance_type")
	private String insuranceType;
	@Column(name = "insurance_kind")
	private String insuranceKind;
	private String status;
}	
