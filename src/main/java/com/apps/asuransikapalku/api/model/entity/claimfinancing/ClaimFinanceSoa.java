package com.apps.asuransikapalku.api.model.entity.claimfinancing;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "claim_finance_soa")
public class ClaimFinanceSoa {
	@Id
	@SequenceGenerator(name = "claim_finance_soa_seq", sequenceName = "claim_finance_soa_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "claim_finance_soa_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_claim_finance", referencedColumnName = "id")
	private ClaimFinance claimFinance;
	private String filename;
	private String file;
}
