package com.apps.asuransikapalku.api.model.entity.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "v_marine_connect_history")
public class ViewMarineConnectHistory {
	@Id
	private Long id;
	@Column(name = "id_supplier")
	private Long idSupplier;
	@Column(name = "id_demand")
	private Long idDemand;
	private String avatar;
	private String name;
	@Column(name = "id_location")
	private String idLocation;
	@Column(name = "location_name")
	private String locationName;
	@Column(name = "id_occupation")
	private Long idOccupation;
	@Column(name = "occupation_name")
	private String occupationName;
	@Column(name = "response_time")
	private String responseTime;
	@Column(name = "insurance_needs")
	private String insuranceNeeds;
	private String status;
}
