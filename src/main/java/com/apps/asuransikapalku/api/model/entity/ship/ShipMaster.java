package com.apps.asuransikapalku.api.model.entity.ship;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.apps.asuransikapalku.api.model.Common;
import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceKind;
import com.apps.asuransikapalku.api.model.entity.master.ClassificationBureau;
import com.apps.asuransikapalku.api.model.entity.user.Demand;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "mst_ship")
@EqualsAndHashCode(callSuper=true)
public class ShipMaster extends Common {
	@Id
	@SequenceGenerator(name = "mst_ship_seq", sequenceName = "mst_ship_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_ship_seq")
	private Long id;
	private String name;
	@ManyToOne
	@JoinColumn(name = "id_demand", referencedColumnName = "id")
	private Demand demand;
	@OneToOne
    @JoinColumn(name = "id_ship_type",referencedColumnName = "id")
	private ShipType shipType;
	@Column(name = "company_name")
	private String companyName;
	@Column(name = "company_address")
	private String companyAddress;
	private Double size = 0D;
	@Column(name = "production_year")
	private Integer productionYear;
	private Integer age;
	@Column(name = "coverage_name")
	private String coverageName;
	@Column(name = "coverage_value")
	private String coverageValue;
	@Column(name = "coverage_address")
	private String covarageAddress;
	@Column(name = "letter_address")
	private String letterAddress;
	@Column(name = "status_approval")
	private String statusApproval;
	@Column(name = "is_public")
	private Boolean isPublic;
	@Column(name = "is_tnc")
	private Boolean isTnc;
	@Column(name = "trading_area")
	private String tradingArea;
	@Column(name = "is_other_ship_type")
	private Boolean isOtherShipType;
	@Column(name = "other_ship_type")
	private String otherShipType;
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "approval_date")
	private Date approvalDate;
	@OneToOne
    @JoinColumn(name = "id_classification_bureau",referencedColumnName = "id")
	private ClassificationBureau classificationBureau;
	@OneToOne
    @JoinColumn(name = "id_insurance_kind",referencedColumnName = "id")
	private InsuranceKind insuranceKind;
	@OneToMany(mappedBy = "shipMaster", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, orphanRemoval = false)
	List<LossRecord> lossRecords;
	@OneToMany(mappedBy = "shipMaster", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, orphanRemoval = false)
	List<InsuranceOnGoing> insuranceOnGoings;
	@OneToMany(mappedBy = "shipMaster", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, orphanRemoval = false)
	private List<ShipInsuranceMap> shipInsuranceMaps;
}
