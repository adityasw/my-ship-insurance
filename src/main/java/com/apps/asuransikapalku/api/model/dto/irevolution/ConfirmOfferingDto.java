package com.apps.asuransikapalku.api.model.dto.irevolution;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ConfirmOfferingDto {
	@JsonProperty("binding-id")
	private Long id;
	@JsonProperty("is-accept")
	private Boolean isAccept;
}
