package com.apps.asuransikapalku.api.model.entity.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
@Table(name="mst_city")
public class City {
	@Id
	private String id;
	private String name;
	private String type;
	@Column(name="postal_code")
	private String postalCode;
	@ManyToOne
	@JoinColumn(name="id_province", referencedColumnName = "id")
	@JsonIgnore
	private Province province;
}
