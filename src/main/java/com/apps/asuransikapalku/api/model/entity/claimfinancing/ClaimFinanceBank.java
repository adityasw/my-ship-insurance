package com.apps.asuransikapalku.api.model.entity.claimfinancing;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "claim_finance_bank")
public class ClaimFinanceBank {
	@Id
	@SequenceGenerator(name = "claim_finance_bank_seq", sequenceName = "claim_finance_bank_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "claim_finance_bank_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name= "id_claim_finance", referencedColumnName = "id")
	private ClaimFinance claimFinance;
	private String username;
	private String name;
	private String account;
	private String iban;
	@Column(name = "swift_code")
	private String swiftCode;
}
