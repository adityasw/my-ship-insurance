package com.apps.asuransikapalku.api.model.dto.irevolution;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AssignSupplierDto {
	@JsonProperty("binding-id")
	private Long bindingId;
	private List<Long> suppliers;
}
