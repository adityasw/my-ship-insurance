package com.apps.asuransikapalku.api.model.dto.irevolution;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class BidingDto {
	@JsonProperty("id-binding")
	private Long id;
	@JsonProperty("id-supplier")
	private Long idSupplier;
	private Boolean bid;
}
