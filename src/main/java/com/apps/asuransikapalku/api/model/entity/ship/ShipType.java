package com.apps.asuransikapalku.api.model.entity.ship;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.Common;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "mst_ship_type")
@EqualsAndHashCode(callSuper=true)
public class ShipType extends Common{
	@Id
	@SequenceGenerator(name = "mst_ship_type_seq", sequenceName = "mst_ship_type_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_ship_type_seq")
	private Long id;
	private String code;
	private String name;
	private String description;
	@Column(name = "is_active", columnDefinition = "boolean default true")
	private Boolean isActive;
}
