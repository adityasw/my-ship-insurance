package com.apps.asuransikapalku.api.model.dto.user;

import lombok.Data;

@Data
public class ResetPasswordManualDto {
	private String email;
}
