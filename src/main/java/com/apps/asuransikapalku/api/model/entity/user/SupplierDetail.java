package com.apps.asuransikapalku.api.model.entity.user;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.entity.master.City;

import lombok.Data;

@Entity
@Data
@Table(name = "mst_supplier_detail")
public class SupplierDetail {
	@EmbeddedId
	private SupplierDetailPk supplierDetailPk = new SupplierDetailPk();
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_supplier", insertable = false, updatable = false)
	private Supplier supplier;
	private String avatar;
	@ManyToOne
	@JoinColumn(name = "id_city", referencedColumnName = "id")
	private City city;
	@Column(name = "object_type")
	private String objectType;
	@Column(columnDefinition = "TEXT")
	private String description;
	@Column(name = "free_scale")
	private String freeScale;
	@Column(columnDefinition = "TEXT")
	private String portofolio;
	@Column(columnDefinition = "TEXT")
	private String address;
}
