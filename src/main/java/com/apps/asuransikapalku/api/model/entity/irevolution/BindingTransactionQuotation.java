package com.apps.asuransikapalku.api.model.entity.irevolution;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "trn_binding_transaction_quotation")
public class BindingTransactionQuotation {
	@Id
	@SequenceGenerator(name = "trn_binding_transaction_quotation_seq", sequenceName = "trn_binding_transaction_quotation_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trn_binding_transaction_quotation_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_binding_transaction", referencedColumnName = "id")
	private BindingTransaction bindingTransaction; 
	private String name;
	private String file;
	@Column(name = "is_accepted")
	private Boolean isAccepted = Boolean.FALSE;
	@OneToMany(mappedBy = "bindingTransactionQuotation", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<QuoatationMap> quoatationMaps;
}
