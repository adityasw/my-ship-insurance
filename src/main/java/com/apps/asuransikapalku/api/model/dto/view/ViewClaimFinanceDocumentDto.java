package com.apps.asuransikapalku.api.model.dto.view;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ViewClaimFinanceDocumentDto {
	private Long id;
	@JsonProperty("claim-finance-id")
	private Long idClaimFinance;
	@JsonProperty("file-name")
	private String fileName;
	private String file;
	@JsonProperty("upload-date")
	private Date uploadDate;
	private String uploader;
	@JsonProperty("uploader-name")
	private String uploaderName;
	private String to;
	@JsonProperty("uploader-type")
	private String uploaderType;
	@JsonProperty("to-name")
	private String toName;
}
