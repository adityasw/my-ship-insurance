package com.apps.asuransikapalku.api.model.entity.marineconnect;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "mst_marine_connect_chat")
public class ChatMarineConnect {
	@Id
	@SequenceGenerator(name = "mst_marine_connect_chat_seq", sequenceName = "mst_marine_connect_chat_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_marine_connect_chat_seq")
	private Long id;
	private String sender;
	private String receiver;
	@Column(columnDefinition = "TEXT")
	private String content;
	@Column(name = "send_time")
	private Date sendTime;
	@Column(name = "is_read")
	private Boolean isRead;
	@Column(name = "is_read_supply")
	private Boolean isReadSupply;
}
