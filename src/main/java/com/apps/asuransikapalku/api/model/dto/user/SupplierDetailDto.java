package com.apps.asuransikapalku.api.model.dto.user;

import java.util.List;

import com.apps.asuransikapalku.api.model.entity.master.City;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SupplierDetailDto {
	
	@Data
	public static class SupplierClientDto {
		private String code;
		@JsonProperty("client-name")
		private String clientName;
		@JsonProperty("client-logo")
		private String clientLogo;
	}
	
	@Data
	public static class SupplierGalleryDto {
		private String code;
		private String file;
	}
	
	@Data
	public static class Citi {
		@JsonProperty("city-id")
		private String id;
		private String name;
		private Prov province;
	}
	
	@Data
	public static class Prov {
		private String id;
		private String name;
	}
	
	@JsonProperty("id-supplier")
	private Long idSupplier;
	private String code;
	private String avatar;
	private Citi city;
	@JsonProperty("object_type")
	private String objectType;
	private String description;
	private String address;
	@JsonProperty("free-scale")
	private String freeScale;
	private String portofolio;
	private List<SupplierClientDto> clients;
	private List<SupplierGalleryDto> galleries;
}
