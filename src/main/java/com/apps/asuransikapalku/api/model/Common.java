package com.apps.asuransikapalku.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@MappedSuperclass
public class Common {
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
	@JsonIgnore
    private Date createdDate;
    @Column(name = "created_by")
    @JsonIgnore
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_date")
    @JsonIgnore
    private Date updatedDate;
    @Column(name = "updated_by")
    @JsonIgnore
    private String updatedBy;
}
