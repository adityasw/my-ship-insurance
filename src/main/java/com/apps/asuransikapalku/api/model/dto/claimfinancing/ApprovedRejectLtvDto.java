package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ApprovedRejectLtvDto {
	private Long id;
	@JsonProperty("is-approved-ltv")
	private Boolean isApprovedLtv;
	@JsonProperty("is-remaining-ltv")
	private Boolean isRemainingLtv; 
}
