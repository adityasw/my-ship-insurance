package com.apps.asuransikapalku.api.model.dto.irevolution;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AssignReplacementDto {
	@JsonProperty("supplier-id")
	private Long idSupplier;
	@JsonProperty("insurance-id")
	private Long idInsurance;
}
