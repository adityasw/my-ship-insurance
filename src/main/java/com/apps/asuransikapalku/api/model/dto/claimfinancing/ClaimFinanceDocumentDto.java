package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClaimFinanceDocumentDto {
	private Long id;
	@JsonProperty("claim-finance-id")
	private Long claimFinanceId;
	@JsonProperty("filename")
	private String fileName;
	private String file;
	private String description;
	private String to;
	@JsonProperty("is-supply")
	private Boolean isSupply;
}
