package com.apps.asuransikapalku.api.model.dto.irevolution;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ChatDto {
	private Long id;
	private String code;
	@JsonProperty("id-reference")
	private Long idReference;
	private String receiver;
	private String content;
	private String sender;
	@JsonProperty("send-time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy' 'HH:mm:ss", timezone = "Asia/Jakarta")
	private Date sendTime;
	@JsonProperty("is-project")
	private Boolean isProject;
	@JsonProperty("is-supply")
	private Boolean isSupply;
	@JsonProperty("sendder-name")
	private String senderName;
	@JsonProperty("receiver-name")
	private String receiverName;
}
