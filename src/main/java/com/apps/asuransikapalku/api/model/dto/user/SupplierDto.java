package com.apps.asuransikapalku.api.model.dto.user;

import java.util.List;

import com.apps.asuransikapalku.api.model.dto.occupation.OccupationDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SupplierDto{
	private Long id;
	private String photo;
	private String username;
	@JsonProperty("phone-number")
	private String phoneNumber;
	private String name;
	private String email;
	@JsonProperty("company-name")
	private String companyName;
	private OccupationDto occupation;
	private String position;
	@ApiModelProperty(notes = "Huruf kapital")
	@JsonProperty("supplier-as")
	private String supplierAs;
	@JsonProperty("is-approve")
	private Boolean isApprove;
	@JsonProperty("is-reject")
	private Boolean isReject;
	private Boolean tnc;
	@JsonProperty("insurance-maps")
	private List<SupplierInsuranceMapDto> supplierInsuranceMapDtos;
	private SupplierDetailDto detail;
}
