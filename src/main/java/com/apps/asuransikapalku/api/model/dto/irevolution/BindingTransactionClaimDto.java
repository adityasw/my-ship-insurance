package com.apps.asuransikapalku.api.model.dto.irevolution;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
public class BindingTransactionClaimDto {
	private Long id;
	@JsonProperty("binding-id")
	private Long bindingId;
	private String summary;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	@JsonProperty("claim-date")
	private Date claimDate;
	private String file;
}
