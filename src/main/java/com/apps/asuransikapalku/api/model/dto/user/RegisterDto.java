package com.apps.asuransikapalku.api.model.dto.user;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RegisterDto {
	private String name;
	private String email;
	private String occupation;
	private String position;
	@JsonProperty("insurance-needs")
	private String insuranceNeeds;
	@JsonProperty("company-name")
	private String companyName;
	@JsonProperty("phone-number")
	private String phoneNumber;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy' 'HH:mm:ss", timezone = "Asia/Jakarta")
	@JsonProperty("schedule-meet")
	private Date scheduleMeet;
}
