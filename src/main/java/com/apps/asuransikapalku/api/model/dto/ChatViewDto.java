package com.apps.asuransikapalku.api.model.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChatViewDto {
	private String code;
	@JsonProperty("full-name")
	private String fullName;
	@JsonProperty("latest-message")
	private String latesMessage;
	@JsonProperty("total-new-message")
	private Long totalNewMessage;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy' 'HH:mm:ss", timezone = "Asia/Jakarta")
	private Date sendTime;
}
