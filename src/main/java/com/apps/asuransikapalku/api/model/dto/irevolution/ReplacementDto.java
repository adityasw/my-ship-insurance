package com.apps.asuransikapalku.api.model.dto.irevolution;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ReplacementDto {
	@JsonProperty("ship-id")
	private Long id;
	@JsonProperty("is-public")
	private Boolean isPublic;
	@JsonProperty("is-selected")
	private Boolean isSelected;
	@JsonProperty("insurances-id")
	private List<Long> idInsurances;
}
