package com.apps.asuransikapalku.api.model.dto.insurance;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class InsuranceMasterDto {
	private Long id;
	private String name;
	private String logo;
	private Double rating;
	@JsonProperty("insurance-kind")
	private InsuranceKindDto insuranceKindDto;
}
