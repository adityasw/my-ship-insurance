package com.apps.asuransikapalku.api.model.entity.claimfinancing;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "claim_finance_ltv")
public class ClaimFinanceLtv {
	@Id
	@SequenceGenerator(name = "claim_finance_ltv_seq", sequenceName = "claim_finance_ltv_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "claim_finance_ltv_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name= "id_claim_finance", referencedColumnName = "id")
	private ClaimFinance claimFinance;
	private String username;
	@Column(name = "approved_ltv")
	private String approvedLtv;
	@Column(name = "remaining_ltv")
	private String remainingLtv;
	@Column(name = "is_approved_ltv")
	private Boolean isApprovedLtv;
	@Column(name = "is_remaining_ltv")
	private Boolean isRemainingLtv;
}
