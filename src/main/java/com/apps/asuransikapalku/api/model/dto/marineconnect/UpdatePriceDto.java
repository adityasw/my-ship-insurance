package com.apps.asuransikapalku.api.model.dto.marineconnect;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UpdatePriceDto {
	@JsonProperty("id-marine-connect")
	private Long idMarineConnect;
	private String price;
	private String description;
}
