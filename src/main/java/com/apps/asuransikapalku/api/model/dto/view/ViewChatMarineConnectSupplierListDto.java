package com.apps.asuransikapalku.api.model.dto.view;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ViewChatMarineConnectSupplierListDto {
	private Long id;
	private String receiver;
	private String sender;
	@JsonProperty("sender-name")
	private String senderName;
	private String content;
	@JsonProperty("send-time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy' 'HH:mm:ss", timezone = "Asia/Jakarta")
	private Date sendTime;
	@JsonProperty("total-unread")
	private Integer totalUnread;
}
