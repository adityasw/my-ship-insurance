package com.apps.asuransikapalku.api.model.dto.view;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ViewInsuranceDemDto {
	private Long id;
	@JsonProperty("vessel-name")
	private String vesselName;
	@JsonProperty("vessel-type")
	private String vesselType;
	@JsonProperty("insurance-type")
	private String insuranceType;
	@JsonProperty("insurance-kind")
	private String insuranceKind;
	private String status;
}
