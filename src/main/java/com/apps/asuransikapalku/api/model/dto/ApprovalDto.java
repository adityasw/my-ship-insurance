package com.apps.asuransikapalku.api.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ApprovalDto {
	private Long id;
	@JsonProperty("is-approve")
	private Boolean isApprove;
	@JsonProperty("is-reject")
	private Boolean isReject;
}
