package com.apps.asuransikapalku.api.model.dto;

import java.util.List;

import com.apps.asuransikapalku.api.model.dto.occupation.OccupationDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AprrovalDto {
	@Data
	public static class Member {
		private String name;
		private String email;
		private String position;
		private String occupation;
		@JsonProperty("insurance-needs")
		private String insuranceNeeds;
		@JsonProperty("company-name")
		private String companyName;
		@JsonProperty("phone-number")
		private String phoneNumber;
	}
	private Long id;
	private Member member;
	@JsonProperty("status-code")
	private String statusCode;
	@JsonProperty("is-demand")
	private Boolean isDemand;
	private OccupationDto occupation;
	@JsonProperty("insurances")
	private List<Long> insuranceIds;
}
