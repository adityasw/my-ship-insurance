package com.apps.asuransikapalku.api.model.dto.master;

import lombok.Data;

@Data
public class ClassifiacationBureauDto {
	private Long id;
	private String name;
}
