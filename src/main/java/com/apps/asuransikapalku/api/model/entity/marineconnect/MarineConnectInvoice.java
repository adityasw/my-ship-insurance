package com.apps.asuransikapalku.api.model.entity.marineconnect;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "marine_connect_invoice")
public class MarineConnectInvoice {
	@Id
	@SequenceGenerator(name = "marine_connect_invoice_seq", sequenceName = "marine_connect_invoice_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "marine_connect_invoice_seq")
	private Long id;
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_marine_connect", referencedColumnName = "id")
	private MarineConnectInquiry marineConnect;
	private String file;
}
