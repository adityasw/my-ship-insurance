package com.apps.asuransikapalku.api.model.entity.user;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.Common;
import com.apps.asuransikapalku.api.model.entity.master.City;
import com.apps.asuransikapalku.api.model.entity.master.Province;
import com.apps.asuransikapalku.api.model.entity.master.SubDistrict;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "mst_admin")
@EqualsAndHashCode(callSuper=true)
public class Admin extends Common{
	@Id
	@SequenceGenerator(name = "mst_admin_seq", sequenceName = "mst_admin_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_admin_seq")
	private Long id;
	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastName;
	private String name;
	private String email;
	private String gender;
	private String photo;
	@Column(name="phone_number")
	private String phoneNumber;
	private Boolean tnc;
	@Column(name = "is_active", columnDefinition = "boolean default true")
	private Boolean isActive;
	@ManyToOne
    @JoinColumn(name = "id_role",referencedColumnName = "id")
	private Role role;
	@OneToOne(mappedBy = "admin", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, optional = false)
	@JsonIgnore
	private User user;
	@ManyToOne
    @JoinColumn(name = "id_province",referencedColumnName = "id")
	private Province province;
	@ManyToOne
    @JoinColumn(name = "id_city",referencedColumnName = "id")
	private City city;
	@ManyToOne
    @JoinColumn(name = "id_subdistrict",referencedColumnName = "id")
	private SubDistrict subDistrict;
}
