package com.apps.asuransikapalku.api.model.entity.user;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "mst_supplier_client")
public class SupplierClient {
	@EmbeddedId
	private SupplierDetailPk supplierDetailPk = new SupplierDetailPk();
	@ManyToOne
	@JoinColumn(name = "id_supplier", insertable = false, updatable = false)
	private Supplier supplier;
	@Column(name = "client_name")
	private String clientName;
	@Column(name = "client_logo")
	private String clientLogo;
}
