package com.apps.asuransikapalku.api.model.entity.user;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.Common;
import com.apps.asuransikapalku.api.model.entity.occupation.Occupation;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "mst_supplier")
@EqualsAndHashCode(callSuper=true)
public class Supplier extends Common{
	@Id
	@SequenceGenerator(name = "mst_supplier_seq", sequenceName = "mst_supplier_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_supplier_seq")
	private Long id;
	private String name;
	private String email;
	@Column(name = "phone_number")
	private String phoneNumber;
	@ManyToOne
	@JoinColumn(name = "id_occupation", referencedColumnName = "id")
	private Occupation occupation;
	private String position;
	@Column(name = "company_name")
	private String companyName;
	@Column(name = "rating_company")
	private Integer ratingCompany = 0;
	@Column(name = "supplier_as")
	private String supplierAs;
	@Column(name = "is_approve")
	private Boolean isApprove = Boolean.FALSE;
	@Column(name = "is_reject")
	private Boolean isReject = Boolean.FALSE;
	private Boolean tnc;
	private String photo;
	@Column(name = "insurance_needs", columnDefinition="TEXT")
	private String insuranceNeeds;
	@OneToOne(mappedBy = "supplier", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, optional = false)
	@JsonIgnore
	private User user;
	@OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, orphanRemoval = false)
	private List<SupplierInsuranceMap> supplierInsuranceMaps;
}
