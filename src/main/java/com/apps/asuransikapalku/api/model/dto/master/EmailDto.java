package com.apps.asuransikapalku.api.model.dto.master;

import java.util.List;

import lombok.Data;

@Data
public class EmailDto<T> {
	private List<String> toIds;
	private List<String> ccIds;
	private List<String> bccIds;
	private String subject;
	private String fromId;
	private T content;
	private boolean attachmentStatus;
}
