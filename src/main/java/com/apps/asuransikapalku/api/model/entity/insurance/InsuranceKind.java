package com.apps.asuransikapalku.api.model.entity.insurance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.Common;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "mst_insurance_kind")
@EqualsAndHashCode(callSuper=true)
public class InsuranceKind extends Common {
	@Id
	@SequenceGenerator(name = "mst_insurance_kind_seq", sequenceName = "mst_insurance_kind_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_insurance_kind_seq")
	private Long id;
	private String code;
	private String name;
	private String description;
	@Column(name = "is_active", columnDefinition = "boolean default true")
	private Boolean isActive;
	@ManyToOne
    @JoinColumn(name = "id_type_insurance",referencedColumnName = "id")
	private InsuranceType insuranceType;
}
