package com.apps.asuransikapalku.api.model.entity.claimfinancing;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.entity.user.Supplier;

import lombok.Data;

@Entity
@Data
@Table(name = "claim_finance_offering")
public class ClaimFinanceOffering {
	@Id
	@SequenceGenerator(name = "claim_finance_offering_seq", sequenceName = "claim_finance_offering_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "claim_finance_offering_seq")
	private Long id;
	private String username;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name= "id_claim_finance", referencedColumnName = "id")
	private ClaimFinance claimFinance;
	@ManyToOne
	@JoinColumn(name = "id_supplier", referencedColumnName = "id")
	private Supplier supplier;
	@Column(columnDefinition = "TEXT")
	private String ltv;
	@Column(name = "annual_interest")
	private String annualInterest;
	@Column(name = "nilai_pendanaan")
	private String nilaiPendanaan;
	@Column(name = "is_selected")
	private Boolean isSelected;
}
