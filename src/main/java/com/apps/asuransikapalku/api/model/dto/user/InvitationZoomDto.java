package com.apps.asuransikapalku.api.model.dto.user;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class InvitationZoomDto {
	private Long id;
	private String link;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy' 'HH:mm:ss", timezone = "Asia/Jakarta")
	@JsonProperty("schedule-meet")
	private Date scheduleMeet;
	private Boolean isZoom;
	private Boolean isRemainder;
}
