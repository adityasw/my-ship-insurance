package com.apps.asuransikapalku.api.model.dto.user;

import com.apps.asuransikapalku.api.model.dto.occupation.OccupationDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DemandDto{
	private Long id;
	private String photo;
	private String username;
	private String name;
	private String email;
	private OccupationDto occupation;
	private String position;
	@JsonProperty("insurance-needs")
	private String insuranceNeeds;
	@JsonProperty("company-name")
	private String companyName;
	@JsonProperty("phone-number")
	private String phoneNumber;
	@ApiModelProperty(notes = "Jika di Approve send 'TRUE'")
	@JsonProperty("is-approve")
	private Boolean isApprove;
	@ApiModelProperty(notes = "Jika di Tolak send 'TRUE'")
	@JsonProperty("is-reject")
	private Boolean isReject;
	private Boolean tnc;
}
