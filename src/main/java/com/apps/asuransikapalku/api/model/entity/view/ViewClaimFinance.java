package com.apps.asuransikapalku.api.model.entity.view;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "v_claim_finance")
public class ViewClaimFinance {
	@Id
	private Long id;
	@Column(name = "company_name")
	private String companyName;
	@Column(name = "vessel_name")
	private String vesselName;
	@Column(name = "vessel_type")
	private String vesselType;
	private String casuality;
	@Column(name = "location_of_loss")
	private String locationOfLoss;
	@Column(name = "date_of_loss")
	private Date dateOfLoss;
	@Column(name = "vessel_type_id")
	private Long idVessleType;
	private String status;
	@Column(name = "id_demand")
	private Long idDemand;
}
