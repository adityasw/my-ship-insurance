package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClaimFinanceLtvDto {
	private Long id;
	@JsonProperty("claim-finance-id")
	private Long claimFinanceId;
	private String username;
	@JsonProperty("approved-ltv")
	private String approvedLtv;
	@JsonProperty("remaning-ltv")
	private String remainingLtv;
	@JsonProperty("is-approved-ltv")
	private Boolean isApprovedLtv;
	@JsonProperty("is-remaining-ltv")
	private Boolean isRemainingLtv;
}
