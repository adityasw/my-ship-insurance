package com.apps.asuransikapalku.api.model.dto.irevolution;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class BindingTransactionDocumentDemandDto {
	private Long id;
	@JsonProperty("binding-id")
	private Long idBinding;
	private String name;
	private String file;
}
