package com.apps.asuransikapalku.api.model.entity.insurance;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.Common;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "mst_insurance")
@EqualsAndHashCode(callSuper=true)
public class InsuranceMaster extends Common {
	@Id
	@SequenceGenerator(name = "mst_insurance_seq", sequenceName = "mst_insurance_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_insurance_seq")
	private Long id;
	private String name;
	private String logo;
	private Double rating;
	@OneToOne
    @JoinColumn(name = "id_insurance_kind",referencedColumnName = "id")
	private InsuranceKind insuranceKind;
}
