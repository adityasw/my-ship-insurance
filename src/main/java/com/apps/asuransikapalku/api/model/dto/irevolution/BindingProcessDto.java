package com.apps.asuransikapalku.api.model.dto.irevolution;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class BindingProcessDto {
	@JsonProperty("binding-id")
	private Long id;
	private String status;
}
