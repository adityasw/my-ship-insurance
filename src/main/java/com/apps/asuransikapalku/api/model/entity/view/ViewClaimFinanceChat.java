package com.apps.asuransikapalku.api.model.entity.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "v_claim_finance_chat")
public class ViewClaimFinanceChat {
	@Id
	private Long id;
	@Column(name = "id_claim_finance")
	private Long idClaimFinance;
	@Column(name = "supplier_name")
	private String supplierName;
	private String sender;
	private String content;
	@Column(name = "total_unread")
	private Integer totalUnread;
}
