package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClaimFinanceQuotationDto {
	private Long id;
	@JsonProperty("claim-finance-id")
	private Long claimFinanceId;
	private String file;
	private String period;
}
