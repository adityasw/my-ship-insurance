package com.apps.asuransikapalku.api.model.entity.ship;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceMaster;

import lombok.Data;

@Entity
@Data
@Table(name = "mst_mapping_ship_insurance")
public class ShipInsuranceMap {
	@Id
	@SequenceGenerator(name = "mst_mapping_ship_insurance_seq", sequenceName = "mst_mapping_ship_insurance_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_mapping_ship_insurance_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_ship", referencedColumnName = "id")
	private ShipMaster shipMaster;
	@ManyToOne
	@JoinColumn(name = "id_insurance", referencedColumnName = "id")
	private InsuranceMaster insuranceMaster;
	@Column(name = "is_check")
	private Boolean isCheck = Boolean.FALSE;
}
