package com.apps.asuransikapalku.api.model.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ResetPasswordDto {
	private String username;
	@JsonProperty("current-password")
	private String password;
	@JsonProperty("new-password")
	private String newPassword;
	private Boolean tnc;
	private String name;
}
