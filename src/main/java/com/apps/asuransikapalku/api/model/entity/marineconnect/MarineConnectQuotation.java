package com.apps.asuransikapalku.api.model.entity.marineconnect;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "marine_connect_quotation")
public class MarineConnectQuotation {
	@Id
	@SequenceGenerator(name = "marine_connect_quotation_seq", sequenceName = "marine_connect_quotation_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "marine_connect_quotation_seq")
	private Long id;
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_marine_connect", referencedColumnName = "id")
	private MarineConnectInquiry marineConnect;
	@Column(name = "quoted_price")
	private String quotedPrice;
	@Column(name = "payment_type")
	private String paymentType;
	@Column(columnDefinition = "TEXT")
	private String description;
	@Column(name = "scope_of_work")
	private String scopeOfWork;
	private String deliverle;
	@Column(name = "estimate_time_completion")
	private String estimateTimeCompletion;
	@Column(name = "negotiate_price")
	private String negotiatePrice;
	@Column(name = "negotiate_description", columnDefinition = "TEXT")
	private String negotiateDescription;
	@Column(name = "final_price")
	private String finalPrice;
	@Column(name = "final_description", columnDefinition = "TEXT")
	private String finalDescription;
}
