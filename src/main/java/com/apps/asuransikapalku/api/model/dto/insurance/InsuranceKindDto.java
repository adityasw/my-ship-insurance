package com.apps.asuransikapalku.api.model.dto.insurance;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class InsuranceKindDto {
	
	private Long id;
	@JsonIgnore
	private String code;
	private String name;
	private String description;
	@JsonProperty("is-active")
	private Boolean isActive;
	@JsonProperty("insurance-type")
	private InsuranceTypeDto insuranceType;
}
