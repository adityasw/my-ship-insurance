package com.apps.asuransikapalku.api.model.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RoleDto {
	private Long id;
	private String name;
	private String description;
	@JsonProperty("is-active")
	private Boolean isActive;
}
