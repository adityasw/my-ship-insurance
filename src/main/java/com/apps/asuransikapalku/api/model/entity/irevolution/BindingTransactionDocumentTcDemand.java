package com.apps.asuransikapalku.api.model.entity.irevolution;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
@Table(name = "trn_binding_transaction_doc_tc_demand")
public class BindingTransactionDocumentTcDemand {
	@Id
	@SequenceGenerator(name = "trn_binding_transaction_doc_tc_demand_seq", sequenceName = "trn_binding_transaction_doc_tc_demand_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trn_binding_transaction_doc_tc_demand_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_binding_transaction", referencedColumnName = "id")
	private BindingTransaction bindingTransaction;
	private String type;
	@Temporal(TemporalType.DATE)
    @Column(name = "upload_date")
	private Date uploadDate;
	@Column(name = "file_name")
	private String fileName;
	private String file;
	private String status;
	private String uploader;
	private String target;
}
