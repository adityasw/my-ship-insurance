package com.apps.asuransikapalku.api.model.dto.view;

import java.util.Date;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ViewClaimFinanceDto {
	@Id
	private Long id;
	@JsonProperty("company-name")
	private String companyName;
	@JsonProperty("vessel-name")
	private String vesselName;
	@JsonProperty("vessel-type")
	private String vesselType;
	private String casuality;
	@JsonProperty("date-of-loss")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Jakarta")
	private Date dateOfLoss;
	@JsonProperty("location-of-loss")
	private String locationOfLoss;
	private String status;
}
