package com.apps.asuransikapalku.api.model.dto.irevolution;

import java.math.BigDecimal;
import java.util.List;

import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceMasterDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDto;

import lombok.Data;

@Data
public class QuotationMapDto {
	private Long id;
	private SupplierDto supplier;
	private InsuranceMasterDto insuranceMaster;
	private BigDecimal commission;
	private List<QuotationMapSecurityListDto> quotationMapSecurityLists;
}
