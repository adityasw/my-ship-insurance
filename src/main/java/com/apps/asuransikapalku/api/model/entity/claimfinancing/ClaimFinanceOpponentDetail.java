package com.apps.asuransikapalku.api.model.entity.claimfinancing;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "claim_finance_opponent_detail")
public class ClaimFinanceOpponentDetail {
	@EmbeddedId
	private ClaimFinanceOpponentDetailPk pk = new ClaimFinanceOpponentDetailPk();
	@ManyToOne
	@JoinColumn(name = "id_claim_finance", referencedColumnName = "id", insertable = false, updatable = false)
	private ClaimFinance claimFinance;
	@Column(name = "object_type")
	private String objectType;
	@Column(name = "object_name")
	private String objectName;
	@Column(name = "owner_name")
	private String ownerName;
	@Column(name = "nature_damage_to_our_vessel", columnDefinition = "TEXT")
	private String natureDamageToOurVessel;
	@Column(name = "nature_damage_to_third_parties", columnDefinition = "TEXT")
	private String natureDamageToThirdParties;
	@Column(name = "claim_estimate_to_our_vessel")
	private String claimEstimateToOurVessel;
	@Column(name = "claim_estimate_to_third_parties")
	private String claimEstimateToThirdParties;
}
