package com.apps.asuransikapalku.api.model.dto.user;

import com.apps.asuransikapalku.api.model.dto.occupation.OccupationDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DemandRegisterDto {
	private String username;
	private String name;
	private String email;
	@JsonProperty("phone-number")
	private String phoneNumber;
	@JsonProperty("company-name")
	private String companyName;
	private OccupationDto occupation;
	private String position;
	@JsonProperty("insurance-needs")
	private String insuranceNeeds;
}
