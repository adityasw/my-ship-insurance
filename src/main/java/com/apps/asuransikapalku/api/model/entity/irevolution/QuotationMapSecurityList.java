package com.apps.asuransikapalku.api.model.entity.irevolution;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceMaster;

import lombok.Data;

@Entity
@Data
@Table(name = "trn_binding_quotation_map_security_list")
public class QuotationMapSecurityList {
	@Id
	@SequenceGenerator(name = "trn_binding_quotation_map_security_list_seq", sequenceName = "trn_binding_quotation_map_security_list_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trn_binding_quotation_map_security_list_seq")
	private Long id;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "id_quotation_map", referencedColumnName = "id")
	private QuoatationMap quoatationMap;
	@ManyToOne
	@JoinColumn(name = "id_insurance", referencedColumnName = "id")
	private InsuranceMaster insuranceMaster;
	private String type;
	private Double percentage;
}
