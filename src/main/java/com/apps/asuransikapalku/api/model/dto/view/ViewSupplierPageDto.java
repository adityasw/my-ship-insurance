package com.apps.asuransikapalku.api.model.dto.view;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ViewSupplierPageDto {
	private Long id;
	@JsonProperty("id-supplier")
	private Long idSupplier;
	private String avatar;
	private String name;
	@JsonProperty("location-name")
	private String locationName;
	@JsonProperty("occupation-name")
	private String occupationName;
	@JsonProperty("response-time")
	private String responseTime;
	@JsonProperty("insurance-needs")
	private String insuranceNeeds;
	@JsonProperty("wish-list-flag")
	private Boolean flag;
}
