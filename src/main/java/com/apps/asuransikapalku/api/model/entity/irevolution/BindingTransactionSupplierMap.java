package com.apps.asuransikapalku.api.model.entity.irevolution;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceMaster;
import com.apps.asuransikapalku.api.model.entity.user.Supplier;

import lombok.Data;

@Entity
@Data
@Table(name = "trn_binding_supplier_map")
public class BindingTransactionSupplierMap {
	@Id
	@SequenceGenerator(name = "trn_binding_supplier_map_seq", sequenceName = "trn_binding_supplier_map_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trn_binding_supplier_map_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_binding_transaction", referencedColumnName = "id")
	private BindingTransaction bindingTransaction;
	@ManyToOne
	@JoinColumn(name = "id_supplier", referencedColumnName = "id")
	private Supplier supplier;
	@ManyToOne
	@JoinColumn(name = "id_insurance", referencedColumnName = "id")
	private InsuranceMaster insuranceMaster;
	@OneToMany(mappedBy = "bindingTransactionSupplierMap", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, orphanRemoval = true)
	private List<BindingTransactionDocument> bindingTransactionDocuments;
	private Boolean bid = Boolean.FALSE;
}
