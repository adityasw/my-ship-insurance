package com.apps.asuransikapalku.api.model.entity.master;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
@Table(name="mst_subdistrict")
public class SubDistrict {
	@Id
	private String id;
	private String name;
	@ManyToOne
	@JoinColumn(name = "id_province", referencedColumnName = "id")
	@JsonIgnore
	private Province province;
	@ManyToOne
	@JoinColumn(name = "id_city", referencedColumnName = "id")
	@JsonIgnore
	private City city;
}
