package com.apps.asuransikapalku.api.model.entity.user;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.apps.asuransikapalku.api.model.Common;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "mst_user")
@EqualsAndHashCode(callSuper=true)
public class User extends Common{
	@Id
	@SequenceGenerator(name = "mst_user_seq", sequenceName = "mst_user_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_user_seq")
	private Long id;
	private String username;
	private String password;
	@JsonIgnore
	private String token;
	@Column(name = "is_login")
	private Boolean isLogin = Boolean.FALSE; 
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_login")
	@JsonIgnore
	private Date lastLogin;
	@Column(name = "is_active")
	private Boolean isActive = Boolean.TRUE;
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_admin",referencedColumnName = "id")
	private Admin admin;
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_demand",referencedColumnName = "id")
	private Demand demand;
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_supplier",referencedColumnName = "id")
	private Supplier supplier;
}
