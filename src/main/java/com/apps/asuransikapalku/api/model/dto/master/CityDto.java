package com.apps.asuransikapalku.api.model.dto.master;

import javax.persistence.Id;

import lombok.Data;

@Data
public class CityDto {
	@Id
	private String id;
	private String name;
	private String type;
}
