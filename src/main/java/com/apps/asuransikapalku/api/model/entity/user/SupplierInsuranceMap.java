package com.apps.asuransikapalku.api.model.entity.user;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceMaster;

import lombok.Data;

@Entity
@Data
@Table(name = "mst_supplier_insurance_map")
public class SupplierInsuranceMap {
	@Id
	@SequenceGenerator(name = "mst_supplier_insurance_map_seq", sequenceName = "mst_supplier_insurance_map_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_supplier_insurance_map_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_supplier", referencedColumnName = "id")
	private Supplier supplier;
	@ManyToOne
	@JoinColumn(name = "id_insurance", referencedColumnName = "id")
	private InsuranceMaster insuranceMaster;
}
