package com.apps.asuransikapalku.api.model.dto.master;

import lombok.Data;

@Data
public class ProvinceDto {
	private String id;
	private String name;
}
