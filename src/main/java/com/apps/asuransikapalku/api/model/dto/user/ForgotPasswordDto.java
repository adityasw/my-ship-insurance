package com.apps.asuransikapalku.api.model.dto.user;

import lombok.Data;

@Data
public class ForgotPasswordDto {
	private String email;
}
