package com.apps.asuransikapalku.api.model.dto.ship;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ShipApprovalDto {
	
	@Data
	public static class InsuranceSuggestion{
		@JsonProperty("supplier-id")
		private Long idSupplier;
		@JsonProperty("insurance-id")
		private Long idInsurance;
	}
	
	@JsonProperty("ship-id")
	private Long id;
	@JsonProperty("status-approval")
	private String statusApproval;
	@JsonProperty("insurances")
	private List<InsuranceSuggestion> insurances;
}
