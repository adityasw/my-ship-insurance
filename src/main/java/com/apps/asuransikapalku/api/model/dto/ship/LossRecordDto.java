package com.apps.asuransikapalku.api.model.dto.ship;

import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceKindDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class LossRecordDto {
	private Long id;
	private Integer year;
	private String incident;
	@JsonProperty("major-drawback")
	private String majorDrawback;
	@JsonProperty("insurance-status")
	private String insuranceStatus;
	@JsonProperty("insurance-name")
	private String insuranceName;
	private String currency;
	@JsonProperty("insurance-kind")
	private InsuranceKindDto insuranceKind;
}
