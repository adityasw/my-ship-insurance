package com.apps.asuransikapalku.api.model.dto.view;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "v_Chat")
public class ViewChat {
	@Id
	private Long id;
	@Column(name = "id_reference")
	private Long idReference;
	@Column(name = "is_project")
	private Boolean isProject;
	@Column(name = "is_supply")
	private Boolean isSupply;
	@Column(name = "full_name")
	private String fullName;
	private String username;
	private String code;
	@Column(name = "send_time")
	private Date sendTime;
	private String content;
	private Long total;
}
