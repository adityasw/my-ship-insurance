package com.apps.asuransikapalku.api.model.entity.marineconnect;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class MarineConnectWishListPk implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4519439727048062927L;
	
	@Column(name = "id_demand")
	private Long idDemand;
	@Column(name = "id_Supplier")
	private Long idSupplier;
}
