package com.apps.asuransikapalku.api.model.dto.marineconnect;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MarineConnectInvoiceDto {
	private Long id;
	@JsonProperty("id-marine-connect")
	private Long idMarinceConnect;
	private String file;
}
