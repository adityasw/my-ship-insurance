package com.apps.asuransikapalku.api.model.dto.irevolution;

import com.apps.asuransikapalku.api.model.dto.user.SupplierDto;

import lombok.Data;

@Data
public class BindingTransactionSupplierMapDto {
	private Long id;
	private SupplierDto supplierDto;
	private Boolean bid;
}
