package com.apps.asuransikapalku.api.model.dto.user;

import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceMasterDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SupplierInsuranceMapDto {
	@JsonProperty("id-map")
	private Long id;
	@JsonProperty("insurance")
	private InsuranceMasterDto insuranceMaster;
}
