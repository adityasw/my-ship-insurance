package com.apps.asuransikapalku.api.model.dto.ship;

import java.util.List;

import com.apps.asuransikapalku.api.model.dto.user.DemandDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DetailVesselDto {
	@JsonProperty("ship-id")
	private Long id;
	private DemandDto demand;
	@JsonProperty("assign-insurance")
	private List<Long> assignInsurance;
}
