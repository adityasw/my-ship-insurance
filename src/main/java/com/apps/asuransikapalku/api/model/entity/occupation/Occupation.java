package com.apps.asuransikapalku.api.model.entity.occupation;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.Common;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "mst_occupation")
@EqualsAndHashCode(callSuper=true)
public class Occupation extends Common{
	@Id
	@SequenceGenerator(name = "mst_occupation_seq", sequenceName = "mst_occupation_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_occupation_seq")
	private Long id;
	private String code;
	private String name;
	private String description;
	@Column(name = "is_active")
	private Boolean isActive = Boolean.TRUE;
	@ManyToOne
	@JoinColumn(name =  "id_occupation_type", referencedColumnName = "id")
	private OccupationType occupationType;
	@OneToOne(mappedBy = "occupation", cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, optional = false)
	private OccupationMap occupationMap;
}
