package com.apps.asuransikapalku.api.model.dto.occupation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class OccupationDto {
	
	@Data
	public static class OccupationMapDto {
		@JsonProperty("is-revolutions")
		private Boolean isIrevolution;
		@JsonProperty("is-claim-solution")
		private Boolean isClaimSolution;
		@JsonProperty("is-claim-financing")
		private Boolean isClaimFinancing;
		@JsonProperty("is-marine-connect")
		private Boolean isMarineConnect;
	}
	
	private Long id;
	@JsonIgnore
	private String code;
	private String name;
	private String description;
	@JsonProperty("is-active")
	private Boolean isActive;
	@JsonProperty("occupation-type")
	private OccupationTypeDto occupationType;
	@JsonProperty("occupation-map")
	private OccupationMapDto occupationMapDto;
}
