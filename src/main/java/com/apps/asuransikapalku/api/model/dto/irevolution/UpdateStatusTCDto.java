package com.apps.asuransikapalku.api.model.dto.irevolution;

import lombok.Data;

@Data
public class UpdateStatusTCDto {
	private Long id;
	private String status;
}
