package com.apps.asuransikapalku.api.model.dto.irevolution;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class BindingTransactionQuotationDto {
	private Long id;
	@JsonProperty("binding-id")
	private Long idBinding;
	private String name;
	private String file;
	@JsonProperty("is-accepted")
	private Boolean isAccepted;
	private List<QuotationMapDto> quoatationMaps;
}
