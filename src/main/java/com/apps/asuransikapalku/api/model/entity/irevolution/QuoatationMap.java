package com.apps.asuransikapalku.api.model.entity.irevolution;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceMaster;
import com.apps.asuransikapalku.api.model.entity.user.Supplier;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
@Table(name = "trn_binding_quotation_map")
public class QuoatationMap {
	@Id
	@SequenceGenerator(name = "trn_binding_quotation_map_seq", sequenceName = "trn_binding_quotation_map_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trn_binding_quotation_map_seq")
	private Long id;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "id_quotation_binding", referencedColumnName = "id")
	@JsonIgnore
	private BindingTransactionQuotation bindingTransactionQuotation;
	@ManyToOne
	@JoinColumn(name = "id_supplier", referencedColumnName = "id")
	private Supplier supplier;
	@ManyToOne
	@JoinColumn(name = "id_insurance", referencedColumnName = "id")
	private InsuranceMaster insuranceMaster;
	private BigDecimal commission;
	@OneToMany(mappedBy = "quoatationMap", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<QuotationMapSecurityList> quotationMapSecurityLists;
}
