package com.apps.asuransikapalku.api.model.dto.irevolution;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UploadTcDto {
	@JsonProperty("binding-id")
	private Long id;
	@JsonProperty("file-name")
	private String fileName;
	private String file;
	private String status;
	private String to;
	@JsonProperty("is-supply")
	private Boolean isSupply;
}
