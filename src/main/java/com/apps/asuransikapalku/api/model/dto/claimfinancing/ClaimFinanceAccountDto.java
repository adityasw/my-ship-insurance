package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClaimFinanceAccountDto {
	private Long id;
	@JsonProperty("claim-finance-id")
	private Long claimFinanceId;
	private String ltv;
	@JsonProperty("annual-interest")
	private String annualInterest;
	@JsonProperty("reject-reason")
	private Boolean rejestReason;
	@JsonProperty("is-give-offer")
	private Boolean isGiveTheOffer;
}
