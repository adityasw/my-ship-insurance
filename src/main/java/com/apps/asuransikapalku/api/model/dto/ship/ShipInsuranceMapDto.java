package com.apps.asuransikapalku.api.model.dto.ship;

import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceMasterDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ShipInsuranceMapDto {
	private Long id;
	@JsonProperty("insurance")
	InsuranceMasterDto insuranceMasterDto;
	@JsonProperty("is-check")
	private Boolean isCheck;
}
