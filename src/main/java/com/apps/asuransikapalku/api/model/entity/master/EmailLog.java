package com.apps.asuransikapalku.api.model.entity.master;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
@Table(name = "email_log")
public class EmailLog {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="email_to")
	private String toIds;
	@Column(name="email_cc")
	private String ccIds;
	@Column(name="email_bcc")
	private String bccIds;
	private String subject;
	@Column(name="email_from")
	private String fromId;
	@Column(columnDefinition = "TEXT")
	private String content;
	@Column(name = "is_send")
	private Boolean isSend = Boolean.TRUE;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="send_date")
	private Date sendDate;
}
