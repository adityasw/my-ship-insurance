package com.apps.asuransikapalku.api.model.entity.ship;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceKind;

import lombok.Data;

@Entity
@Data
@Table(name = "mst_insurance_ongoing")
public class InsuranceOnGoing {
	@Id
	@SequenceGenerator(name = "mst_insurance_ongoing_seq", sequenceName = "mst_insurance_ongoing_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_insurance_ongoing_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_ship_master", referencedColumnName = "id")
	private ShipMaster shipMaster;
	@Column(name = "insurance_name")
	private String insuranceName;
	@ManyToOne
    @JoinColumn(name = "id_insurance_kind",referencedColumnName = "id")
	private InsuranceKind insuranceKind;
	private String periode;
}
