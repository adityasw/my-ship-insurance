package com.apps.asuransikapalku.api.model.dto.view;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ViewClaimFinanceChatDto {
	@JsonProperty("id-claim-finance")
	private Long idClaimFinance;
	@JsonProperty("supplier-name")
	private String supplierName;
	private String sender;
	private String content;
	@JsonProperty("total-unread")
	private Integer totalUnread;
}
