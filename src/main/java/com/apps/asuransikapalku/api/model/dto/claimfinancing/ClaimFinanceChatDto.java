package com.apps.asuransikapalku.api.model.dto.claimfinancing;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ClaimFinanceChatDto {
	private Long id;
	@JsonProperty("id-claim-finance")
	private Long idClaimFinance;
	private String receiver;
	private String content;
	private String sender;
	@JsonProperty("send-time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy' 'HH:mm:ss", timezone = "Asia/Jakarta")
	private Date sendTime;
	@JsonProperty("sendder-name")
	private String senderName;
	@JsonProperty("receiver-name")
	private String receiverName;
}
