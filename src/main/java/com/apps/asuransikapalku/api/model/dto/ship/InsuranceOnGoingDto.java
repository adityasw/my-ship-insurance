package com.apps.asuransikapalku.api.model.dto.ship;

import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceKindDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class InsuranceOnGoingDto {
	private Long id;
	@JsonProperty("insurance-name")
	private String insuranceName;
	@JsonProperty("insurance-kind")
	private InsuranceKindDto insuranceKindDto;
	private String periode;
}
