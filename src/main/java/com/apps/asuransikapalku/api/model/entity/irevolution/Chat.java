package com.apps.asuransikapalku.api.model.entity.irevolution;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "mst_chat")
public class Chat {
	@Id
	@SequenceGenerator(name = "mst_chat_seq", sequenceName = "mst_chat_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_chat_seq")
	private Long id;
	private String code;
	@Column(name = "id_reference")
	private Long idReference;
	private String sender;
	private String receiver;
	@Column(columnDefinition = "TEXT")
	private String content;
	@Column(name = "send_time")
	private Date sendTime;
	@Column(name = "is_project")
	private Boolean isProject;
	@Column(name = "is_read")
	private Boolean isRead;
	@Column(name = "is_supply")
	private Boolean isSupply;
}
