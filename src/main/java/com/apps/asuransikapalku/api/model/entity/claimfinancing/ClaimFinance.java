package com.apps.asuransikapalku.api.model.entity.claimfinancing;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.apps.asuransikapalku.api.model.Common;
import com.apps.asuransikapalku.api.model.entity.ship.ShipType;
import com.apps.asuransikapalku.api.model.entity.user.Demand;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "claim_finance")
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class ClaimFinance extends Common{
	@Id
	@SequenceGenerator(name = "claim_finance_seq", sequenceName = "claim_finance_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "claim_finance_seq")
	private Long id;
	@ManyToOne
	@JoinColumn(name = "id_demand", referencedColumnName = "id")
	private Demand demand;
	@Column(name = "company_name")
	private String companyName;
	@Column(name = "vessel_name")
	private String vesselName;
	@ManyToOne
	@JoinColumn(name = "id_vessel_type", referencedColumnName = "id")
	private ShipType vesselType;
	@Column(name = "is_other_vessel_type")
	private Boolean isOtherVesselType;
	@Column(name = "vessel_type_other")
	private String vesselTypeOther;
	@Column(name = "casuality")
	private String casuality;
	@Column(name = "date_of_loss")
	@Temporal(TemporalType.DATE)
	private Date dateOfLoss;
	@Column(name = "location_of_loss")
	private String locationOfLoss;
	private String status;
	
	@Column(name = "master_name")
	private String masterName;
	@Column(name = "master_phone")
	private String masterPhone;
	@Column(name = "master_email")
	private String masterEmail;
	
	@Column(name = "owner_pic_name")
	private String ownerPicName;
	@Column(name = "owner_pic_phone")
	private String ownerPicPhone;
	@Column(name = "owner_pic_email")
	private String ownerPicEmail;
	
	@Column(name = "vessel_current_condition", columnDefinition = "TEXT")
	private String vesselCurrentCondition;
	@Column(name = "owner_next_course_of_action", columnDefinition = "TEXT")
	private String ownerNextCourseOfAction;
	
	@OneToMany(mappedBy = "claimFinance", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<ClaimFinanceSoa> claimFinanceSoas;
	@OneToMany(mappedBy = "claimFinance", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<ClaimFinanceActiveInsuranceDetail> claimFinanceActiveInsuranceDetails;
	@OneToMany(mappedBy = "claimFinance", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<ClaimFinanceDocument> claimFinanceDocuments;
	@OneToMany(mappedBy = "claimFinance", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<ClaimFinanceTracking> claimFinanceTrackings;
	@OneToMany(mappedBy = "claimFinance", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<ClaimFinanceLtv> claimFinanceLtvs;
	@OneToMany(mappedBy = "claimFinance", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<ClaimFinanceOffering> claimFinanceOfferings;
	@OneToMany(mappedBy = "claimFinance", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<ClaimFinanceComission> claimFinanceComissions;
	@OneToMany(mappedBy = "claimFinance", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
	private List<ClaimFinanceMapFinancer> claimFinanceMapFinancers;
}
