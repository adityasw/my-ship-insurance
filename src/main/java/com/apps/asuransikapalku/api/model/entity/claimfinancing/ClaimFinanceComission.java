package com.apps.asuransikapalku.api.model.entity.claimfinancing;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
@Table(name = "claim_finance_comission")
public class ClaimFinanceComission {
	@Id
	@SequenceGenerator(name = "claim_finance_comission_seq", sequenceName = "claim_finance_comission_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "claim_finance_comission_seq")
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name= "id_claim_finance", referencedColumnName = "id")
	private ClaimFinance claimFinance;
	private String username;
	@Column(name = "detail_comission")
	private String detailComission;
	private String amount;
	@Temporal(TemporalType.DATE)
	@Column(name = "due_date")
	private Date dueDate;
	private String filename;
	private String file;
	@Column(name = "payment_status")
	private String paymentStatus;
	private String reminder;
}
