package com.apps.asuransikapalku.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.ViewChatDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.ChatDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.ChatMarineConnectDto;
import com.apps.asuransikapalku.api.service.ChatService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.CHAT)
@CrossOrigin(origins = "*")
public class ChatController extends BaseController{
	private ChatService chatService;
	
	@Autowired
	public void service(ChatService chatService) {
		this.chatService = chatService;
	}
	
	@PostMapping
	public APIResponse<ChatDto> create(@RequestBody ChatDto chatDto) {
		return chatService.save(chatDto, getUser());
	}
	
	@PostMapping("/marine-connect")
	public APIResponse<String> create(@RequestBody ChatMarineConnectDto chatMarineConnectDto) {
		return chatService.save(chatMarineConnectDto, getUser());
	}
	
	@GetMapping(value = "/{idReference}/{isProject}/{isSupply}")
	public APIResponse<List<ViewChatDto>> findChat(@PathVariable Long idReference,
			@PathVariable boolean isProject, @PathVariable boolean isSupply) {
		return chatService.findChatList(idReference, isProject, isSupply);
	} 
	
	@GetMapping(value = "/by-code/{code}")
	public APIResponse<List<ChatDto>> findChatByCode(@PathVariable String code) {
		return chatService.findChatByCode(code);
	}
	
	@DeleteMapping(value = "/{id}")
	public APIResponse<String> delete(@PathVariable Long id) {
		return chatService.delete(id);
	}
	
	@GetMapping(value = "/by-username/{username}/{isSupply}")
	public APIResponse<List<ChatDto>> findChatByCode(@PathVariable String username, @PathVariable boolean isSupply) {
		return chatService.findBySenderAndIsSupply(username, isSupply);
	}
	
	@GetMapping(value = "/marine-connect/by-username/{sender}/{receiver}")
	public APIResponse<List<ChatMarineConnectDto>> findChatByUsername(@PathVariable String sender, @PathVariable String receiver) {
		return chatService.findBySenderAndReceiver(sender, receiver, getUser());
	}
}
