package com.apps.asuransikapalku.api.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.model.APIResponse;

@RestController
@CrossOrigin(origins = "*")
public class MyErrorController implements ErrorController {

	@RequestMapping("/error")
	public APIResponse<String> handleError(HttpServletRequest request) {
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

		if (status != null) {
			Integer statusCode = Integer.valueOf(status.toString());

			if (statusCode == HttpStatus.BAD_REQUEST.value()) {
				return new APIResponse<>(HttpStatus.BAD_REQUEST.value(), "Bad Request");
			}
			if (statusCode == HttpStatus.UNAUTHORIZED.value() || statusCode == HttpStatus.FORBIDDEN.value()) {
				return new APIResponse<>(HttpStatus.UNAUTHORIZED.value(), "Unauthorized");
			}
			if (statusCode == HttpStatus.NOT_FOUND.value()) {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Not found");
			} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
				return new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal server error");
			}
		}
		return new APIResponse<>(HttpStatus.BAD_REQUEST.value(), "Bad Request");
	}

	@Override
	public String getErrorPath() {
		return null;
	}

}
