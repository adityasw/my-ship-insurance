package com.apps.asuransikapalku.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.model.APIResponse;

@RestController
@RequestMapping(value = "/")
@CrossOrigin(origins = "*")
public class HelloController {
	@GetMapping
	public APIResponse<String> hello() {
		return new APIResponse<>(HttpStatus.OK.value(), "Api Asuransi Kapalku");
	}
}
