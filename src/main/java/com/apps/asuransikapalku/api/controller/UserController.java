package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.AprrovalDto;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.user.AdminRegisterDto;
import com.apps.asuransikapalku.api.model.dto.user.ForgotPasswordDto;
import com.apps.asuransikapalku.api.model.dto.user.InvitationZoomDto;
import com.apps.asuransikapalku.api.model.dto.user.LoginDto;
import com.apps.asuransikapalku.api.model.dto.user.MemberTemporaryDto;
import com.apps.asuransikapalku.api.model.dto.user.RegisterDto;
import com.apps.asuransikapalku.api.model.dto.user.ResetPasswordDto;
import com.apps.asuransikapalku.api.model.dto.user.ResetPasswordManualDto;
import com.apps.asuransikapalku.api.service.AdminService;
import com.apps.asuransikapalku.api.service.DemandService;
import com.apps.asuransikapalku.api.service.MemberTemporaryService;
import com.apps.asuransikapalku.api.service.SupplierService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.USER)
@CrossOrigin(origins = "*")
public class UserController extends BaseController{
	
	private AdminService adminService;
	
	private DemandService demandService;
	
	private MemberTemporaryService memberTemporaryService;
	
	private SupplierService supplierService;
	
	@Autowired
	public void service(AdminService adminService, DemandService demandService, MemberTemporaryService memberTemporaryService, SupplierService supplierService) {
		this.adminService = adminService;
		this.demandService = demandService;
		this.memberTemporaryService = memberTemporaryService;
		this.supplierService = supplierService;
	}
	
	@PostMapping(value = "/admin-login")
	public APIResponse<Map<String, Object>> loginAdmin(@RequestBody LoginDto loginDto) {
		return adminService.loginAdmin(loginDto);
	}
	
	@PostMapping(value = "/admin-register")
	public APIResponse<String> register(@RequestBody AdminRegisterDto adminRegisterDto) {
		return adminService.registerAdmin(adminRegisterDto, "SYSTEM");
	}
	
	@PostMapping(value = "/client-login")
	public APIResponse<Map<String, Object>> loginClient(@RequestBody LoginDto loginDto) {
		APIResponse<Map<String, Object>> respon = demandService.loginDemand(loginDto);
		if(respon.getStatus() != HttpStatus.OK.value()) {
			return supplierService.loginSupplier(loginDto);
		}
		return respon;
	}
	
	@PostMapping(value = "/client-register")
	public APIResponse<String> register(@RequestBody RegisterDto registerDto) {
		return memberTemporaryService.register(registerDto, "SYSTEM");
	}
	
	@DeleteMapping(value = "/delete-incoming/{id}")
	public APIResponse<String> deleteIncoming(@PathVariable Long id) {
		return memberTemporaryService.delete(id);
	}
	
	@PostMapping(value = "/send-zoom-link")
	public APIResponse<String> sendZoomLink(@RequestBody InvitationZoomDto invitationZoomDto) {
		return memberTemporaryService.sendLinkMeeting(invitationZoomDto, getUser().getUsername());
	}
	
	@PostMapping(value="/reset-password")
	public APIResponse<String> resetPassword(@RequestBody ResetPasswordDto resetPasswordDto) {
		return memberTemporaryService.resetPassword(resetPasswordDto, getUser());
	}
	
	@PutMapping(value="/forgot-password")
	public APIResponse<String> forgotPassword(@RequestBody ForgotPasswordDto forgotPasswordDto) {
		return memberTemporaryService.forgotPassword(forgotPasswordDto.getEmail());
	}
	
	@GetMapping(value="/get-new-password/{email}")
	public APIResponse<String> getNewPassword(@PathVariable String email) {
		return memberTemporaryService.getNewPassword(email);
	}
	
	@GetMapping(value = "/find-status")
	public APIResponse<List<Map<String, Object>>> findAllUserStatus() {
		return memberTemporaryService.findStatus();
	}
	
	@PutMapping(value = "/approval-client")
	public APIResponse<String> approval(@RequestBody AprrovalDto aprrovalDto) {
		return memberTemporaryService.updateStatus(aprrovalDto, getUser().getUsername());
	}
	
	@PostMapping(value = "/incoming-user")
	public APIResponse<Map<String, Object>> incomingUser(@RequestBody FilterDto filterDto) {
		return memberTemporaryService.findAll(filterDto);
	}
	
	@GetMapping(value = "/incoming-user-dtl/{id}")
	public APIResponse<MemberTemporaryDto> findById(@PathVariable Long id) {
		return memberTemporaryService.findById(id);
	}
	
	@PostMapping(value="/reset-password-manual")
	public APIResponse<String> resetPasswordManual(@RequestBody ResetPasswordManualDto resetPasswordManualDto) {
		return memberTemporaryService.resetPasswordManual(resetPasswordManualDto);
	}
}
