package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.MarineConnectWishListDto;
import com.apps.asuransikapalku.api.model.dto.UpdateStatusDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectInquiryDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectInvoiceDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectQuotationDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectUploadTcDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.UpdatePriceDto;
import com.apps.asuransikapalku.api.service.MarineConnectService;
import com.apps.asuransikapalku.api.service.SupplierService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.MARINE_CONNECT)
@CrossOrigin(origins = "*")
public class MarineConnectController extends BaseController {
	
	private MarineConnectService marineConnectService;
	
	private SupplierService supplierService;
	
	@Autowired
	public void service(MarineConnectService marineConnectService, SupplierService supplierService) {
		this.marineConnectService = marineConnectService;
		this.supplierService = supplierService;
	}
	
	@PostMapping(value = "/find-supplier")
	public APIResponse<Map<String, Object>> findSupplier(@RequestBody FilterDto filterDto) {
		return supplierService.findAllSupplierMarineConnect(filterDto, getUser());
	}
	
	@GetMapping(value = "/find-supplier/detail/{idSupplier}")
	public APIResponse<Map<String, Object>> findSupplierDetail(@PathVariable Long idSupplier) {
		return supplierService.findByIdForMarineConnect(idSupplier, getUser().getUsername());
	}
	
	@PostMapping(value = "/project-on-progress")
	public APIResponse<Map<String, Object>> findProjectOnProgress(@RequestBody FilterDto filterDto) {
		return marineConnectService.findAllMarineConnect(filterDto, getUser());
	}
	
	@GetMapping(value = "/project-on-progress/detail/{idMarineConnect}")
	public APIResponse<Map<String, Object>> findProjectOnProgressDetail(@PathVariable Long idMarineConnect) {
		return marineConnectService.findMarineConnect(idMarineConnect, getUser());
	}
	
	@PostMapping(value = "/project-history")
	public APIResponse<Map<String, Object>> findProjectHistory(@RequestBody FilterDto filterDto) {
		return marineConnectService.findAllMarineConnectHistory(filterDto, getUser());
	}
	
	@GetMapping(value = "/project-history/detail/{idMarineConnect}")
	public APIResponse<Map<String, Object>> findProjectHistoryDetail(@PathVariable Long idMarineConnect) {
		return marineConnectService.findMarineConnect(idMarineConnect, getUser());
	}
	
	@PostMapping(value = "/inquiry")
	public APIResponse<String> inquiry(@RequestBody MarineConnectInquiryDto marineConnectInquiryDto) {
		return marineConnectService.createInquiry(marineConnectInquiryDto, getUser());
	}
	
	@PostMapping(value = "/find-all-inquiry")
	public APIResponse<Map<String, Object>> findMarineConnectInquiry(@RequestBody FilterDto filterDto) {
		return marineConnectService.findAllMarineConnectInquiry(filterDto);
	}
	
	@GetMapping(value = "/find-all-inquiry/detail/{idMarineConnect}")
	public APIResponse<Map<String, Object>> findMarineConnectInquiryDetail(@PathVariable Long idMarineConnect) {
		return marineConnectService.findMarineConnectById(idMarineConnect);
	}
	
	@PutMapping(value = "/update-quoted")
	public APIResponse<String> updateQuoted(@RequestBody MarineConnectQuotationDto marineConnectQuotationDto) {
		return marineConnectService.updateQuoted(marineConnectQuotationDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update-wish-list")
	public APIResponse<String> updateWishList(@RequestBody MarineConnectWishListDto marineConnectWishListDto) {
		return marineConnectService.updateWishList(marineConnectWishListDto, getUser());
	}
	
	@PutMapping(value = "/update-nego-price")
	public APIResponse<String> updateNegoPrice(@RequestBody UpdatePriceDto updatePriceDto) {
		return marineConnectService.updatePriceQuoted(updatePriceDto, false);
	}
	
	@PutMapping(value = "/update-final-price")
	public APIResponse<String> updateFinalPrice(@RequestBody UpdatePriceDto updatePriceDto) {
		return marineConnectService.updatePriceQuoted(updatePriceDto, true);
	}
	
	@PutMapping(value = "/update-status")
	public APIResponse<String> updateStatus(@RequestBody UpdateStatusDto updateStatusDto) {
		return marineConnectService.updateStatus(updateStatusDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/upload-invoice")
	public APIResponse<String> uploadInvoice(@RequestBody MarineConnectInvoiceDto marineConnectInvoiceDto) {
		return marineConnectService.uploadInvoice(marineConnectInvoiceDto);
	}
	
	@PutMapping(value = "/upload-tc")
	public APIResponse<String> updateTc(@RequestBody MarineConnectUploadTcDto marineConnectUploadTcDto) {
		return marineConnectService.uploadTC(marineConnectUploadTcDto, getUser());
	}
	
	@GetMapping(value = "/find-status")
	public APIResponse<List<Map<String, Object>>> findAllStatus() {
		return marineConnectService.findStatus();
	}
}
