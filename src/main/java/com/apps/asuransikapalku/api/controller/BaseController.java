package com.apps.asuransikapalku.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.service.UserService;

@Controller
public class BaseController {
	private UserService userService;
	
	@Autowired
	public void service(UserService userService) {
		this.userService = userService;
	}
	
	public User getUser() {
		User user = null;
		if (null != SecurityContextHolder.getContext()) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (null != auth) {
				UserDetails userDetail = (UserDetails) auth.getPrincipal();
				user = userService.findByUsername(userDetail.getUsername());
			}
	    }
		return user;
    }
}
