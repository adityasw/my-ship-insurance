package com.apps.asuransikapalku.api.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.user.AdminDto;
import com.apps.asuransikapalku.api.service.AdminService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.ADMIN)
@CrossOrigin(origins = "*")
public class AdminController extends BaseController {

	private AdminService adminService;

	@Autowired
	public void service(AdminService adminService) {
		this.adminService = adminService;
	}

	@PutMapping
	public APIResponse<String> update(@RequestBody AdminDto adminDto) {
		return adminService.save(adminDto, getUser().getUsername());
	}

	@GetMapping(value = "/by-id/{id}")
	public APIResponse<AdminDto> findAdminById(@PathVariable Long id) {
		return adminService.findById(id);
	}

	@GetMapping(value = "/by-email/{email}")
	public APIResponse<AdminDto> findAdminById(@PathVariable String email) {
		return adminService.findByEmail(email);
	}

	@PostMapping(value = "find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return adminService.findAllAdmin(filterDto);
	}
}
