package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceMasterDto;
import com.apps.asuransikapalku.api.service.InsuranceMasterService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.INSURANCE_MASTER)
@CrossOrigin(origins = "*")
public class InsuranceMasterController extends BaseController {
	private InsuranceMasterService insuranceMasterService;
	
	@Autowired
	public void service(InsuranceMasterService insuranceMasterService) {
		this.insuranceMasterService = insuranceMasterService;
	}
	
	@PostMapping
	public APIResponse<String> create(@RequestBody InsuranceMasterDto insuranceMasterDto) {
		return insuranceMasterService.save(insuranceMasterDto, getUser().getUsername());
	}
	
	@PutMapping
	public APIResponse<String> update(@RequestBody InsuranceMasterDto insuranceMasterDto) {
		return insuranceMasterService.save(insuranceMasterDto, getUser().getUsername());
	}
	
	@DeleteMapping(value = "/hard-delete/{id}")
	public APIResponse<String> hardDelete(@PathVariable Long id) {
		return insuranceMasterService.delete(id, getUser().getUsername());
	}
	
	@GetMapping(value = "/by-id/{id}")
	public APIResponse<InsuranceMasterDto> findById(@PathVariable Long id) {
		return insuranceMasterService.findById(id);
	}
	
	@GetMapping(value = "/find-all-without-paging")
	public APIResponse<List<InsuranceMasterDto>> findAllWithoutPaging() {
		return insuranceMasterService.findAllWithoutPaging();
	}
	
	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return insuranceMasterService.findAll(filterDto);
	}
}
