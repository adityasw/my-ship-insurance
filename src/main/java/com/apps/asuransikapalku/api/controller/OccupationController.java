package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.UpdateStatusDto;
import com.apps.asuransikapalku.api.model.dto.occupation.OccupationDto;
import com.apps.asuransikapalku.api.service.OccupationService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.OCCUPATION)
@CrossOrigin(origins = "*")
public class OccupationController extends BaseController{
	private OccupationService occupationService;
	
	@Autowired
	public void service(OccupationService occupationService) {
		this.occupationService = occupationService;
	}
	
	@PostMapping
	public APIResponse<String> create(@RequestBody OccupationDto occupationDto) {
		return occupationService.save(occupationDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update")
	public APIResponse<String> update(@RequestBody OccupationDto occupationDto) {
		return occupationService.save(occupationDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update-status")
	public APIResponse<String> update(@RequestBody UpdateStatusDto updateStatusDto) {
		return occupationService.updateStatus(updateStatusDto.getId(), updateStatusDto.getStatus(), getUser().getUsername());
	}
	
	@DeleteMapping(value = "/hard-delete/{id}")
	public APIResponse<String> hardDelete(@PathVariable Long id) {
		return occupationService.delete(id, getUser().getUsername());
	}
	
	@DeleteMapping(value = "/soft-delete/{id}")
	public APIResponse<String> softDelete(@PathVariable Long id) {
		return occupationService.softDelete(id, getUser().getUsername());
	}
	
	@GetMapping(value = "/by-id/{id}")
	public APIResponse<OccupationDto> findById(@PathVariable Long id) {
		return occupationService.findById(id);
	}
	
	@GetMapping(value = "/occupation-type-id/{id}")
	public APIResponse<List<OccupationDto>> findByOccupatioanTypeId(@PathVariable Long id) {
		return occupationService.findByOccupationTypeId(id);
	}
	
	@GetMapping(value = "/find-all-without-paging")
	public APIResponse<List<OccupationDto>> findAllWithoutPageableAndActive() {
		return occupationService.findAllWithoutPageableAndActive();
	}
	
	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return occupationService.findAllOccupation(filterDto);
	}
}
