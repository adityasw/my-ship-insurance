package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.ship.DetailVesselDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipApprovalDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipMasterDto;
import com.apps.asuransikapalku.api.service.ShipService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.SHIP)
@CrossOrigin(origins = "*")
public class ShipController extends BaseController{
	private ShipService shipService;
	
	@Autowired
	public void service(ShipService shipService) {
		this.shipService = shipService;
	}
	
	@PostMapping(value = "/approval")
	public APIResponse<String> approval(@RequestBody ShipApprovalDto shipApprovalDto) {
		return shipService.approvalShip(shipApprovalDto, getUser().getUsername());
	}
	
	@PostMapping(value = "/update")
	public APIResponse<String> updateDetailShip(@RequestBody DetailVesselDto detailVesselDto) {
		return shipService.updateDetailShip(detailVesselDto, getUser().getUsername());
	}
	
	@PostMapping
	public APIResponse<String> create(@RequestBody ShipMasterDto shipMasterDto) {
		return shipService.save(shipMasterDto, getUser().getUsername());
	}
	
	@PutMapping
	public APIResponse<String> update(@RequestBody ShipMasterDto shipMasterDto) {
		return shipService.save(shipMasterDto, getUser().getUsername());
	}
	
	@DeleteMapping(value = "/hard-delete/{id}")
	public APIResponse<String> hardDelete(@PathVariable Long id) {
		return shipService.delete(id, getUser().getUsername());
	}
	
	@GetMapping(value = "/by-id/{id}")
	public APIResponse<ShipMasterDto> findById(@PathVariable Long id) {
		return shipService.findById(id);
	}
	
	@GetMapping(value = "/find-status")
	public APIResponse<List<Map<String, Object>>> findAllVesselStatus() {
		return shipService.findShipStatus();
	}
	
	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return shipService.findAll(filterDto);
	}
}
