package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDetailDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewChatMarineConnectSupplierListDto;
import com.apps.asuransikapalku.api.service.ChatService;
import com.apps.asuransikapalku.api.service.ShipService;
import com.apps.asuransikapalku.api.service.SupplierService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.SUPPLIER)
@CrossOrigin(origins = "*")
public class SupplierController extends BaseController {
	private ShipService shipService;
	private SupplierService supplierService;
	private ChatService charService;

	@Autowired
	public void service(ChatService charService, ShipService shipService, SupplierService supplierService) {
		this.charService = charService;
		this.shipService = shipService;
		this.supplierService = supplierService;
	}

	@PutMapping(value = "/update")
	public APIResponse<String> update(@RequestBody SupplierDto supplierDto) {
		return supplierService.save(supplierDto, getUser().getUsername());
	}

	@PutMapping(value = "/update-my-page")
	public APIResponse<String> updateMyPage(@RequestBody SupplierDetailDto supplierDetailDto) {
		return supplierService.updateMyPage(supplierDetailDto, getUser().getUsername());
	}

	@GetMapping(value = "/by-id/{id}")
	public APIResponse<SupplierDto> findAdminById(@PathVariable Long id) {
		return supplierService.findById(id);
	}

	@GetMapping(value = "/by-email/{email}")
	public APIResponse<SupplierDto> findAdminById(@PathVariable String email) {
		return supplierService.findByEmail(email);
	}

	@GetMapping(value = "/find-without-paging")
	public APIResponse<List<SupplierDto>> findWithoutPaging() {
		return supplierService.findWithoutPaging();
	}
	
	@GetMapping(value = "/find-without-paging/for-claim-finance")
	public APIResponse<List<SupplierDto>> findWithoutPagingForClaimFinance() {
		return supplierService.findWithoutPaging("JOB-7");
	}

	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return supplierService.findAll(filterDto);
	}

	@PostMapping(value = "/insurance-cell")
	public APIResponse<Map<String, Object>> insuranceCell(@RequestBody FilterDto filterDto) {
		return supplierService.findAllShip(filterDto);
	}

	@GetMapping(value = "/insurance-cell/detail/{id}/{idSupply}")
	public APIResponse<Map<String, Object>> insuranceCellDetail(@PathVariable Long id, @PathVariable Long idSupply) {
		return shipService.findDetailShipFromSupply(id, idSupply);
	}

	@PostMapping(value = "/insurance-inprogress")
	public APIResponse<Map<String, Object>> insuranceInprogress(@RequestBody FilterDto filterDto) {
		return supplierService.findAllShipInprogress(filterDto);
	}

	@GetMapping(value = "/insurance-inprogress/detail/{id}/{idSupply}")
	public APIResponse<Map<String, Object>> insuranceInprogressDetail(@PathVariable Long id,
			@PathVariable Long idSupply) {
		return shipService.findDetailShipFromSupply(id, idSupply);
	}

	@PostMapping(value = "/my-insurance-client")
	public APIResponse<Map<String, Object>> myInsuranceClient(@RequestBody FilterDto filterDto) {
		return supplierService.findAllShipBind(filterDto);
	}

	@GetMapping(value = "/my-insurance-client/detail/{id}/{idSupply}")
	public APIResponse<Map<String, Object>> myInsuranceClientDetail(@PathVariable Long id,
			@PathVariable Long idSupply) {
		return shipService.findDetailShipFromSupply(id, idSupply);
	}

	@PostMapping(value = "/my-insurance-client-rej")
	public APIResponse<Map<String, Object>> myInsuranceClientRej(@RequestBody FilterDto filterDto) {
		return supplierService.findAllShipRejected(filterDto);
	}

	@GetMapping(value = "/my-insurance-client-rej/detail/{id}/{idSupply}")
	public APIResponse<Map<String, Object>> myInsuranceClientRejDetail(@PathVariable Long id,
			@PathVariable Long idSupply) {
		return shipService.findDetailShipFromSupply(id, idSupply);
	}

	@GetMapping(value = "/get-repassword/{password}")
	public String getRepassword(@PathVariable String password) {
		return supplierService.getEncodePassword(password);
	}
	
	@GetMapping(value = "/my-chat")
	public APIResponse<List<ViewChatMarineConnectSupplierListDto>> findMyChat() {
		return charService.findChatList(getUser());
	}
}
