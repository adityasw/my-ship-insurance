package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.UpdateStatusDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ApprovedRejectLtvDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceAccountDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceBankDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceChatDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceComissionDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceDocumentDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceInvoiceDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceLtvDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceMapFinancerDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceOfferingDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceQuotationDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceTrackingDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinancingDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.UpdateStatusOfferingDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewClaimFinanceChatDto;
import com.apps.asuransikapalku.api.service.ClaimFinanceService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.CLAIM_FINANCE)
@CrossOrigin(origins = "*")
public class ClaimFinanceController extends BaseController{
	private ClaimFinanceService claimFinanceService;
	
	@Autowired
	public void service(ClaimFinanceService claimFinanceService) {
		this.claimFinanceService = claimFinanceService;
	}
	
	@PostMapping
	public APIResponse<String> create(@RequestBody ClaimFinancingDto claimFinancingDto) {
		return this.claimFinanceService.save(claimFinancingDto, getUser().getUsername());
	}
	
	@PutMapping
	public APIResponse<String> update(@RequestBody ClaimFinancingDto claimFinancingDto) {
		return this.claimFinanceService.save(claimFinancingDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update-status")
	public APIResponse<String> updateStatus(@RequestBody UpdateStatusDto updateStatusDto) {
		return this.claimFinanceService.updateStatus(updateStatusDto, getUser());
	}
	
	@PutMapping(value = "/update-status-offering")
	public APIResponse<String> updateStatusOffering(@RequestBody UpdateStatusOfferingDto updateStatusOfferingDto) {
		return this.claimFinanceService.offeringResult(updateStatusOfferingDto);
	}
	
	@PutMapping(value = "/reject-ltv")
	public APIResponse<String> rejectLtv(@RequestBody ApprovedRejectLtvDto approvedRejectLtvDto) {
		return this.claimFinanceService.approvedRejectLtv(approvedRejectLtvDto);
	}
	
	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return this.claimFinanceService.findAllByFilter(filterDto);
	}
	
	@PostMapping(value = "/find-all-for-financer")
	public APIResponse<Map<String, Object>> findAllForFinancer(@RequestBody FilterDto filterDto) {
		return this.claimFinanceService.findAllByFilterForSup(filterDto);
	}
	
	@GetMapping(value = "/detail/{idClaimFinance}")
	public APIResponse<ClaimFinancingDto> findDetailByIdClaimFinance(@PathVariable Long idClaimFinance) {
		return this.claimFinanceService.findById(idClaimFinance);
	}
	
	@PostMapping(value = "/list-document")
	public APIResponse<Map<String, Object>> findClaimFinanceDocument(@RequestBody FilterDto filterDto) {
		return this.claimFinanceService.findClaimFinanceDocument(filterDto);
	}
	
	@PostMapping(value = "/quotation")
	public APIResponse<Map<String, Object>> findClaimFinanceQuotation(@RequestBody FilterDto filterDto) {
		return this.claimFinanceService.findClaimFinanceQuotation(filterDto);
	}
	
	@PostMapping(value = "/financing-offer")
	public APIResponse<Map<String, Object>> findClaimFinanceOffering(@RequestBody FilterDto filterDto) {
		return this.claimFinanceService.findClaimFinanceOffering(filterDto);
	} 
	
	@PostMapping(value = "/financing-offer/add-offering")
	public APIResponse<String> findClaimFinanceOffering(@RequestBody ClaimFinanceOfferingDto claimFinanceOfferingDto) {
		return this.claimFinanceService.givingOffering(claimFinanceOfferingDto);
	}
	
	@PostMapping(value = "/financing-track")
	public APIResponse<Map<String, Object>> findClaimFinanceTracking(@RequestBody FilterDto filterDto) {
		return this.claimFinanceService.findClaimFinanceTracking(filterDto);
	} 
	
	@PostMapping(value = "/financing-track/add-track")
	public APIResponse<String> createTrack(@RequestBody ClaimFinanceTrackingDto claimFinanceTrackingDto) {
		return this.claimFinanceService.createTrack(claimFinanceTrackingDto);
	} 
	
	@PostMapping(value = "/financing-track/add-ltv")
	public APIResponse<String> createLtv(@RequestBody ClaimFinanceLtvDto claimFinanceLtvDto) {
		return this.claimFinanceService.createLtv(claimFinanceLtvDto);
	}
	
	@PostMapping(value = "/invoice")
	public APIResponse<Map<String, Object>> findClaimFinanceInvoice(@RequestBody FilterDto filterDto) {
		return this.claimFinanceService.findClaimFinanceInvoice(filterDto);
	}
	
	@PostMapping(value = "/invoice/bank")
	public APIResponse<Map<String, Object>> findClaimFinanceInvoiceBank(@RequestBody FilterDto filterDto) {
		return this.claimFinanceService.findClaimFinanceInvoiceBank(filterDto);
	}
	
	@PostMapping(value = "/invoice/add-invoice")
	public APIResponse<String> addInvoice(@RequestBody ClaimFinanceInvoiceDto claimFinanceInvoiceDto) {
		return this.claimFinanceService.createInvoice(claimFinanceInvoiceDto);
	}
	
	@PostMapping(value = "/invoice/bank/add-invoice-bank")
	public APIResponse<String> addInvoiceBank(@RequestBody ClaimFinanceBankDto claimFinanceBankDto) {
		return this.claimFinanceService.createBank(claimFinanceBankDto);
	}
	
	@PostMapping(value = "/comission")
	public APIResponse<Map<String, Object>> findClaimFinanceComission(@RequestBody FilterDto filterDto) {
		return this.claimFinanceService.findClaimFinanceComission(filterDto);
	}
	
	@PostMapping(value = "/comission/add-comission")
	public APIResponse<String> createComission(@RequestBody ClaimFinanceComissionDto claimFinanceComissionDto) {
		return this.claimFinanceService.createComission(claimFinanceComissionDto);
	}
	
	@PostMapping(value = "/list-document/add-document")
	public APIResponse<String> addDocument(@RequestBody ClaimFinanceDocumentDto claimFinanceDocumentDto) {
		return this.claimFinanceService.uploadTC(claimFinanceDocumentDto, getUser());
	}
	
	@PostMapping(value = "/quotation/add-quotation")
	public APIResponse<String> addQuotation(@RequestBody ClaimFinanceQuotationDto claimFinanceQuotationDto) {
		return this.claimFinanceService.uploadQuotation(claimFinanceQuotationDto, getUser());
	}
	
	@PostMapping(value = "/assignee-financer")
	public APIResponse<String> assigneeFinancer(@RequestBody ClaimFinanceMapFinancerDto claimFinanceMapFinancerDto) {
		return this.claimFinanceService.assignFinancer(claimFinanceMapFinancerDto);
	}
	
	@GetMapping(value = "/assignee-financer/find-all/{idClaimFinance}") 
	public APIResponse<ClaimFinanceMapFinancerDto> findAssignee(@PathVariable Long idClaimFinance) {
		return this.claimFinanceService.findClaimFinanceAssignee(idClaimFinance);
	}
	
	@GetMapping(value = "/account/{idClaimFinance}") 
	public APIResponse<ClaimFinanceAccountDto> findTabAccount(@PathVariable Long idClaimFinance) {
		return this.claimFinanceService.getSupplierOfferingDetail(idClaimFinance);
	}
	
	@PostMapping(value = "/account/give-offering-detail")
	public APIResponse<String> addQuotation(@RequestBody ClaimFinanceAccountDto claimFinanceAccountDto) {
		return this.claimFinanceService.supplierOfferingDetail(claimFinanceAccountDto);
	}
	
	@PostMapping(value = "/chat")
	public APIResponse<String> chat(@RequestBody ClaimFinanceChatDto claimFinanceChatDto) {
		return this.claimFinanceService.saveChat(claimFinanceChatDto, getUser());
	}
	
	@GetMapping(value = "/chat/tab-supplier/{idClaimFinance}")
	public APIResponse<List<ViewClaimFinanceChatDto>> findChatTabSupplier(@PathVariable Long idClaimFinance) {
		return this.claimFinanceService.findChatTabSupplier(idClaimFinance);
	}
	
	@GetMapping(value = "/chat/detail/{idClaimFinance}/{senderOrReceiver}")
	public APIResponse<List<ClaimFinanceChatDto>> findChatByIdClaimFinanceAndSenderReceiver(@PathVariable Long idClaimFinance, @PathVariable String senderOrReceiver) {
		return this.claimFinanceService.findChatByIdClaimFinanceAndSenderReceiver(idClaimFinance, senderOrReceiver);
	}
	
	@GetMapping(value = "/find-status")
	public APIResponse<List<Map<String, Object>>> findAllStatus() {
		return this.claimFinanceService.findStatus();
	}
}
