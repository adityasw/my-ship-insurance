package com.apps.asuransikapalku.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.AddQuotationDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.AssignSupplierDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BidingDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingProcessDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionClaimDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionDocumentDemandDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.ReplacementDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.UpdateStatusTCDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.UploadTcDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDto;
import com.apps.asuransikapalku.api.service.BindingProcessService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.BINDING_PROCESS)
@CrossOrigin(origins = "*")
public class BindingProcessController extends BaseController{
	
	private BindingProcessService bindingProcessService;
	
	@Autowired
	public void service(BindingProcessService bindingProcessService) {
		this.bindingProcessService = bindingProcessService;
	}
	
	@PostMapping(value = "/req-replacement")
	public APIResponse<String> requestReplacement(@RequestBody ReplacementDto replacementDto) {
		return bindingProcessService.requestReplacement(replacementDto, getUser().getUsername());
	}
	
	@PostMapping(value = "/update-status")
	public APIResponse<String> updateStatus(@RequestBody BindingProcessDto bindingProcessDto) {
		return bindingProcessService.updateStatus(bindingProcessDto, getUser().getUsername());
	}
	
	@PostMapping(value = "/update-assignee")
	public APIResponse<String> updateAssignee(@RequestBody AssignSupplierDto assignSupplierDto) {
		return bindingProcessService.updateAssignee(assignSupplierDto);
	}
	
	@PostMapping(value = "/upload-tc")
	public APIResponse<String> uploadTc(@RequestBody UploadTcDto uploadTcDto) {
		return bindingProcessService.uploadTC1(uploadTcDto, getUser());
	}
	
	@PutMapping(value = "/upload-tc/update-status")
	public APIResponse<String> uploadTc(@RequestBody UpdateStatusTCDto updateStatusTCDto) {
		return bindingProcessService.updateStatusTc(updateStatusTCDto);
	}
	
	@DeleteMapping(value = "/upload-tc/{id}") 
	public APIResponse<String> deleteTc(@PathVariable Long id) {
		return bindingProcessService.deleteTc(id);
	}
	
	@PostMapping(value = "/find-all-tc")
	public APIResponse<Map<String, Object>> findAllDocumentTc(@RequestBody FilterDto filterDto) {
		return bindingProcessService.findAllDocumentTc(filterDto);
	}
	
	@DeleteMapping(value = "/delete-quotation/{id}") 
	public APIResponse<String> addQuotation(@PathVariable Long id) {
		return bindingProcessService.deleteQuotation(id);
	}
	
	@PostMapping(value = "/add-quotation")
	public APIResponse<String> addQuotation(@RequestBody AddQuotationDto addQuotationDto) {
		return bindingProcessService.addQuotation(addQuotationDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/select-quotation")
	public APIResponse<String> selectedQuotation(@RequestBody Map<String, Object> map) {
		return bindingProcessService.selectedQuotation(map);
	}
	
	@PostMapping(value = "/add-doc-demand")
	public APIResponse<String> addDocumentDemand(@RequestBody BindingTransactionDocumentDemandDto bindingTransactionDocumentDemandDto) {
		return bindingProcessService.addDocumentDemand(bindingTransactionDocumentDemandDto, getUser().getUsername());
	}
	
	@PostMapping(value = "/upload-tc-test")
	public APIResponse<String> uploadTc(@RequestParam("binding-id") Long bindingId,
			@RequestParam("file") MultipartFile file,
			@RequestParam("status") String status) {
		Map<String, Object> map = new HashMap<>();
		map.put("bindingId", bindingId);
		map.put("file", file);
		map.put("status", status);
		return bindingProcessService.uploadTC(map, getUser());
	}
	
	@PostMapping(value = "/bid")
	public APIResponse<String> bidProcess(@RequestBody BidingDto bidingDto) {
		return bindingProcessService.bidingShip(bidingDto, getUser().getUsername());
	}
	
	@PostMapping(value = "/add-claim")
	public APIResponse<String> addClaim(@RequestBody BindingTransactionClaimDto bindingTransactionClaimDto) {
		return bindingProcessService.addClaim(bindingTransactionClaimDto);
	}
	
	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAllBindingProcess(@RequestBody FilterDto filterDto) {
		return bindingProcessService.findAllBindingProcess(filterDto);
	}
	
	@GetMapping(value = "/by-id/{id}")
	public APIResponse<Map<String, Object>> findById(@PathVariable Long id) {
		return bindingProcessService.findBindingProcessById(id);
	}
	
	@GetMapping(value = "/find-status")
	public APIResponse<List<Map<String, Object>>> findStatus() {
		return bindingProcessService.findStatus();
	}
	
	@GetMapping(value = "/find-status-type")
	public APIResponse<List<Map<String, Object>>> findStatusType() {
		return bindingProcessService.findStatusType();
	}
	
	@GetMapping(value = "/find-supplier-for-tc/{id}")
	public APIResponse<List<SupplierDto>> getSupplierForTc(@PathVariable Long id) {
		return bindingProcessService.getSupplierForTc(id);
	}
	
	@GetMapping(value = "/find-supplier-for-quotation/{id}")
	public APIResponse<List<SupplierDto>> getSupplierForQuotation(@PathVariable Long id) {
		return bindingProcessService.getSupplierForTc(id);
	}
}
