package com.apps.asuransikapalku.api.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.user.DemandDto;
import com.apps.asuransikapalku.api.service.DemandService;
import com.apps.asuransikapalku.api.service.ShipService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.DEMAND)
@CrossOrigin(origins = "*")
public class DemandController extends BaseController {
	private DemandService demandService;
	private ShipService shipService;
	
	@Autowired
	public void service(DemandService demandService, ShipService shipService) {
		this.demandService = demandService;
		this.shipService = shipService;
	}
	
	@PutMapping(value = "/update")
	public APIResponse<String> update(@RequestBody DemandDto demandDto) {
		return demandService.save(demandDto, getUser().getUsername());
	}
	
	@GetMapping(value = "/by-id/{id}")
	public APIResponse<DemandDto> findAdminById(@PathVariable Long id) {
		return demandService.findById(id);
	}

	@GetMapping(value = "/by-email/{email}")
	public APIResponse<DemandDto> findAdminById(@PathVariable String email) {
		return demandService.findByEmail(email);
	}

	@PostMapping(value = "find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return demandService.findAll(filterDto);
	}
	
	@PostMapping(value = "/insurance-inprogress")
	public APIResponse<Map<String, Object>> insuranceInprogress(@RequestBody FilterDto filterDto) {
		return demandService.findAllShipInprogress(filterDto);
	}
	
	@GetMapping(value = "/insurance-inprogress/detail/{id}")
	public APIResponse<Map<String, Object>> insuranceInprogressDetail(@PathVariable Long id) {
		return shipService.findDetailShipFromDemand(id);
	}
	
	@PostMapping(value = "/my-insurance")
	public APIResponse<Map<String, Object>> myInsurance(@RequestBody FilterDto filterDto) {
		return demandService.findAllShipBind(filterDto);
	}
	
	@GetMapping(value = "/my-insurance/detail/{id}")
	public APIResponse<Map<String, Object>> myInsuranceDetail(@PathVariable Long id) {
		return shipService.findDetailShipFromDemand(id);
	}
}
