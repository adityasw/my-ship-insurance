package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.UpdateStatusDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipTypeDto;
import com.apps.asuransikapalku.api.service.ShipTypeService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.SHIP_TYPE)
@CrossOrigin(origins = "*")
public class ShipTypeController extends BaseController {
	private ShipTypeService shipTypeService;
	
	@Autowired
	public void service( ShipTypeService shipTypeService) {
		this.shipTypeService = shipTypeService;
	}
	
	@PostMapping
	public APIResponse<String> create(@RequestBody ShipTypeDto shipTypeDto) {
		return shipTypeService.save(shipTypeDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update")
	public APIResponse<String> update(@RequestBody ShipTypeDto shipTypeDto) {
		return shipTypeService.save(shipTypeDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update-status")
	public APIResponse<String> update(@RequestBody UpdateStatusDto updateStatusDto) {
		return shipTypeService.updateStatus(updateStatusDto.getId(), updateStatusDto.getStatus(), getUser().getUsername());
	}
	
	@DeleteMapping(value = "/hard-delete/{id}")
	public APIResponse<String> hardDelete(@PathVariable Long id) {
		return shipTypeService.delete(id, getUser().getUsername());
	}
	
	@DeleteMapping(value = "/soft-delete/{id}")
	public APIResponse<String> softDelete(@PathVariable Long id) {
		return shipTypeService.softDelete(id, getUser().getUsername());
	}
	
	@GetMapping(value = "/by-id/{id}")
	public APIResponse<ShipTypeDto> findById(@PathVariable Long id) {
		return shipTypeService.findById(id);
	}
	
	@GetMapping(value = "/by-code/{code}")
	public APIResponse<ShipTypeDto> findById(@PathVariable String code) {
		return shipTypeService.findByCode(code);
	}
	
	@GetMapping(value = "/find-all-without-paging")
	public APIResponse<List<ShipTypeDto>> findAllWithoutPageableAndActive() {
		return shipTypeService.findAllWithoutPageableAndActive();
	}
	
	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return shipTypeService.findAllShipType(filterDto);
	}
}
