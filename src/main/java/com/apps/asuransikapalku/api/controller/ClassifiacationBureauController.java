package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.master.ClassifiacationBureauDto;
import com.apps.asuransikapalku.api.service.ClassifiacationBureauService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.CLASSIFICATION_BUREAU)
@CrossOrigin(origins = "*")
public class ClassifiacationBureauController extends BaseController{
	private ClassifiacationBureauService classifiacationBureauService;
	
	@Autowired
	public void service(ClassifiacationBureauService classifiacationBureauService) {
		this.classifiacationBureauService = classifiacationBureauService;
	}
	
	@PostMapping
	public APIResponse<String> create(@RequestBody ClassifiacationBureauDto classifiacationBureauDto) {
		return classifiacationBureauService.save(classifiacationBureauDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update")
	public APIResponse<String> update(@RequestBody ClassifiacationBureauDto classifiacationBureauDto) {
		return classifiacationBureauService.save(classifiacationBureauDto, getUser().getUsername());
	}
	
	@DeleteMapping(value = "/hard-delete/{id}")
	public APIResponse<String> hardDelete(@PathVariable Long id) {
		return classifiacationBureauService.delete(id, getUser().getUsername());
	}
	
	@GetMapping(value = "/by-id/{id}")
	public APIResponse<ClassifiacationBureauDto> findById(@PathVariable Long id) {
		return classifiacationBureauService.findById(id);
	}
	
	@GetMapping(value = "/find-all-without-paging")
	public APIResponse<List<ClassifiacationBureauDto>> findAllWithoutPageableAndActive() {
		return classifiacationBureauService.findAllWithoutPageable();
	}
	
	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return classifiacationBureauService.findAll(filterDto);
	}
}
