package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.occupation.OccupationTypeDto;
import com.apps.asuransikapalku.api.service.OccupationTypeService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.OCCUPATION_TYPE)
@CrossOrigin(origins = "*")
public class OccupationTypeController extends BaseController {
	private OccupationTypeService occupationTypeService;
	
	@Autowired
	public void service(OccupationTypeService occupationTypeService) {
		this.occupationTypeService = occupationTypeService;
	}
	
	@PostMapping
	public APIResponse<String> create(@RequestBody OccupationTypeDto occupationTypeDto) {
		return occupationTypeService.save(occupationTypeDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update")
	public APIResponse<String> update(@RequestBody OccupationTypeDto occupationTypeDto) {
		return occupationTypeService.save(occupationTypeDto, getUser().getUsername());
	}
	
	@DeleteMapping(value = "/hard-delete/{id}")
	public APIResponse<String> hardDelete(@PathVariable Long id) {
		return occupationTypeService.delete(id);
	}
	
	@DeleteMapping(value = "/soft-delete/{id}")
	public APIResponse<String> softDelete(@PathVariable Long id) {
		return occupationTypeService.softDelete(id, getUser().getUsername());
	}
	
	@GetMapping(value = "/by-id/{id}")
	public APIResponse<OccupationTypeDto> findById(@PathVariable Long id) {
		return occupationTypeService.findById(id);
	}
	
	@GetMapping(value = "/find-all-without-paging")
	public APIResponse<List<OccupationTypeDto>> findAllWithoutPageableAndActive() {
		return occupationTypeService.findAllWithoutPageable();
	}
	
	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return occupationTypeService.findAll(filterDto);
	}
}
