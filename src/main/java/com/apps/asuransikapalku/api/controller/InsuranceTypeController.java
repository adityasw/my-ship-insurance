package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.UpdateStatusDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceTypeDto;
import com.apps.asuransikapalku.api.service.InsuranceTypeService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.INSURANCE_TYPE)
@CrossOrigin(origins = "*")
public class InsuranceTypeController extends BaseController {
	private InsuranceTypeService insuranceTypeService;
	
	@Autowired
	public void service(InsuranceTypeService insuranceTypeService) {
		this.insuranceTypeService = insuranceTypeService;
	}
	
	@PostMapping
	public APIResponse<String> create(@RequestBody InsuranceTypeDto insuranceTypeDto) {
		return insuranceTypeService.save(insuranceTypeDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update")
	public APIResponse<String> update(@RequestBody InsuranceTypeDto insuranceTypeDto) {
		return insuranceTypeService.save(insuranceTypeDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update-status")
	public APIResponse<String> updateStatus(@RequestBody UpdateStatusDto updateStatusDto) {
		return insuranceTypeService.updateStatus(updateStatusDto.getId(), updateStatusDto.getStatus(), getUser().getUsername());
	}
	
	@DeleteMapping(value = "/hard-delete/{id}")
	public APIResponse<String> hardDelete(@PathVariable Long id) {
		return insuranceTypeService.delete(id, getUser().getUsername());
	}
	
	@DeleteMapping(value = "/soft-delete/{id}")
	public APIResponse<String> softDelete(@PathVariable Long id) {
		return insuranceTypeService.softDelete(id, getUser().getUsername());
	}
	
	@GetMapping(value = "/by-id/{id}")
	public APIResponse<InsuranceTypeDto> findById(@PathVariable Long id) {
		return insuranceTypeService.findById(id);
	}
	
	@GetMapping(value = "/find-all-without-paging")
	public APIResponse<List<InsuranceTypeDto>> findAllWithoutPageableAndActive() {
		return insuranceTypeService.findAllWithoutPageableAndActive();
	}
	
	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return insuranceTypeService.findAllInsuranceType(filterDto);
	}
}
