package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.UpdateStatusDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceKindDto;
import com.apps.asuransikapalku.api.service.InsuranceKindService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.INSURANCE_KIND)
@CrossOrigin(origins = "*")
public class InsuranceKindController extends BaseController {
	private InsuranceKindService insuranceKindService;
	
	@Autowired
	public void service(InsuranceKindService insuranceKindService) {
		this.insuranceKindService = insuranceKindService;
	}
	
	@PostMapping
	public APIResponse<String> create(@RequestBody InsuranceKindDto insuranceKindDto) {
		return insuranceKindService.save(insuranceKindDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update")
	public APIResponse<String> update(@RequestBody InsuranceKindDto insuranceKindDto) {
		return insuranceKindService.save(insuranceKindDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update-status")
	public APIResponse<String> updatestatus(@RequestBody UpdateStatusDto updateStatusDto) {
		return insuranceKindService.updateStatus(updateStatusDto.getId(), updateStatusDto.getStatus(), getUser().getUsername());
	}
	
	@DeleteMapping(value = "/hard-delete/{id}")
	public APIResponse<String> hardDelete(@PathVariable Long id) {
		return insuranceKindService.delete(id, getUser().getUsername());
	}
	
	@DeleteMapping(value = "/soft-delete/{id}")
	public APIResponse<String> softDelete(@PathVariable Long id) {
		return insuranceKindService.softDelete(id, getUser().getUsername());
	}
	
	@GetMapping(value = "/by-id/{id}")
	public APIResponse<InsuranceKindDto> findById(@PathVariable Long id) {
		return insuranceKindService.findById(id);
	}
	
	@GetMapping(value = "/by-code/{code}")
	public APIResponse<InsuranceKindDto> findById(@PathVariable String code) {
		return insuranceKindService.findByCode(code);
	}
	
	@GetMapping(value = "/find-by-insurance-type-id/{id}")
	public APIResponse<List<InsuranceKindDto>> findAllWithoutPageableAndActive(@PathVariable Long id) {
		return insuranceKindService.findAllWithoutPageableAndActiveByTypeId(id);
	}
	
	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return insuranceKindService.findAllInsuranceKind(filterDto);
	}
}
