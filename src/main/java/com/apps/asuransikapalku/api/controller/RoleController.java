package com.apps.asuransikapalku.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.UpdateStatusDto;
import com.apps.asuransikapalku.api.model.dto.user.RoleDto;
import com.apps.asuransikapalku.api.service.RoleService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.ROLE)
@CrossOrigin(origins = "*")
public class RoleController extends BaseController{
	private RoleService roleService;
	
	@Autowired
	public void service(RoleService roleService) {
		this.roleService = roleService;
	}
	
	@PostMapping
	public APIResponse<String> create(@RequestBody RoleDto roleDto) {
		return roleService.save(roleDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update")
	public APIResponse<String> update(@RequestBody RoleDto roleDto) {
		return roleService.save(roleDto, getUser().getUsername());
	}
	
	@PutMapping(value = "/update-status")
	public APIResponse<String> update(@RequestBody UpdateStatusDto updateStatusDto) {
		return roleService.updateStatus(updateStatusDto.getId(), updateStatusDto.getStatus(), getUser().getUsername());
	}
	
	@DeleteMapping(value = "/hard-delete/{id}")
	public APIResponse<String> hardDelete(@PathVariable Long id) {
		return roleService.delete(id, getUser().getUsername());
	}
	
	@DeleteMapping(value = "/soft-delete/{id}")
	public APIResponse<String> softDelete(@PathVariable Long id) {
		return roleService.softDelete(id, getUser().getUsername());
	}
	
	@GetMapping(value = "/by-id/{id}")
	public APIResponse<RoleDto> findById(@PathVariable Long id) {
		return roleService.findById(id);
	}
	
	@GetMapping(value = "/find-all-without-paging")
	public APIResponse<List<RoleDto>> findAllWithoutPageableAndActive() {
		return roleService.findAllWithoutPageableAndActive();
	}
	
	@PostMapping(value = "/find-all")
	public APIResponse<Map<String, Object>> findAll(@RequestBody FilterDto filterDto) {
		return roleService.findAllRole(filterDto);
	}
}
