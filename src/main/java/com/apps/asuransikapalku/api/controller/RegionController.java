package com.apps.asuransikapalku.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.master.CityDto;
import com.apps.asuransikapalku.api.model.dto.master.ProvinceDto;
import com.apps.asuransikapalku.api.model.dto.master.SubDistrictDto;
import com.apps.asuransikapalku.api.service.RegionService;

@RestController
@RequestMapping(value = AsuransiKapalkuConstant.ControllerMapping.REGION)
@CrossOrigin(origins = "*")
public class RegionController {
	private RegionService regionService;
	
	@Autowired
	public void service(RegionService regionService) {
		this.regionService = regionService;
	}
	
	@GetMapping(value = "/get-province")
	public APIResponse<List<ProvinceDto>> getProvince() {
		return regionService.findAllProvince();
	}
	
	@GetMapping(value = "/get-city-by-prov-id/{id}")
	public APIResponse<List<CityDto>> getCityByProvinceId(@PathVariable String id) {
		return regionService.findByProvinceId(id);
	}
	
	@GetMapping(value = "/get-subdistrict-by-city-id/{id}")
	public APIResponse<List<SubDistrictDto>> getSubDistrictByCityId(@PathVariable String id) {
		return regionService.findByCityId(id);
	}
}
