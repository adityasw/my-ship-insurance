package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.occupation.OccupationTypeDto;
import com.apps.asuransikapalku.api.model.entity.occupation.OccupationType;
import com.apps.asuransikapalku.api.repository.occupation.OccupationTypeRepository;
import com.apps.asuransikapalku.api.service.OccupationTypeService;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class OccupationTypeServiceImpl implements OccupationTypeService {
	private static final Logger logger = LoggerFactory.getLogger(OccupationTypeServiceImpl.class);

	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";

	private OccupationTypeRepository occupationTypeRepository;

	private ModelMapper modelMapper = new ModelMapper();

	private String user;

	@Autowired
	public void repository(OccupationTypeRepository occupationTypeRepository) {
		this.occupationTypeRepository = occupationTypeRepository;
	}

	@Override
	public APIResponse<String> save(OccupationTypeDto occupationTypeDto, String createdBy) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = createdBy;
			this.validation(occupationTypeDto);
			occupationTypeRepository.save(convertDtoToEntity(occupationTypeDto));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA TIPE PEKERJAAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			occupationTypeRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA TIPE PEKERJAAN [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Tipe pekerjaan dengan id [" + id + "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDelete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			OccupationType occupationType = occupationTypeRepository.findById(id).orElse(null);
			occupationType.setIsActive(Boolean.FALSE);
			occupationType.setUpdatedBy(user);
			occupationType.setUpdatedDate(new Date());
			occupationTypeRepository.save(occupationType);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA TIPE PEKERJAAN [SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Tipe pekerjaan dengan id [" + id + "] gagal terhapus");
		}
		return respon;
	}

	@Override
	public APIResponse<OccupationTypeDto> findById(Long id) {
		APIResponse<OccupationTypeDto> respon = new APIResponse<>();
		try {
			OccupationType occupationType = occupationTypeRepository.findById(id).orElse(null);
			if (occupationType == null) {
				logger.info("TIPE PEKERJAAN TIDAK DITEMUKAN [BY ID] : {}", occupationType);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Tipe pekerjaan tidak ditemukan.");
			}
			respon.setData(modelMapper.map(occupationType, OccupationTypeDto.class));
		} catch (Exception e) {
			logger.error("TIPE PEKERJAAN TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Tipe pekerjaan tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<List<OccupationTypeDto>> findAllWithoutPageable() {
		APIResponse<List<OccupationTypeDto>> respon = new APIResponse<>();
		try {
			List<OccupationType> occupations = occupationTypeRepository.findByIsActive(true);

			respon.setData(occupations.stream().map(ot -> modelMapper.map(ot, OccupationTypeDto.class))
					.collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST TIPE PEKERJAAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "List tipe pekerjaan tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<OccupationType> occupationTypePage = occupationTypeRepository
					.findAll(new Specification<OccupationType>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<OccupationType> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();

							if (filterDto.getIsActive() != null && filterDto.getIsActive()) {
								predicates.add(criteriaBuilder.equal(root.get("isActive"), filterDto.getIsActive()));
							}

							if (null != filterDto.getSearchBy()) {
								Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate description = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("description")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								predicates.add(criteriaBuilder.or(name, description));
							}
							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<OccupationTypeDto> occupationTypes = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != occupationTypePage) {
				occupationTypes = occupationTypePage.getContent().stream()
						.map(ot -> modelMapper.map(ot, OccupationTypeDto.class)).collect(Collectors.toList());
				currentPage = occupationTypePage.getNumber();
				totalElement = occupationTypePage.getTotalElements();
				totalPage = occupationTypePage.getTotalPages();
			}

			dataRes.put("occupation-types", occupationTypes);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST TIPE PEKERJAAN : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List tipe pekerjaan tidak ditemukan.");
		}
		return response;
	}

	private OccupationType convertDtoToEntity(OccupationTypeDto occupationTypeDto) {
		OccupationType occupationType = modelMapper.map(occupationTypeDto, OccupationType.class);

		if (null == occupationTypeDto.getId()) {
			occupationType.setCreatedBy(user);
			occupationType.setCreatedDate(new Date());
		} else {
			OccupationType occupationTypeOld = occupationTypeRepository.findById(occupationTypeDto.getId())
					.orElse(null);
			if (occupationTypeOld != null) {
				occupationType.setCreatedBy(occupationTypeOld.getCreatedBy());
				occupationType.setCreatedDate(occupationTypeOld.getCreatedDate());
			}
			occupationType.setCreatedBy(user);
			occupationType.setCreatedDate(new Date());
		}

		return occupationType;
	}

	private void validation(OccupationTypeDto occupationTypeDto) throws ValidateException {

		if (occupationTypeDto.getName() == null) {
			throw new ValidateException("Nama tipe pekerjaan harus diisi.");
		}

		Integer count = occupationTypeRepository.getCountByName(occupationTypeDto.getName());
		count = count == null ? 0 : count;
		if ((occupationTypeDto.getId() == null && count > 0) || (occupationTypeDto.getId() != null && count > 1)) {
			throw new ValidateException("Nama tipe pekerjaan sudah digunakan.");
		}
	}
}
