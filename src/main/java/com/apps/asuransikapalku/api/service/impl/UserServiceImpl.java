package com.apps.asuransikapalku.api.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.repository.user.UserRepository;
import com.apps.asuransikapalku.api.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	public static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	private UserRepository userRepository;
	
	@Autowired
	public void repository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	public User findByToken(String token) {
		return userRepository.findByToken(token);
	}

	@Override
	public void save(User user) {
		userRepository.save(user);
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
}
