package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.ViewChatDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.ChatDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.ChatMarineConnectDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewChatMarineConnectSupplierListDto;
import com.apps.asuransikapalku.api.model.entity.user.User;

public interface ChatService {
	public APIResponse<ChatDto> save(ChatDto chatDto, User user);
	
	public APIResponse<String> save(ChatMarineConnectDto chatMarineConnectDto, User user);
	
	public APIResponse<String> delete(Long id);
	
	public APIResponse<List<ChatDto>> findAllWithoutPaging(Long idReference, boolean isProject);
	
	public APIResponse<List<ChatDto>> findChatByCode(String code);
	
	public APIResponse<List<ViewChatDto>> findChatList(Long idReference, boolean isProject, boolean supplyTab);
	
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto);

	public APIResponse<List<ChatDto>> findBySenderAndIsSupply(String sender, boolean isSupply);
	
	public APIResponse<List<ChatMarineConnectDto>> findBySenderAndReceiver(String sender, String receiver, User user);
	
	public APIResponse<List<ViewChatMarineConnectSupplierListDto>> findChatList(User user);
}
