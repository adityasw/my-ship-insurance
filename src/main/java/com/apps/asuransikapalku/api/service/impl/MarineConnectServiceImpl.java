package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.enums.StatusMarineConnectEnum;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.MarineConnectWishListDto;
import com.apps.asuransikapalku.api.model.dto.UpdateStatusDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.ChatMarineConnectDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectInquiryDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectInvoiceDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectQuotationDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectUploadTcDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.UpdatePriceDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewMarineConnectDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewMarineConnectHistoryDto;
import com.apps.asuransikapalku.api.model.entity.marineconnect.ChatMarineConnect;
import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectInquiry;
import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectInvoice;
import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectQuotation;
import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectUploadTc;
import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectWishList;
import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.model.entity.view.ViewMarineConnect;
import com.apps.asuransikapalku.api.model.entity.view.ViewMarineConnectHistory;
import com.apps.asuransikapalku.api.repository.marineconnect.ChatMarineConnectRepository;
import com.apps.asuransikapalku.api.repository.marineconnect.MarineConnectInquiryRepository;
import com.apps.asuransikapalku.api.repository.marineconnect.MarineConnectInvoiceRepository;
import com.apps.asuransikapalku.api.repository.marineconnect.MarineConnectQuotationRepository;
import com.apps.asuransikapalku.api.repository.marineconnect.MarineConnectUploadTcRepository;
import com.apps.asuransikapalku.api.repository.marineconnect.MarineConnectWishListRepository;
import com.apps.asuransikapalku.api.repository.user.SupplierRepository;
import com.apps.asuransikapalku.api.repository.view.ViewMarineConnectHistoryRepository;
import com.apps.asuransikapalku.api.repository.view.ViewMarineConnectRepository;
import com.apps.asuransikapalku.api.service.MarineConnectService;
import com.apps.asuransikapalku.api.utils.FileUtil;
import com.apps.asuransikapalku.api.utils.PasswordUtil;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class MarineConnectServiceImpl implements MarineConnectService {
	private static final Logger logger = LoggerFactory.getLogger(MarineConnectServiceImpl.class);

	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";
	private static final String KESALAHAN_DALAM_SERVER = "Kesalahan dalam server.";

	private MarineConnectInquiryRepository marineConnectInquiryRepository;

	private MarineConnectUploadTcRepository marineConnectUploadTcRepository;

	private MarineConnectQuotationRepository marineConnectQuotationRepository;

	private MarineConnectWishListRepository marineConnectWishListRepository;

	private MarineConnectInvoiceRepository marineConnectInvoiceRepository;

	private ViewMarineConnectRepository viewMarineConnectRepository;
	
	private ViewMarineConnectHistoryRepository viewMarineConnectHistoryRepository;

	private ChatMarineConnectRepository chatMarineConnectRepository;

	private SupplierRepository supplierRepository;

	private ModelMapper modelMapper = new ModelMapper();

	@Value("${asuransikapalku.url}")
	private String url;

	@Value("${path.root}")
	private String pathRoot;

	@Value("${dir.file.upload}")
	private String dirUploadFile;

	@Value("${dir.marine.connect}")
	private String dirMarineConnect;

	@Autowired
	public void repository(MarineConnectInquiryRepository marineConnectInquiryRepository,
			MarineConnectUploadTcRepository marineConnectUploadTcRepository, SupplierRepository supplierRepository,
			ViewMarineConnectRepository viewMarineConnectRepository) {
		this.marineConnectInquiryRepository = marineConnectInquiryRepository;
		this.marineConnectUploadTcRepository = marineConnectUploadTcRepository;
		this.supplierRepository = supplierRepository;
		this.viewMarineConnectRepository = viewMarineConnectRepository;
	}

	@Autowired
	public void repository(MarineConnectWishListRepository marineConnectWishListRepository,
			MarineConnectQuotationRepository marineConnectQuotationRepository,
			MarineConnectInvoiceRepository marineConnectInvoiceRepository,
			ViewMarineConnectHistoryRepository viewMarineConnectHistoryRepository) {
		this.marineConnectQuotationRepository = marineConnectQuotationRepository;
		this.marineConnectWishListRepository = marineConnectWishListRepository;
		this.marineConnectInvoiceRepository = marineConnectInvoiceRepository;
		this.viewMarineConnectHistoryRepository = viewMarineConnectHistoryRepository;
	}

	@Autowired
	public void respository(ChatMarineConnectRepository chatMarineConnectRepository) {
		this.chatMarineConnectRepository = chatMarineConnectRepository;
	}

	@Override
	public APIResponse<String> createInquiry(MarineConnectInquiryDto marineConnectInquiryDto, User user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			MarineConnectInquiry marineConnectInquiry = modelMapper.map(marineConnectInquiryDto,
					MarineConnectInquiry.class);
			marineConnectInquiry
					.setSupplier(supplierRepository.findById(marineConnectInquiryDto.getIdSupplier()).orElse(null));
			marineConnectInquiry.setDemand(user.getDemand());
			marineConnectInquiry.setStatus(StatusMarineConnectEnum.WAITING_QUOTATION.code);
			marineConnectInquiry.setCreatedBy(user.getUsername());
			marineConnectInquiry.setCreatedDate(new Date());
			marineConnectInquiryRepository.save(marineConnectInquiry);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA INQUIRY : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> updateQuoted(MarineConnectQuotationDto marineConnectQuotationDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			MarineConnectQuotation marineConnectQuotation = modelMapper.map(marineConnectQuotationDto,
					MarineConnectQuotation.class);
			marineConnectQuotation.setMarineConnect(marineConnectInquiryRepository
					.findById(marineConnectQuotationDto.getIdMarineConnect()).orElse(null));
			marineConnectQuotation.getMarineConnect().setStatus(StatusMarineConnectEnum.INPROGRESS.code);
			marineConnectQuotationRepository.save(marineConnectQuotation);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA MARINE CONNECT QUOTED : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> updateWishList(MarineConnectWishListDto marineConnectWishListDto, User user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			MarineConnectWishList marineConnectWishList = marineConnectWishListRepository.findByIdDemandAndIdSupplier(user.getDemand().getId(), marineConnectWishListDto.getIdSupplier());
			if(null == marineConnectWishList) {
				marineConnectWishList = new MarineConnectWishList();
				marineConnectWishList
				.setSupplier(supplierRepository.findById(marineConnectWishListDto.getIdSupplier()).orElse(null));
				marineConnectWishList.setDemand(user.getDemand());
			} 
			marineConnectWishList.setFlag(marineConnectWishListDto.getFlag());		
			marineConnectWishListRepository.save(marineConnectWishList);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA MARINE CONNECT WISH LIST : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> updateStatus(UpdateStatusDto updateStatusDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			MarineConnectInquiry marineConnectInquiry = marineConnectInquiryRepository.findById(updateStatusDto.getId())
					.orElse(null);
			if (null == marineConnectInquiry) {
				logger.info("DATA MARINE CONNECT TIDAK DITEMUKAN");
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data supplier tidak ditemukan.");
			}
			marineConnectInquiry.setStatus(updateStatusDto.getStatusCode());
			marineConnectInquiry.setUpdatedBy(user);
			marineConnectInquiry.setUpdatedDate(new Date());
			marineConnectInquiryRepository.save(marineConnectInquiry);
		} catch (Exception e) {
			logger.error("GAGAL UPDATE STATUS DATA MARINE CONNECT INQUIRY : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> uploadTC(MarineConnectUploadTcDto marineConnectUploadTcDto, User user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			Date currentDate = new Date();
			String dirFile = pathRoot + dirUploadFile + dirMarineConnect;
			String locatioanFile = url + dirUploadFile + dirMarineConnect;
			MarineConnectUploadTc marineConnectUploadTc = modelMapper.map(marineConnectUploadTcDto,
					MarineConnectUploadTc.class);
			marineConnectUploadTc.setTarget(marineConnectUploadTcDto.getTo());
			marineConnectUploadTc.setUploadDate(currentDate);
			marineConnectUploadTc.setUploader(user.getUsername());
			marineConnectUploadTc.setUploadDate(currentDate);
			marineConnectUploadTc.setMarineConnect(marineConnectInquiryRepository
					.findById(marineConnectUploadTcDto.getMarineConnectId()).orElse(null));

			if (marineConnectUploadTcDto.getFile().contains("/pdf;base64")) {
				marineConnectUploadTc.setFile(FileUtil.fileDecodeToDir(marineConnectUploadTcDto.getFile(),
						marineConnectUploadTcDto.getFileName(), dirFile, AsuransiKapalkuConstant.FileTypeExtension.PDF,
						locatioanFile));
			}

			if (marineConnectUploadTcDto.getFile().contains("/doc;base64")) {
				marineConnectUploadTc.setFile(FileUtil.fileDecodeToDir(marineConnectUploadTcDto.getFile(),
						marineConnectUploadTcDto.getFileName(), dirFile, AsuransiKapalkuConstant.FileTypeExtension.DOC,
						locatioanFile));
			}

			if (marineConnectUploadTcDto.getFile().contains("/xls;base64")) {
				marineConnectUploadTc.setFile(FileUtil.fileDecodeToDir(marineConnectUploadTcDto.getFile(),
						marineConnectUploadTcDto.getFileName(), dirFile, AsuransiKapalkuConstant.FileTypeExtension.XLS,
						locatioanFile));
			}

			if (marineConnectUploadTcDto.getFile().contains("/xlsx;base64")) {
				marineConnectUploadTc.setFile(FileUtil.fileDecodeToDir(marineConnectUploadTcDto.getFile(),
						marineConnectUploadTcDto.getFileName(), dirFile, AsuransiKapalkuConstant.FileTypeExtension.XLSX,
						locatioanFile));
			}

			if (marineConnectUploadTcDto.getFile().contains("/png;base64")) {
				marineConnectUploadTc.setFile(FileUtil.fileDecodeToDir(marineConnectUploadTcDto.getFile(),
						marineConnectUploadTcDto.getFileName(), dirFile, AsuransiKapalkuConstant.FileTypeExtension.PNG,
						locatioanFile));
			}

			if (marineConnectUploadTcDto.getFile().contains("/jpeg;base64")
					|| marineConnectUploadTcDto.getFile().contains("/jpg;base64")) {
				marineConnectUploadTc.setFile(FileUtil.fileDecodeToDir(marineConnectUploadTcDto.getFile(),
						marineConnectUploadTcDto.getFileName(), dirFile, AsuransiKapalkuConstant.FileTypeExtension.JPG,
						locatioanFile));
			}

			marineConnectUploadTcRepository.save(marineConnectUploadTc);
		} catch (Exception e) {
			logger.error("GAGAL UPLOAD TC MARINE CONNECT : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			MarineConnectUploadTc marineConnectUploadTc = marineConnectUploadTcRepository.findById(id).orElse(null);
			if (null == marineConnectUploadTc) {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data tidak ditemukan.");
			}
			marineConnectUploadTc.setMarineConnect(null);
			marineConnectUploadTcRepository.save(marineConnectUploadTc);
			marineConnectUploadTcRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL DELETE TC MARINE CONNECT : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllMarineConnectInquiry(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<MarineConnectInquiry> page = marineConnectInquiryRepository.findAll(pageable);
			List<MarineConnectInquiryDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(p -> modelMapper.map(p, MarineConnectInquiryDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			dataRes.put("marine-connects", list);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST DATA MARINE CONNECT : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return response;
	}

	@Override
	public APIResponse<Map<String, Object>> findMarineConnectById(Long id) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		Map<String, Object> map = new HashMap<>();
		try {
			MarineConnectInquiry marineConnectInquiry = marineConnectInquiryRepository.findById(id).orElse(null);
			if (null == marineConnectInquiry) {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data marine connect tidak ditemukan.");
			}

			MarineConnectInquiryDto marineConnectInquiryDto = modelMapper.map(marineConnectInquiry,
					MarineConnectInquiryDto.class);

			List<MarineConnectUploadTc> marineConnectUploadTcDemands = marineConnectUploadTcRepository
					.findByMarineConnectAndIsSupplyOrderByIdAsc(marineConnectInquiry, false);
			List<MarineConnectUploadTcDto> connectUploadTcDtoDemands = new ArrayList<>();
			if (!marineConnectUploadTcDemands.isEmpty()) {
				connectUploadTcDtoDemands = marineConnectUploadTcDemands.stream()
						.map(mtc -> modelMapper.map(mtc, MarineConnectUploadTcDto.class)).collect(Collectors.toList());
			}

			List<MarineConnectUploadTc> marineConnectUploadTcSuppliers = marineConnectUploadTcRepository
					.findByMarineConnectAndIsSupplyOrderByIdAsc(marineConnectInquiry, true);
			List<MarineConnectUploadTcDto> connectUploadTcDtoSuppliers = new ArrayList<>();
			if (!marineConnectUploadTcSuppliers.isEmpty()) {
				connectUploadTcDtoSuppliers = marineConnectUploadTcSuppliers.stream()
						.map(mtc -> modelMapper.map(mtc, MarineConnectUploadTcDto.class)).collect(Collectors.toList());
			}

			MarineConnectQuotation marineConnectQuotation = marineConnectQuotationRepository.findByIdMarineConnect(id);
			MarineConnectQuotationDto marineConnectQuotationDto = new MarineConnectQuotationDto();

			if (null != marineConnectQuotation) {
				marineConnectQuotationDto = modelMapper.map(marineConnectQuotation, MarineConnectQuotationDto.class);
			}
			
			MarineConnectInvoiceDto marineConnectInvoiceDto = new MarineConnectInvoiceDto();
			MarineConnectInvoice marineConnectInvoice = marineConnectInvoiceRepository.findByIdMarineConnect(id);
			if(null != marineConnectInvoice) {
				marineConnectInvoiceDto = modelMapper.map(marineConnectInvoice, MarineConnectInvoiceDto.class);
			}
			
			map.put("invoice", marineConnectInvoiceDto);
			map.put("marine-connect-inquiry", marineConnectInquiryDto);
			map.put("tab-demand-documents", connectUploadTcDtoDemands);
			map.put("tab-demand-quoted", marineConnectQuotationDto);
			map.put("tab-supply-documents", connectUploadTcDtoSuppliers);
			respon.setData(map);
		} catch (Exception e) {
			logger.error("GAGAL MENDAPATKAN DATA MARINE CONNECT DENGAN ID : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findMarineConnect(Long idMarineConnect, User user) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		Map<String, Object> map = new HashMap<>();
		try {
			MarineConnectInquiry marineConnectInquiry = marineConnectInquiryRepository.findById(idMarineConnect)
					.orElse(null);
			if (null == marineConnectInquiry) {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data marine connect tidak ditemukan.");
			}

			MarineConnectInquiryDto marineConnectInquiryDto = this
					.convertEntityToMarineConnectInquiryDto(marineConnectInquiry);

			List<MarineConnectUploadTc> marineConnectUploadTcDemands = marineConnectUploadTcRepository
					.findByMarineConnectAndIsSupplyOrderByIdAsc(marineConnectInquiry, false);
			List<MarineConnectUploadTcDto> connectUploadTcDtoDemands = new ArrayList<>();
			if (!marineConnectUploadTcDemands.isEmpty()) {
				connectUploadTcDtoDemands = marineConnectUploadTcDemands.stream()
						.map(mtc -> modelMapper.map(mtc, MarineConnectUploadTcDto.class)).collect(Collectors.toList());
			}

			List<MarineConnectUploadTc> marineConnectUploadTcSuppliers = marineConnectUploadTcRepository
					.findByMarineConnectAndIsSupplyOrderByIdAsc(marineConnectInquiry, true);
			List<MarineConnectUploadTcDto> connectUploadTcDtoSuppliers = new ArrayList<>();
			if (!marineConnectUploadTcSuppliers.isEmpty()) {
				connectUploadTcDtoSuppliers = marineConnectUploadTcSuppliers.stream()
						.map(mtc -> modelMapper.map(mtc, MarineConnectUploadTcDto.class)).collect(Collectors.toList());
			}

			MarineConnectQuotation marineConnectQuotation = marineConnectQuotationRepository
					.findByIdMarineConnect(idMarineConnect);
			MarineConnectQuotationDto marineConnectQuotationDto = new MarineConnectQuotationDto();

			if (null != marineConnectQuotation) {
				marineConnectQuotationDto = modelMapper.map(marineConnectQuotation, MarineConnectQuotationDto.class);
			}

			List<ChatMarineConnectDto> chatMarineConnectDtos = new ArrayList<>();
			if (null != user.getDemand()) {
				List<ChatMarineConnect> chatMarineConnects = chatMarineConnectRepository
						.findBySenderAndReceiverOrderByIdAsc(user.getUsername(), marineConnectInquiry.getSupplier().getUser().getUsername());
				logger.info("CHAT SIZE MARINE CONNECT : {}", chatMarineConnects.size());
				if (!chatMarineConnects.isEmpty()) {
					chatMarineConnectDtos = chatMarineConnects.stream()
							.map(cmc -> modelMapper.map(cmc, ChatMarineConnectDto.class)).collect(Collectors.toList());
				}
			}
			
			MarineConnectInvoiceDto marineConnectInvoiceDto = new MarineConnectInvoiceDto();
			MarineConnectInvoice marineConnectInvoice = marineConnectInvoiceRepository.findByIdMarineConnect(idMarineConnect);
			if(null != marineConnectInvoice) {
				marineConnectInvoiceDto = modelMapper.map(marineConnectInvoice, MarineConnectInvoiceDto.class);
			}

			map.put("marine-connect-inquiry", marineConnectInquiryDto);
			map.put("tab-demand-documents", connectUploadTcDtoDemands);
			map.put("tab-demand-quoted", marineConnectQuotationDto);
			map.put("invoice", marineConnectInvoiceDto);
			map.put("tab-supply-documents", connectUploadTcDtoSuppliers);
			map.put("chat", chatMarineConnectDtos);
			respon.setData(map);
		} catch (Exception e) {
			logger.error("GAGAL DELETE TC MARINE CONNECT : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	private MarineConnectInquiryDto convertEntityToMarineConnectInquiryDto(MarineConnectInquiry marineConnectInquiry) {
		MarineConnectInquiryDto marineConnectInquiryDto = modelMapper.map(marineConnectInquiry,
				MarineConnectInquiryDto.class);
		marineConnectInquiryDto.setEmailSupplier(marineConnectInquiry.getSupplier().getEmail());
		return marineConnectInquiryDto;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllMarineConnect(FilterDto filterDto, User user) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewMarineConnect> page = viewMarineConnectRepository.findAll(new Specification<ViewMarineConnect>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ViewMarineConnect> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					
					if(null != user.getDemand()) {
						predicates
						.add(criteriaBuilder.equal(root.get("idDemand"), user.getDemand().getId()));
					}
					
					if(null != user.getSupplier()) {
						predicates
						.add(criteriaBuilder.equal(root.get("idSupplier"), user.getSupplier().getId()));
					}
					
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<ViewMarineConnectDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(this::convertToViewMarineConnectDto).collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			dataRes.put("marine-connects", list);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST DATA MARINE CONNECT : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return response;
	}
	
	@Override
	public APIResponse<Map<String, Object>> findAllMarineConnectHistory(FilterDto filterDto, User user) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewMarineConnectHistory> page = viewMarineConnectHistoryRepository.findAll(new Specification<ViewMarineConnectHistory>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ViewMarineConnectHistory> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					
					if(null != user.getDemand()) {
						predicates
						.add(criteriaBuilder.equal(root.get("idDemand"), user.getDemand().getId()));
					}
					
					if(null != user.getSupplier()) {
						predicates
						.add(criteriaBuilder.equal(root.get("idSupplier"), user.getSupplier().getId()));
					}
					
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<ViewMarineConnectHistoryDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(this::convertToViewMarineConnectHistoryDto).collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			dataRes.put("marine-connects", list);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST DATA MARINE CONNECT HISTORY : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return response;
	}

	@Override
	public APIResponse<String> updatePriceQuoted(UpdatePriceDto updatePriceDto, boolean isFinal) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			MarineConnectQuotation marineConnectQuotation = marineConnectQuotationRepository
					.findByIdMarineConnect(updatePriceDto.getIdMarineConnect());

			if (isFinal) {
				marineConnectQuotation.setFinalPrice(updatePriceDto.getPrice());
				marineConnectQuotation.setFinalDescription(updatePriceDto.getDescription());
			}
			if (!isFinal) {
				marineConnectQuotation.setNegotiatePrice(updatePriceDto.getPrice());
				marineConnectQuotation.setNegotiateDescription(updatePriceDto.getDescription());
			}
			marineConnectQuotationRepository.save(marineConnectQuotation);
		} catch (Exception e) {
			logger.error("GAGAL MENGUBAH DATA HARGA QUOTATION : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<List<Map<String, Object>>> findStatus() {
		APIResponse<List<Map<String, Object>>> respon = new APIResponse<>();
		List<Map<String, Object>> maps = new ArrayList<>();
		for (StatusMarineConnectEnum sse : StatusMarineConnectEnum.values()) {
			Map<String, Object> map = new HashMap<>();
			map.put("code", sse.code);
			map.put("name", sse.label);
			maps.add(map);
		}
		respon.setData(maps);
		return respon;
	}

	@Override
	public APIResponse<String> uploadInvoice(MarineConnectInvoiceDto marineConnectInvoiceDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			MarineConnectInquiry marineConnectInquiry = marineConnectInquiryRepository
					.findById(marineConnectInvoiceDto.getId()).orElse(null);

			if (null == marineConnectInquiry) {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data marine connect inquiry tidak ditemukan.");
			}

			MarineConnectInvoice marineConnectInvoice = modelMapper.map(marineConnectInvoiceDto,
					MarineConnectInvoice.class);
			marineConnectInvoice.setMarineConnect(marineConnectInquiry);
			String dirFile = pathRoot + dirUploadFile + dirMarineConnect;
			String locatioanFile = url + dirUploadFile + dirMarineConnect;
			String fileName = "INVOICE_" + marineConnectInvoiceDto.getIdMarinceConnect() + "_"
					+ PasswordUtil.generateRandomPassword(10);
			if (marineConnectInvoiceDto.getFile().contains("/pdf;base64")) {
				marineConnectInvoice.setFile(FileUtil.fileDecodeToDir(marineConnectInvoiceDto.getFile(), fileName,
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.PDF, locatioanFile));
			}

			if (marineConnectInvoiceDto.getFile().contains("/doc;base64")) {
				marineConnectInvoice.setFile(FileUtil.fileDecodeToDir(marineConnectInvoiceDto.getFile(), fileName,
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.DOC, locatioanFile));
			}

			if (marineConnectInvoiceDto.getFile().contains("/xls;base64")) {
				marineConnectInvoice.setFile(FileUtil.fileDecodeToDir(marineConnectInvoiceDto.getFile(), fileName,
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.XLS, locatioanFile));
			}

			if (marineConnectInvoiceDto.getFile().contains("/xlsx;base64")) {
				marineConnectInvoice.setFile(FileUtil.fileDecodeToDir(marineConnectInvoiceDto.getFile(), fileName,
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.XLSX, locatioanFile));
			}

			if (marineConnectInvoiceDto.getFile().contains("/png;base64")) {
				marineConnectInvoice.setFile(FileUtil.fileDecodeToDir(marineConnectInvoiceDto.getFile(), fileName,
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.PNG, locatioanFile));
			}

			if (marineConnectInvoiceDto.getFile().contains("/jpeg;base64")
					|| marineConnectInvoiceDto.getFile().contains("/jpg;base64")) {
				marineConnectInvoice.setFile(FileUtil.fileDecodeToDir(marineConnectInvoiceDto.getFile(), fileName,
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.JPG, locatioanFile));
			}
			marineConnectInvoiceRepository.save(marineConnectInvoice);
		} catch (Exception e) {
			logger.error("GAGAL UPLOAD INCOICE : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	private ViewMarineConnectDto convertToViewMarineConnectDto(ViewMarineConnect viewMarineConnect) {
		ViewMarineConnectDto viewMarineConnectDto = modelMapper.map(viewMarineConnect, ViewMarineConnectDto.class);
		for (StatusMarineConnectEnum s : StatusMarineConnectEnum.values()) {
			if (s.code.equalsIgnoreCase(viewMarineConnectDto.getStatus())) {
				viewMarineConnectDto.setStatusDescription(s.label);
				break;
			}
		}
		return viewMarineConnectDto;
	}
	
	private ViewMarineConnectHistoryDto convertToViewMarineConnectHistoryDto(ViewMarineConnectHistory viewMarineConnectHistory) {
		ViewMarineConnectHistoryDto viewMarineConnectHistoryDto = modelMapper.map(viewMarineConnectHistory, ViewMarineConnectHistoryDto.class);
		for (StatusMarineConnectEnum s : StatusMarineConnectEnum.values()) {
			if (s.code.equalsIgnoreCase(viewMarineConnectHistoryDto.getStatus())) {
				viewMarineConnectHistoryDto.setStatusDescription(s.label);
				break;
			}
		}
		return viewMarineConnectHistoryDto;
	}
}
