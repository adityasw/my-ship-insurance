package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.user.AdminDto;
import com.apps.asuransikapalku.api.model.dto.user.AdminRegisterDto;
import com.apps.asuransikapalku.api.model.dto.user.LoginDto;

public interface AdminService {
	
	public APIResponse<Map<String, Object>> loginAdmin(LoginDto loginDto);
	
	public APIResponse<String> registerAdmin(AdminRegisterDto adminRegisterDto, String userCreated);

	public APIResponse<String> save(AdminDto adminDto, String user);

	public APIResponse<String> delete(Long id, String user);

	public APIResponse<String> deleteAll(List<Long> id, String user);

	public APIResponse<String> sofDelete(Long id, String user);

	public APIResponse<String> softDeleteAll(List<Long> id, String user);
	
	public APIResponse<AdminDto> findById(Long id);
	
	public APIResponse<AdminDto> findByEmail(String email);

	public APIResponse<Map<String, Object>> findAllAdmin(FilterDto filterDto);

	void authenticateCheck(LoginDto loginDto) throws ValidateException;

}
