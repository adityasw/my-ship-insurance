package com.apps.asuransikapalku.api.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.master.CityDto;
import com.apps.asuransikapalku.api.model.dto.master.ProvinceDto;
import com.apps.asuransikapalku.api.model.dto.master.SubDistrictDto;
import com.apps.asuransikapalku.api.repository.master.CityRepository;
import com.apps.asuransikapalku.api.repository.master.ProvinceRepository;
import com.apps.asuransikapalku.api.repository.master.SubDistrictRepository;
import com.apps.asuransikapalku.api.service.RegionService;

@Service
public class RegionServiceImpl implements RegionService {
	private static final Logger logger = LoggerFactory.getLogger(RegionServiceImpl.class);

	private static final String DATA_NOT_FOUND = "Data tidak ditemukan.";

	private ProvinceRepository provinceRepository;

	private CityRepository cityRepository;

	private SubDistrictRepository subDistrictRepository;

	private ModelMapper modelMapper = new ModelMapper();

	@Autowired
	public void service(ProvinceRepository provinceRepository, CityRepository cityRepository,
			SubDistrictRepository subDistrictRepository) {
		this.provinceRepository = provinceRepository;
		this.cityRepository = cityRepository;
		this.subDistrictRepository = subDistrictRepository;
	}

	@Override
	public APIResponse<List<ProvinceDto>> findAllProvince() {
		APIResponse<List<ProvinceDto>> respon = new APIResponse<>();
		try {
			respon.setData(provinceRepository.findAll().stream().map(p -> modelMapper.map(p, ProvinceDto.class))
					.collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("DATA LIST PROVINSI TIDAK DITEMUKAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_NOT_FOUND);
		}
		return respon;
	}

	@Override
	public APIResponse<List<CityDto>> findByProvinceId(String id) {
		APIResponse<List<CityDto>> respon = new APIResponse<>();
		try {
			respon.setData(cityRepository.findByProvinceId(id).stream().map(c -> modelMapper.map(c, CityDto.class))
					.collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("DATA LIST KABUPATEN/KOTA TIDAK DITEMUKAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_NOT_FOUND);
		}
		return respon;
	}

	@Override
	public APIResponse<List<SubDistrictDto>> findByCityId(String id) {
		APIResponse<List<SubDistrictDto>> respon = new APIResponse<>();
		try {
			respon.setData(subDistrictRepository.findByCityId(id).stream()
					.map(s -> modelMapper.map(s, SubDistrictDto.class)).collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("DATA LIST KECAMATAN TIDAK DITEMUKAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_NOT_FOUND);
		}
		return respon;
	}
}
