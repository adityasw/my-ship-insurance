package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.occupation.OccupationTypeDto;

public interface OccupationTypeService {
	public APIResponse<String> save(OccupationTypeDto occupationTypeDto, String createdBy);
	
	public APIResponse<String> delete(Long id);
	
	public APIResponse<List<OccupationTypeDto>> findAllWithoutPageable();
	
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto);

	public APIResponse<String> softDelete(Long id, String user);

	public APIResponse<OccupationTypeDto> findById(Long id);
}
