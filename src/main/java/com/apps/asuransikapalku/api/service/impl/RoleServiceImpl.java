package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.user.RoleDto;
import com.apps.asuransikapalku.api.model.entity.user.Role;
import com.apps.asuransikapalku.api.repository.user.RoleRepository;
import com.apps.asuransikapalku.api.service.RoleService;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class RoleServiceImpl implements RoleService {
	private static final Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";
	
	private RoleRepository roleRepository;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	private String user;
	
	@Autowired
	public void repository(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	@Override
	public APIResponse<String> save(RoleDto roleDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = user;
			this.validation(roleDto);
			roleRepository.save(convertDtoToEntity(roleDto));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA ROLE : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			roleRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA ROLE [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Role [" + id + "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<Role> roles = roleRepository.findByIdIn(ids);
			roleRepository.deleteAll(roles);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA ROLE [BATCH] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal menghapus data role dengan batch.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<String> updateStatus(Long id, boolean isActive, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			Role role = roleRepository.findById(id).orElse(null);
			role.setIsActive(isActive);
			role.setUpdatedBy(user);
			role.setUpdatedDate(new Date());
			roleRepository.save(role);
		} catch (Exception e) {
			logger.error("GAGAL MENGUBAH STATUS ACTIVE ROLE : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"status role [" + id + "] gagal diubah.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDelete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			Role role = roleRepository.findById(id).orElse(null);
			role.setIsActive(Boolean.FALSE);
			role.setUpdatedBy(user);
			role.setUpdatedDate(new Date());
			roleRepository.save(role);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA ROLE [SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Role [" + id + "] gagal terhapus");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDeleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<Role> roles = roleRepository.findByIdIn(ids);
			for(Role r : roles) {
				r.setIsActive(Boolean.FALSE);
				r.setUpdatedBy(user);
				r.setUpdatedDate(new Date());
			}
			roleRepository.deleteAll(roles);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA ROLE [BATCH - SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal menghapus data role dengan batch.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<RoleDto> findById(Long id) {
		APIResponse<RoleDto> respon =new APIResponse<>();
		try {
			Role role = roleRepository.findById(id).orElse(null);
			if(role == null) {
				logger.info("ROLE TIDAK DITEMUKAN : {}", role);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Role tidak ditemukan.");
			}
			respon.setData(modelMapper.map(role, RoleDto.class));
		} catch (Exception e) {
			logger.info("ROLE TIDAK DITEMUKAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Role tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<List<RoleDto>> findAllWithoutPageableAndActive() {
		APIResponse<List<RoleDto>> respon = new APIResponse<>();
		try {
			List<Role> roles = roleRepository.findByIsActive(true);
			
			respon.setData(roles.stream().map(r -> modelMapper.map(r, RoleDto.class)).collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST ROLE : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"List role tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllRole(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<Role> rolePage = roleRepository.findAll(new Specification<Role>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Role> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					
					if (filterDto.getIsActive() != null && filterDto.getIsActive()) {
						predicates.add(criteriaBuilder.equal(root.get("isActive"), filterDto.getIsActive()));
					}
					
					if (null != filterDto.getSearchBy()) {
						
						Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate description = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("description")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(name, description));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<RoleDto> roles = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != rolePage) {
				roles = rolePage.getContent().stream().map(r -> modelMapper.map(r, RoleDto.class))
						.collect(Collectors.toList());
				currentPage = rolePage.getNumber();
				totalElement = rolePage.getTotalElements();
				totalPage = rolePage.getTotalPages();
			}

			dataRes.put("roles", roles);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST ROLE : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List role tidak ditemukan.");
		}
		return response;
	}
	
	private Role convertDtoToEntity(RoleDto roleDto) {
		Role role = modelMapper.map(roleDto, Role.class);
		if (roleDto.getId() == null || roleDto.getId() == 0) {
			role.setCreatedBy(user);
			role.setCreatedDate(new Date());
		} else {
			Role roleOld = roleRepository.findById(roleDto.getId()).orElse(null);
			if(roleOld != null) {
				role.setCreatedBy(roleOld.getCreatedBy());
				role.setCreatedDate(roleOld.getCreatedDate());
			}
			role.setUpdatedBy(user);
			role.setUpdatedDate(new Date());
		}
		
		return role;
	}
	
	private void validation(RoleDto roleDto) throws ValidateException {
		if(null == roleDto.getName()) {
			throw new ValidateException("Nama role harus diisi.");
		}
		
		Integer count = roleRepository.getCountByName(roleDto.getName().toUpperCase());
		count = count == null ? 0 : count;
		if((null == roleDto.getId() && count > 0) || (null != roleDto.getId() && count > 1)) {
			throw new ValidateException("Nama role sudah digunakan.");
		}
	}

}
