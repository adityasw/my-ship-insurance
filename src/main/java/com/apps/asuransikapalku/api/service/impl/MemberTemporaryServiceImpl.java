package com.apps.asuransikapalku.api.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.enums.StatusEnum;
import com.apps.asuransikapalku.api.exception.CommonException;
import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.AprrovalDto;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.FilterDto.Sorting;
import com.apps.asuransikapalku.api.model.dto.user.AdminDto;
import com.apps.asuransikapalku.api.model.dto.user.DemandDto;
import com.apps.asuransikapalku.api.model.dto.user.DemandRegisterDto;
import com.apps.asuransikapalku.api.model.dto.user.InvitationZoomDto;
import com.apps.asuransikapalku.api.model.dto.user.LoginDto;
import com.apps.asuransikapalku.api.model.dto.user.MemberTemporaryDto;
import com.apps.asuransikapalku.api.model.dto.user.MemberTemporaryHistoryDto;
import com.apps.asuransikapalku.api.model.dto.user.RegisterDto;
import com.apps.asuransikapalku.api.model.dto.user.ResetPasswordDto;
import com.apps.asuransikapalku.api.model.dto.user.ResetPasswordManualDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierRegisterDto;
import com.apps.asuransikapalku.api.model.entity.user.Admin;
import com.apps.asuransikapalku.api.model.entity.user.Demand;
import com.apps.asuransikapalku.api.model.entity.user.MemberTemporary;
import com.apps.asuransikapalku.api.model.entity.user.MemberTemporaryHistory;
import com.apps.asuransikapalku.api.model.entity.user.Supplier;
import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.repository.occupation.OccupationRepository;
import com.apps.asuransikapalku.api.repository.user.AdminRepository;
import com.apps.asuransikapalku.api.repository.user.DemandRepository;
import com.apps.asuransikapalku.api.repository.user.MemberTemporaryHistoryRepository;
import com.apps.asuransikapalku.api.repository.user.MemberTemporaryRepository;
import com.apps.asuransikapalku.api.repository.user.SupplierRepository;
import com.apps.asuransikapalku.api.repository.user.UserRepository;
import com.apps.asuransikapalku.api.service.AdminService;
import com.apps.asuransikapalku.api.service.DemandService;
import com.apps.asuransikapalku.api.service.EmailService;
import com.apps.asuransikapalku.api.service.MemberTemporaryService;
import com.apps.asuransikapalku.api.service.SupplierService;
import com.apps.asuransikapalku.api.service.UserService;
import com.apps.asuransikapalku.api.utils.EncryptionUtil;
import com.apps.asuransikapalku.api.utils.FormatDateUtil;
import com.apps.asuransikapalku.api.utils.PasswordUtil;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class MemberTemporaryServiceImpl implements MemberTemporaryService {
	public static final Logger logger = LoggerFactory.getLogger(MemberTemporaryServiceImpl.class);
	private static final String EMAIL_EXISTS = "Email sudah digunakan.";
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";

	private MemberTemporaryRepository memberTemporaryRepository;

	private MemberTemporaryHistoryRepository memberTemporaryHistoryRepository;

	private OccupationRepository occupationRepository;
	
	private AdminRepository adminRepository;
	
	private DemandRepository demandRepository;
	
	private SupplierRepository supplierRepository;

	private AdminService adminService;

	private DemandService demandService;

	private EmailService emailService;

	private SupplierService supplierService;

	private UserRepository userRepository;

	private UserService userService;

	private ModelMapper modelMapper = new ModelMapper();

	private String user;

	@Autowired
	public void repository(MemberTemporaryRepository memberTemporaryRepository,
			MemberTemporaryHistoryRepository memberTemporaryHistoryRepository,
			OccupationRepository occupationRepository, UserRepository userRepository) {
		this.memberTemporaryRepository = memberTemporaryRepository;
		this.memberTemporaryHistoryRepository = memberTemporaryHistoryRepository;
		this.occupationRepository = occupationRepository;
		this.userRepository = userRepository;
	}
	
	@Autowired
	public void repository(AdminRepository adminRepository,
			DemandRepository demandRepository,
			SupplierRepository supplierRepository) {
		this.adminRepository = adminRepository;
		this.demandRepository = demandRepository;
		this.supplierRepository = supplierRepository;
	}

	@Autowired
	public void serviceOther(AdminService adminService, DemandService demandService, EmailService emailService,
			SupplierService supplierService, UserService userService) {
		this.adminService = adminService;
		this.demandService = demandService;
		this.emailService = emailService;
		this.supplierService = supplierService;
		this.userService = userService;
	}

	@Override
	public APIResponse<String> register(RegisterDto registerDto, String userCreated) {
		APIResponse<String> respon = new APIResponse<>();
		MemberTemporaryHistory history = new MemberTemporaryHistory();
		boolean isError = false;
		try {
			this.validationToRegister(registerDto);
			Date createdDate = new Date();

			MemberTemporary memberTemporary = new MemberTemporary();
			memberTemporary.setName(registerDto.getName());
			memberTemporary.setEmail(registerDto.getEmail());
			memberTemporary.setPhoneNumber(registerDto.getPhoneNumber());
			memberTemporary.setCompanyName(registerDto.getCompanyName());
			memberTemporary.setOccupation(registerDto.getOccupation());
			memberTemporary.setPosition(registerDto.getPosition());
			memberTemporary.setInsuranceNeeds(registerDto.getInsuranceNeeds());
			memberTemporary.setCreatedDate(createdDate);
			memberTemporary.setScheduleMeet(registerDto.getScheduleMeet());
			memberTemporary.setCreatedBy(userCreated == null ? "SYSTEM" : userCreated);
			memberTemporary.setStatus(StatusEnum.PENDING_ASSESSMENT.code);

			history.setMemberTemporary(memberTemporary);
			history.setStatus(StatusEnum.PENDING_ASSESSMENT.code);
			history.setStatusDescription(StatusEnum.PENDING_ASSESSMENT.label);
			history.setDescription("Client melakukan register");
			history.setHistoryDate(createdDate);

			Map<String, String> params = new HashMap<>();
			params.put(AsuransiKapalkuConstant.Email.SUBJECT,
					"Thanks for registering to asuransikapalku.com! One Step Closer....");
			params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, registerDto.getName());
			params.put(AsuransiKapalkuConstant.Email.TO, registerDto.getEmail());
			params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/confirmation-sign-up.html");
			
			logger.info("Email To : {}", registerDto.getEmail());

			emailService.sendMail(params);
		} catch (Exception e) {
			isError = true;
			logger.error("PENDAFTARAN AKUN GAGAL : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		} finally {
			if(!isError) {
				memberTemporaryHistoryRepository.save(history);
			}
		}
		return respon;
	}

	@Override
	public APIResponse<String> save(MemberTemporaryDto memberTemporaryDto, String userCreated) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = userCreated;
			this.validationToUpdate(memberTemporaryDto);
			memberTemporaryRepository.save(convertDtoToEntity(memberTemporaryDto));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA AKUN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> updateStatus(AprrovalDto aprrovalDto, String userCreated) {
		logger.info("Approval : {}", aprrovalDto);
		APIResponse<String> respon = new APIResponse<>();
		MemberTemporary memberTemporary = null;
		MemberTemporaryHistory history = new MemberTemporaryHistory();
		boolean isError = false;
		StatusEnum se = null;
		String statusOld = "";
		Date createdDate = new Date();
		String status = "";

		try {
			this.user = userCreated;
			memberTemporary = memberTemporaryRepository.findById(aprrovalDto.getId()).orElse(null);
			for (StatusEnum s : StatusEnum.values()) {
				if (s.code.equalsIgnoreCase(memberTemporary.getStatus())) {
					logger.info("STATUS SEBELUMNYA : {}", s.code);
					se = s;
					statusOld = s.code;
					break;
				}
			}

			for (StatusEnum s : StatusEnum.values()) {
				if (s.code.equalsIgnoreCase(aprrovalDto.getStatusCode())) {
					status = s.label;
					break;
				}
			}
			memberTemporary.setName(aprrovalDto.getMember().getName());
			memberTemporary.setEmail(aprrovalDto.getMember().getEmail());
			memberTemporary.setPosition(aprrovalDto.getMember().getPosition());
			memberTemporary.setOccupation(aprrovalDto.getMember().getOccupation());
			memberTemporary.setInsuranceNeeds(aprrovalDto.getMember().getInsuranceNeeds());
			memberTemporary.setCompanyName(aprrovalDto.getMember().getCompanyName());
			memberTemporary.setPhoneNumber(aprrovalDto.getMember().getPhoneNumber());

			if (memberTemporary.getStatus().equalsIgnoreCase(StatusEnum.VERIFIED.code)) {
				logger.error("GAGAL MENGUBAH STATUS AKUN SEMENTARA : AKUN SUDAH DI VERIFIKASI");
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
						"Status akun dengan id [" + aprrovalDto.getId() + "] telah terverifikasi.");
			}

			memberTemporary.setStatus(aprrovalDto.getStatusCode());
			memberTemporary.setUpdatedDate(createdDate);
			memberTemporary.setUpdatedBy(userCreated);
			// memberTemporaryRepository.save(memberTemporary);

			history.setMemberTemporary(memberTemporary);
			history.setStatus(aprrovalDto.getStatusCode());
			history.setStatusDescription(status);
			history.setDescription("Admin melakukan approval [" + aprrovalDto.getStatusCode() + "]");
			history.setHistoryDate(createdDate);
			memberTemporaryHistoryRepository.save(history);

			if ("VER".equalsIgnoreCase(aprrovalDto.getStatusCode())) {
				if (Boolean.TRUE.equals(aprrovalDto.getIsDemand())) {
					saveAsDemand(memberTemporary, aprrovalDto);
				} else {
					saveAsSupplier(memberTemporary, aprrovalDto);
				}
			} else if ("REJ".equalsIgnoreCase(aprrovalDto.getStatusCode())) {
				Map<String, String> params = new HashMap<>();
				params.put(AsuransiKapalkuConstant.Email.SUBJECT, "Verifikasi Akun Asuransi Kapalku");
				params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, memberTemporary.getName());
				params.put(AsuransiKapalkuConstant.Email.TO, memberTemporary.getEmail());
				params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/rejection-account.html");

				emailService.sendMail(params);
			}
		} catch (Exception e) {
			isError = true;
			logger.error("GAGAL MENGUBAH STATUS AKUN SEMENTARA : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Status akun dengan id [" + aprrovalDto.getId() + "] gagal diubah.");
		} finally {
			if (isError) {
				memberTemporary.setStatus(statusOld);
				memberTemporary.setUpdatedDate(new Date());
				memberTemporary.setUpdatedBy(userCreated);
				memberTemporaryRepository.save(memberTemporary);

				history.setMemberTemporary(null);
				memberTemporaryHistoryRepository.save(history);
				memberTemporaryHistoryRepository.delete(history);
			}
		}
		return respon;
	}

	@Override
	public APIResponse<String> sendLinkMeeting(InvitationZoomDto invitationZoomDto, String userCreated) {
		APIResponse<String> respon = new APIResponse<>();
		Date createdDate = new Date();
		try {
			this.user = userCreated;
			String activityInvitation = "";
			Map<String, String> params = new HashMap<>();
			MemberTemporary memberTemporary = memberTemporaryRepository.findById(invitationZoomDto.getId())
					.orElse(null);

			if (null == memberTemporary) {
				logger.error("DATA MEMBER TEMPORARY TIDAK DITEMUKAN : {}", memberTemporary);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data tidak ditemukan");
			}

			if (Boolean.TRUE.equals(invitationZoomDto.getIsZoom())
					&& Boolean.FALSE.equals(invitationZoomDto.getIsRemainder())) {
				params.put(AsuransiKapalkuConstant.Email.SUBJECT,
						"Confirmation of Schedule for KYC Session with AsuransiKapalku.com");
				params.put(AsuransiKapalkuConstant.Email.SCHEDULE_MEET,
						FormatDateUtil.dateToStr(invitationZoomDto.getScheduleMeet()));
				params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/invitation-meeting.html");
				activityInvitation = "Admin mengirimkan konfimasi KYC via zoom kepada Klien";
			}

			if (Boolean.FALSE.equals(invitationZoomDto.getIsZoom())
					&& Boolean.FALSE.equals(invitationZoomDto.getIsRemainder())) {
				params.put(AsuransiKapalkuConstant.Email.SUBJECT,
						"Schedule Confirmation for KYC Session with AsuransiKapalku.com");
				params.put(AsuransiKapalkuConstant.Email.SCHEDULE_MEET,
						FormatDateUtil.dateToStr(invitationZoomDto.getScheduleMeet()));
				params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/invitation-meeting-vidcall.html");
				activityInvitation = "Admin mengirimkan konfimasi KYC via video call kepada Klien";
			}

			if (Boolean.TRUE.equals(invitationZoomDto.getIsZoom())
					&& Boolean.TRUE.equals(invitationZoomDto.getIsRemainder())) {
				params.put(AsuransiKapalkuConstant.Email.SUBJECT,
						"Confirmation of Schedule for KYC Session with AsuransiKapalku.com");
				params.put(AsuransiKapalkuConstant.Email.SCHEDULE_MEET,
						FormatDateUtil.dateToStr(invitationZoomDto.getScheduleMeet()));
				params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/invitation-meeting.html");
				activityInvitation = "Admin mengingatkan Klien atas KYC via zoom";
			}

			if (Boolean.FALSE.equals(invitationZoomDto.getIsZoom())
					&& Boolean.TRUE.equals(invitationZoomDto.getIsRemainder())) {
				params.put(AsuransiKapalkuConstant.Email.SUBJECT,
						"Schedule Confirmation for KYC Session with AsuransiKapalku.com");
				params.put(AsuransiKapalkuConstant.Email.SCHEDULE_MEET,
						FormatDateUtil.dateToStr(invitationZoomDto.getScheduleMeet()));
				params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/invitation-meeting-vidcall.html");
				activityInvitation = "Admin mengingatkan Klien atas KYC via video call";
			}

			params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, memberTemporary.getName());
			params.put(AsuransiKapalkuConstant.Email.ZOOM_LINK, invitationZoomDto.getLink());
			params.put(AsuransiKapalkuConstant.Email.TO, memberTemporary.getEmail());

			emailService.sendMail(params);

			memberTemporary.setScheduleMeet(invitationZoomDto.getScheduleMeet());
			memberTemporary.setStatus(StatusEnum.PENDING_VERIFICATION.code);
			memberTemporary.setUpdatedDate(createdDate);
			memberTemporary.setUpdatedBy(userCreated);

			MemberTemporaryHistory history = new MemberTemporaryHistory();
			history.setMemberTemporary(memberTemporary);
			history.setStatus(StatusEnum.PENDING_VERIFICATION.code);
			history.setStatusDescription(StatusEnum.PENDING_VERIFICATION.label);
			history.setDescription(activityInvitation);
			history.setHistoryDate(createdDate);

			// memberTemporaryRepository.save(memberTemporary);
			memberTemporaryHistoryRepository.save(history);

		} catch (Exception e) {
			logger.error("GAGAL MENGIRIMKAN LINK MEETING : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Link zoom gagal terkirim");
		}
		return respon;
	}

	@Override
	public APIResponse<MemberTemporaryDto> findById(Long id) {
		APIResponse<MemberTemporaryDto> respon = new APIResponse<>();
		try {
			MemberTemporary memberTemporary = memberTemporaryRepository.findById(id).orElse(null);
			if (memberTemporary == null) {
				logger.info("AKUN SEMENTARA TIDAK DITEMUKAN [BY ID] : {}", memberTemporary);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Akun dengan id [" + id + "] tidak ditemukan.");
			}
			respon.setData(convertEntityToDto(memberTemporary));
		} catch (Exception e) {
			logger.info("AKUN SEMENTARA TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Akun dengan id [" + id + "] tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			if (null == filterDto.getSorting()) {
				List<Sorting> sortings = new ArrayList<>();
				Sorting sorting = new Sorting();
				sorting.setColumnName("id");
				sorting.setIsAscending(false);
				sortings.add(sorting);
				filterDto.setSorting(sortings);
			}
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<MemberTemporary> memberTemporaryPage = memberTemporaryRepository
					.findAll(new Specification<MemberTemporary>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<MemberTemporary> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();

							predicates.add(criteriaBuilder.notEqual(root.get("status"), "VER"));

							if (null != filterDto.getSearchBy()) {
								Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate email = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("email")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate occupation = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("occupation")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate position = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("position")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate insuranceNeeds = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("insuranceNeeds")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate companyName = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("companyName")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate phoneNumber = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("phoneNumber")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								predicates.add(criteriaBuilder.or(name, email, occupation, position, insuranceNeeds,
										companyName, phoneNumber));
							}
							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<MemberTemporaryDto> memberTemporaries = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != memberTemporaryPage) {
				memberTemporaries = memberTemporaryPage.getContent().stream().map(this::convertEntityToDto)
						.collect(Collectors.toList());
				currentPage = memberTemporaryPage.getNumber();
				totalElement = memberTemporaryPage.getTotalElements();
				totalPage = memberTemporaryPage.getTotalPages();
			}

			dataRes.put("accounts", memberTemporaries);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST AKUN SEMENTARA : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List akun tidak ditemukan.");
		}
		return response;
	}

	@Override
	public APIResponse<List<Map<String, Object>>> findStatus() {
		APIResponse<List<Map<String, Object>>> respon = new APIResponse<>();
		List<Map<String, Object>> maps = new ArrayList<>();
		for (StatusEnum sse : StatusEnum.values()) {
			Map<String, Object> map = new HashMap<>();
			map.put("code", sse.code);
			map.put("name", sse.label);
			maps.add(map);
		}
		respon.setData(maps);
		return respon;
	}

	@Override
	public APIResponse<String> resetPassword(ResetPasswordDto resetPasswordDto, User user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			User userChanged = userRepository.findByUsername(resetPasswordDto.getUsername());
			LoginDto loginDto = new LoginDto();
			loginDto.setUsername(resetPasswordDto.getUsername());
			loginDto.setPassword(resetPasswordDto.getPassword());
			String name = "";
			String email = "";
			if (null != userChanged.getDemand()) {
				demandService.authenticateCheck(loginDto);
				userChanged.getDemand().setName(resetPasswordDto.getName());
				userChanged.getDemand()
						.setTnc(null == resetPasswordDto.getTnc() ? Boolean.FALSE : resetPasswordDto.getTnc());
				name = userChanged.getDemand().getName();
				email = userChanged.getDemand().getEmail();
			}
			if (null != userChanged.getSupplier()) {
				supplierService.authenticateCheck(loginDto);
				userChanged.getSupplier().setName(resetPasswordDto.getName());
				userChanged.getSupplier()
						.setTnc(null == resetPasswordDto.getTnc() ? Boolean.FALSE : resetPasswordDto.getTnc());
				name = userChanged.getSupplier().getName();
				email = userChanged.getSupplier().getEmail();
			}
			if (null != userChanged.getAdmin()) {
				adminService.authenticateCheck(loginDto);
				userChanged.getAdmin().setName(resetPasswordDto.getName());
				userChanged.getAdmin()
						.setTnc(null == resetPasswordDto.getTnc() ? Boolean.FALSE : resetPasswordDto.getTnc());
				name = userChanged.getAdmin().getName();
				email = userChanged.getAdmin().getEmail();
			}
			userChanged.setPassword(demandService.getBycryptPassword(resetPasswordDto.getNewPassword()));
			userChanged.setUpdatedBy(user.getUsername());
			userChanged.setUpdatedDate(new Date());

			Map<String, String> params = new HashMap<>();
			params.put(AsuransiKapalkuConstant.Email.SUBJECT, "UPDATES PASSWORD AsuransiKapalku.com");
			params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, name);
			params.put(AsuransiKapalkuConstant.Email.PASSWORD, resetPasswordDto.getNewPassword());
			params.put(AsuransiKapalkuConstant.Email.TO, email);
			params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/new-password.html");

			userRepository.save(userChanged);
		} catch (Exception e) {
			logger.error("PASSWORD GAGAL DIUBAH : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Password gagal diubah.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<String> delete(Long id) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			memberTemporaryRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("AKUN SEMENTARA GAGAL TERHAPUS : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal dihapus.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<String> forgotPassword(String email) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			Map<String, String> params = new HashMap<>();
			Admin admin = adminRepository.findByEmail(email);
			Demand demand = demandRepository.findByEmail(email);
			Supplier supplier = supplierRepository.findByEmail(email);
			if(null != admin) {
				params.put(AsuransiKapalkuConstant.Email.USERNAME, admin.getUser().getUsername());
				params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, admin.getName());
			} else if (null != demand) {
				params.put(AsuransiKapalkuConstant.Email.USERNAME, demand.getUser().getUsername());
				params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, demand.getName());
			} else if (null != supplier) {
				params.put(AsuransiKapalkuConstant.Email.USERNAME, supplier.getUser().getUsername());
				params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, supplier.getName());
			} else {
				logger.error("EMAIL TIDAK TERDAFTAR DALAM ASURANSI KAPALKU [LUPA PASSWORD] : {}", email);
				respon = new APIResponse<>(HttpStatus.BAD_REQUEST.value(), "Alamat email tidak terdaftar");
			}
			
			params.put(AsuransiKapalkuConstant.Email.SUBJECT, "Permintaan Reset Password Akun Asuransi Kapalku");
			params.put(AsuransiKapalkuConstant.Email.LINK, "https://staging.asuransikapalku.com/forgot-success/"
					+ EncryptionUtil.encryptUrl(EncryptionUtil.KEY, EncryptionUtil.INITVECTOR, email));
			params.put(AsuransiKapalkuConstant.Email.TO, email);
			params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/forgot.html");

			emailService.sendMail(params);
		} catch (Exception e) {
			logger.error("GAGAL MENGIRIMKAN LINK LUPA PASSWORD : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.BAD_REQUEST.value(), "Permintaan gagal.");
		}
		return respon;
	} 
	
	@Override
	public APIResponse<String> getNewPassword(String email) {
		Map<String, String> params = new HashMap<>();
		APIResponse<String> respon = new APIResponse<>();
		logger.info("EMAIL ENCRYPTION : {}", email);
		email = EncryptionUtil.decryptUrl(EncryptionUtil.KEY, EncryptionUtil.INITVECTOR, email);
		logger.info("EMAIL DECRYPTION : {}", email);
		Admin admin = adminRepository.findByEmail(email);
		Demand demand = demandRepository.findByEmail(email);
		Supplier supplier = supplierRepository.findByEmail(email);
		String password = PasswordUtil.generateRandomPassword(8);
		if(null != admin) {
			admin.getUser().setPassword(demandService.getBycryptPassword(password));
			admin.getUser().setUpdatedBy(email);
			admin.getUser().setUpdatedDate(new Date());
			params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, admin.getName());
		} else if (null != demand) {
			demand.getUser().setPassword(demandService.getBycryptPassword(password));
			demand.getUser().setUpdatedBy(email);
			demand.getUser().setUpdatedDate(new Date());
			params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, demand.getName());
		} else if (null != supplier) {
			supplier.getUser().setPassword(demandService.getBycryptPassword(password));
			supplier.getUser().setUpdatedBy(email);
			supplier.getUser().setUpdatedDate(new Date());
			params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, supplier.getName());
		} 
		
		params.put(AsuransiKapalkuConstant.Email.SUBJECT, "Permintaan Reset Password untuk Gudangemas");
		params.put(AsuransiKapalkuConstant.Email.PASSWORD, password);
		params.put(AsuransiKapalkuConstant.Email.TO, email);
		params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/new-password.html");

		try {
			if(null != admin) {
				adminRepository.save(admin);
			} else if (null != demand) {
				demandRepository.save(demand);
			} else if (null != supplier) {
				supplierRepository.save(supplier);
			}
			emailService.sendMail(params);
		} catch (Exception e) {
			logger.error("Gagal mendapatkan password baru : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.BAD_REQUEST.value(), e.getMessage());
		}
		return respon;
	}

	private MemberTemporary convertDtoToEntity(MemberTemporaryDto memberTemporaryDto) throws ParseException {
		MemberTemporary memberTemporary = modelMapper.map(memberTemporaryDto, MemberTemporary.class);
		if (memberTemporaryDto.getId() == null || memberTemporaryDto.getId() == 0) {
			memberTemporary.setCreatedBy(user);
			memberTemporary.setCreatedDate(new Date());
		} else {
			MemberTemporary memberTemporaryOld = memberTemporaryRepository.findById(memberTemporaryDto.getId())
					.orElse(null);
			if (memberTemporaryOld != null) {
				memberTemporary.setCreatedBy(memberTemporaryOld.getCreatedBy());
				memberTemporary.setCreatedDate(memberTemporaryOld.getCreatedDate());
			}
			memberTemporary.setUpdatedBy(user);
			memberTemporary.setUpdatedDate(new Date());
		}

		if (null != memberTemporaryDto.getScheduleMeet()) {
			logger.info("SCHEDULE MEET BEFORE CONVERT : {}", memberTemporaryDto.getScheduleMeet());
			memberTemporary.setScheduleMeet(
					FormatDateUtil.strToDate(memberTemporaryDto.getScheduleMeet(), "dd-MM-YYYY HH:mm:ss"));
			logger.info("SCHEDULE MEET AFTER CONVERT : {}", memberTemporary.getScheduleMeet());
		}

		return memberTemporary;
	}

	private MemberTemporaryDto convertEntityToDto(MemberTemporary memberTemporary) {
		MemberTemporaryDto memberTemporaryDto = modelMapper.map(memberTemporary, MemberTemporaryDto.class);
		if (null != memberTemporary.getScheduleMeet()) {
			memberTemporaryDto.setScheduleMeet(
					FormatDateUtil.dateToStr(memberTemporary.getScheduleMeet(), "dd-MM-YYYY HH:mm:ss"));
		}

		for (StatusEnum se : StatusEnum.values()) {
			if (se.code.equals(memberTemporary.getStatus())) {
				memberTemporaryDto.setStatus(se.label);
				break;
			}
		}
		memberTemporaryDto.setOccupationFromRegister(memberTemporary.getOccupation());
		memberTemporaryDto.setHistories(memberTemporary.getMemberTemporaryHistories().stream()
				.map(h -> modelMapper.map(h, MemberTemporaryHistoryDto.class)).collect(Collectors.toList()));
		return memberTemporaryDto;
	}

	private void saveAsDemand(MemberTemporary memberTemporary, AprrovalDto aprrovalDto) throws CommonException {
		DemandRegisterDto demandRegisterDto = new DemandRegisterDto();
		demandRegisterDto.setName(memberTemporary.getName());
		demandRegisterDto.setEmail(memberTemporary.getEmail());
		demandRegisterDto.setPhoneNumber(memberTemporary.getPhoneNumber());
		demandRegisterDto.setCompanyName(memberTemporary.getCompanyName());
		demandRegisterDto.setOccupation(aprrovalDto.getOccupation());
		demandRegisterDto.setPosition(memberTemporary.getPosition());
		demandRegisterDto.setInsuranceNeeds(memberTemporary.getInsuranceNeeds());

		APIResponse<String> respon = demandService.updateApprovalRegister(demandRegisterDto, user);

		if (HttpStatus.OK.value() != respon.getStatus()) {
			throw new CommonException(respon.getMessage().get(0));
		}
	}

	private void saveAsSupplier(MemberTemporary memberTemporary, AprrovalDto aprrovalDto) throws CommonException {
		SupplierRegisterDto supplierRegisterDto = new SupplierRegisterDto();
		supplierRegisterDto.setName(memberTemporary.getName());
		supplierRegisterDto.setEmail(memberTemporary.getEmail());
		supplierRegisterDto.setPhoneNumber(memberTemporary.getPhoneNumber());
		supplierRegisterDto.setCompanyName(memberTemporary.getCompanyName());
		supplierRegisterDto.setOccupation(aprrovalDto.getOccupation());
		supplierRegisterDto.setPosition(memberTemporary.getPosition());
		supplierRegisterDto.setInsuranceNeeds(memberTemporary.getInsuranceNeeds());

		APIResponse<String> respon = supplierService.updateApprovalRegister(supplierRegisterDto, user);

		if (HttpStatus.OK.value() != respon.getStatus()) {
			throw new CommonException(respon.getMessage().get(0));
		}
	}

	private void validationToRegister(RegisterDto registerDto) throws ValidateException {
		if (null == registerDto.getName()) {
			throw new ValidateException("Nama harus diisi.");
		}

		if (null == registerDto.getEmail()) {
			throw new ValidateException("Email harus diisi.");
		}

		APIResponse<DemandDto> responDemand = demandService.findByEmail(registerDto.getEmail());

		APIResponse<SupplierDto> responSupplier = supplierService.findByEmail(registerDto.getEmail());

		APIResponse<AdminDto> responAdmin = adminService.findByEmail(registerDto.getEmail());

		if (HttpStatus.OK.value() == responDemand.getStatus() && responDemand.getData() != null) {
			throw new ValidateException(EMAIL_EXISTS);
		}

		if (HttpStatus.OK.value() == responSupplier.getStatus() && responSupplier.getData() != null) {
			throw new ValidateException(EMAIL_EXISTS);
		}

		if (HttpStatus.OK.value() == responAdmin.getStatus() && responAdmin.getData() != null) {
			throw new ValidateException(EMAIL_EXISTS);
		}

		Integer count = memberTemporaryRepository.getCountByEmail(registerDto.getEmail());
		count = count == null ? 0 : count;
		if (count > 0) {
			throw new ValidateException(EMAIL_EXISTS);
		}
	}

	private void validationToUpdate(MemberTemporaryDto memberTemporaryDto) throws ValidateException {
		if (null == memberTemporaryDto.getName()) {
			throw new ValidateException("Nama harus diisi.");
		}

		if (null == memberTemporaryDto.getEmail()) {
			throw new ValidateException("Email harus diisi.");
		}

		APIResponse<DemandDto> responDemand = demandService.findByEmail(memberTemporaryDto.getEmail());

		APIResponse<SupplierDto> responSupplier = supplierService.findByEmail(memberTemporaryDto.getEmail());

		if (HttpStatus.OK.value() == responDemand.getStatus() && responDemand.getData() != null) {
			throw new ValidateException(EMAIL_EXISTS);
		}

		if (HttpStatus.OK.value() == responSupplier.getStatus() && responSupplier.getData() != null) {
			throw new ValidateException(EMAIL_EXISTS);
		}

		Integer count = memberTemporaryRepository.getCountByEmail(memberTemporaryDto.getEmail());
		count = count == null ? 0 : count;
		if ((null == memberTemporaryDto.getId() && count > 0) || (null != memberTemporaryDto.getId() && count > 1)) {
			throw new ValidateException(EMAIL_EXISTS);
		}
	}
	
	@Override
	public APIResponse<String> resetPasswordManual(ResetPasswordManualDto resetPasswordManualDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			User userToBeReset = userRepository.findByUsername(resetPasswordManualDto.getEmail());
			if(null == userToBeReset) {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data tidak ditemukan dalam database.");
			}
			
			String password = PasswordUtil.generateRandomPassword(8);
			userToBeReset.setPassword(demandService.getBycryptPassword(password));
			userToBeReset.setUpdatedDate(new Date());
			userToBeReset.setUpdatedBy("RESET PASSWORD MANUAL");
			userRepository.save(userToBeReset);
			
			respon.setData("Password baru untuk email : "+resetPasswordManualDto.getEmail()+" yaitu "+password);
		} catch (Exception e) {
			logger.error("Gagal mendapatkan password baru : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.BAD_REQUEST.value(), e.getMessage());
		}
		return respon;
	}
}
