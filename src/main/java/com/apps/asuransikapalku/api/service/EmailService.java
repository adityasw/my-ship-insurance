package com.apps.asuransikapalku.api.service;

import java.util.Map;

import com.apps.asuransikapalku.api.exception.CommonException;

public interface EmailService {
	String render(String template, Map<String, String> params);

	String renderByTemplate(String templatePath, String charset, Map<String, String> params) throws CommonException;

	void sendMail(Map<String, String> params) throws CommonException;
}
