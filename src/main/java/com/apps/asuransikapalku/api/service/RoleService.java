package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.user.RoleDto;

public interface RoleService {
	public APIResponse<String> save(RoleDto roleDto, String user);

	public APIResponse<String> updateStatus(Long id, boolean isActive, String user);

	public APIResponse<String> delete(Long id, String user);

	public APIResponse<String> deleteAll(List<Long> id, String user);

	public APIResponse<String> softDelete(Long id, String user);

	public APIResponse<String> softDeleteAll(List<Long> id, String user);
	
	public APIResponse<RoleDto> findById(Long id);

	public APIResponse<List<RoleDto>> findAllWithoutPageableAndActive();

	public APIResponse<Map<String, Object>> findAllRole(FilterDto filterDto);
}
