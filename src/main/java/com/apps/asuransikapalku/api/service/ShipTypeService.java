package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipTypeDto;
import com.apps.asuransikapalku.api.model.entity.ship.ShipType;

public interface ShipTypeService {

	public APIResponse<String> save(ShipTypeDto shipTypeDto, String user);

	public APIResponse<String> updateStatus(Long id, boolean isActive, String user);

	public APIResponse<String> delete(Long id, String user);

	public APIResponse<String> deleteAll(List<Long> id, String user);

	public APIResponse<String> softDelete(Long id, String user);

	public APIResponse<String> softDeleteAll(List<Long> id, String user);
	
	public APIResponse<ShipTypeDto> findById(Long id);

	public APIResponse<ShipTypeDto> findByCode(String code);

	public APIResponse<List<ShipTypeDto>> findAllWithoutPageableAndActive();

	public APIResponse<Map<String, Object>> findAllShipType(FilterDto filterDto);

	public ShipType saveShipType(ShipTypeDto shipTypeDto, String user) throws Exception;

}
