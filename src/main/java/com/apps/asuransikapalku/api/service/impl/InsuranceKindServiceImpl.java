package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceKindDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceTypeDto;
import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceKind;
import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceType;
import com.apps.asuransikapalku.api.repository.insurance.InsuranceKindRepository;
import com.apps.asuransikapalku.api.repository.insurance.InsuranceTypeRepository;
import com.apps.asuransikapalku.api.service.InsuranceKindService;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class InsuranceKindServiceImpl implements InsuranceKindService {
	private static final Logger logger = LoggerFactory.getLogger(InsuranceKindServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";
	
	private InsuranceKindRepository insuranceKindRepository;
	
	private InsuranceTypeRepository insuranceTypeRepository;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	private String user;
	
	@Autowired
	public void repository(InsuranceKindRepository insuranceKindRepository, InsuranceTypeRepository insuranceTypeRepository) {
		this.insuranceKindRepository = insuranceKindRepository;
		this.insuranceTypeRepository = insuranceTypeRepository;
	}

	@Override
	public APIResponse<String> save(InsuranceKindDto insuranceKindDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = user;
			this.validation(insuranceKindDto);
			if (null == insuranceKindDto.getId()) {
				Integer maxId = insuranceKindRepository.getMaxId();
				maxId = maxId == null ? 0 : maxId;
				insuranceKindDto.setCode("INS_KIND-" + (maxId + 1));
			}
			insuranceKindDto.setCode(insuranceKindDto.getCode().toUpperCase());
			insuranceKindRepository.save(convertDtoToEntity(insuranceKindDto));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA JENIS ASURANSI : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}
	
	@Override
	public APIResponse<String> updateStatus(Long id, boolean isActive, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			InsuranceKind insuranceKind = insuranceKindRepository.findById(id).orElse(null);
			insuranceKind.setIsActive(isActive);
			insuranceKind.setUpdatedBy(user);
			insuranceKind.setUpdatedDate(new Date());
			insuranceKindRepository.save(insuranceKind);
		} catch (Exception e) {
			logger.error("GAGAL MENGUBAH STATUS ACTIVE JENIS ASURANSI : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Status jenis asuransi dengan id [" + id + "] gagal diubah.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			insuranceKindRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA JENIS ASURANSI [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Jenis asuransi dengan id [" + id + "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			insuranceKindRepository.deleteAll(insuranceKindRepository.findByIdIn(ids));
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA JENIS ASURANSI [BATCH] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal menghapus data jenis asuransi dengan batch.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDelete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			InsuranceKind insuranceKind = insuranceKindRepository.findById(id).orElse(null);
			insuranceKind.setIsActive(Boolean.FALSE);
			insuranceKind.setUpdatedBy(user);
			insuranceKind.setUpdatedDate(new Date());
			insuranceKindRepository.save(insuranceKind);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA JENIS ASURANSI [SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Jenis asuransi dengan id [" + id + "] gagal terhapus");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDeleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<InsuranceKind> insuranceKinds = insuranceKindRepository.findByIdIn(ids);
			for(InsuranceKind i : insuranceKinds) {
				i.setIsActive(Boolean.FALSE);
				i.setUpdatedBy(user);
				i.setUpdatedDate(new Date());
			}
			insuranceKindRepository.deleteAll(insuranceKinds);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA Jenis ASURANSI [BATCH - SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal menghapus data jenis asuransi dengan batch.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<InsuranceKindDto> findById(Long id) {
		APIResponse<InsuranceKindDto> respon =new APIResponse<>();
		try {
			InsuranceKind insuranceKind = insuranceKindRepository.findById(id).orElse(null);
			if(null == insuranceKind) {
				logger.error("JENIS ASURANSI TIDAK DITEMUKAN [BY ID] : {}", insuranceKind);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Jenis asuransi tidak ditemukan.");
			}
			respon.setData(convertEntityToDto(insuranceKind));
		} catch (Exception e) {
			logger.error("JENIS ASURANSI TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Jenis asuransi tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<InsuranceKindDto> findByCode(String code) {
		APIResponse<InsuranceKindDto> respon =new APIResponse<>();
		try {
			InsuranceKind insuranceKind = insuranceKindRepository.findByCode(code);
			if(null == insuranceKind) {
				logger.error("JENIS ASURANSI TIDAK DITEMUKAN [BY CODE] : {}", insuranceKind);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Jenis asuransi tidak ditemukan.");
			}
			respon.setData(convertEntityToDto(insuranceKind));
		} catch (Exception e) {
			logger.error("JENIS ASURANSI TIDAK DITEMUKAN [BY CODE] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Jenis asuransi tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<List<InsuranceKindDto>> findAllWithoutPageableAndActiveByTypeId(Long id) {
		APIResponse<List<InsuranceKindDto>> respon = new APIResponse<>();
		try {
			InsuranceType insuranceType = insuranceTypeRepository.findById(id).orElse(null);
			List<InsuranceKind> insuranceKinds = insuranceKindRepository.findByInsuranceTypeAndIsActive(insuranceType, true);
			respon.setData(insuranceKinds.stream().map(this::convertEntityToDto).collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST JENIS ASURANSI : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"List jenis asuransi tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllInsuranceKind(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<InsuranceKind> insuranceKindPage = insuranceKindRepository.findAll(new Specification<InsuranceKind>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<InsuranceKind> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					Join<InsuranceKind,InsuranceType> insuranceTypeJoin = root.join("insuranceType");
					
					if(null != filterDto.getInsuranceTypeId()) {
						predicates.add(criteriaBuilder.equal(insuranceTypeJoin.get("id"), filterDto.getInsuranceTypeId()));
					}
					
					if (filterDto.getIsActive() != null && filterDto.getIsActive()) {
						predicates.add(criteriaBuilder.equal(root.get("isActive"), filterDto.getIsActive()));
					}
					
					if (null != filterDto.getSearchBy()) {
						Predicate code = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("code")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate description = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("description")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(code, name, description));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<InsuranceKindDto> insuranceKinds = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != insuranceKindPage) {
				insuranceKinds = insuranceKindPage.getContent().stream().map(this::convertEntityToDto)
						.collect(Collectors.toList());
				currentPage = insuranceKindPage.getNumber();
				totalElement = insuranceKindPage.getTotalElements();
				totalPage = insuranceKindPage.getTotalPages();
			}

			dataRes.put("insuranceKinds", insuranceKinds);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST JENIS ASURANSI : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List jenis asuransi tidak ditemukan.");
		}
		return response;
	}
	
	private InsuranceKind convertDtoToEntity(InsuranceKindDto insuranceKindDto) {
		InsuranceKind insuranceKind = modelMapper.map(insuranceKindDto, InsuranceKind.class);
		if (insuranceKindDto.getId() == null || insuranceKindDto.getId() == 0) {
			insuranceKind.setCreatedBy(user);
			insuranceKind.setCreatedDate(new Date());
		} else {
			InsuranceKind insuranceKindOld = insuranceKindRepository.findById(insuranceKindDto.getId()).orElse(null);
			if(insuranceKindOld != null) {
				insuranceKind.setCreatedBy(insuranceKindOld.getCreatedBy());
				insuranceKind.setCreatedDate(insuranceKindOld.getCreatedDate());
			}
			insuranceKind.setUpdatedBy(user);
			insuranceKind.setUpdatedDate(new Date());
		}
		
		return insuranceKind;
	}
	
	private InsuranceKindDto convertEntityToDto(InsuranceKind insuranceKind) {
		InsuranceKindDto insuranceKindDto = modelMapper.map(insuranceKind, InsuranceKindDto.class);
		insuranceKindDto.setInsuranceType(modelMapper.map(insuranceKind.getInsuranceType(), InsuranceTypeDto.class));
		return insuranceKindDto;
	}
	
	private void validation(InsuranceKindDto insuranceKindDto) throws ValidateException {
		if(null == insuranceKindDto.getName()) {
			throw new ValidateException("Nama jenis asuransi harus diisi.");
		}
		
		if(null == insuranceKindDto.getInsuranceType()) {
			throw new ValidateException("Tipe asuransi harus diisi.");
		}
		
		Integer count = insuranceKindRepository.getCountByName(insuranceKindDto.getName().toUpperCase());
		count = count == null ? 0 : count;
		if((null == insuranceKindDto.getId() && count > 0) || (null != insuranceKindDto.getId() && count > 1)) {
			throw new ValidateException("Nama jenis asuransi sudah digunakan.");
		}
	}
}
