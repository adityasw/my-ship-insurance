package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceTypeDto;

public interface InsuranceTypeService {

	public APIResponse<String> save(InsuranceTypeDto insuranceTypeDto, String user);

	public APIResponse<String> updateStatus(Long id, boolean isActive, String user);

	public APIResponse<String> delete(Long id, String user);

	public APIResponse<String> deleteAll(List<Long> id, String user);

	public APIResponse<String> softDelete(Long id, String user);

	public APIResponse<String> softDeleteAll(List<Long> id, String user);
	
	public APIResponse<InsuranceTypeDto> findById(Long id);
	
	public APIResponse<InsuranceTypeDto> findByCode(String code);

	public APIResponse<List<InsuranceTypeDto>> findAllWithoutPageableAndActive();

	public APIResponse<Map<String, Object>> findAllInsuranceType(FilterDto filterDto);

}
