package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.configs.TokenJwtConfig;
import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.exception.CommonException;
import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.master.CityDto;
import com.apps.asuransikapalku.api.model.dto.master.ProvinceDto;
import com.apps.asuransikapalku.api.model.dto.master.SubDistrictDto;
import com.apps.asuransikapalku.api.model.dto.user.AdminDto;
import com.apps.asuransikapalku.api.model.dto.user.AdminRegisterDto;
import com.apps.asuransikapalku.api.model.dto.user.LoginDto;
import com.apps.asuransikapalku.api.model.entity.user.Admin;
import com.apps.asuransikapalku.api.model.entity.user.Role;
import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.repository.master.CityRepository;
import com.apps.asuransikapalku.api.repository.master.ProvinceRepository;
import com.apps.asuransikapalku.api.repository.master.SubDistrictRepository;
import com.apps.asuransikapalku.api.repository.user.AdminRepository;
import com.apps.asuransikapalku.api.repository.user.RoleRepository;
import com.apps.asuransikapalku.api.repository.user.UserRepository;
import com.apps.asuransikapalku.api.service.AdminService;
import com.apps.asuransikapalku.api.service.EmailService;
import com.apps.asuransikapalku.api.utils.FileUtil;
import com.apps.asuransikapalku.api.utils.PasswordUtil;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class AdminServiceImpl implements AdminService {
	public static final Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";
	private static final String DIR_PROFILE = "dir.profile";
	private static final String PATH_ROOT = "path.root";
	private static final String URL = "asuransikapalku.url";

	private AdminRepository adminRepository;

	private RoleRepository roleRepository;

	private UserRepository userRepository;
	
	private ProvinceRepository provinceRepository;
	
	private CityRepository cityRepository;
	
	private SubDistrictRepository subDistrictRepository;

	private PasswordEncoder bcryptEncoder;
	
	private AuthenticationManager authenticationManager;
	
	private TokenJwtConfig tokenJwtConfig;
	
	private EmailService emailService;
	
	private Environment env;

	private ModelMapper modelMapper = new ModelMapper();

	private String user;

	@Autowired
	public void repository(AdminRepository adminRepository, UserRepository userRepository,
			RoleRepository roleRepository) {
		this.adminRepository = adminRepository;
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
	}
	
	@Autowired
	public void repository1(ProvinceRepository provinceRepository, CityRepository cityRepository,
			SubDistrictRepository subDistrictRepository) {
		this.provinceRepository = provinceRepository;
		this.cityRepository = cityRepository;
		this.subDistrictRepository = subDistrictRepository;
	}

	@Autowired
	public void serviceOther(EmailService emailService, PasswordEncoder bcryptEncoder, AuthenticationManager authenticationManager, TokenJwtConfig tokenJwtConfig) {
		this.bcryptEncoder = bcryptEncoder;
		this.authenticationManager = authenticationManager;
		this.tokenJwtConfig = tokenJwtConfig;
		this.emailService = emailService;
	}
	
	@Autowired
	public void serviceOther1(Environment env) {
		this.env = env;
	}
	
	@Override
	public APIResponse<Map<String, Object>> loginAdmin(LoginDto loginDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		Map<String, Object> map = new HashMap<>();
		try {
			authentication(loginDto);
			User user = userRepository.findByUsername(loginDto.getUsername());
			final String token = tokenJwtConfig.generateToken(new org.springframework.security.core.userdetails.User(
					user.getUsername(), user.getPassword(), new ArrayList<>()));
			user.setIsLogin(Boolean.TRUE);
			user.setLastLogin(new Date());
			user.setToken(token);
			userRepository.save(user);
			
			map.put("username", user.getUsername());
			map.put("token", user.getToken());
			
			respon.setData(map);
		} catch (Exception e) {
			logger.error("TIDAK BERHASIL LOGIN [ADMIN] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal login. Periksa kembali username dan password.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> registerAdmin(AdminRegisterDto adminRegisterDto, String userCreated) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.validationToRegister(adminRegisterDto);
			Role role = roleRepository.findByNameIgnoreCase("ADMIN");
			
			if (null == role) {
				logger.info("Role tidak ditemukan");
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan [Role tidak ditemukan]");
			}
			
			Date createdDate = new Date();
			String password = PasswordUtil.generateRandomPassword(8);
			
			Admin admin = new Admin();
			admin.setFirstName(adminRegisterDto.getFirstName());
			admin.setLastName(adminRegisterDto.getLastName());
			admin.setName(adminRegisterDto.getFirstName().trim()+ " " + adminRegisterDto.getLastName().trim());
			admin.setEmail(adminRegisterDto.getEmail());
			admin.setPhoneNumber(adminRegisterDto.getPhoneNumber());
			admin.setGender(adminRegisterDto.getGender());
			admin.setRole(role);
			admin.setCreatedDate(createdDate);
			admin.setCreatedBy(userCreated == null ? "SYSTEM" : userCreated);
			admin.setTnc(adminRegisterDto.getTnc());
			admin.setIsActive(Boolean.TRUE);
			
			if (null != adminRegisterDto.getPhoto()) {
				String dirInsurance = env.getProperty(PATH_ROOT) + env.getProperty(DIR_PROFILE);
//				admin.setPhoto(FileUtil.imgDecodeToDir(adminRegisterDto.getPhoto(), admin.getName(), dirInsurance,
//						AsuransiKapalkuConstant.FileTypeExtension.PNG, env.getProperty(URL) + env.getProperty(DIR_PROFILE)));
			}
			
			if(null != adminRegisterDto.getProvince()) {
				admin.setProvince(provinceRepository.findById(adminRegisterDto.getProvince().getId()).orElse(null));
			}
			
			if(null != adminRegisterDto.getCity()) {
				admin.setCity(cityRepository.findById(adminRegisterDto.getCity().getId()).orElse(null));
			}
			
			if(null != adminRegisterDto.getSubDistrict()) {
				admin.setSubDistrict(subDistrictRepository.findById(adminRegisterDto.getSubDistrict().getId()).orElse(null));
			}

			User user = new User();
			user.setUsername(adminRegisterDto.getEmail());
			user.setPassword(bcryptEncoder.encode(password));
			user.setCreatedDate(createdDate);
			user.setCreatedBy(userCreated == null ? "SYSTEM" : userCreated);
			admin.setUser(user);
			user.setAdmin(admin);
			
			Map<String, String> params = new HashMap<>();
			params.put(AsuransiKapalkuConstant.Email.SUBJECT, "Akun Asuransi Kapalku");
			params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, admin.getName());
			params.put(AsuransiKapalkuConstant.Email.USERNAME, admin.getEmail());
			params.put(AsuransiKapalkuConstant.Email.TO, admin.getEmail());
			params.put(AsuransiKapalkuConstant.Email.PASSWORD, password);
			params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/register-admin.html");
			
			emailService.sendMail(params);

			userRepository.save(user);

		} catch (Exception e) {
			logger.error("PENDAFTARAN ADMIN GAGAL : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> save(AdminDto adminDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = user;
			this.validationToUpdate(adminDto);
			adminRepository.save(convertDtoToEntity(adminDto));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA ADMIN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			adminRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA ADMIN [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Admin [" + id + "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<Admin> admins = adminRepository.findByIdIn(ids);
			adminRepository.deleteAll(admins);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA ADMIN [BATCH] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal menghapus data admin dengan batch.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> sofDelete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			Admin admin = adminRepository.findById(id).orElse(null);
			admin.setIsActive(Boolean.FALSE);
			admin.setUpdatedBy(user);
			admin.setUpdatedDate(new Date());
			adminRepository.save(admin);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA ADMIN [SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Admin [" + id + "] gagal terhapus");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDeleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<Admin> admins = adminRepository.findByIdIn(ids);
			for (Admin a : admins) {
				a.setIsActive(Boolean.FALSE);
				a.setUpdatedBy(user);
				a.setUpdatedDate(new Date());
			}
			adminRepository.deleteAll(admins);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA ADMIN [BATCH - SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal menghapus data admin dengan batch.");
		}
		return respon;
	}

	@Override
	public APIResponse<AdminDto> findById(Long id) {
		APIResponse<AdminDto> respon = new APIResponse<>();
		try {
			Admin admin = adminRepository.findById(id).orElse(null);
			if (admin == null) {
				logger.info("ADMIN TIDAK DITEMUKAN [BY ID] : {}", admin);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Admin dengan id [" + id + "] tidak ditemukan.");
			}
			respon.setData(convertEntityToDto(admin));
		} catch (Exception e) {
			logger.error("ADMIN TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Admin dengan id [" + id + "] tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<AdminDto> findByEmail(String email) {
		APIResponse<AdminDto> respon = new APIResponse<>();
		try {
			Admin admin = adminRepository.findByEmail(email);
			if (admin == null) {
				logger.error("ADMIN TIDAK DITEMUKAN [BY EMAIL] : {}", admin);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Admin dengan email [" + email + "] tidak ditemukan.");
			}
			respon.setData(convertEntityToDto(admin));
		} catch (Exception e) {
			logger.error("ADMIN TIDAK DITEMUKAN [BY EMAIL] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Admin dengan email [" + email + "] tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllAdmin(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<Admin> adminPage = adminRepository.findAll(new Specification<Admin>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Admin> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					if (filterDto.getIsActive() != null && filterDto.getIsActive()) {
						predicates.add(criteriaBuilder.equal(root.get("isActive"), filterDto.getIsActive()));
					}

					if (null != filterDto.getSearchBy()) {
						Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate email = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("email")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate title = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("title")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate username = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("username")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(name, email, title, username));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<AdminDto> admins = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != adminPage) {
				admins = adminPage.getContent().stream().map(this::convertEntityToDto).collect(Collectors.toList());
				currentPage = adminPage.getNumber();
				totalElement = adminPage.getTotalElements();
				totalPage = adminPage.getTotalPages();
			}

			dataRes.put("admins", admins);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST ADMIN : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List admin tidak ditemukan.");
		}
		return response;
	}

	private Admin convertDtoToEntity(AdminDto adminDto) throws CommonException {
		Admin admin = modelMapper.map(adminDto, Admin.class);
		if (adminDto.getId() == null || adminDto.getId() == 0) {
			admin.setCreatedBy(user);
			admin.setCreatedDate(new Date());
		} else {
			Admin adminOld = adminRepository.findById(adminDto.getId()).orElse(null);
			if (adminOld != null) {
				admin.setCreatedBy(adminOld.getCreatedBy());
				admin.setCreatedDate(adminOld.getCreatedDate());
			}
			admin.setUpdatedBy(user);
			admin.setUpdatedDate(new Date());
		}
		
		if (null != adminDto.getPhoto()) {
			String dirInsurance = env.getProperty(PATH_ROOT) + env.getProperty(DIR_PROFILE);
			admin.setPhoto(FileUtil.imgDecodeToDir(adminDto.getPhoto(), admin.getName(), dirInsurance,
					AsuransiKapalkuConstant.FileTypeExtension.PNG, env.getProperty(URL) + env.getProperty(DIR_PROFILE)));
		}
		
		if(null != adminDto.getProvince()) {
			admin.setProvince(provinceRepository.findById(adminDto.getProvince().getId()).orElse(null));
		}
		
		if(null != adminDto.getCity()) {
			admin.setCity(cityRepository.findById(adminDto.getCity().getId()).orElse(null));
		}
		
		if(null != adminDto.getSubDistrict()) {
			admin.setSubDistrict(subDistrictRepository.findById(adminDto.getSubDistrict().getId()).orElse(null));
		}

		return admin;
	}

	private AdminDto convertEntityToDto(Admin admin) {
		AdminDto adminDto = modelMapper.map(admin, AdminDto.class);
		if(null != admin.getProvince()) {
			adminDto.setProvince(modelMapper.map(admin.getProvince(), ProvinceDto.class));
		}
		
		if(null != admin.getCity()) {
			adminDto.setCity(modelMapper.map(admin.getCity(), CityDto.class));
		}
		
		if(null != admin.getSubDistrict()) {
			adminDto.setSubDistrict(modelMapper.map(admin.getSubDistrict(), SubDistrictDto.class));
		}
		return adminDto;
	}
	
	@Override
	public void authenticateCheck(LoginDto loginDto) throws ValidateException {
		authentication(loginDto);
	}
	
	private void authentication(LoginDto loginDto) throws ValidateException {
		if (loginDto.getUsername().contains("@")) {
			Admin admin = adminRepository.findByEmail(loginDto.getUsername());
			
			if(null == admin) {
				throw new ValidateException("Akun tidak ditemukan");
			}
			
			if(!admin.getUser().getIsActive()) {
				throw new ValidateException("Akun tidak aktif");
			}
			
			loginDto.setUsername(admin.getUser().getUsername());
		}	
		
		if (null == loginDto.getUsername()) {
			throw new ValidateException("Mohon masukkan username");
		}
		
		if (null == loginDto.getUsername()) {
			throw new ValidateException("Mohon masukkan password");
		}
		
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
		} catch (DisabledException e) {
			throw new ValidateException("Akun kamu tidak aktif.");
		} catch (BadCredentialsException e) {
			throw new ValidateException("Username atau password salah.");
		}
	}
	
	private void validationToRegister(AdminRegisterDto adminRegisterDto) throws ValidateException {
		if (null == adminRegisterDto.getFirstName()) {
			throw new ValidateException("Nama depan harus diisi.");
		}
		
		if (null == adminRegisterDto.getEmail()) {
			throw new ValidateException("Email harus diisi.");
		}
		
		Integer count = adminRepository.getCountByEmail(adminRegisterDto.getEmail());
		count = count == null ? 0 : count;
		if(count > 0) {
			throw new ValidateException("Email sudah digunakan.");
		}
	}	
	
	private void validationToUpdate(AdminDto adminDto) throws ValidateException {
		if (null == adminDto.getFirstName()) {
			throw new ValidateException("Nama depan harus diisi.");
		}
		
		if (null == adminDto.getEmail()) {
			throw new ValidateException("Email harus diisi.");
		}
		
		Integer count = adminRepository.getCountByEmail(adminDto.getEmail());
		count = count == null ? 0 : count;
		if((null == adminDto.getId() && count > 0) || (null != adminDto.getId() && count > 1)) {
			throw new ValidateException("Email sudah digunakan.");
		}
	}
}
