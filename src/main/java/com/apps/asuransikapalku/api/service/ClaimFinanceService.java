package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.UpdateStatusDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ApprovedRejectLtvDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceAccountDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceBankDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceChatDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceComissionDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceDocumentDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceInvoiceDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceLtvDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceMapFinancerDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceOfferingDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceQuotationDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceTrackingDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinancingDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.UpdateStatusOfferingDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewClaimFinanceChatDto;
import com.apps.asuransikapalku.api.model.entity.user.User;

public interface ClaimFinanceService {
	APIResponse<String> save(ClaimFinancingDto claimFinancingDto, String user);
	
	APIResponse<ClaimFinancingDto> findById(Long id);

	APIResponse<Map<String, Object>> findAllByFilter(FilterDto filterDto);

	APIResponse<String> uploadTC(ClaimFinanceDocumentDto claimFinanceDocumentDto, User user);

	APIResponse<String> givingOffering(ClaimFinanceOfferingDto claimFinanceOfferingDto);

	APIResponse<String> uploadQuotation(ClaimFinanceQuotationDto claimFinanceQuotationDto, User user);

	APIResponse<String> createTrack(ClaimFinanceTrackingDto claimFinanceTrackingDto);

	APIResponse<String> createLtv(ClaimFinanceLtvDto claimFinanceLtvDto);

	APIResponse<String> createInvoice(ClaimFinanceInvoiceDto claimFinanceInvoiceDto);

	APIResponse<String> createBank(ClaimFinanceBankDto claimFinanceBankDto);

	APIResponse<String> updateStatus(UpdateStatusDto updateStatusDto, User user);

	APIResponse<String> offeringResult(UpdateStatusOfferingDto dto);

	APIResponse<String> approvedRejectLtv(ApprovedRejectLtvDto dto);

	APIResponse<Map<String, Object>> findClaimFinanceDocument(FilterDto filterDto);

	APIResponse<Map<String, Object>> findClaimFinanceOffering(FilterDto filterDto);

	APIResponse<Map<String, Object>> findClaimFinanceTracking(FilterDto filterDto);

	APIResponse<Map<String, Object>> findClaimFinanceQuotation(FilterDto filterDto);

	APIResponse<Map<String, Object>> findClaimFinanceInvoice(FilterDto filterDto);

	APIResponse<Map<String, Object>> findClaimFinanceInvoiceBank(FilterDto filterDto);

	APIResponse<String> saveChat(ClaimFinanceChatDto dto, User user);

	APIResponse<String> deleteChat(Long id);

	APIResponse<List<ViewClaimFinanceChatDto>> findChatTabSupplier(Long idClaimFinance);

	APIResponse<List<ClaimFinanceChatDto>> findChatByIdClaimFinanceAndSenderReceiver(Long idClaimFinance,
			String senderOrReceiver);

	APIResponse<String> supplierOfferingDetail(ClaimFinanceAccountDto claimFinanceAccountDto);

	APIResponse<ClaimFinanceAccountDto> getSupplierOfferingDetail(Long idClaimFinance);

	APIResponse<List<Map<String, Object>>> findStatus();

	APIResponse<Map<String, Object>> findClaimFinanceComission(FilterDto filterDto);

	APIResponse<String> createComission(ClaimFinanceComissionDto claimFinanceComissionDto);

	APIResponse<String> assignFinancer(ClaimFinanceMapFinancerDto claimFinanceMapFinancerDto);

	APIResponse<ClaimFinanceMapFinancerDto> findClaimFinanceAssignee(Long idClaimFinance);

	APIResponse<Map<String, Object>> findAllByFilterForSup(FilterDto filterDto);
}
