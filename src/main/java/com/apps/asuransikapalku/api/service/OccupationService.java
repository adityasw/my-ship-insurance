package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.occupation.OccupationDto;

public interface OccupationService {
	public APIResponse<String> save(OccupationDto occupationDto, String user);

	public APIResponse<String> updateStatus(Long id, boolean isActive, String user);

	public APIResponse<String> delete(Long id, String user);

	public APIResponse<String> deleteAll(List<Long> id, String user);

	public APIResponse<String> softDelete(Long id, String user);

	public APIResponse<String> softDeleteAll(List<Long> id, String user);

	public APIResponse<OccupationDto> findById(Long id);

	public APIResponse<OccupationDto> findByCode(String code);

	public APIResponse<List<OccupationDto>> findAllWithoutPageableAndActive();

	public APIResponse<Map<String, Object>> findAllOccupation(FilterDto filterDto);

	public APIResponse<List<OccupationDto>> findByOccupationTypeId(Long id);
}
