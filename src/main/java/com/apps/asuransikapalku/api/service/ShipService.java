package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.ship.DetailVesselDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipApprovalDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipMasterDto;

public interface ShipService {
	public APIResponse<String> save(ShipMasterDto shipMasterDto, String user);
	
	public APIResponse<String> delete(Long id, String user);

	public APIResponse<String> deleteAll(List<Long> id, String user);

	public APIResponse<String> softDelete(Long id, String user);

	public APIResponse<String> softDeleteAll(List<Long> id, String user);
	
	public APIResponse<ShipMasterDto> findById(Long id);
	
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto);

	public APIResponse<String> approvalShip(ShipApprovalDto shipApprovalDto, String user);

	public APIResponse<List<Map<String, Object>>> findShipStatus();
	
	public APIResponse<String> updateDetailShip(DetailVesselDto detailVesselDto, String user);
	
	public APIResponse<Map<String, Object>> findDetailShipFromSupply(Long id, Long idSupplier);
	
	public APIResponse<Map<String, Object>> findDetailShipFromDemand(Long id);
}
