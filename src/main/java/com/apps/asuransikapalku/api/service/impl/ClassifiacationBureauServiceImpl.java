package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.master.ClassifiacationBureauDto;
import com.apps.asuransikapalku.api.model.entity.master.ClassificationBureau;
import com.apps.asuransikapalku.api.repository.master.ClassifiacationBureauRepository;
import com.apps.asuransikapalku.api.service.ClassifiacationBureauService;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class ClassifiacationBureauServiceImpl implements ClassifiacationBureauService {
	private static final Logger logger = LoggerFactory.getLogger(ClassifiacationBureauServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";

	private ClassifiacationBureauRepository classifiacationBureauRepository;

	private ModelMapper modelMapper = new ModelMapper();

	private String user;

	@Autowired
	public void repository(ClassifiacationBureauRepository classifiacationBureauRepository) {
		this.classifiacationBureauRepository = classifiacationBureauRepository;
	}

	@Override
	public APIResponse<String> save(ClassifiacationBureauDto classifiacationBureauDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = user;
			this.validation(classifiacationBureauDto);
			classifiacationBureauRepository.save(convertDtoToEntity(classifiacationBureauDto));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA BADAN KLASIFIKASI : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			classifiacationBureauRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA BADAN KLASIFIKASI [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Badan klasifikasi dengan id [" + id
					+ "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<ClassifiacationBureauDto> findById(Long id) {
		APIResponse<ClassifiacationBureauDto> respon = new APIResponse<>();
		try {
			ClassificationBureau classifiacationBureau = classifiacationBureauRepository.findById(id).orElse(null);
			if (classifiacationBureau == null) {
				logger.info("BADAN KLASIFIKASI TIDAK DITEMUKAN [BY ID] : {}", classifiacationBureau);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Badan klasifikasi dengan id [" + id + "] tidak ditemukan.");
			}
			respon.setData(modelMapper.map(classifiacationBureau, ClassifiacationBureauDto.class));
		} catch (Exception e) {
			logger.error("BADAN KLASIFIKASI TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Badan klasifikasi dengan id [" + id + "] tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<List<ClassifiacationBureauDto>> findAllWithoutPageable() {
		APIResponse<List<ClassifiacationBureauDto>> respon = new APIResponse<>();
		try {
			List<ClassificationBureau> classifiacationBureaus = classifiacationBureauRepository.findAll();
			respon.setData(classifiacationBureaus.stream().map(r -> modelMapper.map(r, ClassifiacationBureauDto.class))
					.collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST BADAN KLASIFIKASI : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "List badan klasifikasi tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ClassificationBureau> adminPage = classifiacationBureauRepository
					.findAll(new Specification<ClassificationBureau>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ClassificationBureau> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();

							if (null != filterDto.getSearchBy()) {
								Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								predicates.add(criteriaBuilder.or(name));
							}
							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ClassifiacationBureauDto> classifiacationBureaus = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != adminPage) {
				classifiacationBureaus = adminPage.getContent().stream()
						.map(r -> modelMapper.map(r, ClassifiacationBureauDto.class)).collect(Collectors.toList());
				currentPage = adminPage.getNumber();
				totalElement = adminPage.getTotalElements();
				totalPage = adminPage.getTotalPages();
			}

			dataRes.put("classifiacationBureaus", classifiacationBureaus);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST BADAN KLASIFIKASI : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List badan klasifikasi tidak ditemukan.");
		}
		return response;
	}

	private ClassificationBureau convertDtoToEntity(ClassifiacationBureauDto classifiacationBureauDto) {
		ClassificationBureau classifiacationBureau = modelMapper.map(classifiacationBureauDto,
				ClassificationBureau.class);
		if (classifiacationBureauDto.getId() == null || classifiacationBureauDto.getId() == 0) {
			classifiacationBureau.setCreatedBy(user);
			classifiacationBureau.setCreatedDate(new Date());
		} else {
			ClassificationBureau classifiacationBureauOld = classifiacationBureauRepository
					.findById(classifiacationBureauDto.getId()).orElse(null);
			if (classifiacationBureauOld != null) {
				classifiacationBureau.setCreatedBy(classifiacationBureauOld.getCreatedBy());
				classifiacationBureau.setCreatedDate(classifiacationBureauOld.getCreatedDate());
			}
			classifiacationBureau.setUpdatedBy(user);
			classifiacationBureau.setUpdatedDate(new Date());
		}

		return classifiacationBureau;
	}
	
	private void validation(ClassifiacationBureauDto classifiacationBureauDto) throws ValidateException {
		if(null == classifiacationBureauDto.getName()) {
			throw new ValidateException("Nama badan klasifikasi harus diisi.");
		}
		
		Integer count = classifiacationBureauRepository.getCountByName(classifiacationBureauDto.getName().toUpperCase());
		count = count == null ? 0 : count;
		if((null == classifiacationBureauDto.getId() && count > 0) || (null != classifiacationBureauDto.getId() && count > 1)) {
			throw new ValidateException("Nama role sudah digunakan.");
		}
	}
}
