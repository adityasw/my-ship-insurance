package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.configs.TokenJwtConfig;
import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.user.DemandDto;
import com.apps.asuransikapalku.api.model.dto.user.DemandRegisterDto;
import com.apps.asuransikapalku.api.model.dto.user.LoginDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewInsuranceDemDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewInsuranceInprogressDemDto;
import com.apps.asuransikapalku.api.model.entity.user.Demand;
import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.model.entity.view.ViewInsuranceDem;
import com.apps.asuransikapalku.api.model.entity.view.ViewInsuranceInprogressDem;
import com.apps.asuransikapalku.api.repository.occupation.OccupationRepository;
import com.apps.asuransikapalku.api.repository.user.DemandRepository;
import com.apps.asuransikapalku.api.repository.user.UserRepository;
import com.apps.asuransikapalku.api.repository.view.ViewInsuranceDemRepository;
import com.apps.asuransikapalku.api.repository.view.ViewInsuranceInprogressDemRepository;
import com.apps.asuransikapalku.api.service.DemandService;
import com.apps.asuransikapalku.api.service.EmailService;
import com.apps.asuransikapalku.api.utils.PasswordUtil;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class DemandServiceImpl implements DemandService {
	public static final Logger logger = LoggerFactory.getLogger(DemandServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";

	private DemandRepository demandRepository;
	
	private OccupationRepository occupationRepository;

	private UserRepository userRepository;
	
	private ViewInsuranceInprogressDemRepository insuranceInprogressDemRepository;
	
	private ViewInsuranceDemRepository viewInsuranceDemRepository;

	private PasswordEncoder bcryptEncoder;

	private AuthenticationManager authenticationManager;

	private TokenJwtConfig tokenJwtConfig;

	private EmailService emailService;

	private ModelMapper modelMapper = new ModelMapper();

	private String user;

	@Autowired
	public void repository(DemandRepository demandRepository, OccupationRepository occupationRepository, UserRepository userRepository, ViewInsuranceInprogressDemRepository insuranceInprogressDemRepository) {
		this.demandRepository = demandRepository;
		this.occupationRepository = occupationRepository;
		this.userRepository = userRepository;
		this.insuranceInprogressDemRepository = insuranceInprogressDemRepository;
	}
	
	@Autowired
	public void repository1(ViewInsuranceDemRepository viewInsuranceDemRepository) {
		this.viewInsuranceDemRepository = viewInsuranceDemRepository;
	}

	@Autowired
	public void serviceOther(PasswordEncoder bcryptEncoder, AuthenticationManager authenticationManager,
			TokenJwtConfig tokenJwtConfig, EmailService emailService) {
		this.bcryptEncoder = bcryptEncoder;
		this.authenticationManager = authenticationManager;
		this.tokenJwtConfig = tokenJwtConfig;
		this.emailService = emailService;
	}

	@Override
	public APIResponse<Map<String, Object>> loginDemand(LoginDto loginDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		Map<String, Object> map = new HashMap<>();
		boolean completeInformationShip = true;
		try {
			authentication(loginDto);
			
			User user = userRepository.findByUsername(loginDto.getUsername());
			
			if(user.getDemand() == null) {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Demand tidak ditemukan");
			}
			
			if(user.getDemand().getShipMaster().isEmpty()) {
				completeInformationShip = false;
			}
			
			final String token = tokenJwtConfig.generateToken(new org.springframework.security.core.userdetails.User(
					user.getUsername(), user.getPassword(), new ArrayList<>()));
			user.setIsLogin(Boolean.TRUE);
			user.setLastLogin(new Date());
			user.setToken(token);
			userRepository.save(user);

			map.put("id-demand", user.getDemand().getId());
			map.put("username", user.getUsername());
			map.put("token", user.getToken());
			map.put("login-as", "DEMAND");
			map.put("completed-information-ship-flag", completeInformationShip);
			
			respon.setData(map);
		} catch (Exception e) {
			logger.error("TIDAK BERHASIL LOGIN [DEMAND]: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal login. Periksa kembali username dan password.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> registerDemand(DemandRegisterDto demandRegisterDto, String userCreated) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.validationToRegister(demandRegisterDto);
			Date createdDate = new Date();

			Demand demand = new Demand();
			demand.setName(demandRegisterDto.getName());
			demand.setEmail(demandRegisterDto.getEmail());
			demand.setPhoneNumber(demandRegisterDto.getPhoneNumber());
			demand.setCompanyName(demandRegisterDto.getCompanyName());
			demand.setPosition(demandRegisterDto.getPosition());
			demand.setInsuranceNeeds(demandRegisterDto.getInsuranceNeeds());
			demand.setCreatedDate(createdDate);
			demand.setCreatedBy(userCreated == null ? "SYSTEM" : userCreated);

			User user = new User();
			user.setUsername(demandRegisterDto.getUsername());
			user.setCreatedDate(createdDate);
			user.setCreatedBy(userCreated == null ? "SYSTEM" : userCreated);
			demand.setUser(user);
			user.setDemand(demand);
			user.setIsActive(Boolean.FALSE);

			userRepository.save(user);
			
			Map<String, String> params = new HashMap<>();
			params.put(AsuransiKapalkuConstant.Email.SUBJECT, "Akun Asuransi Kapalku Anda telah dibuat");
			params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, demand.getName());
			//params.put(AsuransiKapalkuConstant.Email.NO_REPLY_SENDER, AsuransiKapalkuConstant.Email.NO_REPLY_SENDER_VALUE);
			params.put(AsuransiKapalkuConstant.Email.TO, demand.getEmail());
			params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/confirmation-sign-up.html");
			
			emailService.sendMail(params);
		} catch (Exception e) {
			logger.error("PENDAFTARAN DEMAND GAGAL : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> updateApprovalRegister(DemandRegisterDto demandRegisterDto, String userCreated) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			Date createdDate = new Date();
			String password = PasswordUtil.generateRandomPassword(8);

			Demand demand = new Demand();
			demand.setName(demandRegisterDto.getName());
			demand.setEmail(demandRegisterDto.getEmail());
			demand.setPhoneNumber(demandRegisterDto.getPhoneNumber());
			demand.setOccupation(occupationRepository.findById(demandRegisterDto.getOccupation().getId()).orElse(null));
			demand.setCompanyName(demandRegisterDto.getCompanyName());
			demand.setPosition(demandRegisterDto.getPosition());
			demand.setInsuranceNeeds(demandRegisterDto.getInsuranceNeeds());
			demand.setCreatedDate(createdDate);
			demand.setCreatedBy(userCreated == null ? "SYSTEM" : userCreated);

			User user = new User();
			user.setUsername(demandRegisterDto.getUsername()==null?demandRegisterDto.getEmail():demandRegisterDto.getUsername());
			user.setPassword(bcryptEncoder.encode(password));
			user.setCreatedDate(createdDate);
			user.setCreatedBy(userCreated == null ? "SYSTEM" : userCreated);
			demand.setUser(user);
			user.setDemand(demand);

			Map<String, String> params = new HashMap<>();
			params.put(AsuransiKapalkuConstant.Email.SUBJECT, "CONGRATULATIONS! You are part of AsuransiKapalku.com Network");
			params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, demand.getName());
			params.put(AsuransiKapalkuConstant.Email.USERNAME, user.getUsername());
			params.put(AsuransiKapalkuConstant.Email.TO, demand.getEmail());
			params.put(AsuransiKapalkuConstant.Email.PASSWORD, password);
			params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/approval-account-demand.html");
			
			emailService.sendMail(params);
			
			userRepository.save(user);
		} catch (Exception e) {
			logger.error("APPROVAL DEMAND GAGAL : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal update status approval.");
		}
		return respon;
	}
	
	@Override
	public String getBycryptPassword(String password) {
		return bcryptEncoder.encode(password);
	}
	
	@Override
	public APIResponse<Map<String, Object>> findAllShipBind(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewInsuranceDem> page = viewInsuranceDemRepository.findAll(new Specification<ViewInsuranceDem>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ViewInsuranceDem> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					
					if (filterDto.getUsername() != null) {
						predicates.add(criteriaBuilder.equal(root.get("username"), filterDto.getUsername()));
					}

					if (null != filterDto.getSearchBy()) {
						Predicate vesselName = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("vesselName")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate vesselType = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("vesselType")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate insuranceType = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("insuranceType")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate insuranceKind = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("insuranceKind")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate status = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("status")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(vesselName, vesselType, insuranceType, insuranceKind, status));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<ViewInsuranceDemDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(p -> modelMapper.map(p, ViewInsuranceDemDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			dataRes.put("ships", list);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST DATA KAPAL ASURANSIKU [DEMAND] : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List kapal tidak ditemukan.");
		}
		return response;
	}
	
	@Override
	public APIResponse<Map<String, Object>> findAllShipInprogress(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewInsuranceInprogressDem> page = insuranceInprogressDemRepository.findAll(new Specification<ViewInsuranceInprogressDem>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ViewInsuranceInprogressDem> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					
					if (filterDto.getUsername() != null) {
						predicates.add(criteriaBuilder.equal(root.get("username"), filterDto.getUsername()));
					}

					if (null != filterDto.getSearchBy()) {
						Predicate vesselName = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("vesselName")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate vesselType = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("vesselType")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate insuranceType = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("insuranceType")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate insuranceKind = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("insuranceKind")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate status = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("status")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(vesselName, vesselType, insuranceType, insuranceKind, status));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<ViewInsuranceInprogressDemDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(p -> modelMapper.map(p, ViewInsuranceInprogressDemDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			dataRes.put("ships", list);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST DATA KAPAL INPROGRESS [DEMAND] : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List kapal tidak ditemukan.");
		}
		return response;
	}

	@Override
	public APIResponse<String> save(DemandDto demandDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = user;
			this.validationToUpdate(demandDto);
			demandRepository.save(convertDtoToEntity(demandDto));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA DEMAND : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			demandRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA DEMAND [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Demand [" + id + "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<Demand> demands = demandRepository.findByIdIn(ids);
			demandRepository.deleteAll(demands);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA DEMAND [BATCH] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal menghapus data demand dengan batch.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDelete(Long id, String user) {
		return null;
	}

	@Override
	public APIResponse<String> softDeleteAll(List<Long> id, String user) {
		return null;
	}

	@Override
	public APIResponse<DemandDto> findById(Long id) {
		APIResponse<DemandDto> respon = new APIResponse<>();
		try {
			Demand demand = demandRepository.findById(id).orElse(null);
			if (demand == null) {
				logger.info("DEMAND TIDAK DITEMUKAN [BY ID] : {}", demand);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Demand dengan id [" + id + "] tidak ditemukan.");
			}
			respon.setData(convertEntityToDto(demand));
		} catch (Exception e) {
			logger.info("DEMAND TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Demand dengan id [" + id + "] tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<DemandDto> findByEmail(String email) {
		APIResponse<DemandDto> respon = new APIResponse<>();
		try {
			Demand demand = demandRepository.findByEmail(email);
			if (demand == null) {
				logger.error("DEMAND TIDAK DITEMUKAN [BY EMAIL] : {}", demand);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Demand dengan email [" + email + "] tidak ditemukan.");
			}
			respon.setData(convertEntityToDto(demand));
		} catch (Exception e) {
			logger.error("DEMAND TIDAK DITEMUKAN [BY EMAIL] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Demand dengan email [" + email + "] tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<Demand> demandPage = demandRepository.findAll(new Specification<Demand>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Demand> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					Join<Demand, User> demandJoin = root.join("user");

					if (null != filterDto.getSearchBy()) {
						Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate email = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("email")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate username = criteriaBuilder.like(
								criteriaBuilder.upper(demandJoin.<String>get("username")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate occupation = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("occupation")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate position = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("position")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate insuranceNeeds = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("insuranceNeeds")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate companyName = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("companyName")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate phoneNumber = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("phoneNumber")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(name, email, username, occupation, position, insuranceNeeds,
								companyName, phoneNumber));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<DemandDto> demands = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != demandPage) {
				demands = demandPage.getContent().stream().map(this::convertEntityToDto).collect(Collectors.toList());
				currentPage = demandPage.getNumber();
				totalElement = demandPage.getTotalElements();
				totalPage = demandPage.getTotalPages();
			}

			dataRes.put("demands", demands);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST DEMAND : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List demand tidak ditemukan.");
		}
		return response;
	}
	
	@Override
	public void authenticateCheck(LoginDto loginDto) throws ValidateException {
		authentication(loginDto);
	}

	private Demand convertDtoToEntity(DemandDto demandDto) {
		Demand demand = modelMapper.map(demandDto, Demand.class);
		if (demandDto.getId() == null || demandDto.getId() == 0) {
			demand.setCreatedBy(user);
			demand.setCreatedDate(new Date());
		} else {
			Demand demandOld = demandRepository.findById(demandDto.getId()).orElse(null);
			if (demandOld != null) {
				demand.setCreatedBy(demandOld.getCreatedBy());
				demand.setCreatedDate(demandOld.getCreatedDate());
			}
			demand.setUpdatedBy(user);
			demand.setUpdatedDate(new Date());
		}

		return demand;
	}

	private DemandDto convertEntityToDto(Demand demand) {
		DemandDto demandDto = modelMapper.map(demand, DemandDto.class);
		demandDto.setUsername(demand.getUser().getUsername());
		return demandDto;
	}

	private void authentication(LoginDto loginDto) throws ValidateException {
		if (loginDto.getUsername().contains("@")) {
			Demand demand = demandRepository.findByEmail(loginDto.getUsername());

			if (null == demand) {
				throw new ValidateException("Akun tidak ditemukan");
			}

			if (!demand.getUser().getIsActive()) {
				throw new ValidateException("Akun tidak aktif");
			}

			loginDto.setUsername(demand.getUser().getUsername());
		}

		if (null == loginDto.getUsername()) {
			throw new ValidateException("Mohon masukkan username");
		}

		if (null == loginDto.getPassword()) {
			throw new ValidateException("Mohon masukkan password");
		}

		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
		} catch (DisabledException e) {
			throw new ValidateException("Akun kamu tidak aktif.");
		} catch (BadCredentialsException e) {
			throw new ValidateException("Username atau password salah.");
		}
	}

	private void validationToRegister(DemandRegisterDto demandRegisterDto) throws ValidateException {
		if (null == demandRegisterDto.getName()) {
			throw new ValidateException("Nama harus diisi.");
		}
		
		if (null == demandRegisterDto.getEmail()) {
			throw new ValidateException("Email harus diisi.");
		}
		
		if (null == demandRegisterDto.getUsername()) {
			throw new ValidateException("Username harus diisi.");
		}
		
		Integer count = demandRepository.getCountByEmail(demandRegisterDto.getEmail());
		count = count == null ? 0 : count;
		if(count > 0) {
			throw new ValidateException("Email sudah digunakan.");
		}
	}	
	
	private void validationToUpdate(DemandDto demandDto) throws ValidateException {
		if (null == demandDto.getName()) {
			throw new ValidateException("Nama harus diisi.");
		}
		
		if (null == demandDto.getEmail()) {
			throw new ValidateException("Email harus diisi.");
		}
		
		if (null == demandDto.getUsername()) {
			throw new ValidateException("Username harus diisi.");
		}
		
		Integer count = demandRepository.getCountByEmail(demandDto.getEmail());
		count = count == null ? 0 : count;
		if((null == demandDto.getId() && count > 0) || (null != demandDto.getId() && count > 1)) {
			throw new ValidateException("Email sudah digunakan.");
		}
	}
}
