package com.apps.asuransikapalku.api.service;

import java.util.List;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.master.CityDto;
import com.apps.asuransikapalku.api.model.dto.master.ProvinceDto;
import com.apps.asuransikapalku.api.model.dto.master.SubDistrictDto;

public interface RegionService {
	APIResponse<List<ProvinceDto>> findAllProvince();
	
	APIResponse<List<CityDto>> findByProvinceId(String id);
	
	APIResponse<List<SubDistrictDto>> findByCityId(String id);
}
