package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.AprrovalDto;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.user.InvitationZoomDto;
import com.apps.asuransikapalku.api.model.dto.user.MemberTemporaryDto;
import com.apps.asuransikapalku.api.model.dto.user.RegisterDto;
import com.apps.asuransikapalku.api.model.dto.user.ResetPasswordDto;
import com.apps.asuransikapalku.api.model.dto.user.ResetPasswordManualDto;
import com.apps.asuransikapalku.api.model.entity.user.User;

public interface MemberTemporaryService {
	public APIResponse<String> register(RegisterDto registerDto, String userCreated);
	
	public APIResponse<String> save(MemberTemporaryDto memberTemporaryDto, String userCreated); 
	
	public APIResponse<String> delete(Long id);
	
	public APIResponse<String> updateStatus(AprrovalDto aprrovalDto, String userCreated); 
	
	public APIResponse<String> sendLinkMeeting(InvitationZoomDto invitationZoomDto, String userCreated);
	
	public APIResponse<MemberTemporaryDto> findById(Long id);
	
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto);

	public APIResponse<List<Map<String, Object>>> findStatus();
	
	public APIResponse<String> resetPassword(ResetPasswordDto resetPasswordDto, User user);
	
	public APIResponse<String> forgotPassword(String email);
	
	public APIResponse<String> resetPasswordManual(ResetPasswordManualDto resetPasswordManualDto);
	public APIResponse<String> getNewPassword(String email);
}
