package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceMasterDto;

public interface InsuranceMasterService {
	public APIResponse<String> save(InsuranceMasterDto insuranceMasterDto, String user);
	
	public APIResponse<String> delete(Long id, String user);

	public APIResponse<String> deleteAll(List<Long> id, String user);

	public APIResponse<String> softDelete(Long id, String user);

	public APIResponse<String> softDeleteAll(List<Long> id, String user);

	public APIResponse<InsuranceMasterDto> findById(Long id);
	
	public APIResponse<List<InsuranceMasterDto>> findAllWithoutPaging();
	
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto);
}
