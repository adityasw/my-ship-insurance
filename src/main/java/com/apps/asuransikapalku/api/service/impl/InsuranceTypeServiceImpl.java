package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceTypeDto;
import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceType;
import com.apps.asuransikapalku.api.repository.insurance.InsuranceTypeRepository;
import com.apps.asuransikapalku.api.service.InsuranceTypeService;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class InsuranceTypeServiceImpl implements InsuranceTypeService {
	private static final Logger logger = LoggerFactory.getLogger(InsuranceTypeServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";
	
	private InsuranceTypeRepository insuranceTypeRepository;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	private String user;
	
	@Autowired
	public void repository(InsuranceTypeRepository insuranceTypeRepository) {
		this.insuranceTypeRepository = insuranceTypeRepository;
	}

	@Override
	public APIResponse<String> save(InsuranceTypeDto insuranceTypeDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = user;
			this.validation(insuranceTypeDto);
			if(null == insuranceTypeDto.getId()) {
				Integer maxId = insuranceTypeRepository.getMaxId();
				maxId = maxId == null ? 0 : maxId;
				insuranceTypeDto.setCode("INS_TYPE-"+(maxId+1));
			}
			insuranceTypeDto.setCode(insuranceTypeDto.getCode().toUpperCase());
			insuranceTypeRepository.save(convertDtoToEntity(insuranceTypeDto));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA TIPE ASURANSI : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> updateStatus(Long id, boolean isActive, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			InsuranceType insuranceType = insuranceTypeRepository.findById(id).orElse(null);
			insuranceType.setIsActive(isActive);
			insuranceType.setUpdatedBy(user);
			insuranceType.setUpdatedDate(new Date());
			insuranceTypeRepository.save(insuranceType);
		} catch (Exception e) {
			logger.error("GAGAL MENGUBAH STATUS ACTIVE TIPE ASURANSI : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Status tipe asuransi dengan id [" + id + "] gagal diubah.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			insuranceTypeRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA TIPE ASURANSI [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Tipe asuransi dengan id [" + id + "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<InsuranceType> insuranceTypes = insuranceTypeRepository.findByIdIn(ids);
			insuranceTypeRepository.deleteAll(insuranceTypes);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA TIPE ASURANSI [BATCH] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal menghapus data tipe asuransi dengan batch.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDelete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			InsuranceType insuranceType = insuranceTypeRepository.findById(id).orElse(null);
			insuranceType.setIsActive(Boolean.FALSE);
			insuranceType.setUpdatedBy(user);
			insuranceType.setUpdatedDate(new Date());
			insuranceTypeRepository.save(insuranceType);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA TIPE ASURANSI [SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Tipe asuransi dengan id [" + id + "] gagal terhapus");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDeleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<InsuranceType> insuranceTypes = insuranceTypeRepository.findByIdIn(ids);
			for(InsuranceType i : insuranceTypes) {
				i.setIsActive(Boolean.FALSE);
				i.setUpdatedBy(user);
				i.setUpdatedDate(new Date());
			}
			insuranceTypeRepository.deleteAll(insuranceTypes);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA TIPE ASURANSI [BATCH - SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal menghapus data tipe asuransi dengan batch.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<InsuranceTypeDto> findById(Long id) {
		APIResponse<InsuranceTypeDto> respon =new APIResponse<>();
		try {
			InsuranceType insuranceType = insuranceTypeRepository.findById(id).orElse(null);
			if(insuranceType == null) {
				logger.info("JENIS ASURANSI TIDAK DITEMUKAN [BY ID] : {}", insuranceType);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Jenis asuransi tidak ditemukan.");
			}
			respon.setData(modelMapper.map(insuranceType, InsuranceTypeDto.class));
		} catch (Exception e) {
			logger.error("TIPE ASURANSI TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Tipe asuransi tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<InsuranceTypeDto> findByCode(String code) {
		APIResponse<InsuranceTypeDto> respon =new APIResponse<>();
		try {
			InsuranceType insuranceType = insuranceTypeRepository.findByCode(code);
			if(insuranceType == null) {
				logger.info("JENIS ASURANSI TIDAK DITEMUKAN [BY CODE] : {}", insuranceType);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Jenis asuransi tidak ditemukan.");
			}
			respon.setData(modelMapper.map(insuranceType, InsuranceTypeDto.class));
		} catch (Exception e) {
			logger.error("Tipe ASURANSI TIDAK DITEMUKAN [BY CODE] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Tipe asuransi tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<List<InsuranceTypeDto>> findAllWithoutPageableAndActive() {
		APIResponse<List<InsuranceTypeDto>> respon = new APIResponse<>();
		try {
			List<InsuranceType> insuranceTypes = insuranceTypeRepository.findByIsActive(true);
			
			respon.setData(insuranceTypes.stream().map(r -> modelMapper.map(r, InsuranceTypeDto.class)).collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST TIPE ASURANSI : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"List tipe asuransi tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllInsuranceType(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<InsuranceType> insuranceTypePage = insuranceTypeRepository.findAll(new Specification<InsuranceType>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<InsuranceType> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					
					if (filterDto.getIsActive() != null && filterDto.getIsActive()) {
						predicates.add(criteriaBuilder.equal(root.get("isActive"), filterDto.getIsActive()));
					}
					
					if (null != filterDto.getSearchBy()) {
						Predicate code = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("code")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate description = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("description")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(code, name, description));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<InsuranceTypeDto> insuranceTypes = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != insuranceTypePage) {
				insuranceTypes = insuranceTypePage.getContent().stream().map(r -> modelMapper.map(r, InsuranceTypeDto.class))
						.collect(Collectors.toList());
				currentPage = insuranceTypePage.getNumber();
				totalElement = insuranceTypePage.getTotalElements();
				totalPage = insuranceTypePage.getTotalPages();
			}

			dataRes.put("insuranceTypes", insuranceTypes);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST TIPE ASURANSI : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List tipe asuransi tidak ditemukan.");
		}
		return response;
	}
	
	private InsuranceType convertDtoToEntity(InsuranceTypeDto insuranceTypeDto) {
		InsuranceType insuranceType = modelMapper.map(insuranceTypeDto, InsuranceType.class);
		if (insuranceTypeDto.getId() == null || insuranceTypeDto.getId() == 0) {
			insuranceType.setCreatedBy(user);
			insuranceType.setCreatedDate(new Date());
		} else {
			InsuranceType insuranceTypeOld = insuranceTypeRepository.findById(insuranceTypeDto.getId()).orElse(null);
			if(insuranceTypeOld != null) {
				insuranceType.setCreatedBy(insuranceTypeOld.getCreatedBy());
				insuranceType.setCreatedDate(insuranceTypeOld.getCreatedDate());
			}
			insuranceType.setUpdatedBy(user);
			insuranceType.setUpdatedDate(new Date());
		}
		
		return insuranceType;
	}
	
	private void validation(InsuranceTypeDto insuranceTypeDto) throws ValidateException {
		if(null == insuranceTypeDto.getName()) {
			throw new ValidateException("Nama tipe asuransi diisi.");
		}
		
		Integer count = insuranceTypeRepository.getCountByName(insuranceTypeDto.getName().toUpperCase());
		count = count == null ? 0 : count;
		if((null == insuranceTypeDto.getId() && count > 0) || (null != insuranceTypeDto.getId() && count > 1)) {
			throw new ValidateException("Nama role sudah digunakan.");
		}
	}
}
