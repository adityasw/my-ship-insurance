package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.ViewChatDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.ChatDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.ChatMarineConnectDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewChat;
import com.apps.asuransikapalku.api.model.dto.view.ViewChatMarineConnectSupplierListDto;
import com.apps.asuransikapalku.api.model.entity.irevolution.Chat;
import com.apps.asuransikapalku.api.model.entity.marineconnect.ChatMarineConnect;
import com.apps.asuransikapalku.api.model.entity.user.Admin;
import com.apps.asuransikapalku.api.model.entity.user.Supplier;
import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.model.entity.view.ViewChatMarineConnectSupplierList;
import com.apps.asuransikapalku.api.repository.irevolution.ChatRepository;
import com.apps.asuransikapalku.api.repository.marineconnect.ChatMarineConnectRepository;
import com.apps.asuransikapalku.api.repository.user.UserRepository;
import com.apps.asuransikapalku.api.repository.view.ViewChatMarineConnectSupplierListRepository;
import com.apps.asuransikapalku.api.repository.view.ViewChatRepository;
import com.apps.asuransikapalku.api.service.ChatService;
import com.apps.asuransikapalku.api.utils.FileUtil;
import com.apps.asuransikapalku.api.utils.FormatDateUtil;

@Service
@Transactional
public class ChatServiceImpl implements ChatService {
	private static final Logger logger = LoggerFactory.getLogger(ChatServiceImpl.class);
	private static final String DATA_TIDAK_DITEMUKAN = "Data tidak ditemukan.";
	private static final String BINDING_DIR = "/Binding";
	
	private ChatRepository chatRepository;
	
	private ChatMarineConnectRepository chatMarineConnectRepository;
	
	private ViewChatRepository viewChatRepository;
	
	private ViewChatMarineConnectSupplierListRepository viewChatMarineConnectSupplierListRepository;
	
	private UserRepository userRepository;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	@Value("${asuransikapalku.url}")
	private String url;

	@Value("${path.root}")
	private String pathRoot;

	@Value("${dir.file.upload}")
	private String dirUploadFile;
	
	@Value("${dir.marine.connect}")
	private String dirMarineConnect;
	
	@Autowired
	public void repository(ChatRepository chatRepository, ChatMarineConnectRepository chatMarineConnectRepository, ViewChatRepository viewChatRepository, ViewChatMarineConnectSupplierListRepository viewChatMarineConnectSupplierListRepository) {
		this.chatRepository = chatRepository;
		this.chatMarineConnectRepository = chatMarineConnectRepository;
		this.viewChatRepository = viewChatRepository;
		this.viewChatMarineConnectSupplierListRepository = viewChatMarineConnectSupplierListRepository;
	}
	
	@Autowired
	public void repository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public APIResponse<ChatDto> save(ChatDto chatDto, User user) {
		logger.info("CHAT : {}", chatDto);
		APIResponse<ChatDto> respon = new APIResponse<>();
		try {
			Date currentDate = new Date();
			Chat chat = modelMapper.map(chatDto, Chat.class);
			
			if(chatDto.getContent().contains("data:application/pdf;base64,")) {
				String dirFile = pathRoot + dirUploadFile + BINDING_DIR;
				chat.setContent(FileUtil.fileDecodeToDir(chatDto.getContent(), "FILE", dirFile, ".pdf",
						url + dirUploadFile + BINDING_DIR));
			}
			
			chat.setSendTime(currentDate);
			chat.setSender(user.getUsername());
			chat.setIsRead(Boolean.FALSE);
			Admin admin = user.getAdmin();
			Supplier supplier = user.getSupplier();
			
			if(null != admin) {
				chat.setSender("ADMIN");
			}
			
			if(null != supplier) {
				chat.setIsSupply(Boolean.TRUE);
			}
			
			if(null == chatDto.getCode()) {
				chat.setCode("CHAT"+chatDto.getIdReference()+FormatDateUtil.dateToStr(currentDate, "ddMMyyyyHHMMSS"));
			}
			
			chatRepository.save(chat);
			
			respon.setData(modelMapper.map(chat, ChatDto.class));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA CHAT KARENA ERROR {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Chat tidak terkirim.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<String> save(ChatMarineConnectDto chatMarineConnectDto, User user) {
		logger.info("CHAT MARINE CONNECT: {}", chatMarineConnectDto);
		APIResponse<String> respon = new APIResponse<>();
		try {
			Date currentDate = new Date();
			ChatMarineConnect chatMarineConnect = modelMapper.map(chatMarineConnectDto, ChatMarineConnect.class);
			String dirFile = pathRoot + dirUploadFile + dirMarineConnect;
			String locatioanFile = url + dirUploadFile + dirMarineConnect;
			if (chatMarineConnectDto.getContent().contains("data:application/pdf")) {
				chatMarineConnect
						.setContent(FileUtil.fileDecodeToDir(chatMarineConnectDto.getContent(), "FILE", dirFile,
								AsuransiKapalkuConstant.FileTypeExtension.PDF, locatioanFile));
			}

			if (chatMarineConnectDto.getContent().contains("data:application/doc")) {
				chatMarineConnect.setContent(FileUtil.fileDecodeToDir(chatMarineConnectDto.getContent(), "FILE",
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.DOC, locatioanFile));
			}

			if (chatMarineConnectDto.getContent().contains("data:application/xls")) {
				chatMarineConnect.setContent(FileUtil.fileDecodeToDir(chatMarineConnectDto.getContent(), "FILE",
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.XLS, locatioanFile));
			}

			if (chatMarineConnectDto.getContent().contains("data:application/xlsx")) {
				chatMarineConnect.setContent(FileUtil.fileDecodeToDir(chatMarineConnectDto.getContent(), "FILE",
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.XLSX, locatioanFile));
			}

			if (chatMarineConnectDto.getContent().contains("data:application/png")) {
				chatMarineConnect.setContent(FileUtil.fileDecodeToDir(chatMarineConnectDto.getContent(), "FILE",
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.PNG, locatioanFile));
			}

			if (chatMarineConnectDto.getContent().contains("data:application/jpeg")
					|| chatMarineConnectDto.getContent().contains("data:application/jpg")) {
				chatMarineConnect.setContent(FileUtil.fileDecodeToDir(chatMarineConnectDto.getContent(), "FILE",
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.JPG, locatioanFile));
			}

			chatMarineConnect.setSendTime(currentDate);
			chatMarineConnect.setSender(user.getUsername());
			chatMarineConnect.setIsRead(Boolean.FALSE);
			chatMarineConnect.setIsReadSupply(Boolean.FALSE);
			if(null != user.getSupplier()) {
				chatMarineConnect.setIsReadSupply(Boolean.TRUE);
			}
			chatMarineConnectRepository.save(chatMarineConnect);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA CHAT KARENA ERROR {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Chat tidak terkirim.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id) {
		APIResponse<String> respon = new APIResponse<>();
		
		try {
			chatRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS CHAT KARENA ERROR {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Tidak dapat dihapus.");
		}
		
		return respon;
	}

	@Override
	public APIResponse<List<ChatDto>> findAllWithoutPaging(Long idReference, boolean isProject) {
		APIResponse<List<ChatDto>> respon = new APIResponse<>();
		
		try {
			List<Chat> chats = chatRepository.findByIdReferenceAndIsProjectOrderByIdAsc(idReference, isProject);
			if(!chats.isEmpty()) {
				respon.setData(chats.stream().map(this::convertEntityToChatDto).collect(Collectors.toList()));
			} else {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_TIDAK_DITEMUKAN);
			}
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST CHAT : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_TIDAK_DITEMUKAN);
		}
		
		return respon;
	}
	
	@Override
	public APIResponse<List<ChatDto>> findBySenderAndIsSupply(String sender, boolean isSupply) {
		APIResponse<List<ChatDto>> respon = new APIResponse<>();
		
		try {
			List<Chat> chats = chatRepository.findBySenderAndIsSupply(sender, isSupply);
			if(!chats.isEmpty()) {
				respon.setData(chats.stream().map(this::convertEntityToChatDto).collect(Collectors.toList()));
			} else {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_TIDAK_DITEMUKAN);
			}
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST CHAT : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_TIDAK_DITEMUKAN);
		}
		
		return respon;
	}
	
	@Override
	public APIResponse<List<ChatMarineConnectDto>> findBySenderAndReceiver(String sender, String receiver, User user) {
		APIResponse<List<ChatMarineConnectDto>> respon = new APIResponse<>();
		
		try {
			List<ChatMarineConnect> chats = chatMarineConnectRepository.findBySenderReceiver(sender, receiver);
			if(!chats.isEmpty()) {
				respon.setData(chats.stream().map(this::convertEntityToChatMarineConnectDto).collect(Collectors.toList()));
				
				if(null != user.getSupplier()) {
					chatMarineConnectRepository.updateStatusRead(sender, receiver);
				}
				
			} else {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_TIDAK_DITEMUKAN);
			}
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST CHAT MARINE CONNECT : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Kesalahan dalam server.");
		}
		
		return respon;
	}
	
	private ChatMarineConnectDto convertEntityToChatMarineConnectDto(ChatMarineConnect chatMarineConnect ) {
		ChatMarineConnectDto chatMarineConnectDto = modelMapper.map(chatMarineConnect, ChatMarineConnectDto.class);
		
		User userSender = userRepository.findByUsername(chatMarineConnect.getSender());
		User userReceiver = userRepository.findByUsername(chatMarineConnect.getReceiver());
		
		if(null != userSender.getDemand()) {
			chatMarineConnectDto.setSenderName(userSender.getDemand().getName());
		} else {
			chatMarineConnectDto.setSenderName(userSender.getSupplier().getName());
		}
		
		if(null != userReceiver.getDemand()) {
			chatMarineConnectDto.setReceiverName(userReceiver.getDemand().getName());
		} else {
			chatMarineConnectDto.setReceiverName(userReceiver.getSupplier().getName());
		}
		
		return chatMarineConnectDto;
	}
	
	@Override
	public APIResponse<List<ViewChatDto>> findChatList(Long idReference, boolean isProject, boolean supplyTab) {
		logger.info("idReference : {}, isProject : {}, isSupply : {}", idReference, isProject, supplyTab);
		APIResponse<List<ViewChatDto>> respon = new APIResponse<>();
		try {
			List<ViewChat> viewChats = viewChatRepository.findByIdReferenceAndIsProjectAndIsSupply(idReference, isProject, supplyTab);
			if(!viewChats.isEmpty()) {
				List<ViewChat> viewChatSeconds = viewChats.stream().filter(c -> !c.getUsername().equalsIgnoreCase("ADMIN")).collect(Collectors.toList());
				respon.setData(viewChatSeconds.stream().map(c -> modelMapper.map(c, ViewChatDto.class)).collect(Collectors.toList()));
			} else {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_TIDAK_DITEMUKAN);
			}
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST CHAT KARENA ERROR {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_TIDAK_DITEMUKAN);
		}
		
		return respon;
	}
	
	@Override
	public APIResponse<List<ViewChatMarineConnectSupplierListDto>> findChatList(User user) {
		APIResponse<List<ViewChatMarineConnectSupplierListDto>> respon = new APIResponse<>();
		try {
			List<ViewChatMarineConnectSupplierListDto> viewDtos = new ArrayList<>();
			List<ViewChatMarineConnectSupplierList> viewChats = viewChatMarineConnectSupplierListRepository.findByReceiver(user.getUsername());
			
			if(!viewChats.isEmpty()) {
				viewDtos = viewChats.stream().map(c -> modelMapper.map(c, ViewChatMarineConnectSupplierListDto.class)).collect(Collectors.toList());
			} 
			
			respon.setData(viewDtos);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST CHAT KARENA ERROR {}", e.getMessage());
			return new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Kesalahan dalam server.");
		}
		
		return respon;
	}
	
	@Override
	public APIResponse<List<ChatDto>> findChatByCode(String code) {
		APIResponse<List<ChatDto>> respon = new APIResponse<>();
		
		try {
			chatRepository.updateReadFlagByCode(code);
			logger.info("UPDATE FLAG IS READ BERHASIL");
			List<Chat> chats = chatRepository.findByCodeOrderByIdAsc(code);
			if(!chats.isEmpty()) {
				respon.setData(chats.stream().map(this::convertEntityToChatDto).collect(Collectors.toList()));
			} else {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_TIDAK_DITEMUKAN);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("GAGAL MEMUAT LIST CHAT KARENA ERROR {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), DATA_TIDAK_DITEMUKAN);
		}
		
		return respon;
	}
	
	private ChatDto convertEntityToChatDto(Chat chat ) {
		ChatDto chatDto = modelMapper.map(chat, ChatDto.class);
		
		User userSender = userRepository.findByUsername(chat.getSender());
		User userReceiver = userRepository.findByUsername(chat.getReceiver());
		
		if("ADMIN".equalsIgnoreCase(chat.getSender())) {
			chatDto.setSenderName(chat.getSender());
		} else if(null != userSender.getDemand()) {
			chatDto.setSenderName(userSender.getDemand().getName());
		} else {
			chatDto.setSenderName(userSender.getSupplier().getName());
		}
		
		if("ADMIN".equalsIgnoreCase(chat.getReceiver())) {
			chatDto.setReceiverName(chat.getReceiver());
		} else if(null != userReceiver.getDemand()) {
			chatDto.setReceiverName(userReceiver.getDemand().getName());
		} else {
			chatDto.setReceiverName(userReceiver.getSupplier().getName());
		}
		
		return chatDto;
	}

	@Override
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto) {
		return null;
	}
	
	
}
