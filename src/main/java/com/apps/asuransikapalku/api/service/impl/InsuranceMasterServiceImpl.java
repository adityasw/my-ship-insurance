package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.exception.CommonException;
import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceKindDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceMasterDto;
import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceKind;
import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceMaster;
import com.apps.asuransikapalku.api.repository.insurance.InsuranceKindRepository;
import com.apps.asuransikapalku.api.repository.insurance.InsuranceMasterRepository;
import com.apps.asuransikapalku.api.service.InsuranceMasterService;
import com.apps.asuransikapalku.api.utils.FileUtil;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class InsuranceMasterServiceImpl implements InsuranceMasterService {
	private static final Logger logger = LoggerFactory.getLogger(InsuranceMasterServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";
	
	private InsuranceKindRepository insuranceKindRepository;
	
	private InsuranceMasterRepository insuranceMasterRepository;
	
	private Environment env;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	private String user;
	
	@Autowired
	public void repository(InsuranceKindRepository insuranceKindRepository, InsuranceMasterRepository insuranceMasterRepository) {
		this.insuranceKindRepository = insuranceKindRepository;
		this.insuranceMasterRepository = insuranceMasterRepository;
	}
	
	@Autowired
	public void anotherService(Environment env) {
		this.env = env;
	}

	@Override
	public APIResponse<String> save(InsuranceMasterDto insuranceMasterDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = user;
			this.validation(insuranceMasterDto);
			insuranceMasterRepository.save(convertDtoToEntity(insuranceMasterDto));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA MASTER ASURANSI : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			insuranceMasterRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA MASTER ASURANSI [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Data master asuransi dengan id [" + id + "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<InsuranceMaster> insuranceMasters = insuranceMasterRepository.findByIdIn(ids);
			insuranceMasterRepository.deleteAll(insuranceMasters);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA MASTER ASURANSI [BATCH] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal menghapus data master asuransi dengan batch.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDelete(Long id, String user) {
		return null;
	}

	@Override
	public APIResponse<String> softDeleteAll(List<Long> id, String user) {
		return null;
	}

	@Override
	public APIResponse<InsuranceMasterDto> findById(Long id) {
		APIResponse<InsuranceMasterDto> respon =new APIResponse<>();
		try {
			respon.setData(convertEntityToDto(insuranceMasterRepository.findById(id).orElse(null)));
		} catch (Exception e) {
			logger.error("DATA MASTER ASURANSI TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Data master asuransi tidak ditemukan.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<List<InsuranceMasterDto>> findAllWithoutPaging() {
		APIResponse<List<InsuranceMasterDto>> respon =new APIResponse<>();
		try {
			List<InsuranceMaster> insuranceMasters = insuranceMasterRepository.findAll();
			if(!insuranceMasters.isEmpty()) {
				respon.setData(insuranceMasters.stream().map(this::convertEntityToDto)
						.collect(Collectors.toList()));
			}
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST MASTER ASURANSI [WITHOUT FILTER] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"List master asuransi tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<InsuranceMaster> insuranceMasterPage = insuranceMasterRepository.findAll(new Specification<InsuranceMaster>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<InsuranceMaster> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					Join<InsuranceMaster,InsuranceKind> insuranceKindJoin = root.join("insuranceKind");
					
					if (filterDto.getInsuranceKindId() != null && filterDto.getInsuranceKindId() != 0) {
						predicates.add(criteriaBuilder.equal(insuranceKindJoin.get("id"), filterDto.getInsuranceKindId()));
					}
					
					if (null != filterDto.getSearchBy()) {
						Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(name));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<InsuranceMasterDto> insuranceMasters = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != insuranceMasterPage) {
				insuranceMasters = insuranceMasterPage.getContent().stream().map(this::convertEntityToDto)
						.collect(Collectors.toList());
				currentPage = insuranceMasterPage.getNumber();
				totalElement = insuranceMasterPage.getTotalElements();
				totalPage = insuranceMasterPage.getTotalPages();
			}

			dataRes.put("insuranceMasters", insuranceMasters);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST MASTER ASURANSI : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List master asuransi tidak ditemukan.");
		}
		return response;
	}
	
	private InsuranceMaster convertDtoToEntity(InsuranceMasterDto insuranceMasterDto) throws CommonException {
		InsuranceMaster insuranceMaster = modelMapper.map(insuranceMasterDto, InsuranceMaster.class);
		insuranceMaster.setInsuranceKind(insuranceKindRepository.findById(insuranceMasterDto.getInsuranceKindDto().getId()).orElse(null));
		if (insuranceMasterDto.getId() == null || insuranceMasterDto.getId() == 0) {
			insuranceMaster.setCreatedBy(user);
			insuranceMaster.setCreatedDate(new Date());
		} else {
			InsuranceMaster insuranceMasterOld = insuranceMasterRepository.findById(insuranceMasterDto.getId()).orElse(null);
			if(insuranceMasterOld != null) {
				insuranceMaster.setCreatedBy(insuranceMasterOld.getCreatedBy());
				insuranceMaster.setCreatedDate(insuranceMasterOld.getCreatedDate());
			}
			insuranceMaster.setUpdatedBy(user);
			insuranceMaster.setUpdatedDate(new Date());
		}
		
		if (null != insuranceMasterDto.getLogo()) {
			String dirInsurance = env.getProperty("path.root") + env.getProperty("dir.insurance");
			insuranceMaster.setLogo(FileUtil.imgDecodeToDir(insuranceMasterDto.getLogo(), insuranceMasterDto.getName(), dirInsurance,
					AsuransiKapalkuConstant.FileTypeExtension.PNG, env.getProperty("asuransikapalku.url") + env.getProperty("dir.insurance")));
		}
		
		return insuranceMaster;
	}
	
	private InsuranceMasterDto convertEntityToDto(InsuranceMaster insuranceMaster) {
		InsuranceMasterDto insuranceMasterDto = modelMapper.map(insuranceMaster, InsuranceMasterDto.class);
		insuranceMasterDto.setInsuranceKindDto(modelMapper.map(insuranceMaster.getInsuranceKind(), InsuranceKindDto.class));
		return insuranceMasterDto;
	}
	
	private void validation(InsuranceMasterDto insuranceMasterDto) throws ValidateException {
		if(null == insuranceMasterDto.getName()) {
			throw new ValidateException("Nama asuransi harus diisi.");
		}
		
		if(null == insuranceMasterDto.getInsuranceKindDto()) {
			throw new ValidateException("Jenis asuransi harus diisi.");
		}
		
		Integer count = insuranceKindRepository.getCountByName(insuranceMasterDto.getName().toUpperCase());
		count = count == null ? 0 : count;
		if((null == insuranceMasterDto.getId() && count > 0) || (null != insuranceMasterDto.getId() && count > 1)) {
			throw new ValidateException("Nama asuransi sudah digunakan.");
		}
	}
}
