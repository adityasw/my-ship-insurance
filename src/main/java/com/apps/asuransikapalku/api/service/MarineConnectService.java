package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.MarineConnectWishListDto;
import com.apps.asuransikapalku.api.model.dto.UpdateStatusDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectInquiryDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectInvoiceDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectQuotationDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.MarineConnectUploadTcDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.UpdatePriceDto;
import com.apps.asuransikapalku.api.model.entity.user.User;

public interface MarineConnectService {
	public APIResponse<String> createInquiry(MarineConnectInquiryDto marineConnectInquiryDto, User user);
	
	public APIResponse<String> updateQuoted(MarineConnectQuotationDto marineConnectQuotationDto, String user);
	
	public APIResponse<String> updateStatus(UpdateStatusDto updateStatusDto, String user);
	
	public APIResponse<String> uploadTC(MarineConnectUploadTcDto marineConnectUploadTcDto, User user);
	
	public APIResponse<String> delete(Long id);
	
	public APIResponse<Map<String, Object>> findAllMarineConnectInquiry(FilterDto filterDto);
	
	public APIResponse<Map<String, Object>> findMarineConnectById(Long id);
	
	public APIResponse<Map<String, Object>> findMarineConnect(Long idMarineConnect, User user);
	
	public APIResponse<Map<String, Object>> findAllMarineConnect(FilterDto filterDto, User user);
	
	public APIResponse<Map<String, Object>> findAllMarineConnectHistory(FilterDto filterDto, User user);
	
	public APIResponse<String> updateWishList(MarineConnectWishListDto marineConnectWishListDto, User user);
	
	public APIResponse<String> updatePriceQuoted(UpdatePriceDto updatePriceDto, boolean isFinal);
	
	public APIResponse<List<Map<String, Object>>> findStatus();
	
	public APIResponse<String> uploadInvoice(MarineConnectInvoiceDto marineConnectInvoiceDto);
}
