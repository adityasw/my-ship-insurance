package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.enums.StatusClaimFinanceEnum;
import com.apps.asuransikapalku.api.exception.CommonException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.UpdateStatusDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ApprovedRejectLtvDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceAccountDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceBankDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceChatDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceComissionDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceDocumentDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceInvoiceDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceLtvDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceMapFinancerDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceMapFinancerDto.Detail;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceOfferingDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceQuotationDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinanceTrackingDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinancingDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinancingDto.ActiveInsuranceDetail;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinancingDto.OpponentDetail;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.ClaimFinancingDto.SoaDto;
import com.apps.asuransikapalku.api.model.dto.claimfinancing.UpdateStatusOfferingDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceKindDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipTypeDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewClaimFinanceChatDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewClaimFinanceDocumentDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewClaimFinanceDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewClaimFinanceSupDto;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinance;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceAccount;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceActiveInsuranceDetail;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceBank;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceChat;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceComission;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceDocument;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceInvoice;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceLtv;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceMapFinancer;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceOffering;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceOpponentDetail;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceOpponentDetailPk;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceQuotation;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceSoa;
import com.apps.asuransikapalku.api.model.entity.claimfinancing.ClaimFinanceTracking;
import com.apps.asuransikapalku.api.model.entity.user.Admin;
import com.apps.asuransikapalku.api.model.entity.user.Demand;
import com.apps.asuransikapalku.api.model.entity.user.Supplier;
import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.model.entity.view.ViewClaimFinance;
import com.apps.asuransikapalku.api.model.entity.view.ViewClaimFinanceDocument;
import com.apps.asuransikapalku.api.model.entity.view.ViewClaimFinanceSup;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceAccountRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceBankRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceChatRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceComissionRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceDocumentRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceInvoiceRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceLtvRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceMapFinancerRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceOfferingRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceOpponentDetailRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceQuotationRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceRepository;
import com.apps.asuransikapalku.api.repository.claimfinancing.ClaimFinanceTrackingRepository;
import com.apps.asuransikapalku.api.repository.insurance.InsuranceKindRepository;
import com.apps.asuransikapalku.api.repository.ship.ShipTypeRepository;
import com.apps.asuransikapalku.api.repository.user.SupplierRepository;
import com.apps.asuransikapalku.api.repository.view.ViewClaimFinanceChatRepository;
import com.apps.asuransikapalku.api.repository.view.ViewClaimFinanceDocumentRepository;
import com.apps.asuransikapalku.api.repository.view.ViewClaimFinanceRepository;
import com.apps.asuransikapalku.api.repository.view.ViewClaimFinanceSupRepository;
import com.apps.asuransikapalku.api.service.ClaimFinanceService;
import com.apps.asuransikapalku.api.service.ShipTypeService;
import com.apps.asuransikapalku.api.utils.FileUtil;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class ClaimFinanceServiceImpl implements ClaimFinanceService {
	private static final Logger logger = LoggerFactory.getLogger(ClaimFinanceServiceImpl.class);

	private static final String CURRENT_PAGE = "current-page";

	private static final String TOTAL_ITEM = "total-items";

	private static final String TOTAL_PAGE = "total-pages";

	private static final String KESALAHAN_DALAM_SERVER = "Kesalahan dalam server.";

	private ClaimFinanceRepository claimFinanceRepository;

	private ClaimFinanceAccountRepository claimFinanceAccountRepository;

	private ClaimFinanceBankRepository claimFinanceBankRepository;

	private ClaimFinanceChatRepository claimFinanceChatRepository;

	private ClaimFinanceComissionRepository claimFinanceComissionRepository;

	private ClaimFinanceDocumentRepository claimFinanceDocumentRepository;

	private ClaimFinanceInvoiceRepository claimFinanceInvoiceRepository;
	
	private ClaimFinanceMapFinancerRepository claimFinanceMapFinancerRepository;

	private ClaimFinanceOfferingRepository claimFinanceOfferingRepository;

	private ClaimFinanceLtvRepository claimFinanceLtvRepository;

	private ClaimFinanceOpponentDetailRepository claimFinanceOpponentDetailRepository;

	private ClaimFinanceTrackingRepository claimFinanceTrackingRepository;

	private ClaimFinanceQuotationRepository claimFinanceQuotationRepository;

	private InsuranceKindRepository insuranceKindRepository;

	private ShipTypeRepository shipTypeRepository;

	private SupplierRepository supplierRepository;

	private ViewClaimFinanceRepository viewClaimFinanceRepository;
	
	private ViewClaimFinanceSupRepository viewClaimFinanceSupRepository;

	private ViewClaimFinanceChatRepository viewClaimFinanceChatRepository;

	private ViewClaimFinanceDocumentRepository viewClaimFinanceDocumentRepository;

	private ShipTypeService shipTypeService;

	private ModelMapper modelMapper = new ModelMapper();

	private String userStr;

	private ClaimFinance claimFinance;

	@Value("${asuransikapalku.url}")
	private String url;

	@Value("${path.root}")
	private String pathRoot;

	@Value("${dir.file.upload}")
	private String dirUploadFile;

	@Value("${dir.claim.financing}")
	private String dirClaimFinancing;

	private String dirFile;

	private String locationFile;

	private void populateDirectory() {
		this.dirFile = this.pathRoot + this.dirUploadFile + this.dirClaimFinancing;
		this.locationFile = this.url + this.dirUploadFile + this.dirClaimFinancing;
	}

	@Autowired
	public void repository(ClaimFinanceAccountRepository claimFinanceAccountRepository,
			ClaimFinanceComissionRepository claimFinanceComissionRepository,
			ClaimFinanceMapFinancerRepository claimFinanceMapFinancerRepository,
			ViewClaimFinanceSupRepository viewClaimFinanceSupRepository) {
		this.claimFinanceAccountRepository = claimFinanceAccountRepository;
		this.claimFinanceComissionRepository = claimFinanceComissionRepository;
		this.claimFinanceMapFinancerRepository = claimFinanceMapFinancerRepository;
		this.viewClaimFinanceSupRepository = viewClaimFinanceSupRepository;
	}

	@Autowired
	public void repository(ClaimFinanceBankRepository claimFinanceBankRepository,
			ClaimFinanceChatRepository claimFinanceChatRepository,
			ViewClaimFinanceChatRepository viewClaimFinanceChatRepository,
			ViewClaimFinanceDocumentRepository viewClaimFinanceDocumentRepository) {
		this.claimFinanceBankRepository = claimFinanceBankRepository;
		this.claimFinanceChatRepository = claimFinanceChatRepository;
		this.viewClaimFinanceChatRepository = viewClaimFinanceChatRepository;
		this.viewClaimFinanceDocumentRepository = viewClaimFinanceDocumentRepository;
	}

	@Autowired
	public void repository(ClaimFinanceRepository claimFinanceRepository,
			ClaimFinanceOpponentDetailRepository claimFinanceOpponentDetailRepository,
			InsuranceKindRepository insuranceKindRepository, ShipTypeRepository shipTypeRepository) {
		this.claimFinanceRepository = claimFinanceRepository;
		this.claimFinanceOpponentDetailRepository = claimFinanceOpponentDetailRepository;
		this.insuranceKindRepository = insuranceKindRepository;
		this.shipTypeRepository = shipTypeRepository;
	}

	@Autowired
	public void repository(ClaimFinanceDocumentRepository claimFinanceDocumentRepository,
			ClaimFinanceOfferingRepository claimFinanceOfferingRepository,
			ClaimFinanceQuotationRepository claimFinanceQuotationRepository,
			ViewClaimFinanceRepository viewClaimFinanceRepository) {
		this.claimFinanceDocumentRepository = claimFinanceDocumentRepository;
		this.claimFinanceOfferingRepository = claimFinanceOfferingRepository;
		this.claimFinanceQuotationRepository = claimFinanceQuotationRepository;
		this.viewClaimFinanceRepository = viewClaimFinanceRepository;
	}

	@Autowired
	public void repository(ClaimFinanceInvoiceRepository claimFinanceInvoiceRepository,
			ClaimFinanceLtvRepository claimFinanceLtvRepository,
			ClaimFinanceTrackingRepository claimFinanceTrackingRepository, SupplierRepository supplierRepository) {
		this.claimFinanceInvoiceRepository = claimFinanceInvoiceRepository;
		this.claimFinanceLtvRepository = claimFinanceLtvRepository;
		this.claimFinanceTrackingRepository = claimFinanceTrackingRepository;
		this.supplierRepository = supplierRepository;
	}

	@Autowired
	public void service(ShipTypeService shipTypeService) {
		this.shipTypeService = shipTypeService;
	}

	@Override
	public APIResponse<String> save(ClaimFinancingDto claimFinancingDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		this.userStr = user;

		try {
			this.convertDtoToClaimFinance(claimFinancingDto);
			this.claimFinanceRepository.save(this.claimFinance);
			if (null != claimFinancingDto.getOpponentDetail()) {
				this.claimFinanceOpponentDetailRepository
						.save((this.convertDtoToClaimFinanceOpponentDetail(claimFinancingDto.getOpponentDetail())));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("GAGAL MENYIMPAN DATA CLAIM FINANCING : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}

		return respon;
	}

	@Override
	public APIResponse<ClaimFinancingDto> findById(Long id) {
		APIResponse<ClaimFinancingDto> respon = new APIResponse<>();

		try {
			respon.setData(this.convertEntityToClaimFinancingDto(claimFinanceRepository.findById(id).orElse(null)));
		} catch (Exception e) {
			logger.error("DETAIL DATA CLAIM FINANCING GAGAL DITEMUKAN: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}

		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findClaimFinanceDocument(FilterDto filterDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> map = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewClaimFinanceDocument> page = this.viewClaimFinanceDocumentRepository
					.findAll(new Specification<ViewClaimFinanceDocument>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ViewClaimFinanceDocument> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();

							if (null != filterDto.getClaimFinanceId()) {
								predicates.add(criteriaBuilder.equal(root.get("idClaimFinance"),
										filterDto.getClaimFinanceId()));
							}

							if (null != filterDto.getUsername()) {
								predicates.add(criteriaBuilder.or(criteriaBuilder.and(
										criteriaBuilder.equal(criteriaBuilder.upper(root.<String>get("uploader")),
												filterDto.getUsername()),
										criteriaBuilder.equal(criteriaBuilder.upper(root.<String>get("to")), "ADMIN")),
										criteriaBuilder.and(
												criteriaBuilder.equal(
														criteriaBuilder.upper(root.<String>get("uploader")), "ADMIN"),
												criteriaBuilder.equal(criteriaBuilder.upper(root.<String>get("to")),
														filterDto.getUsername()))));
							}

							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ViewClaimFinanceDocumentDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(r -> this.modelMapper.map(r, ViewClaimFinanceDocumentDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			map.put("claim-finance-documents", list);
			map.put(CURRENT_PAGE, currentPage);
			map.put(TOTAL_ITEM, totalElement);
			map.put(TOTAL_PAGE, totalPage);
			respon.setData(map);
		} catch (Exception e) {
			logger.error("GAGAL MENEMUKAN DATA CLAIM FINANCE DOCUMENT: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findClaimFinanceQuotation(FilterDto filterDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> map = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ClaimFinanceQuotation> page = this.claimFinanceQuotationRepository
					.findAll(new Specification<ClaimFinanceQuotation>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ClaimFinanceQuotation> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();
							Join<ClaimFinanceQuotation, ClaimFinance> claimFinanceJoin = root.join("claimFinance");

							if (null != filterDto.getClaimFinanceId()) {
								predicates.add(criteriaBuilder.equal(claimFinanceJoin.get("id"),
										filterDto.getClaimFinanceId()));
							}

							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ClaimFinanceQuotationDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(r -> this.modelMapper.map(r, ClaimFinanceQuotationDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			map.put("claim-finance-quotations", list);
			map.put(CURRENT_PAGE, currentPage);
			map.put(TOTAL_ITEM, totalElement);
			map.put(TOTAL_PAGE, totalPage);
			respon.setData(map);
		} catch (Exception e) {
			logger.error("GAGAL MENEMUKAN DATA CLAIM FINANCE QUOTATION: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findClaimFinanceOffering(FilterDto filterDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> map = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ClaimFinanceOffering> page = this.claimFinanceOfferingRepository
					.findAll(new Specification<ClaimFinanceOffering>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ClaimFinanceOffering> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();
							Join<ClaimFinanceOffering, ClaimFinance> claimFinanceJoin = root.join("claimFinance");
							Join<ClaimFinanceOffering, Supplier> supplierJoin = root.join("supplier");

							if (null != filterDto.getClaimFinanceId()) {
								predicates.add(criteriaBuilder.equal(claimFinanceJoin.get("id"),
										filterDto.getClaimFinanceId()));
							}

							if (null != filterDto.getSupplierId()) {
								predicates
										.add(criteriaBuilder.equal(supplierJoin.get("id"), filterDto.getSupplierId()));
							}
							
							if (null != filterDto.getUsername()) {
								predicates.add(criteriaBuilder.equal(root.get("username"),
										filterDto.getUsername()));
							}

							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ClaimFinanceOfferingDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(r -> this.modelMapper.map(r, ClaimFinanceOfferingDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			map.put("claim-finance-offerings", list);
			map.put(CURRENT_PAGE, currentPage);
			map.put(TOTAL_ITEM, totalElement);
			map.put(TOTAL_PAGE, totalPage);
			respon.setData(map);
		} catch (Exception e) {
			logger.error("GAGAL MENEMUKAN DATA CLAIM FINANCE OFFERING: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findClaimFinanceTracking(FilterDto filterDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> map = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ClaimFinanceTracking> page = this.claimFinanceTrackingRepository
					.findAll(new Specification<ClaimFinanceTracking>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ClaimFinanceTracking> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();
							Join<ClaimFinanceTracking, ClaimFinance> claimFinanceJoin = root.join("claimFinance");

							if (null != filterDto.getClaimFinanceId()) {
								predicates.add(criteriaBuilder.equal(claimFinanceJoin.get("id"),
										filterDto.getClaimFinanceId()));
							}
							
							if (null != filterDto.getUsername()) {
								predicates.add(criteriaBuilder.equal(root.get("username"),
										filterDto.getUsername()));
							}

							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ClaimFinanceTrackingDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(r -> this.modelMapper.map(r, ClaimFinanceTrackingDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			List<ClaimFinanceLtv> claimFinanceLtvs = claimFinanceLtvRepository
					.findByIdClaimFinance(filterDto.getClaimFinanceId());

			ClaimFinanceLtvDto claimFinanceLtvDto = new ClaimFinanceLtvDto();

			if (!claimFinanceLtvs.isEmpty()) {
				ClaimFinanceLtv claimFinanceLtv = claimFinanceLtvs.get(0);
				logger.info("LTV : {}", claimFinanceLtv);
				claimFinanceLtvDto = this.modelMapper.map(claimFinanceLtv, ClaimFinanceLtvDto.class);
			}

			map.put("claim-finance-trackings", list);
			map.put("claim-finance-ltv", claimFinanceLtvDto);
			map.put(CURRENT_PAGE, currentPage);
			map.put(TOTAL_ITEM, totalElement);
			map.put(TOTAL_PAGE, totalPage);
			respon.setData(map);
		} catch (Exception e) {
			logger.error("GAGAL MENEMUKAN DATA CLAIM FINANCE TRACKING: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findClaimFinanceInvoice(FilterDto filterDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> map = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ClaimFinanceInvoice> page = this.claimFinanceInvoiceRepository
					.findAll(new Specification<ClaimFinanceInvoice>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ClaimFinanceInvoice> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();
							Join<ClaimFinanceInvoice, ClaimFinance> claimFinanceJoin = root.join("claimFinance");

							if (null != filterDto.getClaimFinanceId()) {
								predicates.add(criteriaBuilder.equal(claimFinanceJoin.get("id"),
										filterDto.getClaimFinanceId()));
							}
							
							if (null != filterDto.getUsername()) {
								predicates.add(criteriaBuilder.equal(root.get("username"),
										filterDto.getUsername()));
							}

							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ClaimFinanceInvoiceDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(r -> this.modelMapper.map(r, ClaimFinanceInvoiceDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			map.put("claim-finance-invoices", list);
			map.put(CURRENT_PAGE, currentPage);
			map.put(TOTAL_ITEM, totalElement);
			map.put(TOTAL_PAGE, totalPage);
			respon.setData(map);
		} catch (Exception e) {
			logger.error("GAGAL MENEMUKAN DATA CLAIM FINANCE INVOICE: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findClaimFinanceInvoiceBank(FilterDto filterDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> map = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ClaimFinanceBank> page = this.claimFinanceBankRepository
					.findAll(new Specification<ClaimFinanceBank>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ClaimFinanceBank> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();
							Join<ClaimFinanceBank, ClaimFinance> claimFinanceJoin = root.join("claimFinance");

							if (null != filterDto.getClaimFinanceId()) {
								predicates.add(criteriaBuilder.equal(claimFinanceJoin.get("id"),
										filterDto.getClaimFinanceId()));
							}
							
							if (null != filterDto.getUsername()) {
								predicates.add(criteriaBuilder.equal(claimFinanceJoin.get("username"),
										filterDto.getUsername()));
							}

							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ClaimFinanceBankDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(r -> this.modelMapper.map(r, ClaimFinanceBankDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			map.put("claim-finance-invoice-banks", list);
			map.put(CURRENT_PAGE, currentPage);
			map.put(TOTAL_ITEM, totalElement);
			map.put(TOTAL_PAGE, totalPage);
			respon.setData(map);
		} catch (Exception e) {
			logger.error("GAGAL MENEMUKAN DATA CLAIM FINANCE BANK: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}
	
	@Override
	public APIResponse<Map<String, Object>> findClaimFinanceComission(FilterDto filterDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> map = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ClaimFinanceComission> page = this.claimFinanceComissionRepository
					.findAll(new Specification<ClaimFinanceComission>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ClaimFinanceComission> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();
							Join<ClaimFinanceComission, ClaimFinance> claimFinanceJoin = root.join("claimFinance");

							if (null != filterDto.getClaimFinanceId()) {
								predicates.add(criteriaBuilder.equal(claimFinanceJoin.get("id"),
										filterDto.getClaimFinanceId()));
							}
							
							if (null != filterDto.getUsername()) {
								predicates.add(criteriaBuilder.equal(claimFinanceJoin.get("username"),
										filterDto.getUsername()));
							}

							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ClaimFinanceComissionDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(r -> this.modelMapper.map(r, ClaimFinanceComissionDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			map.put("claim-finance-comissions", list);
			map.put(CURRENT_PAGE, currentPage);
			map.put(TOTAL_ITEM, totalElement);
			map.put(TOTAL_PAGE, totalPage);
			respon.setData(map);
		} catch (Exception e) {
			logger.error("GAGAL MENEMUKAN DATA CLAIM FINANCE COMISSION: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}
	
	@Override
	public APIResponse<ClaimFinanceMapFinancerDto> findClaimFinanceAssignee(Long idClaimFinance) {
		APIResponse<ClaimFinanceMapFinancerDto> respon = new APIResponse<>();
		try {
			this.claimFinance = claimFinanceRepository.findById(idClaimFinance).orElse(null);
			List<ClaimFinanceMapFinancer> claimFinanceMapFinancers = this.claimFinanceMapFinancerRepository.findByClaimFinanceOrderByIdAsc(this.claimFinance);
			if(!claimFinanceMapFinancers.isEmpty()) {
				ClaimFinanceMapFinancerDto claimFinanceMapFinancerDto = new ClaimFinanceMapFinancerDto();
				claimFinanceMapFinancerDto.setFinancers(claimFinanceMapFinancers.stream().map(this::convertToDetail).collect(Collectors.toList()));
				respon.setData(claimFinanceMapFinancerDto);
			}
		} catch (Exception e) {
			logger.error("GAGAL MENEMUKAN DATA CLAIM FINANCE ASSIGNEE: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}
	
	private Detail convertToDetail(ClaimFinanceMapFinancer claimFinanceMapFinancer) {
		Detail detail = new Detail();
		detail.setClaimFinanceId(claimFinanceMapFinancer.getClaimFinance().getId());
		detail.setSupplierId(claimFinanceMapFinancer.getSupplier().getId());
		return detail;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllByFilter(FilterDto filterDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> map = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewClaimFinance> page = this.viewClaimFinanceRepository
					.findAll(new Specification<ViewClaimFinance>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ViewClaimFinance> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();

							if (null != filterDto.getShipTypeId()) {
								predicates.add(
										criteriaBuilder.equal(root.get("idVessleType"), filterDto.getShipTypeId()));
							}

							if (null != filterDto.getStatus()) {
								predicates.add(criteriaBuilder.equal(criteriaBuilder.upper(root.<String>get("status")),
										filterDto.getStatus()));
							}

							if (null != filterDto.getStatusList() && !filterDto.getStatusList().isEmpty()) {
								predicates.add(criteriaBuilder.in(root.get("status")).value(filterDto.getStatusList()));
							}

							if (null != filterDto.getDemandId()) {
								predicates.add(criteriaBuilder.equal(root.get("idDemand"), filterDto.getDemandId()));
							}

							if (null != filterDto.getSupplierId()) {
								List<Long> idClaimFinanceList = claimFinanceOfferingRepository
										.getClaimFinanceIdBySupplierId(filterDto.getSupplierId());
								predicates.add(criteriaBuilder.in(root.get("id")).value(idClaimFinanceList));
							}

							if (null != filterDto.getSearchBy()) {
								Predicate vesselName = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("vesselName")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate vesselType = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("vesselType")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate locationOfLoss = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("locationOfLoss")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate status = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("status")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								predicates.add(criteriaBuilder.or(vesselName, vesselType, locationOfLoss, status));
							}
							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ViewClaimFinanceDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(r -> this.modelMapper.map(r, ViewClaimFinanceDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			map.put("claim-finances", list);
			map.put(CURRENT_PAGE, currentPage);
			map.put(TOTAL_ITEM, totalElement);
			map.put(TOTAL_PAGE, totalPage);
			respon.setData(map);
		} catch (Exception e) {
			logger.error("GAGAL MENEMUKAN DATA CLAIM FINANCE: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}
	
	@Override
	public APIResponse<Map<String, Object>> findAllByFilterForSup(FilterDto filterDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> map = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewClaimFinanceSup> page = this.viewClaimFinanceSupRepository
					.findAll(new Specification<ViewClaimFinanceSup>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ViewClaimFinanceSup> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();

							if (null != filterDto.getShipTypeId()) {
								predicates.add(
										criteriaBuilder.equal(root.get("idVessleType"), filterDto.getShipTypeId()));
							}

							if (null != filterDto.getStatus()) {
								predicates.add(criteriaBuilder.equal(criteriaBuilder.upper(root.<String>get("status")),
										filterDto.getStatus()));
							}

							if (null != filterDto.getStatusList() && !filterDto.getStatusList().isEmpty()) {
								predicates.add(criteriaBuilder.in(root.get("status")).value(filterDto.getStatusList()));
							}

							if (null != filterDto.getDemandId()) {
								predicates.add(criteriaBuilder.equal(root.get("idDemand"), filterDto.getDemandId()));
							}
							
							if (null != filterDto.getSupplierId()) {
								predicates.add(criteriaBuilder.equal(root.get("idSupplier"), filterDto.getSupplierId()));
							}

							if (null != filterDto.getSearchBy()) {
								Predicate vesselName = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("vesselName")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate vesselType = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("vesselType")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate locationOfLoss = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("locationOfLoss")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate status = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("status")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								predicates.add(criteriaBuilder.or(vesselName, vesselType, locationOfLoss, status));
							}
							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ViewClaimFinanceSupDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(r -> this.modelMapper.map(r, ViewClaimFinanceSupDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			map.put("claim-finances", list);
			map.put(CURRENT_PAGE, currentPage);
			map.put(TOTAL_ITEM, totalElement);
			map.put(TOTAL_PAGE, totalPage);
			respon.setData(map);
		} catch (Exception e) {
			logger.error("GAGAL MENEMUKAN DATA CLAIM FINANCE: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> uploadTC(ClaimFinanceDocumentDto claimFinanceDocumentDto, User user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.populateDirectory();
			Date currentDate = new Date();
			ClaimFinanceDocument claimFinanceDocument = this.modelMapper.map(claimFinanceDocumentDto,
					ClaimFinanceDocument.class);
			claimFinanceDocument.setTarget(claimFinanceDocumentDto.getTo());
			claimFinanceDocument.setUploadDate(currentDate);
			claimFinanceDocument.setUploader(user.getUsername());
			claimFinanceDocument.setUploadDate(currentDate);
			claimFinanceDocument.setClaimFinance(
					this.claimFinanceRepository.findById(claimFinanceDocumentDto.getClaimFinanceId()).orElse(null));
			claimFinanceDocument.setFile(FileUtil.saveFileDocumentSingle(claimFinanceDocumentDto.getFileName(),
					claimFinanceDocumentDto.getFile(), this.dirFile, this.locationFile));

			this.claimFinanceDocumentRepository.save(claimFinanceDocument);
		} catch (Exception e) {
			logger.error("GAGAL UPLOAD DOCUMENT CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> uploadQuotation(ClaimFinanceQuotationDto claimFinanceQuotationDto, User user) {
		APIResponse<String> respon = new APIResponse<>();

		try {
			this.populateDirectory();
			ClaimFinanceQuotation claimFinanceQuotation = new ClaimFinanceQuotation();
			claimFinanceQuotation.setClaimFinance(
					this.claimFinanceRepository.findById(claimFinanceQuotationDto.getClaimFinanceId()).orElse(null));
			claimFinanceQuotation.setPeriod(claimFinanceQuotationDto.getPeriod());
			claimFinanceQuotation.setFile(FileUtil.saveFileDocumentSingle("UPLOAD_QUOTATION",
					claimFinanceQuotationDto.getFile(), this.dirFile, this.locationFile));
			this.claimFinanceQuotationRepository.save(claimFinanceQuotation);
		} catch (Exception e) {
			logger.error("GAGAL UPLOAD QUOTATION CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}

		return respon;
	}

	@Override
	public APIResponse<String> givingOffering(ClaimFinanceOfferingDto claimFinanceOfferingDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			ClaimFinanceOffering claimFinanceOffering = this.modelMapper.map(claimFinanceOfferingDto,
					ClaimFinanceOffering.class);
			claimFinanceOffering.setClaimFinance(
					this.claimFinanceRepository.findById(claimFinanceOfferingDto.getClaimFinanceId()).orElse(null));
			claimFinanceOffering.setSupplier(
					this.supplierRepository.findById(claimFinanceOfferingDto.getSupplierId()).orElse(null));
			claimFinanceOffering.setIsSelected(Boolean.FALSE);
			this.claimFinanceOfferingRepository.save(claimFinanceOffering);
		} catch (Exception e) {
			logger.error("GAGAL MEMBERIKAN PENAWARAN CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> createTrack(ClaimFinanceTrackingDto claimFinanceTrackingDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.populateDirectory();
			ClaimFinanceTracking claimFinanceTracking = this.modelMapper.map(claimFinanceTrackingDto,
					ClaimFinanceTracking.class);
			claimFinanceTracking.setClaimFinance(
					this.claimFinanceRepository.findById(claimFinanceTrackingDto.getClaimFinanceId()).orElse(null));
			if (null != claimFinanceTrackingDto.getFile() && !claimFinanceTrackingDto.getFile().contains(this.url)) {
				claimFinanceTracking.setFile(FileUtil.saveFileDocumentSingle("CLAIM_FINANCE_TRACKING",
						claimFinanceTrackingDto.getFile(), this.dirFile, this.locationFile));
			}
			this.claimFinanceTrackingRepository.save(claimFinanceTracking);
		} catch (Exception e) {
			logger.error("GAGAL MEMBUAT TRACK CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> createLtv(ClaimFinanceLtvDto claimFinanceLtvDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			ClaimFinanceLtv claimFinanceLtv = this.modelMapper.map(claimFinanceLtvDto, ClaimFinanceLtv.class);
			claimFinanceLtv.setClaimFinance(
					this.claimFinanceRepository.findById(claimFinanceLtvDto.getClaimFinanceId()).orElse(null));
			if (null == claimFinanceLtv.getId()) {
				claimFinanceLtv.setIsApprovedLtv(Boolean.TRUE);
				claimFinanceLtv.setIsRemainingLtv(Boolean.TRUE);
			}
			this.claimFinanceLtvRepository.save(claimFinanceLtv);
		} catch (Exception e) {
			logger.error("GAGAL MEMBUAT LTV CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> createInvoice(ClaimFinanceInvoiceDto claimFinanceInvoiceDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.populateDirectory();
			ClaimFinanceInvoice claimFinanceInvoice = this.modelMapper.map(claimFinanceInvoiceDto,
					ClaimFinanceInvoice.class);
			claimFinanceInvoice.setClaimFinance(
					this.claimFinanceRepository.findById(claimFinanceInvoiceDto.getClaimFinanceId()).orElse(null));
			if (null != claimFinanceInvoiceDto.getFile() && !claimFinanceInvoiceDto.getFile().contains(this.url)) {
				String fileInSaved = FileUtil.saveFileDocumentSingle("CLAIM_FINANCE_INVOICE",
						claimFinanceInvoiceDto.getFile(), this.dirFile, this.locationFile);
				claimFinanceInvoice.setFile(fileInSaved);
				String[] strSplit = fileInSaved.split("/");
				claimFinanceInvoice.setFilename(strSplit[strSplit.length - 1]);
			}
			this.claimFinanceInvoiceRepository.save(claimFinanceInvoice);
		} catch (Exception e) {
			logger.error("GAGAL MEMBUAT TRACK CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> createBank(ClaimFinanceBankDto claimFinanceBankDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			ClaimFinanceBank claimFinanceBank = this.modelMapper.map(claimFinanceBankDto, ClaimFinanceBank.class);
			claimFinanceBank.setClaimFinance(
					this.claimFinanceRepository.findById(claimFinanceBankDto.getClaimFinanceId()).orElse(null));
			this.claimFinanceBankRepository.save(claimFinanceBank);
		} catch (Exception e) {
			logger.error("GAGAL MEMBUAT BANK CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}
	
	@Override
	public APIResponse<String> createComission(ClaimFinanceComissionDto claimFinanceComissionDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			ClaimFinanceComission claimFinanceComission = this.modelMapper.map(claimFinanceComissionDto, ClaimFinanceComission.class);
			claimFinanceComission.setClaimFinance(
					this.claimFinanceRepository.findById(claimFinanceComissionDto.getClaimFinanceId()).orElse(null));
			if (null != claimFinanceComissionDto.getFile() && !claimFinanceComissionDto.getFile().contains(this.url)) {
				this.populateDirectory();
				String fileInSaved = FileUtil.saveFileDocumentSingle(Objects.isNull(claimFinanceComissionDto.getFilename())? "CLAIM_FINANCE_COMISSION":claimFinanceComissionDto.getFilename(),
						claimFinanceComissionDto.getFile(), this.dirFile, this.locationFile);
				claimFinanceComission.setFile(fileInSaved);
				String[] strSplit = fileInSaved.split("/");
				claimFinanceComission.setFilename(strSplit[strSplit.length - 1]);
			}
			this.claimFinanceComissionRepository.save(claimFinanceComission);
		} catch (Exception e) {
			logger.error("GAGAL MEMBUAT COMISSION CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}
	
	@Transactional
	@Override
	public APIResponse<String> assignFinancer(ClaimFinanceMapFinancerDto claimFinanceMapFinancerDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<Long> ids = claimFinanceMapFinancerDto.getFinancers().stream().map(this::toLongId)
					.collect(Collectors.toList());
			this.claimFinanceMapFinancerRepository.deleteBatch(ids);

			List<ClaimFinanceMapFinancer> claimFinanceMapFinancers = claimFinanceMapFinancerDto.getFinancers().stream()
					.map(this::convertToClaimFinanceMapFinancer).collect(Collectors.toList());
			this.claimFinanceMapFinancerRepository.saveAll(claimFinanceMapFinancers);
		} catch (Exception e) {
			logger.error("GAGAL MEMBUAT ASSIGN CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}
	
	private Long toLongId(Detail detail) {
		return detail.getSupplierId();
	}
	
	private ClaimFinanceMapFinancer convertToClaimFinanceMapFinancer(Detail detail) {
		ClaimFinanceMapFinancer claimFinanceMapFinancer = new ClaimFinanceMapFinancer();
		claimFinanceMapFinancer.setClaimFinance(claimFinanceRepository.findById(detail.getClaimFinanceId()).orElse(null));
		claimFinanceMapFinancer.setSupplier(supplierRepository.findById(detail.getSupplierId()).orElse(null));
		return claimFinanceMapFinancer;
	}

	@Override
	public APIResponse<String> updateStatus(UpdateStatusDto updateStatusDto, User user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.claimFinance = this.claimFinanceRepository.findById(updateStatusDto.getId()).orElse(null);
			if (null == this.claimFinance) {
				return new APIResponse<>(HttpStatus.OK.value(), "Claim Finance tidak ditemukan.");
			}
			this.claimFinance.setStatus(updateStatusDto.getStatusCode());
			this.claimFinanceRepository.save(this.claimFinance);
		} catch (Exception e) {
			logger.error("GAGAL UPDATE STATUS CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> offeringResult(UpdateStatusOfferingDto dto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.claimFinance = this.claimFinanceRepository.findById(dto.getIdClaimFinance()).orElse(null);
			if (null == this.claimFinance) {
				return new APIResponse<>(HttpStatus.OK.value(), "Claim Finance tidak ditemukan.");
			}
			if (!this.claimFinance.getClaimFinanceOfferings().isEmpty()) {
				for(ClaimFinanceOffering cfo : this.claimFinance.getClaimFinanceOfferings()) {
					if (Boolean.TRUE.equals(dto.getIsRejectAll())) {
						cfo.setIsSelected(Boolean.FALSE);
					}

					if (Boolean.FALSE.equals(dto.getIsRejectAll()) && dto.getId().equals(cfo.getId())) {
						cfo.setIsSelected(Boolean.TRUE);
					}

					if (Boolean.FALSE.equals(dto.getIsRejectAll()) && dto.getId().equals(cfo.getId())) {
						cfo.setIsSelected(Boolean.TRUE);
					}
				}
				claimFinanceOfferingRepository.saveAll(this.claimFinance.getClaimFinanceOfferings());
			}
		} catch (Exception e) {
			logger.error("GAGAL UPDATE STATUS CLAIM FINANCE OFFERING : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> approvedRejectLtv(ApprovedRejectLtvDto dto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			ClaimFinanceLtv claimFinanceLtv = this.claimFinanceLtvRepository.findById(dto.getId()).orElse(null);
			if (null == claimFinanceLtv) {
				return new APIResponse<>(HttpStatus.OK.value(), "Claim Finance LTV tidak ditemukan.");
			}
			claimFinanceLtv.setIsApprovedLtv(dto.getIsApprovedLtv());
			claimFinanceLtv.setIsRemainingLtv(dto.getIsRemainingLtv());
			this.claimFinanceLtvRepository.save(claimFinanceLtv);
		} catch (Exception e) {
			logger.error("GAGAL UPDATE STATUS CLAIM FINANCE OFFERING : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> saveChat(ClaimFinanceChatDto dto, User user) {
		logger.info("CHAT CLAIM FINANCE: {}", dto);
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.populateDirectory();
			Date currentDate = new Date();
			ClaimFinanceChat claimFinanceChat = modelMapper.map(dto, ClaimFinanceChat.class);

			claimFinanceChat.setContent(FileUtil.saveFileDocumentSingle("FILE_CLAIM_FINANCE", dto.getContent(),
					this.dirFile, this.locationFile));
			claimFinanceChat.setSendTime(currentDate);
			claimFinanceChat.setSender(user.getUsername());
			claimFinanceChat.setIsReadAdmin(Boolean.FALSE);
			claimFinanceChat.setIsReadDemand(Boolean.FALSE);
			claimFinanceChat.setIsReadSupply(Boolean.TRUE);
			Admin admin = user.getAdmin();
			Supplier supplier = user.getSupplier();
			Demand demand = user.getDemand();

			if (null != admin) {
				claimFinanceChat.setSender("ADMIN");
			}

			if (null != supplier) {
				claimFinanceChat.setIsSupply(Boolean.TRUE);
			}

			if (null != demand) {
				claimFinanceChat.setIsSupply(Boolean.FALSE);
			}

			claimFinanceChatRepository.save(claimFinanceChat);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN CHAT CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteChat(Long id) {
		APIResponse<String> respon = new APIResponse<>();

		try {
			claimFinanceChatRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS CHAT CLAIM FINANCE : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}

		return respon;
	}

	@Override
	public APIResponse<List<ViewClaimFinanceChatDto>> findChatTabSupplier(Long idClaimFinance) {
		APIResponse<List<ViewClaimFinanceChatDto>> respon = new APIResponse<>();
		try {
			respon.setData(viewClaimFinanceChatRepository.findByIdClaimFinance(idClaimFinance).stream()
					.map(v -> this.modelMapper.map(v, ViewClaimFinanceChatDto.class)).collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("LIST DATA RESUME CHAT GAGAL DITEMUKAN : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<List<ClaimFinanceChatDto>> findChatByIdClaimFinanceAndSenderReceiver(Long idClaimFinance,
			String senderOrReceiver) {
		APIResponse<List<ClaimFinanceChatDto>> respon = new APIResponse<>();
		try {
			respon.setData(claimFinanceChatRepository
					.findByIdClaimFinanceAndSenderAndReceiverOrderByIdAsc(idClaimFinance, senderOrReceiver,
							senderOrReceiver)
					.stream().map(c -> this.modelMapper.map(c, ClaimFinanceChatDto.class))
					.collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("LIST DATA CHAT GAGAL DITEMUKAN : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<String> supplierOfferingDetail(ClaimFinanceAccountDto dto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			ClaimFinanceAccount claimFinanceAccount = this.modelMapper.map(dto, ClaimFinanceAccount.class);
			claimFinanceAccount.setClaimFinance(claimFinanceRepository.findById(dto.getClaimFinanceId()).orElse(null));
			claimFinanceAccountRepository.save(claimFinanceAccount);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA CLAIM FINANCE OFFERING DETAIL ACCOUNT : {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<ClaimFinanceAccountDto> getSupplierOfferingDetail(Long idClaimFinance) {
		APIResponse<ClaimFinanceAccountDto> respon = new APIResponse<>();
		try {
			this.claimFinance = claimFinanceRepository.findById(idClaimFinance).orElse(null);
			List<ClaimFinanceAccount> claimFinanceAccounts = claimFinanceAccountRepository
					.findByClaimFinance(this.claimFinance);
			if (!claimFinanceAccounts.isEmpty()) {
				ClaimFinanceAccount claimFinanceAccount = claimFinanceAccounts.get(0);
				ClaimFinanceAccountDto claimFinanceAccountDto = this.modelMapper.map(claimFinanceAccount,
						ClaimFinanceAccountDto.class);
				respon.setData(claimFinanceAccountDto);
			}
		} catch (Exception e) {
			logger.error("DATA CLAIM FINANCE OFFERING DETAIL ACCOUNT TIDAK DITEMUKAN: {}", e.getMessage());
			respon = new APIResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), KESALAHAN_DALAM_SERVER);
		}
		return respon;
	}

	@Override
	public APIResponse<List<Map<String, Object>>> findStatus() {
		APIResponse<List<Map<String, Object>>> respon = new APIResponse<>();
		List<Map<String, Object>> maps = new ArrayList<>();
		for (StatusClaimFinanceEnum sse : StatusClaimFinanceEnum.values()) {
			Map<String, Object> map = new HashMap<>();
			map.put("code", sse.code);
			map.put("name", sse.label);
			maps.add(map);
		}
		respon.setData(maps);
		return respon;
	}

	private void updateStatusOffering(boolean isRejectAll, Long idClaimFinanceOffering,
			ClaimFinanceOffering claimFinanceOffering) {
		if (isRejectAll) {
			claimFinanceOffering.setIsSelected(Boolean.FALSE);
		}

		if (!isRejectAll && idClaimFinanceOffering.equals(claimFinanceOffering.getId())) {
			claimFinanceOffering.setIsSelected(Boolean.TRUE);
		}

		if (!isRejectAll && idClaimFinanceOffering.equals(claimFinanceOffering.getId())) {
			claimFinanceOffering.setIsSelected(Boolean.TRUE);
		}
	}

	private void convertDtoToClaimFinance(ClaimFinancingDto claimFinancingDto) throws Exception {
		this.claimFinance = modelMapper.map(claimFinancingDto, ClaimFinance.class);
		Date currentDate = new Date();
		if (null == claimFinance.getId()) {
			this.claimFinance.setCreatedBy(this.userStr);
			this.claimFinance.setCreatedDate(currentDate);
			this.claimFinance.setStatus(StatusClaimFinanceEnum.WAITING_FOR_AK_REVIEW.code);
		}

		if (null != claimFinancingDto.getVesselType() && null != claimFinancingDto.getVesselType().getId()) {
			this.claimFinance
					.setVesselType(shipTypeRepository.findById(claimFinancingDto.getVesselType().getId()).orElse(null));
		}

		if (null != claimFinancingDto.getVesselTypeOther() && Boolean.TRUE.equals(claimFinancingDto.getIsOtherVesselType())) {
			this.claimFinance.setIsOtherVesselType(Boolean.TRUE);
			this.claimFinance.setVesselTypeOther(claimFinancingDto.getVesselTypeOther());
		}

		if (null != claimFinance.getId()) {
			ClaimFinance claimFinanceOld = this.claimFinanceRepository.findById(claimFinance.getId()).orElse(null);
			if (null != claimFinanceOld) {
				this.claimFinance.setCreatedBy(claimFinanceOld.getCreatedBy());
				this.claimFinance.setCreatedDate(claimFinanceOld.getCreatedDate());
			}
		}

		if (!claimFinancingDto.getSoas().isEmpty()) {
			this.claimFinance.setClaimFinanceSoas(claimFinancingDto.getSoas().stream().map(t -> {
				try {
					return convertDtoToClaimFinanceSoa(t);
				} catch (CommonException e) {
					logger.error("TERJADI KESALAHAN KETIKA CONVERT SOA : {}", e.getMessage());
				}
				return null;
			}).collect(Collectors.toList()));
		}

		if (!claimFinancingDto.getActiveInsuranceDetails().isEmpty()) {
			this.claimFinance.setClaimFinanceActiveInsuranceDetails(
					claimFinancingDto.getActiveInsuranceDetails().stream().map(t -> {
						try {
							return convertDtoToClaimFinanceActiveInsuranceDetail(t);
						} catch (CommonException e) {
							logger.error("TERJADI KESALAHAN KETIKA CONVERT DETAIL ASURANSI AKTIF : {}", e.getMessage());
							e.printStackTrace();
						}
						return null;
					}).collect(Collectors.toList()));
		}
	}

	private ClaimFinanceSoa convertDtoToClaimFinanceSoa(SoaDto soaDto) throws CommonException {
		this.populateDirectory();
		ClaimFinanceSoa claimFinanceSoa = new ClaimFinanceSoa();

		if (null != soaDto.getId()) {
			claimFinanceSoa.setId(soaDto.getId());
		}

		if (null == soaDto.getFilename()) {
			claimFinanceSoa.setFilename("CLAIM_FINANCING");
		} else {
			claimFinanceSoa.setFilename(soaDto.getFilename());
		}

		if (!soaDto.getFile().contains(this.url)) {
			claimFinanceSoa.setFile(FileUtil.saveFileDocumentSingle("CLAIM_FINANCE_SOA", soaDto.getFile(),
					this.dirFile, this.locationFile));
		}

		claimFinanceSoa.setClaimFinance(this.claimFinance);
		return claimFinanceSoa;
	}

	private ClaimFinanceActiveInsuranceDetail convertDtoToClaimFinanceActiveInsuranceDetail(ActiveInsuranceDetail dto)
			throws CommonException {
		this.populateDirectory();
		ClaimFinanceActiveInsuranceDetail claimFinanceActiveInsuranceDetail = this.modelMapper.map(dto,
				ClaimFinanceActiveInsuranceDetail.class);
		claimFinanceActiveInsuranceDetail.setClaimFinance(this.claimFinance);

		if (null != dto.getInsuranceKindDto() && null != dto.getInsuranceKindDto().getId()) {
			claimFinanceActiveInsuranceDetail.setInsuranceKind(
					this.insuranceKindRepository.findById(dto.getInsuranceKindDto().getId()).orElse(null));
		}

		if (null != dto.getFile() && !dto.getFile().contains(this.url)) {
			claimFinanceActiveInsuranceDetail.setFile(FileUtil.saveFileDocumentSingle("ACTIVE_INSURANCE_DETAIL",
					claimFinanceActiveInsuranceDetail.getFile(), this.dirFile, this.locationFile));
		}
		return claimFinanceActiveInsuranceDetail;
	}

	private ClaimFinanceOpponentDetail convertDtoToClaimFinanceOpponentDetail(OpponentDetail dto) {
		ClaimFinanceOpponentDetail claimFinanceOpponentDetail = this.modelMapper.map(dto,
				ClaimFinanceOpponentDetail.class);
		ClaimFinanceOpponentDetailPk pk = new ClaimFinanceOpponentDetailPk(this.claimFinance.getId());
		claimFinanceOpponentDetail.setPk(pk);
		return claimFinanceOpponentDetail;
	}

	private ClaimFinancingDto convertEntityToClaimFinancingDto(ClaimFinance claimFinance) {
		ClaimFinancingDto claimFinancingDto = this.modelMapper.map(claimFinance, ClaimFinancingDto.class);
		claimFinancingDto.setSoas(claimFinance.getClaimFinanceSoas().stream().map(this::convertEntityToSoaDto)
				.collect(Collectors.toList()));
		claimFinancingDto.setActiveInsuranceDetails(claimFinance.getClaimFinanceActiveInsuranceDetails().stream()
				.map(this::convertEntityToActiveInsuranceDetail).collect(Collectors.toList()));

		ClaimFinanceOpponentDetailPk pk = new ClaimFinanceOpponentDetailPk(claimFinance.getId());
		ClaimFinanceOpponentDetail claimFinanceOpponentDetail = claimFinanceOpponentDetailRepository.findByPk(pk);

		if (null != claimFinanceOpponentDetail) {
			claimFinancingDto.setOpponentDetail(this.convertEntityToOpponentDetail(claimFinanceOpponentDetail));
		}

		if (null != claimFinance.getVesselType()) {
			claimFinancingDto.setVesselType(this.modelMapper.map(claimFinance.getVesselType(), ShipTypeDto.class));
		}

		claimFinancingDto.setUsernameDemand(claimFinance.getDemand().getUser().getUsername());

		return claimFinancingDto;
	}

	private SoaDto convertEntityToSoaDto(ClaimFinanceSoa claimFinanceSoa) {
		SoaDto soaDto = this.modelMapper.map(claimFinanceSoa, SoaDto.class);
		return soaDto;
	}

	private ActiveInsuranceDetail convertEntityToActiveInsuranceDetail(
			ClaimFinanceActiveInsuranceDetail claimFinanceActiveInsuranceDetail) {
		ActiveInsuranceDetail activeInsuranceDetail = this.modelMapper.map(claimFinanceActiveInsuranceDetail,
				ActiveInsuranceDetail.class);
		activeInsuranceDetail.setInsuranceKindDto(
				this.modelMapper.map(claimFinanceActiveInsuranceDetail.getInsuranceKind(), InsuranceKindDto.class));
		return activeInsuranceDetail;
	}

	private OpponentDetail convertEntityToOpponentDetail(ClaimFinanceOpponentDetail claimFinanceOpponentDetail) {
		OpponentDetail opponentDetail = this.modelMapper.map(claimFinanceOpponentDetail, OpponentDetail.class);
		return opponentDetail;
	}
}
