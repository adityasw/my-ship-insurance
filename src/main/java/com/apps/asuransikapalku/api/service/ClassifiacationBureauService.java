package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.master.ClassifiacationBureauDto;

public interface ClassifiacationBureauService {
	
	APIResponse<String> save(ClassifiacationBureauDto classifiacationBureauDto, String user);

	APIResponse<String> delete(Long id, String user);
	
	APIResponse<ClassifiacationBureauDto> findById(Long id);
	
	APIResponse<List<ClassifiacationBureauDto>> findAllWithoutPageable();
	
	APIResponse<Map<String, Object>> findAll(FilterDto filterDto);
	
}
