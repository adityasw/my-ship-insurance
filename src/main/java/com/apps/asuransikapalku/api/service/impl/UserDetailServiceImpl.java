package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.repository.user.UserRepository;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
	private UserRepository userRepository;
	
	@Autowired
	public void repository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsernameAndIsActive(username, Boolean.TRUE);
		if (user == null) {
			throw new UsernameNotFoundException("Data user ["+username+"] tidak ditemukan dalam sistem.");
		}
		if (Boolean.FALSE.equals(user.getIsActive()) || null == user.getIsActive()) {
			throw new UsernameNotFoundException("Data user ["+username+"] tidak ditemukan dalam sistem.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}
}
