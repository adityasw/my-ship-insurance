package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceKindDto;

public interface InsuranceKindService {
	public APIResponse<String> save(InsuranceKindDto insuranceKindDto, String user);
	
	public APIResponse<String> updateStatus(Long id, boolean isActive, String user);

	public APIResponse<String> delete(Long id, String user);

	public APIResponse<String> deleteAll(List<Long> id, String user);

	public APIResponse<String> softDelete(Long id, String user);

	public APIResponse<String> softDeleteAll(List<Long> id, String user);
	
	public APIResponse<InsuranceKindDto> findById(Long id);
	
	public APIResponse<InsuranceKindDto> findByCode(String code);

	public APIResponse<List<InsuranceKindDto>> findAllWithoutPageableAndActiveByTypeId(Long id);

	public APIResponse<Map<String, Object>> findAllInsuranceKind(FilterDto filterDto);
}
