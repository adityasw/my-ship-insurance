package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.user.DemandDto;
import com.apps.asuransikapalku.api.model.dto.user.DemandRegisterDto;
import com.apps.asuransikapalku.api.model.dto.user.LoginDto;

public interface DemandService {
	public APIResponse<Map<String, Object>> loginDemand(LoginDto loginDto);

	public APIResponse<String> registerDemand(DemandRegisterDto demandRegisterDto, String userCreated);

	public APIResponse<String> save(DemandDto demandDto, String user);
	
	public APIResponse<String> updateApprovalRegister(DemandRegisterDto demandRegisterDto, String user);

	public APIResponse<String> delete(Long id, String user);

	public APIResponse<String> deleteAll(List<Long> id, String user);

	public APIResponse<String> softDelete(Long id, String user);

	public APIResponse<String> softDeleteAll(List<Long> id, String user);

	public APIResponse<DemandDto> findById(Long id);

	public APIResponse<DemandDto> findByEmail(String email);

	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto);
	
	public APIResponse<Map<String, Object>> findAllShipBind(FilterDto filterDto);
	
	public APIResponse<Map<String, Object>> findAllShipInprogress(FilterDto filterDto);
	
	public void authenticateCheck(LoginDto loginDto) throws ValidateException;
	
	public String getBycryptPassword(String password);
}
