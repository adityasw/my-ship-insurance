package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.user.LoginDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDetailDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierRegisterDto;
import com.apps.asuransikapalku.api.model.entity.user.User;

public interface SupplierService {
	public APIResponse<Map<String, Object>> loginSupplier(LoginDto loginDto);

	public APIResponse<String> registerSupplier(SupplierRegisterDto supplierRegisterDto, String userCreated);

	public APIResponse<String> save(SupplierDto supplierDto, String user);
	
	public APIResponse<String> updateApprovalRegister(SupplierRegisterDto supplierRegisterDto, String user);

	public APIResponse<String> delete(Long id, String user);

	public APIResponse<String> deleteAll(List<Long> id, String user);

	public APIResponse<String> softDelete(Long id, String user);

	public APIResponse<String> softDeleteAll(List<Long> id, String user);

	public APIResponse<SupplierDto> findById(Long id);

	public APIResponse<SupplierDto> findByEmail(String email);

	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto);
	
	public APIResponse<Map<String, Object>> findAllShip(FilterDto filterDto);
	
	public APIResponse<Map<String, Object>> findAllShipInprogress(FilterDto filterDto);
	
	public APIResponse<Map<String, Object>> findAllShipBind(FilterDto filterDto);

	public APIResponse<List<SupplierDto>> findWithoutPaging();
	
	public void authenticateCheck(LoginDto loginDto) throws ValidateException;
	
	public APIResponse<Map<String, Object>> findAllShipRejected(FilterDto filterDto);
	
	public String getEncodePassword(String password);
	
	public APIResponse<String> updateMyPage(SupplierDetailDto supplierDetailDto, String user);
	
	public APIResponse<Map<String, Object>> findAllSupplierMarineConnect(FilterDto filterDto, User user);
	
	public APIResponse<Map<String, Object>> findByIdForMarineConnect(Long id, String user);

	APIResponse<List<SupplierDto>> findWithoutPaging(String occupationCode);
}
