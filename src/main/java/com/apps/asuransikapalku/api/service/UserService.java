package com.apps.asuransikapalku.api.service;

import com.apps.asuransikapalku.api.model.entity.user.User;

public interface UserService {
	//EXCEPTION
	User findByUsername(String username);
	
	User findByToken(String token);
	
	void save(User user);
}
