package com.apps.asuransikapalku.api.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.exception.CommonException;
import com.apps.asuransikapalku.api.model.dto.master.EmailDto;
import com.apps.asuransikapalku.api.model.entity.master.EmailLog;
import com.apps.asuransikapalku.api.repository.master.EmailLogRepository;
import com.apps.asuransikapalku.api.service.EmailService;

@Service
public class EmailServiceImpl implements EmailService{
	public static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);
	
	@Autowired
	private JavaMailSender javaMailSender;
	@Autowired
	private EmailLogRepository emailLogRepository;
	
	private EmailLog emailLog;

	@Override
	public String render(String template, Map<String, String> params) {
		String emailTemplate;
		for (Entry<String, String> param : params.entrySet()){
			template = template.replaceAll("\\{\\{"+param.getKey()+"\\}\\}", (param.getValue() == null ? "" : param.getValue()));
		}
		emailTemplate = template;
		return emailTemplate;
	}

	@Override
	public String renderByTemplate(String templatePath, String charset, Map<String, String> params) throws CommonException {
		StringBuilder sb = new StringBuilder();
		String template = "";
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource(templatePath).getFile());
			List<String> contents = Files.readAllLines(Paths.get(file.getPath()), Charset.forName(charset));
			for (String content : contents) {
				sb.append(content);
			}
			template = this.render(sb.toString(), params);
		} catch (IOException e) {
			throw new CommonException(e.getMessage());
		}
		return template;
	}

	@Override
	public void sendMail(Map<String, String> params) throws CommonException {
		MimeMessage msg = javaMailSender.createMimeMessage();

        try {
        	EmailDto<String> emailProp = populateSendMail(params);
			msg.setRecipients(Message.RecipientType.TO, fromListTostr(emailProp.getToIds()));
			if (!emailProp.getCcIds().isEmpty()) {
				msg.setRecipients(Message.RecipientType.CC, fromListTostr(emailProp.getCcIds()));
			}
			if (!emailProp.getBccIds().isEmpty()) {
				msg.setRecipients(Message.RecipientType.BCC, fromListTostr(emailProp.getBccIds()));
			}
			if (AsuransiKapalkuConstant.Email.NO_REPLY_SENDER_VALUE.equalsIgnoreCase(params.get(AsuransiKapalkuConstant.Email.NO_REPLY_SENDER))) {
				msg.setFrom(new InternetAddress(params.get(AsuransiKapalkuConstant.Email.NO_REPLY_SENDER), "Asuransi Kapalku"));
				emailLog.setFromId(params.get(AsuransiKapalkuConstant.Email.NO_REPLY_SENDER));
			} else {
				msg.setFrom(emailProp.getFromId()==null?new InternetAddress("developer@asuransikapalku.com", "Asuransi Kapalku"):new InternetAddress(emailProp.getFromId(), "Asuransi Kapalku"));
				emailLog.setFromId(emailProp.getFromId()==null?"developer@asuransikapalku.com":emailProp.getFromId());
			}
        	msg.setSubject(emailProp.getSubject());
        	msg.setContent(emailProp.getContent(), "text/html; charset=UTF-8");
        	
	        javaMailSender.send(msg);
	        emailLog.setSendDate(new Date());
	        emailLogRepository.save(emailLog);
		} catch (MessagingException | IOException e) {
			emailLog.setIsSend(Boolean.FALSE);
			emailLogRepository.save(emailLog);
			throw new CommonException(e.getMessage());
		}
	}

	private InternetAddress[] fromListTostr(List<String> strs) throws AddressException {
		LOGGER.info("Total : {}", strs.size());
		int size = strs.size();
		int count = 0;
		InternetAddress[] str = new InternetAddress[size];
		for(String s : strs) {
			str[count++] = new InternetAddress(s);
		}
		return str;
	}

	private EmailDto<String> populateSendMail(Map<String, String> params) throws CommonException {
		EmailDto<String> emailDTO = new EmailDto<>(); 
		emailLog = new EmailLog();
		if (params.containsKey(AsuransiKapalkuConstant.Email.SUBJECT) && null != params.get(AsuransiKapalkuConstant.Email.SUBJECT)) {
			emailDTO.setSubject(params.get(AsuransiKapalkuConstant.Email.SUBJECT));
			emailLog.setSubject(params.get(AsuransiKapalkuConstant.Email.SUBJECT));
		}
		if (params.containsKey(AsuransiKapalkuConstant.Email.TO) && null != params.get(AsuransiKapalkuConstant.Email.TO)) {
			emailDTO.setToIds(toCcBcc(params.get(AsuransiKapalkuConstant.Email.TO)));
			emailLog.setToIds(params.get(AsuransiKapalkuConstant.Email.TO));
		}
		if (params.containsKey(AsuransiKapalkuConstant.Email.CC) && null != params.get(AsuransiKapalkuConstant.Email.CC)) {
			emailDTO.setCcIds(toCcBcc(params.get(AsuransiKapalkuConstant.Email.CC)));
			emailLog.setCcIds(params.get(AsuransiKapalkuConstant.Email.CC));
		} else {
			emailDTO.setCcIds(new ArrayList<>());
		}
		if (params.containsKey(AsuransiKapalkuConstant.Email.BCC) && null != params.get(AsuransiKapalkuConstant.Email.BCC)) {
			emailDTO.setBccIds(toCcBcc(params.get(AsuransiKapalkuConstant.Email.BCC)));
			emailLog.setBccIds(params.get(AsuransiKapalkuConstant.Email.BCC));
		} else {
			emailDTO.setBccIds(new ArrayList<>());
		}
		if (params.containsKey(AsuransiKapalkuConstant.Email.TEMPLATE) && null != params.get(AsuransiKapalkuConstant.Email.TEMPLATE)) {
			String content = this.renderByTemplate(params.get(AsuransiKapalkuConstant.Email.TEMPLATE), "UTF-8", params);
			emailDTO.setContent(content);
			emailLog.setContent(content);
		}
		
		return emailDTO;
	}
	
	private List<String> toCcBcc(String to){
		LOGGER.info("Email to {}", to);
		String[] str = to.split(",");
		return Arrays.asList(str);
	}
	
}
