package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipTypeDto;
import com.apps.asuransikapalku.api.model.entity.ship.ShipType;
import com.apps.asuransikapalku.api.repository.ship.ShipTypeRepository;
import com.apps.asuransikapalku.api.service.ShipTypeService;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class ShipTypeServiceImpl implements ShipTypeService {
	private static final Logger logger = LoggerFactory.getLogger(ShipTypeServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";
	
	private ShipTypeRepository shipTypeRepository;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	private String user;
	
	@Autowired
	public void repository(ShipTypeRepository shipTypeRepository) {
		this.shipTypeRepository = shipTypeRepository;
	}

	@Override
	public APIResponse<String> save(ShipTypeDto shipTypeDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = user;
			this.validation(shipTypeDto);
			this.saveShipType(shipTypeDto);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA TIPE KAPAL : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}
	
	@Override
	public ShipType saveShipType(ShipTypeDto shipTypeDto, String user) throws Exception {
		this.user = user;
		return this.saveShipType(shipTypeDto);
	}
	
	private ShipType saveShipType(ShipTypeDto shipTypeDto) {
		if(null == shipTypeDto.getCode() || "".equalsIgnoreCase(shipTypeDto.getCode())) {
			Integer maxId = shipTypeRepository.getMaxId();
			maxId = maxId == null ? 0 : maxId;
			shipTypeDto.setCode("SHIP-"+(maxId+1));
		}
		shipTypeDto.setCode(shipTypeDto.getCode().toUpperCase());
		ShipType shipType = convertDtoToEntity(shipTypeDto);
		shipTypeRepository.save(shipType);
		return shipType;
	}

	@Override
	public APIResponse<String> updateStatus(Long id, boolean isActive, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			ShipType shipType = shipTypeRepository.findById(id).orElse(null);
			shipType.setIsActive(isActive);
			shipType.setUpdatedBy(user);
			shipType.setUpdatedDate(new Date());
			shipTypeRepository.save(shipType);
		} catch (Exception e) {
			logger.error("GAGAL MENGUBAH STATUS ACTIVE TIPE KAPAL : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Status tipe kapal dengan id [" + id + "] gagal diubah.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			shipTypeRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA TIPE KAPAL [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Tipe kapal dengan id [" + id + "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<ShipType> shipTypes = shipTypeRepository.findByIdIn(ids);
			shipTypeRepository.deleteAll(shipTypes);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA TIPE KAPAL [BATCH] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal menghapus data tipe kapal dengan batch.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDelete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			ShipType shipType = shipTypeRepository.findById(id).orElse(null);
			shipType.setIsActive(Boolean.FALSE);
			shipType.setUpdatedBy(user);
			shipType.setUpdatedDate(new Date());
			shipTypeRepository.save(shipType);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA TIPE KAPAL [SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Tipe kapal dengan id [" + id + "] gagal dihapus.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDeleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<ShipType> shipTypes = shipTypeRepository.findByIdIn(ids);
			for(ShipType s : shipTypes) {
				s.setIsActive(Boolean.FALSE);
				s.setUpdatedBy(user);
				s.setUpdatedDate(new Date());
			}
			shipTypeRepository.deleteAll(shipTypes);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA TIPE KAPAL [BATCH - SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal menghapus data tipe kapal dengan batch.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<ShipTypeDto> findById(Long id) {
		APIResponse<ShipTypeDto> respon =new APIResponse<>();
		try {
			ShipType shipType = shipTypeRepository.findById(id).orElse(null);
			if(shipType == null) {
				logger.info("JENIS KAPAL TIDAK DITEMUKAN [BY ID] : {}", shipType);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Jenis kapal tidak ditemukan.");
			}
			respon.setData(modelMapper.map(shipType, ShipTypeDto.class));
		} catch (Exception e) {
			logger.info("JENIS KAPAL TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Jenis kapal tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<ShipTypeDto> findByCode(String code) {
		APIResponse<ShipTypeDto> respon =new APIResponse<>();
		try {
			ShipType shipType = shipTypeRepository.findByCode(code);
			if(shipType == null) {
				logger.error("JENIS KAPAL TIDAK DITEMUKAN [BY CODE] : {}", shipType);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Jenis pekerjaan tidak ditemukan.");
			}
			respon.setData(modelMapper.map(shipType, ShipTypeDto.class));
		} catch (Exception e) {
			logger.error("JENIS KAPAL TIDAK DITEMUKAN [BY CODE] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Jenis kapal tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<List<ShipTypeDto>> findAllWithoutPageableAndActive() {
		APIResponse<List<ShipTypeDto>> respon = new APIResponse<>();
		try {
			List<ShipType> shipTypes = shipTypeRepository.findByIsActive(true);
			respon.setData(shipTypes.stream().map(r -> modelMapper.map(r, ShipTypeDto.class)).collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST TIPE KAPAL : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"List tipe kapal tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllShipType(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ShipType> shipTypePage = shipTypeRepository.findAll(new Specification<ShipType>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ShipType> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					
					if (filterDto.getIsActive() != null && filterDto.getIsActive()) {
						predicates.add(criteriaBuilder.equal(root.get("isActive"), filterDto.getIsActive()));
					}
					
					if (null != filterDto.getSearchBy()) {
						Predicate code = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("code")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate description = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("description")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(code, name, description));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<ShipTypeDto> shipTypes = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != shipTypePage) {
				shipTypes = shipTypePage.getContent().stream().map(r -> modelMapper.map(r, ShipTypeDto.class))
						.collect(Collectors.toList());
				currentPage = shipTypePage.getNumber();
				totalElement = shipTypePage.getTotalElements();
				totalPage = shipTypePage.getTotalPages();
			}

			dataRes.put("shipTypes", shipTypes);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST TIPE KAPAL : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List tipa kapal tidak ditemukan.");
		}
		return response;
	}
	
	private ShipType convertDtoToEntity(ShipTypeDto shipTypeDto) {
		ShipType shipType = modelMapper.map(shipTypeDto, ShipType.class);
		if (shipTypeDto.getId() == null || shipTypeDto.getId() == 0) {
			shipType.setCreatedBy(user);
			shipType.setCreatedDate(new Date());
		} else {
			ShipType shipTypeOld = shipTypeRepository.findById(shipTypeDto.getId()).orElse(null);
			if(shipTypeOld != null) {
				shipType.setCreatedBy(shipTypeOld.getCreatedBy());
				shipType.setCreatedDate(shipTypeOld.getCreatedDate());
			}
			shipType.setUpdatedBy(user);
			shipType.setUpdatedDate(new Date());
		}
		
		return shipType;
	}
	
	private void validation(ShipTypeDto shipTypeDto) throws ValidateException {
		if(null == shipTypeDto.getName()) {
			throw new ValidateException("Nama tipe kapal harus diisi.");
		}
		
		Integer count = shipTypeRepository.getCountByName(shipTypeDto.getName().toUpperCase());
		count = count == null ? 0 : count;
		if((null == shipTypeDto.getId() && count > 0) || (null != shipTypeDto.getId() && count > 1)) {
			throw new ValidateException("Nama tipe kapal sudah digunakan.");
		}
	}
}
