package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.configs.TokenJwtConfig;
import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.exception.CommonException;
import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.marineconnect.ChatMarineConnectDto;
import com.apps.asuransikapalku.api.model.dto.occupation.OccupationDto;
import com.apps.asuransikapalku.api.model.dto.user.LoginDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDetailDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierRegisterDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDetailDto.Citi;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDetailDto.Prov;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDetailDto.SupplierClientDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDetailDto.SupplierGalleryDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewInsuranceCellDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewInsuranceInprogressSupDto2;
import com.apps.asuransikapalku.api.model.dto.view.ViewInsuranceSupDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewInsuranceSupRejDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewSupplierPageDto;
import com.apps.asuransikapalku.api.model.entity.marineconnect.ChatMarineConnect;
import com.apps.asuransikapalku.api.model.entity.marineconnect.MarineConnectWishList;
import com.apps.asuransikapalku.api.model.entity.user.Demand;
import com.apps.asuransikapalku.api.model.entity.user.Supplier;
import com.apps.asuransikapalku.api.model.entity.user.SupplierClient;
import com.apps.asuransikapalku.api.model.entity.user.SupplierDetail;
import com.apps.asuransikapalku.api.model.entity.user.SupplierDetailPk;
import com.apps.asuransikapalku.api.model.entity.user.SupplierGallery;
import com.apps.asuransikapalku.api.model.entity.user.SupplierInsuranceMap;
import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.model.entity.view.ViewInsuranceCell;
import com.apps.asuransikapalku.api.model.entity.view.ViewInsuranceInprogressSup;
import com.apps.asuransikapalku.api.model.entity.view.ViewInsuranceSup;
import com.apps.asuransikapalku.api.model.entity.view.ViewInsuranceSupRej;
import com.apps.asuransikapalku.api.model.entity.view.ViewSupplierPage;
import com.apps.asuransikapalku.api.repository.insurance.InsuranceMasterRepository;
import com.apps.asuransikapalku.api.repository.marineconnect.ChatMarineConnectRepository;
import com.apps.asuransikapalku.api.repository.marineconnect.MarineConnectWishListRepository;
import com.apps.asuransikapalku.api.repository.master.CityRepository;
import com.apps.asuransikapalku.api.repository.occupation.OccupationRepository;
import com.apps.asuransikapalku.api.repository.user.DemandRepository;
import com.apps.asuransikapalku.api.repository.user.SupplierClientRepository;
import com.apps.asuransikapalku.api.repository.user.SupplierDetailRepository;
import com.apps.asuransikapalku.api.repository.user.SupplierGalleryRepository;
import com.apps.asuransikapalku.api.repository.user.SupplierRepository;
import com.apps.asuransikapalku.api.repository.user.UserRepository;
import com.apps.asuransikapalku.api.repository.view.ViewInsuranceCellRepository;
import com.apps.asuransikapalku.api.repository.view.ViewInsuranceInprogressSupRepository;
import com.apps.asuransikapalku.api.repository.view.ViewInsuranceSupRejRepository;
import com.apps.asuransikapalku.api.repository.view.ViewInsuranceSupRepository;
import com.apps.asuransikapalku.api.repository.view.ViewSupplierPageRepository;
import com.apps.asuransikapalku.api.service.EmailService;
import com.apps.asuransikapalku.api.service.SupplierService;
import com.apps.asuransikapalku.api.utils.FileUtil;
import com.apps.asuransikapalku.api.utils.PasswordUtil;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class SupplierServiceImpl implements SupplierService {
	public static final Logger logger = LoggerFactory.getLogger(SupplierServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";

	private ChatMarineConnectRepository chatMarineConnectRepository;
	
	private CityRepository cityRepository;
	
	private DemandRepository demandRepository;
	
	private Environment env;
	
	private MarineConnectWishListRepository marineConnectWishListRepository;

	private OccupationRepository occupationRepository;

	private SupplierRepository supplierRepository;

	private SupplierDetailRepository supplierDetailRepository;

	private SupplierClientRepository supplierClientRepository;

	private SupplierGalleryRepository supplierGalleryRepository;

	private UserRepository userRepository;

	private InsuranceMasterRepository insuranceMasterRepository;

	private ViewInsuranceCellRepository viewInsuranceCellRepository;

	private ViewInsuranceInprogressSupRepository viewInsuranceInprogressSupRepository;

	private ViewInsuranceSupRepository viewInsuranceSupRepository;

	private ViewInsuranceSupRejRepository viewInsuranceSupRejRepository;

	private ViewSupplierPageRepository viewSupplierPageRepository;

	private PasswordEncoder bcryptEncoder;

	private AuthenticationManager authenticationManager;

	private TokenJwtConfig tokenJwtConfig;

	private EmailService emailService;

	private ModelMapper modelMapper = new ModelMapper();

	private String userGlobal;

	private Supplier supplier;

	private SupplierDetailPk supplierDetailPk;

	@Autowired
	public void repository(OccupationRepository occupationRepository, SupplierRepository supplierRepository,
			UserRepository userRepository, InsuranceMasterRepository insuranceMasterRepository) {
		this.occupationRepository = occupationRepository;
		this.supplierRepository = supplierRepository;
		this.userRepository = userRepository;
		this.insuranceMasterRepository = insuranceMasterRepository;
	}

	@Autowired
	public void repository(SupplierDetailRepository supplierDetailRepository,
			SupplierClientRepository supplierClientRepository, SupplierGalleryRepository supplierGalleryRepository,
			ViewSupplierPageRepository viewSupplierPageRepository) {
		this.supplierDetailRepository = supplierDetailRepository;
		this.supplierClientRepository = supplierClientRepository;
		this.supplierGalleryRepository = supplierGalleryRepository;
		this.viewSupplierPageRepository = viewSupplierPageRepository;
	}

	@Autowired
	public void repository(ViewInsuranceCellRepository viewInsuranceCellRepository,
			ViewInsuranceInprogressSupRepository viewInsuranceInprogressSupRepository,
			ViewInsuranceSupRepository viewInsuranceSupRepository,
			ViewInsuranceSupRejRepository viewInsuranceSupRejRepository) {
		this.viewInsuranceCellRepository = viewInsuranceCellRepository;
		this.viewInsuranceInprogressSupRepository = viewInsuranceInprogressSupRepository;
		this.viewInsuranceSupRepository = viewInsuranceSupRepository;
		this.viewInsuranceSupRejRepository = viewInsuranceSupRejRepository;
	}
	
	@Autowired
	public void repository(CityRepository cityRepository, ChatMarineConnectRepository chatMarineConnectRepository, DemandRepository demandRepository, MarineConnectWishListRepository marineConnectWishListRepository) {
		this.cityRepository = cityRepository;
		this.chatMarineConnectRepository = chatMarineConnectRepository;
		this.demandRepository = demandRepository;
		this.marineConnectWishListRepository = marineConnectWishListRepository;
	}

	@Autowired
	public void serviceOther(PasswordEncoder bcryptEncoder, AuthenticationManager authenticationManager,
			TokenJwtConfig tokenJwtConfig, EmailService emailService) {
		this.bcryptEncoder = bcryptEncoder;
		this.authenticationManager = authenticationManager;
		this.tokenJwtConfig = tokenJwtConfig;
		this.emailService = emailService;
	}

	@Autowired
	public void anotherService(Environment env) {
		this.env = env;
	}

	@Override
	public APIResponse<Map<String, Object>> loginSupplier(LoginDto loginDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		Map<String, Object> map = new HashMap<>();
		try {
			authentication(loginDto);
			User user = userRepository.findByUsername(loginDto.getUsername());
			final String token = tokenJwtConfig.generateToken(new org.springframework.security.core.userdetails.User(
					user.getUsername(), user.getPassword(), new ArrayList<>()));
			user.setIsLogin(Boolean.TRUE);
			user.setLastLogin(new Date());
			user.setToken(token);
			userRepository.save(user);

			map.put("id-supplier", user.getSupplier().getId());
			map.put("username", user.getUsername());
			map.put("token", user.getToken());
			map.put("login-as", "SUPPLIER");

			respon.setData(map);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("TIDAK BERHASIL LOGIN [SUPPLIER]: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal login. Periksa kembali username dan password.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> registerSupplier(SupplierRegisterDto supplierRegisterDto, String userCreated) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.validationToRegister(supplierRegisterDto);
			Date createdDate = new Date();

			Supplier supplier = new Supplier();
			supplier.setName(supplierRegisterDto.getName());
			supplier.setEmail(supplierRegisterDto.getEmail());
			supplier.setSupplierAs(userCreated);
			supplier.setCreatedDate(createdDate);
			supplier.setCreatedBy(userCreated == null ? "SYSTEM" : userCreated);

			User user = new User();
			user.setUsername(supplierRegisterDto.getUsername());
			user.setCreatedDate(createdDate);
			user.setCreatedBy(userCreated == null ? "SYSTEM" : userCreated);
			supplier.setUser(user);
			user.setSupplier(supplier);
			user.setIsActive(Boolean.FALSE);

			userRepository.save(user);

			Map<String, String> params = new HashMap<>();
			params.put(AsuransiKapalkuConstant.Email.SUBJECT, "Akun Gudangemas Kamu telah dibuat");
			params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, supplier.getName());
			params.put(AsuransiKapalkuConstant.Email.TO, supplier.getEmail());
			params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/confirmation-sign-up.html");

			emailService.sendMail(params);

		} catch (Exception e) {
			logger.error("PENDAFTARAN SUPPLIER GAGAL : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> updateApprovalRegister(SupplierRegisterDto supplierRegisterDto, String userCreated) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			Date createdDate = new Date();
			String password = PasswordUtil.generateRandomPassword(8);

			Supplier supplier = new Supplier();
			supplier.setName(supplierRegisterDto.getName());
			supplier.setEmail(supplierRegisterDto.getEmail());
			supplier.setPhoneNumber(supplierRegisterDto.getPhoneNumber());
			supplier.setOccupation(
					occupationRepository.findById(supplierRegisterDto.getOccupation().getId()).orElse(null));
			supplier.setCompanyName(supplierRegisterDto.getCompanyName());
			supplier.setPosition(supplierRegisterDto.getPosition());
			supplier.setInsuranceNeeds(supplierRegisterDto.getInsuranceNeeds());
			supplier.setCreatedDate(createdDate);
			supplier.setCreatedBy(userCreated == null ? "SYSTEM" : userCreated);

			if (null != supplierRegisterDto.getInsuranceIds()) {
				if (!supplierRegisterDto.getInsuranceIds().isEmpty()) {
					List<SupplierInsuranceMap> supplierInsuranceMaps = new ArrayList<>();
					for (Long l : supplierRegisterDto.getInsuranceIds()) {
						SupplierInsuranceMap supplierInsuranceMap = new SupplierInsuranceMap();
						supplierInsuranceMap.setSupplier(supplier);
						supplierInsuranceMap.setInsuranceMaster(insuranceMasterRepository.findById(l).orElse(null));
						supplierInsuranceMaps.add(supplierInsuranceMap);
					}
					supplier.setSupplierInsuranceMaps(supplierInsuranceMaps);
				}
			}

			User user = new User();
			user.setUsername(supplierRegisterDto.getUsername() == null ? supplierRegisterDto.getEmail()
					: supplierRegisterDto.getUsername());
			user.setPassword(bcryptEncoder.encode(password));
			user.setCreatedDate(createdDate);
			user.setCreatedBy(userCreated == null ? "SYSTEM" : userCreated);
			supplier.setUser(user);
			user.setSupplier(supplier);
			user.setIsActive(Boolean.TRUE);

			Map<String, String> params = new HashMap<>();
			params.put(AsuransiKapalkuConstant.Email.SUBJECT,
					"CONGRATULATIONS! You are part of AsuransiKapalku.com Great Partner");
			params.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, supplier.getName());
			params.put(AsuransiKapalkuConstant.Email.USERNAME, user.getUsername());
			params.put(AsuransiKapalkuConstant.Email.TO, supplier.getEmail());
			params.put(AsuransiKapalkuConstant.Email.PASSWORD, password);
			params.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/approval-account-supply.html");

			emailService.sendMail(params);

			userRepository.save(user);
		} catch (Exception e) {
			logger.error("APPROVAL SUPPLIER GAGAL : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal update status approval.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> updateMyPage(SupplierDetailDto supplierDetailDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.supplier = supplierRepository.findById(supplierDetailDto.getIdSupplier()).orElse(null);

			if (null == this.supplier) {
				return new APIResponse<>(HttpStatus.OK.value(), "Data tidak ditemukan.");
			}
			this.supplier.setUpdatedBy(user);
			this.supplier.setCreatedDate(new Date());

			this.supplierDetailPk = new SupplierDetailPk();
			this.supplierDetailPk.setIdSupplier(supplierDetailDto.getIdSupplier());
			
			List<MarineConnectWishList> marineConnectWishLists = new ArrayList<>();
			
			if(null == supplierDetailDto.getCode()) {
				this.supplierDetailPk.setCode(PasswordUtil.generateRandomPassword(10));
				marineConnectWishLists = populateWishList();
			} else {
				this.supplierDetailPk.setCode(supplierDetailDto.getCode());
			}
			
			SupplierDetail supplierDetail = modelMapper.map(supplierDetailDto, SupplierDetail.class);
			supplierDetail.setSupplierDetailPk(this.supplierDetailPk);
			supplierDetail.setSupplier(supplier);
			supplierDetail.setCity(cityRepository.findById(supplierDetailDto.getCity().getId()).orElse(null));
			String dirSupplier = env.getProperty("path.root") + env.getProperty("dir.supplier");
			String locationFile = env.getProperty("asuransikapalku.url") + env.getProperty("dir.supplier");
			if (null != supplierDetail.getAvatar() && supplierDetail.getAvatar().contains("data:")) {
				supplierDetail.setAvatar(FileUtil.imgDecodeToDir(supplierDetailDto.getAvatar(),
						"AVATAR_"+supplier.getName() + PasswordUtil.generateRandomPassword(8), dirSupplier,
						AsuransiKapalkuConstant.FileTypeExtension.PNG,
						locationFile));
			}
			
			if (null != supplierDetail.getFreeScale() && supplierDetail.getFreeScale().contains("/pdf;base64")) {
				supplierDetail.setFreeScale(FileUtil.fileDecodeToDir(supplierDetail.getFreeScale(),
						"FREESCALE_"+supplier.getName(), dirSupplier, AsuransiKapalkuConstant.FileTypeExtension.PDF,
						locationFile));
			}
			if (null != supplierDetail.getFreeScale() && supplierDetail.getFreeScale().contains("/doc;base64")) {
				supplierDetail.setFreeScale(FileUtil.fileDecodeToDir(supplierDetail.getFreeScale(),
						"FREESCALE_"+supplier.getName(), dirSupplier, AsuransiKapalkuConstant.FileTypeExtension.DOC,
						locationFile));
			}
			if (null != supplierDetail.getFreeScale() && supplierDetail.getFreeScale().contains("/xls;base64")) {
				supplierDetail.setFreeScale(FileUtil.fileDecodeToDir(supplierDetail.getFreeScale(),
						"FREESCALE_"+supplier.getName(), dirSupplier, AsuransiKapalkuConstant.FileTypeExtension.XLS,
						locationFile));
			}
			if (null != supplierDetail.getFreeScale() && supplierDetail.getFreeScale().contains("/xlsx;base64")) {
				supplierDetail.setFreeScale(FileUtil.fileDecodeToDir(supplierDetail.getFreeScale(),
						"FREESCALE_"+supplier.getName(), dirSupplier, AsuransiKapalkuConstant.FileTypeExtension.XLSX,
						locationFile));
			}
			if (null != supplierDetail.getFreeScale() && supplierDetail.getFreeScale().contains("/png;base64")) {
				supplierDetail.setFreeScale(FileUtil.fileDecodeToDir(supplierDetail.getFreeScale(),
						"FREESCALE_"+supplier.getName(), dirSupplier, AsuransiKapalkuConstant.FileTypeExtension.PNG,
						locationFile));
			}
			if (null != supplierDetail.getFreeScale() && (supplierDetail.getFreeScale().contains("/jpeg;base64") || supplierDetail.getFreeScale().contains("/jpg;base64"))) {
				supplierDetail.setFreeScale(FileUtil.fileDecodeToDir(supplierDetail.getFreeScale(),
						"FREESCALE_"+supplier.getName(), dirSupplier, AsuransiKapalkuConstant.FileTypeExtension.JPG,
						locationFile));
			}
			
			supplierDetailRepository.save(supplierDetail);

			if (null != supplierDetailDto.getClients()) {
				this.convertToSupplierClient(supplierDetailDto.getClients());
			}

			if (null != supplierDetailDto.getGalleries()) {
				this.convertToSupplierGallery(supplierDetailDto.getGalleries());
			}
			
			if(!marineConnectWishLists.isEmpty()) {
				marineConnectWishListRepository.saveAll(marineConnectWishLists);
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("GAGAL MENYIMPAN DATA SUPPLIER : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan.");
		}
		return respon;
	}
	
	private List<MarineConnectWishList> populateWishList() {
		List<MarineConnectWishList> marineConnectWishLists = new ArrayList<>();
		List<Demand> demands = demandRepository.findAll();
		for(Demand d : demands) {
			MarineConnectWishList marineConnectWishList = new MarineConnectWishList();
			marineConnectWishList.setFlag(false);
			marineConnectWishList.setDemand(d);
			marineConnectWishList.setSupplier(this.supplier);
			marineConnectWishLists.add(marineConnectWishList);
		}
		return marineConnectWishLists;
	}

	private void convertToSupplierClient(List<SupplierClientDto> supplierClientDtos)
			throws CommonException {
		for (SupplierClientDto dto : supplierClientDtos) {
			SupplierClient supplierClient = modelMapper.map(dto, SupplierClient.class);
			this.supplierDetailPk.setCode(PasswordUtil.generateRandomPassword(10));
			if(null != dto.getCode()) {
				this.supplierDetailPk.setCode(dto.getCode());
			}
			logger.info("KUNCI PRIMER KLIEN SUPPLIER : {}", this.supplierDetailPk);
			supplierClient.setSupplierDetailPk(this.supplierDetailPk);
			supplierClient.setSupplier(this.supplier);
			if (null != supplierClient.getClientLogo() && supplierClient.getClientLogo().contains("data:")) {
				String dirSupplier = env.getProperty("path.root") + env.getProperty("dir.supplier");
				supplierClient.setClientLogo(FileUtil.imgDecodeToDir(supplierClient.getClientLogo(),
						supplierClient.getClientName(), dirSupplier, AsuransiKapalkuConstant.FileTypeExtension.PNG,
						env.getProperty("asuransikapalku.url") + env.getProperty("dir.supplier")));
			}
			supplierClientRepository.save(supplierClient);
		}
	}

	private void convertToSupplierGallery(List<SupplierGalleryDto> supplierGalleryDtos)
			throws CommonException {
		for (SupplierGalleryDto dto : supplierGalleryDtos) {
			SupplierGallery supplierGallery = new SupplierGallery();
			this.supplierDetailPk.setCode(PasswordUtil.generateRandomPassword(10));
			if(null != dto.getCode()) {
				this.supplierDetailPk.setCode(dto.getCode());
			}
			logger.info("KUNCI PRIMER GALERI SUPPLIER : {}", this.supplierDetailPk);
			supplierGallery.setSupplierDetailPk(this.supplierDetailPk);
			supplierGallery.setSupplier(this.supplier);
			if (null != dto.getFile() && dto.getFile().contains("data:")) {
				String dirSupplier = env.getProperty("path.root") + env.getProperty("dir.supplier");
				supplierGallery.setFile(FileUtil.imgDecodeToDir(dto.getFile(),
						this.supplier.getName() + PasswordUtil.generateRandomPassword(8), dirSupplier,
						AsuransiKapalkuConstant.FileTypeExtension.PNG,
						env.getProperty("asuransikapalku.url") + env.getProperty("dir.supplier")));
			}
			supplierGalleryRepository.save(supplierGallery);
		}
	}

	@Override
	public APIResponse<String> save(SupplierDto supplierDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.userGlobal = user;
			this.validationToUpdate(supplierDto);
			supplierRepository.save(convertDtoToEntity(supplierDto));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA SUPPLIER : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			supplierRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA SUPPLIER [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Supplier [" + id + "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<Supplier> suppliers = supplierRepository.findByIdIn(ids);
			supplierRepository.deleteAll(suppliers);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA SUPPLIER [BATCH] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal menghapus data supplier dengan batch.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDelete(Long id, String user) {
		return null;
	}

	@Override
	public APIResponse<String> softDeleteAll(List<Long> id, String user) {
		return null;
	}

	@Override
	public APIResponse<SupplierDto> findById(Long id) {
		APIResponse<SupplierDto> respon = new APIResponse<>();
		try {
			Supplier supplierLocal = supplierRepository.findById(id).orElse(null);
			if (supplierLocal == null) {
				logger.info("SUPPLIER TIDAK DITEMUKAN [BY ID] : {}", supplier);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Supplier dengan id [" + id + "] tidak ditemukan.");
			}
			respon.setData(convertEntityToDto(supplierLocal));
		} catch (Exception e) {
			logger.info("SUPPLIER TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Supplier dengan id [" + id + "] tidak ditemukan.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<Map<String, Object>> findByIdForMarineConnect(Long id, String user) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		Map<String, Object> map = new HashMap<>();
		try {
			Supplier supplierLocal = supplierRepository.findById(id).orElse(null);
			if (supplierLocal == null) {
				logger.info("SUPPLIER TIDAK DITEMUKAN [BY ID] : {}", supplier);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Supplier dengan id [" + id + "] tidak ditemukan.");
			}
			List<ChatMarineConnectDto> chatMarineConnectDtos = new ArrayList<>();
			List<ChatMarineConnect> chatMarineConnects = chatMarineConnectRepository.findBySenderAndReceiverOrderByIdAsc(supplierLocal.getUser().getUsername(), supplierLocal.getUser().getUsername());
			
			if (!chatMarineConnects.isEmpty()) {
				chatMarineConnectDtos = chatMarineConnects.stream().map(cmc -> modelMapper.map(cmc, ChatMarineConnectDto.class)).collect(Collectors.toList());
			}
			
			map.put("supply", convertEntityToDto(supplierLocal));
			map.put("chat", chatMarineConnectDtos);
			respon.setData(map);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("SUPPLIER TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Supplier dengan id [" + id + "] tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<SupplierDto> findByEmail(String email) {
		APIResponse<SupplierDto> respon = new APIResponse<>();
		try {
			Supplier supplier = supplierRepository.findByEmail(email);
			if (supplier == null) {
				logger.error("SUPPLIER TIDAK DITEMUKAN [BY EMAIL] : {}", supplier);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Supplier dengan email [" + email + "] tidak ditemukan.");
			}
			respon.setData(convertEntityToDto(supplier));
		} catch (Exception e) {
			logger.error("SUPPLIER TIDAK DITEMUKAN [BY EMAIL] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Supplier dengan email [" + email + "] tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<List<SupplierDto>> findWithoutPaging() {
		APIResponse<List<SupplierDto>> respon = new APIResponse<>();
		try {
			respon.setData(
					supplierRepository.findAll().stream().map(this::convertEntityToDto).collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("LIST DATA SUPPLIER TIDAK DITEMUKAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List data tidak ditemukan.");
		}
		return respon;
	} 
	
	@Override
	public APIResponse<List<SupplierDto>> findWithoutPaging(String occupationCode) {
		APIResponse<List<SupplierDto>> respon = new APIResponse<>();
		try {
			respon.setData(
					supplierRepository.findByOccupationCode(occupationCode).stream().map(this::convertEntityToDto).collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("LIST DATA SUPPLIER TIDAK DITEMUKAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List data tidak ditemukan.");
		}
		return respon;
	} 

	@Override
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<Supplier> supplierPage = supplierRepository.findAll(new Specification<Supplier>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Supplier> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					Join<Supplier, User> supplierJoin = root.join("user");

					if (null != filterDto.getSearchBy()) {
						Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate email = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("email")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate username = criteriaBuilder.like(
								criteriaBuilder.upper(supplierJoin.<String>get("username")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(name, email, username));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<SupplierDto> suppliers = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != supplierPage) {
				suppliers = supplierPage.getContent().stream().map(this::convertEntityToDto)
						.collect(Collectors.toList());
				currentPage = supplierPage.getNumber();
				totalElement = supplierPage.getTotalElements();
				totalPage = supplierPage.getTotalPages();
			}

			dataRes.put("suppliers", suppliers);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST SUPPLIER : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List supplier tidak ditemukan.");
		}
		return response;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllShip(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewInsuranceCell> page = viewInsuranceCellRepository.findAll(new Specification<ViewInsuranceCell>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ViewInsuranceCell> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					if (filterDto.getSupplierId() != null) {
						predicates.add(criteriaBuilder.equal(root.get("idSupplier"), filterDto.getSupplierId()));
					}

					if (null != filterDto.getSearchBy()) {
						Predicate vesselName = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("vesselName")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate vesselType = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("vesselType")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate insuranceType = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("insuranceType")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate insuranceKind = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("insuranceKind")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate status = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("status")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates
								.add(criteriaBuilder.or(vesselName, vesselType, insuranceType, insuranceKind, status));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<ViewInsuranceCellDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(p -> modelMapper.map(p, ViewInsuranceCellDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			dataRes.put("ships", list);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST DATA KAPAL [SUPPLIER] : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List kapal tidak ditemukan.");
		}
		return response;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllShipInprogress(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewInsuranceInprogressSup> page = viewInsuranceInprogressSupRepository
					.findAll(new Specification<ViewInsuranceInprogressSup>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ViewInsuranceInprogressSup> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();

							if (filterDto.getSupplierId() != null) {
								predicates
										.add(criteriaBuilder.equal(root.get("idSupplier"), filterDto.getSupplierId()));
							}

							if (null != filterDto.getSearchBy()) {
								Predicate vesselName = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("vesselName")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate vesselType = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("vesselType")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate insuranceType = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("insuranceType")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate insuranceKind = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("insuranceKind")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate status = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("status")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								predicates.add(criteriaBuilder.or(vesselName, vesselType, insuranceType, insuranceKind,
										status));
							}
							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ViewInsuranceInprogressSupDto2> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(p -> modelMapper.map(p, ViewInsuranceInprogressSupDto2.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			dataRes.put("ships", list);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST DATA KAPAL INPROGRESS [SUPPLIER] : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List kapal tidak ditemukan.");
		}
		return response;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllShipBind(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewInsuranceSup> page = viewInsuranceSupRepository.findAll(new Specification<ViewInsuranceSup>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ViewInsuranceSup> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					if (filterDto.getSupplierId() != null) {
						predicates.add(criteriaBuilder.equal(root.get("idSupplier"), filterDto.getSupplierId()));
					}

					if (null != filterDto.getSearchBy()) {
						Predicate vesselName = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("vesselName")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate vesselType = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("vesselType")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate insuranceType = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("insuranceType")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate insuranceKind = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("insuranceKind")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate status = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("status")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates
								.add(criteriaBuilder.or(vesselName, vesselType, insuranceType, insuranceKind, status));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<ViewInsuranceSupDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(p -> modelMapper.map(p, ViewInsuranceSupDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			dataRes.put("ships", list);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST DATA KAPAL ASURANSIKU [SUPPLIER] : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List kapal tidak ditemukan.");
		}
		return response;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllShipRejected(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewInsuranceSupRej> page = viewInsuranceSupRejRepository
					.findAll(new Specification<ViewInsuranceSupRej>() {
						/**
						* 
						*/
						private static final long serialVersionUID = 1L;

						@Override
						public Predicate toPredicate(Root<ViewInsuranceSupRej> root, CriteriaQuery<?> query,
								CriteriaBuilder criteriaBuilder) {
							List<Predicate> predicates = new ArrayList<>();

							if (filterDto.getSupplierId() != null) {
								predicates
										.add(criteriaBuilder.equal(root.get("idSupplier"), filterDto.getSupplierId()));
							}

							if (null != filterDto.getSearchBy()) {
								Predicate vesselName = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("vesselName")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate vesselType = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("vesselType")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate insuranceType = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("insuranceType")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate insuranceKind = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("insuranceKind")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								Predicate status = criteriaBuilder.like(
										criteriaBuilder.upper(root.<String>get("status")),
										"%" + filterDto.getSearchBy().toUpperCase() + "%");
								predicates.add(criteriaBuilder.or(vesselName, vesselType, insuranceType, insuranceKind,
										status));
							}
							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, pageable);
			List<ViewInsuranceSupRejDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(p -> modelMapper.map(p, ViewInsuranceSupRejDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			dataRes.put("ships", list);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST DATA KAPAL ASURANSIKU REJECT [SUPPLIER] : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List kapal tidak ditemukan.");
		}
		return response;
	}

	@Override
	public void authenticateCheck(LoginDto loginDto) throws ValidateException {
		authentication(loginDto);
	}
	
	@Override
	public APIResponse<Map<String, Object>> findAllSupplierMarineConnect(FilterDto filterDto, User user) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewSupplierPage> supplierPage = viewSupplierPageRepository.findAll(new Specification<ViewSupplierPage>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ViewSupplierPage> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					
					predicates
					.add(criteriaBuilder.equal(root.get("idDemand"), user.getDemand().getId()));
					
					if(null != filterDto.getSupplierFilter()) {
//						if(null != filterDto.getSupplierFilter().getInsuranceNeeds()) {
//							predicates
//							.add(criteriaBuilder.like(
//									criteriaBuilder.upper(root.<String>get("insuranceNeeds")),
//									"%" + filterDto.getSupplierFilter().getInsuranceNeeds().toUpperCase() + "%"));
//						}
						if(null != filterDto.getSupplierFilter().getObjectType()) {
							predicates
							.add(criteriaBuilder.equal(root.get("objectType"), filterDto.getSupplierFilter().getObjectType()));
						}
						if(null != filterDto.getSupplierFilter().getCityId()) {
							predicates
							.add(criteriaBuilder.equal(root.get("idLocation"), filterDto.getSupplierFilter().getCityId()));
						}
					}
					
					if(null != filterDto.getWishListFlag()) {
						predicates
						.add(criteriaBuilder.equal(root.get("flag"), filterDto.getWishListFlag()));
						Predicate demandNotNull = criteriaBuilder.equal(root.get("idDemand"), filterDto.getDemandId());
						Predicate demandNull = criteriaBuilder.isNull(root.get("idDemand"));
						predicates.add(criteriaBuilder.or(demandNotNull, demandNull));
					}
					
					if(null != filterDto.getSearchBy()) {
						Predicate insuranceNeeds = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("insuranceNeeds")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate occupationName = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("occupationName")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate objectType = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("objectType")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate locationName = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("locationName")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(insuranceNeeds, occupationName, objectType, locationName));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<ViewSupplierPageDto> pageDtos = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != supplierPage) {
				pageDtos = supplierPage.getContent().stream().map(s -> modelMapper.map(s, ViewSupplierPageDto.class))
						.collect(Collectors.toList());
				currentPage = supplierPage.getNumber();
				totalElement = supplierPage.getTotalElements();
				totalPage = supplierPage.getTotalPages();
			}

			dataRes.put("suppliers", pageDtos);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST SUPPLIER : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List supplier tidak ditemukan.");
		}
		return response;
	}

	private Supplier convertDtoToEntity(SupplierDto supplierDto) {
		Supplier supplierLocal = modelMapper.map(supplierDto, Supplier.class);
		if (supplierDto.getId() == null || supplierDto.getId() == 0) {
			supplierLocal.setCreatedBy(userGlobal);
			supplierLocal.setCreatedDate(new Date());
		} else {
			Supplier supplierOld = supplierRepository.findById(supplierDto.getId()).orElse(null);
			if (supplierOld != null) {
				supplierLocal.setCreatedBy(supplierOld.getCreatedBy());
				supplierLocal.setCreatedDate(supplierOld.getCreatedDate());
			}
			supplierLocal.setUpdatedBy(userGlobal);
			supplierLocal.setUpdatedDate(new Date());
		}

		return supplierLocal;
	}

	private SupplierDto convertEntityToDto(Supplier supplier) {
		SupplierDto supplierDto = modelMapper.map(supplier, SupplierDto.class);
		supplierDto.setUsername(supplier.getUser().getUsername());
		if (null != supplier.getOccupation()) {
			supplierDto.setOccupation(modelMapper.map(supplier.getOccupation(), OccupationDto.class));
		}

		this.supplierDetailPk = new SupplierDetailPk();
		this.supplierDetailPk.setIdSupplier(supplier.getId());
		SupplierDetail supplierDetail = supplierDetailRepository.findByIdSupplier(supplier.getId());

		if (null == supplierDetail) {
			supplierDto.setDetail(new SupplierDetailDto());
		} else {
			supplierDto.setDetail(this.convertEntityToSupplierDetailDto(supplierDetail));
		}

		List<SupplierClient> supplierClients = supplierClientRepository.findByIdSupplier(supplier.getId());
		List<SupplierGallery> supplierGalleries = supplierGalleryRepository.findByIdSupplier(supplier.getId());

		if (supplierClients.isEmpty()) {
			supplierDto.getDetail().setClients(new ArrayList<>());
		} else {
			supplierDto.getDetail().setClients(supplierClients.stream()
					.map(this::convertEntityToSupplierClientDto).collect(Collectors.toList()));
		}

		if (supplierGalleries.isEmpty()) {
			supplierDto.getDetail().setGalleries(new ArrayList<>());
		} else {
			supplierDto.getDetail().setGalleries(supplierGalleries.stream()
					.map(this::convertEntityToSupplierGalleryDto).collect(Collectors.toList()));
		}

		return supplierDto;
	}
	
	private SupplierDetailDto convertEntityToSupplierDetailDto(SupplierDetail supplierDetail) {
		SupplierDetailDto supplierDetailDto = modelMapper.map(supplierDetail, SupplierDetailDto.class);
		supplierDetailDto.setCode(supplierDetail.getSupplierDetailPk().getCode());
		supplierDetailDto.setIdSupplier(supplierDetail.getSupplierDetailPk().getIdSupplier());
		supplierDetailDto.setCity(modelMapper.map(supplierDetail.getCity(), Citi.class));
		supplierDetailDto.getCity().setProvince(modelMapper.map(supplierDetail.getCity().getProvince(), Prov.class));
		return supplierDetailDto;
	}
	
	private SupplierClientDto convertEntityToSupplierClientDto(SupplierClient supplierClient) {
		SupplierClientDto supplierClientDto = modelMapper.map(supplierClient, SupplierClientDto.class);
		supplierClientDto.setCode(supplierClient.getSupplierDetailPk().getCode());
		return supplierClientDto;
	}
	
	private SupplierGalleryDto convertEntityToSupplierGalleryDto(SupplierGallery supplierGallery) {
		SupplierGalleryDto supplierGalleryDto = modelMapper.map(supplierGallery, SupplierGalleryDto.class);
		supplierGalleryDto.setCode(supplierGallery.getSupplierDetailPk().getCode());
		return supplierGalleryDto;
	}

	private void authentication(LoginDto loginDto) throws ValidateException {
		if (loginDto.getUsername().contains("@")) {
			Supplier supplierLocal = supplierRepository.findByEmail(loginDto.getUsername());

			if (null == supplierLocal) {
				throw new ValidateException("Akun tidak ditemukan");
			}

			if (Boolean.FALSE.equals(supplierLocal.getUser().getIsActive())) {
				throw new ValidateException("Akun tidak aktif");
			}

			loginDto.setUsername(supplierLocal.getUser().getUsername());
		}

		if (null == loginDto.getUsername()) {
			throw new ValidateException("Mohon masukkan username");
		}

		if (null == loginDto.getUsername()) {
			throw new ValidateException("Mohon masukkan password");
		}

		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
		} catch (DisabledException e) {
			throw new ValidateException("Akun kamu tidak aktif.");
		} catch (BadCredentialsException e) {
			throw new ValidateException("Username atau password salah.");
		}
	}

	private void validationToRegister(SupplierRegisterDto supplierRegisterDto) throws ValidateException {
		if (null == supplierRegisterDto.getName()) {
			throw new ValidateException("Nama harus diisi.");
		}

		if (null == supplierRegisterDto.getEmail()) {
			throw new ValidateException("Email harus diisi.");
		}

		if (null == supplierRegisterDto.getUsername()) {
			throw new ValidateException("Username harus diisi.");
		}

		Integer count = supplierRepository.getCountByEmail(supplierRegisterDto.getEmail());
		count = count == null ? 0 : count;
		if (count > 0) {
			throw new ValidateException("Email sudah digunakan.");
		}
	}

	private void validationToUpdate(SupplierDto supplierDto) throws ValidateException {
		if (null == supplierDto.getName()) {
			throw new ValidateException("Nama harus diisi.");
		}

		if (null == supplierDto.getEmail()) {
			throw new ValidateException("Email harus diisi.");
		}

		if (null == supplierDto.getUsername()) {
			throw new ValidateException("Username harus diisi.");
		}

		Integer count = supplierRepository.getCountByEmail(supplierDto.getEmail());
		count = count == null ? 0 : count;
		if ((null == supplierDto.getId() && count > 0) || (null != supplierDto.getId() && count > 1)) {
			throw new ValidateException("Email sudah digunakan.");
		}
	}

	@Override
	public String getEncodePassword(String password) {
		return bcryptEncoder.encode(password);
	}
}
