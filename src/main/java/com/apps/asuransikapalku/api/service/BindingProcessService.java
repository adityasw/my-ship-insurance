package com.apps.asuransikapalku.api.service;

import java.util.List;
import java.util.Map;

import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.AddQuotationDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.AssignSupplierDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BidingDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingProcessDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionClaimDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionDocumentDemandDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionQuotationDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.ReplacementDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.UpdateStatusTCDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.UploadTcDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDto;
import com.apps.asuransikapalku.api.model.entity.user.User;

public interface BindingProcessService {
	public APIResponse<String> requestReplacement(ReplacementDto replacementDto, String user);
	
	public APIResponse<String> updateStatus(BindingProcessDto bindingProcessDto, String user);
	
	public APIResponse<String> uploadTC(Map<String, Object> map, User user);

	public APIResponse<String> bidingShip(BidingDto bidingDto, String user);
	
	public APIResponse<Map<String, Object>> findBindingProcessById(Long id);
	
	public APIResponse<Map<String, Object>> findAllBindingProcess(FilterDto filterDto);
	
	public APIResponse<List<Map<String, Object>>> findStatus();
	
	public APIResponse<List<Map<String, Object>>> findStatusType();

	public APIResponse<String> uploadTC1(UploadTcDto uploadTcDto, User user);
	
	public APIResponse<String> deleteQuotation(Long id);
	
	public APIResponse<String> addQuotation(AddQuotationDto addQuotationDto, String user);
	
	public APIResponse<String> addDocumentDemand(BindingTransactionDocumentDemandDto bindingTransactionDocumentDemandDto, String user);

	public APIResponse<String> updateStatusTc(UpdateStatusTCDto updateStatusTCDto);
	
	public APIResponse<String> deleteTc(Long id);
	
	public APIResponse<String> updateAssignee(AssignSupplierDto assignSupplierDto);
	
	public APIResponse<Map<String, Object>> findAllDocumentTc(FilterDto filterDto);
	
	public APIResponse<String> selectedQuotation(Map<String, Object> map);
	
	public APIResponse<List<SupplierDto>> getSupplierForTc(Long id);
	
	public APIResponse<String> addClaim(BindingTransactionClaimDto bindingTransactionClaimDto);

	public APIResponse<List<SupplierDto>> getSupplierForQuotation(Long id);
}
