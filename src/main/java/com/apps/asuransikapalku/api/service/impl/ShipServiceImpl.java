package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.enums.ShipStatusEnum;
import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.FilterDto.Sorting;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceKindDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceMasterDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceTypeDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionClaimDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionDocumentDemandDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionDocumentTcDemandDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionQuotationDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.ChatDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.QuotationMapDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.QuotationMapSecurityListDto;
import com.apps.asuransikapalku.api.model.dto.ship.DetailVesselDto;
import com.apps.asuransikapalku.api.model.dto.ship.InsuranceOnGoingDto;
import com.apps.asuransikapalku.api.model.dto.ship.LossRecordDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipApprovalDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipInsuranceMapDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipMasterDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipTypeDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewProjectTcDto;
import com.apps.asuransikapalku.api.model.entity.insurance.InsuranceKind;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransaction;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionDocument;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionQuotation;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionSupplierMap;
import com.apps.asuransikapalku.api.model.entity.irevolution.Chat;
import com.apps.asuransikapalku.api.model.entity.irevolution.QuoatationMap;
import com.apps.asuransikapalku.api.model.entity.irevolution.QuotationMapSecurityList;
import com.apps.asuransikapalku.api.model.entity.master.ClassificationBureau;
import com.apps.asuransikapalku.api.model.entity.ship.InsuranceOnGoing;
import com.apps.asuransikapalku.api.model.entity.ship.LossRecord;
import com.apps.asuransikapalku.api.model.entity.ship.ShipInsuranceMap;
import com.apps.asuransikapalku.api.model.entity.ship.ShipMaster;
import com.apps.asuransikapalku.api.model.entity.ship.ShipType;
import com.apps.asuransikapalku.api.model.entity.user.Demand;
import com.apps.asuransikapalku.api.model.entity.user.Supplier;
import com.apps.asuransikapalku.api.repository.insurance.InsuranceKindRepository;
import com.apps.asuransikapalku.api.repository.insurance.InsuranceMasterRepository;
import com.apps.asuransikapalku.api.repository.insurance.InsuranceTypeRepository;
import com.apps.asuransikapalku.api.repository.irevolution.BindingTransactionRepository;
import com.apps.asuransikapalku.api.repository.irevolution.ChatRepository;
import com.apps.asuransikapalku.api.repository.master.ClassifiacationBureauRepository;
import com.apps.asuransikapalku.api.repository.ship.ShipInsuranceMapRepository;
import com.apps.asuransikapalku.api.repository.ship.ShipMasterRepository;
import com.apps.asuransikapalku.api.repository.ship.ShipTypeRepository;
import com.apps.asuransikapalku.api.repository.user.DemandRepository;
import com.apps.asuransikapalku.api.repository.user.SupplierRepository;
import com.apps.asuransikapalku.api.service.ShipService;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Transactional
@Service
public class ShipServiceImpl implements ShipService {
	private static final Logger logger = LoggerFactory.getLogger(ShipServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";

	private BindingTransactionRepository bindingTransactionRepository;

	private ChatRepository chatRepository;

	private ClassifiacationBureauRepository classificationBureauRepository;

	private DemandRepository demandRepository;

	private InsuranceMasterRepository insuranceMasterRepository;

	private InsuranceKindRepository insuranceKindRepository;

	private ShipInsuranceMapRepository shipInsuranceMapRepository;

	private ShipMasterRepository shipMasterRepository;

	private ShipTypeRepository shipTypeRepository;

	private SupplierRepository supplierRepository;
	
	private InsuranceTypeRepository insuranceTypeRepository;

	private ModelMapper modelMapper = new ModelMapper();

	private Long id = null;

	private String username = null;

	private boolean isProject = Boolean.FALSE;
	
	private boolean isSupply = Boolean.FALSE;

	private Long idBinding = null;

	@Autowired
	public void repository(ClassifiacationBureauRepository classificationBureauRepository,
			ShipMasterRepository shipMasterRepository, ShipTypeRepository shipTypeRepository,
			SupplierRepository supplierRepository) {
		this.classificationBureauRepository = classificationBureauRepository;
		this.shipMasterRepository = shipMasterRepository;
		this.shipTypeRepository = shipTypeRepository;
		this.supplierRepository = supplierRepository;
	}

	@Autowired
	public void repository1(InsuranceKindRepository insuranceKindRepository,
			InsuranceMasterRepository insuranceMasterRepository, DemandRepository demandRepository,
			ShipInsuranceMapRepository shipInsuranceMapRepository) {
		this.demandRepository = demandRepository;
		this.insuranceKindRepository = insuranceKindRepository;
		this.insuranceMasterRepository = insuranceMasterRepository;
		this.shipInsuranceMapRepository = shipInsuranceMapRepository;
	}

	@Autowired
	public void repository2(BindingTransactionRepository bindingTransactionRepository, ChatRepository chatRepository) {
		this.bindingTransactionRepository = bindingTransactionRepository;
		this.chatRepository = chatRepository;
	}

	@Override
	public APIResponse<String> approvalShip(ShipApprovalDto shipApprovalDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			Date currentDate = new Date();
			ShipMaster shipMaster = shipMasterRepository.findById(shipApprovalDto.getId()).orElse(null);
			if (null == shipMaster) {
				logger.info("KAPAL TIDAK DITEMUKAN [APPROVAL KAPAL]");
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Kapal tidak ditemukan");
			}

			if (shipMaster.getStatusApproval().equalsIgnoreCase(ShipStatusEnum.APPROVED.code)) {
				logger.error("GAGAL MENGUBAH STATUS KAPAL : KAPAL SUDAH DI APPROVED");
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Kapal telah di setujui admin.");
			}

			shipMaster.setStatusApproval(shipApprovalDto.getStatusApproval());

			if (ShipStatusEnum.APPROVED.code.equalsIgnoreCase(shipApprovalDto.getStatusApproval())) {
				shipMaster.setApprovalDate(currentDate);
			}
			shipMaster.setUpdatedBy(user);
			shipMaster.setUpdatedDate(currentDate);
			shipMasterRepository.save(shipMaster);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA KAPAL : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	private Long getListInsuranceId(ShipInsuranceMapDto shipInsuranceMapDto) {
		return shipInsuranceMapDto.getInsuranceMasterDto().getId();
	}

	@Override
	public APIResponse<String> save(ShipMasterDto shipMasterDto, String user) {
		logger.info("SUBMIT DATA DETAIL KAPAL : {}", shipMasterDto);
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.validation(shipMasterDto);
			List<Long> insuranceIds = new ArrayList<>();
			List<ShipInsuranceMap> shipInsuranceMaps = new ArrayList<>();
			if (null != shipMasterDto.getInsuranceMapDtos() && !shipMasterDto.getInsuranceMapDtos().isEmpty()) {
				insuranceIds = shipMasterDto.getInsuranceMapDtos().stream().map(this::getListInsuranceId)
						.collect(Collectors.toList());
			}

			if (!insuranceIds.isEmpty()) {
				shipInsuranceMapRepository.deleteMap(insuranceIds, shipMasterDto.getId());
			}

			ShipMaster shipMaster = modelMapper.map(shipMasterDto, ShipMaster.class);
			shipMaster.setDemand(demandRepository.findById(shipMasterDto.getDemand().getId()).orElse(null));
			
			if(Boolean.TRUE.equals(shipMasterDto.getIsOtherShipType())) {
				shipMaster.setOtherShipType(shipMasterDto.getOtherShipType());
			} else {
				shipMaster.setShipType(shipTypeRepository.findById(shipMasterDto.getShipType().getId()).orElse(null));
			}
			
			shipMaster.setInsuranceKind(
					insuranceKindRepository.findById(shipMasterDto.getInsuranceKindDto().getId()).orElse(null));
			if (shipMasterDto.getId() == null || shipMasterDto.getId() == 0) {
				shipMaster.setCreatedBy(user);
				shipMaster.setCreatedDate(new Date());
				shipMaster.setStatusApproval(ShipStatusEnum.WAITING_FOR_APPROVAL.code);
			} else {
				ShipMaster shipMasterOld = shipMasterRepository.findById(shipMasterDto.getId()).orElse(null);
				if (null != shipMasterOld) {
					shipMaster.setCreatedBy(shipMasterOld.getCreatedBy());
					shipMaster.setCreatedDate(shipMasterOld.getCreatedDate());
					shipInsuranceMaps = shipMasterOld.getShipInsuranceMaps();
					if (null == shipMasterDto.getStatusApproval()) {
						shipMaster.setStatusApproval(shipMasterOld.getStatusApproval());
					}
				}
				shipMaster.setUpdatedBy(user);
				shipMaster.setUpdatedDate(new Date());
			}

			shipMaster.setShipType(shipTypeRepository.findById(shipMasterDto.getShipType().getId()).orElse(null));

			if (null != shipMasterDto.getClassificationBureau()) {
				shipMaster.setClassificationBureau(classificationBureauRepository
						.findById(shipMasterDto.getClassificationBureau().getId()).orElse(null));
			}

			if (!shipMasterDto.getLossRecords().isEmpty()) {
				List<LossRecord> lossRecords = new ArrayList<>();
				for (LossRecordDto l : shipMasterDto.getLossRecords()) {
					LossRecord lossRecord = modelMapper.map(l, LossRecord.class);
					lossRecord.setShipMaster(shipMaster);
					lossRecord.setInsuranceKind(
							insuranceKindRepository.findById(l.getInsuranceKind().getId()).orElse(null));
					lossRecords.add(lossRecord);
				}
				shipMaster.setLossRecords(lossRecords);
			}

			if (!shipMasterDto.getInsuranceOnGoings().isEmpty()) {
				List<InsuranceOnGoing> insuranceOnGoings = new ArrayList<>();
				for (InsuranceOnGoingDto l : shipMasterDto.getInsuranceOnGoings()) {
					InsuranceOnGoing insuranceOnGoing = modelMapper.map(l, InsuranceOnGoing.class);
					insuranceOnGoing.setShipMaster(shipMaster);
					insuranceOnGoing.setInsuranceKind(
							insuranceKindRepository.findById(l.getInsuranceKindDto().getId()).orElse(null));
					insuranceOnGoing.setPeriode(l.getPeriode());
					insuranceOnGoings.add(insuranceOnGoing);
				}
				shipMaster.setInsuranceOnGoings(insuranceOnGoings);
			}

			if (!insuranceIds.isEmpty()) {
				if (!shipInsuranceMaps.isEmpty()) {
					for (Iterator<Long> iter = insuranceIds.listIterator(); iter.hasNext();) {
						Long l = iter.next();
						for (ShipInsuranceMap map : shipInsuranceMaps) {
							if (l.equals(map.getInsuranceMaster().getId())) {
								iter.remove();
							}
						}
					}
				}

				shipInsuranceMaps = new ArrayList<>();
				for (Long l : insuranceIds) {
					ShipInsuranceMap shipInsuranceMap = new ShipInsuranceMap();
					shipInsuranceMap.setShipMaster(shipMaster);
					shipInsuranceMap.setInsuranceMaster(insuranceMasterRepository.findById(l).orElse(null));
					shipInsuranceMaps.add(shipInsuranceMap);
				}
				shipMaster.setShipInsuranceMaps(shipInsuranceMaps);
			}

			shipMasterRepository.save(shipMaster);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("GAGAL MENYIMPAN DATA KAPAL : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}

	@Override
	public APIResponse<String> updateDetailShip(DetailVesselDto detailVesselDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			shipInsuranceMapRepository.deleteMap(detailVesselDto.getAssignInsurance(), detailVesselDto.getId());
			logger.info("BERHASIL HAPUS MAPPING ASURANSI");

			ShipMaster shipMaster = shipMasterRepository.findById(detailVesselDto.getId()).orElse(null);
			List<Long> newInsurances = new ArrayList<>();
			List<ShipInsuranceMap> insuranceMaps = new ArrayList<>();
			if (!shipMaster.getShipInsuranceMaps().isEmpty() && !detailVesselDto.getAssignInsurance().isEmpty()) {
				for (Long l : detailVesselDto.getAssignInsurance()) {
					boolean exists = false;
					for (ShipInsuranceMap sim : shipMaster.getShipInsuranceMaps()) {
						if (l.equals(sim.getId())) {
							exists = true;
							break;
						}

						if (!exists) {
							newInsurances.add(l);
						}
					}
				}
			} else {
				newInsurances.addAll(detailVesselDto.getAssignInsurance());
			}

			for (Long l : newInsurances) {
				ShipInsuranceMap shipInsuranceMap = new ShipInsuranceMap();
				shipInsuranceMap.setInsuranceMaster(insuranceMasterRepository.findById(l).orElse(null));
				shipInsuranceMap.setIsCheck(Boolean.FALSE);
				shipInsuranceMap.setShipMaster(shipMaster);
				insuranceMaps.add(shipInsuranceMap);
			}
			shipMaster.getShipInsuranceMaps().addAll(insuranceMaps);
			shipMaster.setUpdatedBy(user);
			shipMaster.setUpdatedDate(new Date());
			shipMasterRepository.save(shipMaster);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			shipMasterRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA KAPAL [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Kapal dengan id [" + id + "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<ShipMaster> shipMasters = shipMasterRepository.findByIdIn(ids);
			shipMasterRepository.deleteAll(shipMasters);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA KAPAL [BATCH] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal menghapus data kapal dengan batch.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDelete(Long id, String user) {
		return null;
	}

	@Override
	public APIResponse<String> softDeleteAll(List<Long> id, String user) {
		return null;
	}

	@Override
	public APIResponse<ShipMasterDto> findById(Long id) {
		APIResponse<ShipMasterDto> respon = new APIResponse<>();
		try {
			this.isProject = Boolean.FALSE;
			respon.setData(convertEntityToDto(shipMasterRepository.findById(id).orElse(null)));
		} catch (Exception e) {
			logger.error("DATA KAPAL TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data kapal tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAll(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {

			if (null == filterDto.getSorting()) {
				Sorting sorting = new Sorting();
				sorting.setColumnName("id");
				sorting.setIsAscending(Boolean.FALSE);
				List<Sorting> sortings = new ArrayList<>();
				sortings.add(sorting);
				filterDto.setSorting(sortings);
			} else if(filterDto.getSorting().isEmpty()) {
				Sorting sorting = new Sorting();
				sorting.setColumnName("id");
				sorting.setIsAscending(Boolean.FALSE);
				List<Sorting> sortings = new ArrayList<>();
				sortings.add(sorting);
				filterDto.setSorting(sortings);
			}

			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ShipMaster> shipMasterPage = shipMasterRepository.findAll(new Specification<ShipMaster>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ShipMaster> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					Join<ShipMaster, Demand> demandJoin = root.join("demand");
					Join<ShipMaster, ShipType> shipTypeJoin = root.join("shipType");
					Join<ShipMaster, InsuranceKind> insuranceKindJoin = root.join("insuranceKind");
					Join<ShipMaster, ClassificationBureau> classificationBureauJoin = root.join("classificationBureau");
					String regex = "\\d+";

					if (filterDto.getDemandId() != null && filterDto.getDemandId() != 0) {
						predicates.add(criteriaBuilder.equal(demandJoin.get("id"), filterDto.getDemandId()));
					}

					if (filterDto.getShipTypeId() != null && filterDto.getShipTypeId() != 0) {
						predicates.add(criteriaBuilder.equal(shipTypeJoin.get("id"), filterDto.getShipTypeId()));
					}

					if (filterDto.getInsuranceKindId() != null && filterDto.getInsuranceKindId() != 0) {
						predicates.add(
								criteriaBuilder.equal(insuranceKindJoin.get("id"), filterDto.getInsuranceKindId()));
					}

					if (filterDto.getClassificationBureauId() != null && filterDto.getClassificationBureauId() != 0) {
						predicates.add(criteriaBuilder.equal(classificationBureauJoin.get("id"),
								filterDto.getClassificationBureauId()));
					}

					if (null != filterDto.getSearchBy()) {
						Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate coverageValue = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("coverageValue")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate productionYear = null;
						Predicate age = null;
						if (filterDto.getSearchBy().matches(regex)) {
							productionYear = criteriaBuilder.equal(root.<Integer>get("productionYear"),
									Integer.valueOf(filterDto.getSearchBy()));
							age = criteriaBuilder.equal(root.<Integer>get("age"),
									Integer.valueOf(filterDto.getSearchBy()));
						}

						if (null != productionYear && null != age) {
							predicates.add(criteriaBuilder.or(name, coverageValue, productionYear, age));
						} else {
							predicates.add(criteriaBuilder.or(name, coverageValue));
						}
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<ShipMasterDto> shipMasters = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != shipMasterPage) {
				this.isProject = Boolean.FALSE;
				shipMasters = shipMasterPage.getContent().stream().map(this::convertEntityToDto)
						.collect(Collectors.toList());
				currentPage = shipMasterPage.getNumber();
				totalElement = shipMasterPage.getTotalElements();
				totalPage = shipMasterPage.getTotalPages();
			}

			dataRes.put("shipMasters", shipMasters);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST KAPAL : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List kapal tidak ditemukan.");
		}
		return response;
	}

	@Override
	public APIResponse<Map<String, Object>> findDetailShipFromSupply(Long id, Long idSupplier) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		Map<String, Object> map = new HashMap<>();
		try {

			Supplier supplier = supplierRepository.findById(idSupplier).orElse(null);
			if (null == supplier) {
				logger.error("SUPPLIER TIDAK DITEMUKAN [FIND DETAIL SHIP FROM SUPPLY]");
				response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data supplier tidak ditemukan.");
			}
			if (null != supplier.getUser()) {
				this.username = supplier.getUser().getUsername();
			}
			this.id = id;
			List<ViewProjectTcDto> documents = new ArrayList<>();
			List<BindingTransactionDocumentDemandDto> otherDocuments = new ArrayList<>();
			List<BindingTransactionQuotationDto> quotations = new ArrayList<>();
			BindingTransaction bindingTransaction = bindingTransactionRepository.findById(id).orElse(null);

			if (null == bindingTransaction) {
				response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data tidak ditemukan.");
			}

			BindingTransactionSupplierMap bindingTransactionSupplierMap = bindingTransaction
					.getBindingTransactionSupplierMaps().stream()
					.filter(btsm -> idSupplier.equals(btsm.getSupplier().getId())).findAny().orElse(null);

			if (null != bindingTransactionSupplierMap
					&& !bindingTransactionSupplierMap.getBindingTransactionDocuments().isEmpty()) {
				documents = bindingTransactionSupplierMap.getBindingTransactionDocuments().stream()
						.map(this::convertDocEntityToDto).collect(Collectors.toList());
			}
			if (!bindingTransaction.getTransactionDocumentDemands().isEmpty()) {
				otherDocuments = bindingTransaction.getTransactionDocumentDemands().stream()
						.map(q -> modelMapper.map(q, BindingTransactionDocumentDemandDto.class))
						.collect(Collectors.toList());
			}
			this.isProject = Boolean.TRUE;
			this.idBinding = id;
			this.isSupply = Boolean.TRUE;
			map.put("vessel", convertEntityToDto(bindingTransaction.getShipMaster()));
			map.put("quotations", quotations);
			map.put("documents", documents);
			map.put("other-documents", otherDocuments);
			map.put("id-binding", id);
			response.setData(map);
		} catch (Exception e) {
			logger.error("DATA TIDAK DITEMUKAN [SUPPLIER] : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data tidak ditemukan.");
		}
		return response;
	}

	@Override
	public APIResponse<Map<String, Object>> findDetailShipFromDemand(Long id) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		Map<String, Object> map = new HashMap<>();
		try {

			this.id = id;
			List<BindingTransactionQuotationDto> quotations = new ArrayList<>();
			List<BindingTransactionDocumentDemandDto> documents = new ArrayList<>();
			List<BindingTransactionClaimDto> claims = new ArrayList<>();
			List<BindingTransactionDocumentTcDemandDto> tcDemands = new ArrayList<>();
			BindingTransaction bindingTransaction = bindingTransactionRepository.findById(id).orElse(null);
			if (null == bindingTransaction) {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data tidak ditemukan.");
			}

			if (!bindingTransaction.getTransactionDocumentDemands().isEmpty()) {
				documents = bindingTransaction.getTransactionDocumentDemands().stream()
						.map(q -> modelMapper.map(q, BindingTransactionDocumentDemandDto.class))
						.collect(Collectors.toList());
			}

			if (!bindingTransaction.getBindingTransactionQuotations().isEmpty()) {
				quotations = bindingTransaction.getBindingTransactionQuotations().stream()
						.map(this::converEntityToBindingTransactionQuotationDto).collect(Collectors.toList());
			}

			if (!bindingTransaction.getBindingTransactionClaims().isEmpty()) {
				claims = bindingTransaction.getBindingTransactionClaims().stream()
						.map(q -> modelMapper.map(q, BindingTransactionClaimDto.class)).collect(Collectors.toList());
			}
			
			if (!bindingTransaction.getBindingTransactionDocumentTcDemands().isEmpty()) {
				tcDemands = bindingTransaction.getBindingTransactionDocumentTcDemands().stream()
						.map(q -> modelMapper.map(q, BindingTransactionDocumentTcDemandDto.class)).collect(Collectors.toList());
			}

			this.isProject = Boolean.TRUE;
			this.idBinding = id;
			this.isSupply = Boolean.FALSE;
			map.put("vessel", convertEntityToDto(bindingTransaction.getShipMaster()));
			map.put("claim-projects", claims);
			map.put("quotations", quotations);
			map.put("documents", documents);
			map.put("id-binding", id);
			map.put("document-tc",tcDemands.stream()
					.sorted(Comparator.comparingLong(BindingTransactionDocumentTcDemandDto::getId))
					.collect(Collectors.toList()));
			response.setData(map);
		} catch (Exception e) {
			logger.error("DATA TIDAK DITEMUKAN [DEMAND] : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data tidak ditemukan.");
		}
		return response;
	}

	private BindingTransactionQuotationDto converEntityToBindingTransactionQuotationDto(
			BindingTransactionQuotation bindingTransactionQuotation) {
		BindingTransactionQuotationDto bindingTransactionQuotationDto = modelMapper.map(bindingTransactionQuotation,
				BindingTransactionQuotationDto.class);
		bindingTransactionQuotationDto.setQuoatationMaps(bindingTransactionQuotation.getQuoatationMaps().stream()
				.map(this::convertEntityToQuotationMapDto).collect(Collectors.toList()));
		return bindingTransactionQuotationDto;
	}

	private QuotationMapDto convertEntityToQuotationMapDto(QuoatationMap quoatationMap) {
		QuotationMapDto quotationMapDto = modelMapper.map(quoatationMap, QuotationMapDto.class);
		quotationMapDto.setQuotationMapSecurityLists(quoatationMap.getQuotationMapSecurityLists().stream()
				.map(this::convertEntityToQuotationMapSecurityListDto).collect(Collectors.toList()));
		quotationMapDto
				.setInsuranceMaster(modelMapper.map(quoatationMap.getInsuranceMaster(), InsuranceMasterDto.class));
		quotationMapDto.setSupplier(modelMapper.map(quoatationMap.getSupplier(), SupplierDto.class));
		return quotationMapDto;
	}

	private QuotationMapSecurityListDto convertEntityToQuotationMapSecurityListDto(
			QuotationMapSecurityList quotationMapSecurityList) {
		QuotationMapSecurityListDto quotationMapSecurityListDto = modelMapper.map(quotationMapSecurityList,
				QuotationMapSecurityListDto.class);
		quotationMapSecurityListDto.setInsuranceMaster(
				modelMapper.map(quotationMapSecurityList.getInsuranceMaster(), InsuranceMasterDto.class));
		return quotationMapSecurityListDto;
	}

	@Override
	public APIResponse<List<Map<String, Object>>> findShipStatus() {
		APIResponse<List<Map<String, Object>>> respon = new APIResponse<>();
		List<Map<String, Object>> maps = new ArrayList<>();
		for (ShipStatusEnum sse : ShipStatusEnum.values()) {
			Map<String, Object> map = new HashMap<>();
			map.put("code", sse.code);
			map.put("name", sse.label);
			maps.add(map);
		}
		respon.setData(maps);
		return respon;
	}

	private ShipMasterDto convertEntityToDto(ShipMaster shipMaster) {
		ShipMasterDto shipMasterDto = modelMapper.map(shipMaster, ShipMasterDto.class);
		shipMasterDto.setInsuranceKindDto(modelMapper.map(shipMaster.getInsuranceKind(), InsuranceKindDto.class));
		shipMasterDto.getInsuranceKindDto().setInsuranceType(
				modelMapper.map(shipMaster.getInsuranceKind().getInsuranceType(), InsuranceTypeDto.class));
		if(Boolean.FALSE.equals(shipMaster.getIsOtherShipType())) {
			shipMasterDto.setShipType(modelMapper.map(shipMasterDto.getShipType(), ShipTypeDto.class));
		}
		
		if (!shipMaster.getLossRecords().isEmpty()) {
			shipMasterDto.setLossRecords(shipMaster.getLossRecords().stream()
					.map(this::convertLossRecordEntityToDto).collect(Collectors.toList()));
		}
		if (!shipMaster.getInsuranceOnGoings().isEmpty()) {
			shipMasterDto.setInsuranceOnGoings(shipMaster.getInsuranceOnGoings().stream()
					.map(this::convertInsuranceOnGoingEntityToDto).collect(Collectors.toList()));
		}
		if (!shipMaster.getShipInsuranceMaps().isEmpty()) {
			List<ShipInsuranceMapDto> insuranceMapDtos = new ArrayList<>();
			for (ShipInsuranceMap sim : shipMaster.getShipInsuranceMaps()) {
				ShipInsuranceMapDto insuranceMapDto = new ShipInsuranceMapDto();
				insuranceMapDto.setId(sim.getId());
				insuranceMapDto
						.setInsuranceMasterDto(modelMapper.map(sim.getInsuranceMaster(), InsuranceMasterDto.class));
				insuranceMapDto.getInsuranceMasterDto().setInsuranceKindDto(
						modelMapper.map(sim.getInsuranceMaster().getInsuranceKind(), InsuranceKindDto.class));
				insuranceMapDto.getInsuranceMasterDto().getInsuranceKindDto().setInsuranceType(modelMapper
						.map(sim.getInsuranceMaster().getInsuranceKind().getInsuranceType(), InsuranceTypeDto.class));
				insuranceMapDtos.add(insuranceMapDto);
			}
			shipMasterDto.setInsuranceMapDtos(insuranceMapDtos);
		}
		for (ShipStatusEnum sse : ShipStatusEnum.values()) {
			if (sse.code.equalsIgnoreCase(shipMaster.getStatusApproval())) {
				shipMasterDto.setStatusApproval(sse.label);
				break;
			}
		}

		Integer count = bindingTransactionRepository.getCountByShipMasterId(shipMaster.getId());
		if (count != null && count > 0) {
			shipMasterDto.setReqReplacementFlag(Boolean.TRUE);
		} else {
			shipMasterDto.setReqReplacementFlag(Boolean.FALSE);
		}
		
		List<Chat> chats = new ArrayList<>();
		if(Boolean.TRUE.equals(this.isSupply)) {
			chats = chatRepository.findByIdReferenceAndIsProjectAndIsSupplyAndSenderAndReceiverOrderByIdAsc(
					this.isProject ? this.idBinding : shipMaster.getId(), this.isProject, this.isSupply, this.username, this.username);
		} else {
			chats = chatRepository.findByIdReferenceAndIsProjectAndIsSupplyOrderByIdAsc(
					this.isProject ? this.idBinding : shipMaster.getId(), this.isProject, this.isSupply);
		}
		
		if (!chats.isEmpty()) {
			shipMasterDto
					.setChats(chats.stream().map(c -> modelMapper.map(c, ChatDto.class)).collect(Collectors.toList()));
		}
		return shipMasterDto;
	}

	private InsuranceOnGoingDto convertInsuranceOnGoingEntityToDto(InsuranceOnGoing insuranceOnGoing) {
		InsuranceOnGoingDto insuranceOnGoingDto = modelMapper.map(insuranceOnGoing, InsuranceOnGoingDto.class);
		if(null != insuranceOnGoing.getInsuranceKind()) {
			insuranceOnGoingDto
			.setInsuranceKindDto(modelMapper.map(insuranceOnGoing.getInsuranceKind(), InsuranceKindDto.class));
			insuranceOnGoingDto.getInsuranceKindDto().setInsuranceType(
			modelMapper.map(insuranceOnGoing.getInsuranceKind().getInsuranceType(), InsuranceTypeDto.class));
		}
		return insuranceOnGoingDto;
	}
	
	private LossRecordDto convertLossRecordEntityToDto(LossRecord lossRecord) {
		LossRecordDto lossRecordDto = modelMapper.map(lossRecord, LossRecordDto.class);
		if (null != lossRecord.getInsuranceKind()) {
			lossRecordDto.setInsuranceKind(modelMapper.map(lossRecord.getInsuranceKind(), InsuranceKindDto.class));
			lossRecordDto.getInsuranceKind().setInsuranceType(
					modelMapper.map(lossRecord.getInsuranceKind().getInsuranceType(), InsuranceTypeDto.class));
		}
		return lossRecordDto;
	}

	private ViewProjectTcDto convertDocEntityToDto(BindingTransactionDocument bindingTransactionDocument) {
		ViewProjectTcDto viewProjectTcDto = new ViewProjectTcDto();
		viewProjectTcDto.setId(bindingTransactionDocument.getId());
		viewProjectTcDto.setIdBinding(this.id);
		viewProjectTcDto.setFileName(bindingTransactionDocument.getFileName());
		viewProjectTcDto.setFile(bindingTransactionDocument.getFile());
		viewProjectTcDto.setUploadDate(bindingTransactionDocument.getUploadDate());
		viewProjectTcDto.setUploader(this.username.equalsIgnoreCase(bindingTransactionDocument.getUploader()) ? "Me"
				: bindingTransactionDocument.getUploader());
		viewProjectTcDto.setStatus(bindingTransactionDocument.getStatus());
		return viewProjectTcDto;
	}

	private void validation(ShipMasterDto shipMasterDto) throws ValidateException {
		if (null == shipMasterDto.getName()) {
			throw new ValidateException("Nama kapal harus diisi.");
		}

		if (null == shipMasterDto.getDemand()) {
			throw new ValidateException("Demand harus diisi.");
		}

		if (null == shipMasterDto.getShipType()) {
			throw new ValidateException("Tipe kapal harus diisi.");
		}

	}
}
