package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.occupation.OccupationDto;
import com.apps.asuransikapalku.api.model.dto.occupation.OccupationDto.OccupationMapDto;
import com.apps.asuransikapalku.api.model.dto.occupation.OccupationTypeDto;
import com.apps.asuransikapalku.api.model.entity.occupation.Occupation;
import com.apps.asuransikapalku.api.model.entity.occupation.OccupationMap;
import com.apps.asuransikapalku.api.repository.occupation.OccupationMapRepository;
import com.apps.asuransikapalku.api.repository.occupation.OccupationRepository;
import com.apps.asuransikapalku.api.repository.occupation.OccupationTypeRepository;
import com.apps.asuransikapalku.api.service.OccupationService;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Service
public class OccupationServiceImpl implements OccupationService {
	private static final Logger logger = LoggerFactory.getLogger(OccupationServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";
	
	private OccupationRepository occupationRepository;
	
	private OccupationTypeRepository occupationTypeRepository;
	
	private OccupationMapRepository occupationMapRepository;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	private String user;
	
	@Autowired
	public void repository(OccupationRepository occupationRepository, OccupationTypeRepository occupationTypeRepository, OccupationMapRepository occupationMapRepository) {
		this.occupationRepository = occupationRepository;
		this.occupationTypeRepository = occupationTypeRepository;
		this.occupationMapRepository = occupationMapRepository;
	}

	@Override
	public APIResponse<String> save(OccupationDto occupationDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = user;
			this.validation(occupationDto);
			if(null == occupationDto.getCode() || "".equalsIgnoreCase(occupationDto.getCode())) {
				Integer maxId = occupationRepository.getMaxId();
				maxId = maxId == null ? 0 : maxId;
				occupationDto.setCode("JOB-"+(maxId+1));
			}
			occupationDto.setCode(occupationDto.getCode().toUpperCase());
			occupationRepository.save(convertDtoToEntity(occupationDto));
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA PEKERJAAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data gagal tersimpan");
		}
		return respon;
	}
	
	@Override
	public APIResponse<String> updateStatus(Long id, boolean isActive, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			Occupation occupation = occupationRepository.findById(id).orElse(null);
			occupation.setIsActive(isActive);
			occupation.setUpdatedBy(user);
			occupation.setUpdatedDate(new Date());
			occupationRepository.save(occupation);
		} catch (Exception e) {
			logger.error("GAGAL MENGUBAH STATUS ACTIVE PEKERJAAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"status pekerjaan dengan id [" + id + "] gagal diubah.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> delete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			occupationRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA PEKERJAAN [HARD] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Pekerjaan dengan id [" + id + "] gagal terhapus, karena sudah terhubung dengan tabel lain.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<Occupation> occupations = occupationRepository.findByIdIn(ids);
			occupationRepository.deleteAll(occupations);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA PEKERJAAN [BATCH] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal menghapus data pekerjaan dengan batch.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDelete(Long id, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			Occupation occupation = occupationRepository.findById(id).orElse(null);
			occupation.setIsActive(Boolean.FALSE);
			occupation.setUpdatedBy(user);
			occupation.setUpdatedDate(new Date());
			occupationRepository.save(occupation);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA PEKERJAAN [SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Pekerjaan dengan id [" + id + "] gagal terhapus");
		}
		return respon;
	}

	@Override
	public APIResponse<String> softDeleteAll(List<Long> ids, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<Occupation> occupations = occupationRepository.findByIdIn(ids);
			for(Occupation o : occupations) {
				o.setIsActive(Boolean.FALSE);
				o.setUpdatedBy(user);
				o.setUpdatedDate(new Date());
			}
			occupationRepository.deleteAll(occupations);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS DATA PEKERJAAN [BATCH - SOFT] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"Gagal menghapus data pekerjaan dengan batch.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<OccupationDto> findById(Long id) {
		APIResponse<OccupationDto> respon =new APIResponse<>();
		try {
			Occupation occupation = occupationRepository.findById(id).orElse(null);
			if(occupation == null) {
				logger.info("JENIS PEKERJAAN TIDAK DITEMUKAN [BY ID] : {}", occupation);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Jenis pekerjaan tidak ditemukan.");
			}
			respon.setData(convertEntityToDto(occupation));
		} catch (Exception e) {
			logger.error("JENIS PEKERJAAN TIDAK DITEMUKAN [BY ID] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Jenis pekerjaan tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<OccupationDto> findByCode(String code) {
		APIResponse<OccupationDto> respon =new APIResponse<>();
		try {
			Occupation occupation = occupationRepository.findByCode(code);
			if(occupation == null) {
				logger.error("JENIS PEKERJAAN TIDAK DITEMUKAN [BY CODE] : {}", occupation);
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
						"Jenis pekerjaan tidak ditemukan.");
			}
			respon.setData(convertEntityToDto(occupation));
		} catch (Exception e) {
			logger.error("JENIS PEKERJAAN TIDAK DITEMUKAN [BY CODE] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Jenis pekerjaan tidak ditemukan.");
		}
		return respon;
	}
	
	@Override
	public APIResponse<List<OccupationDto>> findByOccupationTypeId(Long id) {
		APIResponse<List<OccupationDto>> respon =new APIResponse<>();
		try {
			List<Occupation> occupations = occupationRepository.findByOccupationType(occupationTypeRepository.findById(id).orElse(null));
			respon.setData(occupations.stream().map(this::convertEntityToDto).collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("JENIS PEKERJAAN TIDAK DITEMUKAN [BY CODE] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_FOUND.value(),
					"Jenis pekerjaan tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<List<OccupationDto>> findAllWithoutPageableAndActive() {
		APIResponse<List<OccupationDto>> respon = new APIResponse<>();
		try {
			List<Occupation> occupations = occupationRepository.findByIsActive(true);
			
			respon.setData(occupations.stream().map(this::convertEntityToDto).collect(Collectors.toList()));
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST PEKERJAAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(),
					"List pekerjaan tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllOccupation(FilterDto filterDto) {
		APIResponse<Map<String, Object>> response = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<Occupation> occupationPage = occupationRepository.findAll(new Specification<Occupation>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<Occupation> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					
					if (filterDto.getIsActive() != null && filterDto.getIsActive()) {
						predicates.add(criteriaBuilder.equal(root.get("isActive"), filterDto.getIsActive()));
					}
					
					if (null != filterDto.getSearchBy()) {
						Predicate code = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("code")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate name = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("name")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate description = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("description")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(code, name, description));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<OccupationDto> occupations = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != occupationPage) {
				occupations = occupationPage.getContent().stream().map(this::convertEntityToDto)
						.collect(Collectors.toList());
				currentPage = occupationPage.getNumber();
				totalElement = occupationPage.getTotalElements();
				totalPage = occupationPage.getTotalPages();
			}

			dataRes.put("occupations", occupations);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			response.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST PEKERJAAN : {}", e.getMessage());
			response = new APIResponse<>(HttpStatus.NOT_FOUND.value(), "List pekerjaan tidak ditemukan.");
		}
		return response;
	}
	
	private Occupation convertDtoToEntity(OccupationDto occupationDto) {
		Occupation occupation = modelMapper.map(occupationDto, Occupation.class);
		if (occupationDto.getId() == null || occupationDto.getId() == 0) {
			occupation.setCreatedBy(user);
			occupation.setCreatedDate(new Date());
		} else {
			Occupation occupationOld = occupationRepository.findById(occupationDto.getId()).orElse(null);
			if(occupationOld != null) {
				occupation.setCreatedBy(occupationOld.getCreatedBy());
				occupation.setCreatedDate(occupationOld.getCreatedDate());
			}
			occupation.setUpdatedBy(user);
			occupation.setUpdatedDate(new Date());
		}
		
		occupation.setOccupationType(occupationTypeRepository.findById(occupationDto.getOccupationType().getId()).orElse(null));
		
		OccupationMap occupationMap = new OccupationMap();
		if(null != occupation.getId()) {
			occupationMap = occupationMapRepository.findByOccupation(occupation);
		} 
		occupationMap.setOccupation(occupation);
		occupationMap.setIsIrevolution(occupationDto.getOccupationMapDto().getIsIrevolution() == null ? Boolean.FALSE : occupationDto.getOccupationMapDto().getIsIrevolution());
		occupationMap.setIsClaimSolution(occupationDto.getOccupationMapDto().getIsClaimSolution() == null ? Boolean.FALSE : occupationDto.getOccupationMapDto().getIsClaimSolution());
		occupationMap.setIsClaimFinancing(occupationDto.getOccupationMapDto().getIsClaimFinancing() == null ? Boolean.FALSE : occupationDto.getOccupationMapDto().getIsClaimFinancing());
		occupationMap.setIsMarineConnect(occupationDto.getOccupationMapDto().getIsMarineConnect() == null ? Boolean.FALSE : occupationDto.getOccupationMapDto().getIsMarineConnect());
		
		occupation.setOccupationMap(occupationMap);
		
		return occupation;
	}
	
	private OccupationDto convertEntityToDto(Occupation occupation) {
		OccupationDto occupationDto = modelMapper.map(occupation, OccupationDto.class);
		occupationDto.setOccupationType(modelMapper.map(occupation.getOccupationType(), OccupationTypeDto.class));
		if(null != occupation.getOccupationMap()) {
			occupationDto.setOccupationMapDto(modelMapper.map(occupation.getOccupationMap(), OccupationMapDto.class));
		}
		return occupationDto;
	}
	
	private void validation(OccupationDto occupationDto) throws ValidateException {
		if(null == occupationDto.getName()) {
			throw new ValidateException("Nama jenis pekerjaan harus diisi.");
		}
		
		if(null == occupationDto.getOccupationType()) {
			throw new ValidateException("Tipe pekerjaan harus diisi.");
		}
		
		Integer count = occupationRepository.getCountByNameAndTypeId(occupationDto.getName().toUpperCase(), occupationDto.getOccupationType().getId());
		count = count == null ? 0 : count;
		if((null == occupationDto.getId() && count > 0) || (null != occupationDto.getId() && count > 1)) {
			throw new ValidateException("Nama jenis pekerjaan sudah digunakan.");
		}
	}
}
