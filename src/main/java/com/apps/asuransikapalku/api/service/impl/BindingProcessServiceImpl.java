package com.apps.asuransikapalku.api.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.enums.StatusBindingProcessEnum;
import com.apps.asuransikapalku.api.enums.StatusTypeEnum;
import com.apps.asuransikapalku.api.exception.CommonException;
import com.apps.asuransikapalku.api.exception.ValidateException;
import com.apps.asuransikapalku.api.model.APIResponse;
import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.FilterDto.Sorting;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceKindDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceMasterDto;
import com.apps.asuransikapalku.api.model.dto.insurance.InsuranceTypeDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.AddQuotationDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.AssignSupplierDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BidingDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingProcessDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionClaimDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionDocumentDemandDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionDocumentTcDemandDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionQuotationDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.BindingTransactionSupplierMapDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.QuotationMapDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.QuotationMapSecurityListDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.ReplacementDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.UpdateStatusTCDto;
import com.apps.asuransikapalku.api.model.dto.irevolution.UploadTcDto;
import com.apps.asuransikapalku.api.model.dto.ship.InsuranceOnGoingDto;
import com.apps.asuransikapalku.api.model.dto.ship.LossRecordDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipInsuranceMapDto;
import com.apps.asuransikapalku.api.model.dto.ship.ShipMasterDto;
import com.apps.asuransikapalku.api.model.dto.user.DemandDto;
import com.apps.asuransikapalku.api.model.dto.user.SupplierDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewProjectDto;
import com.apps.asuransikapalku.api.model.dto.view.ViewProjectTcDto;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransaction;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionClaim;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionDocument;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionDocumentDemand;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionDocumentTcDemand;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionHistory;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionQuotation;
import com.apps.asuransikapalku.api.model.entity.irevolution.BindingTransactionSupplierMap;
import com.apps.asuransikapalku.api.model.entity.irevolution.QuoatationMap;
import com.apps.asuransikapalku.api.model.entity.irevolution.QuotationMapSecurityList;
import com.apps.asuransikapalku.api.model.entity.ship.InsuranceOnGoing;
import com.apps.asuransikapalku.api.model.entity.ship.ShipInsuranceMap;
import com.apps.asuransikapalku.api.model.entity.ship.ShipMaster;
import com.apps.asuransikapalku.api.model.entity.user.Supplier;
import com.apps.asuransikapalku.api.model.entity.user.SupplierInsuranceMap;
import com.apps.asuransikapalku.api.model.entity.user.User;
import com.apps.asuransikapalku.api.model.entity.view.ViewProject;
import com.apps.asuransikapalku.api.model.entity.view.ViewProjectTc;
import com.apps.asuransikapalku.api.repository.insurance.InsuranceMasterRepository;
import com.apps.asuransikapalku.api.repository.irevolution.BindingTransactionClaimRepository;
import com.apps.asuransikapalku.api.repository.irevolution.BindingTransactionDocumentDemandRepository;
import com.apps.asuransikapalku.api.repository.irevolution.BindingTransactionDocumentRepository;
import com.apps.asuransikapalku.api.repository.irevolution.BindingTransactionDocumentTcDemandRepository;
import com.apps.asuransikapalku.api.repository.irevolution.BindingTransactionHistoryRepository;
import com.apps.asuransikapalku.api.repository.irevolution.BindingTransactionQuotationRepository;
import com.apps.asuransikapalku.api.repository.irevolution.BindingTransactionRepository;
import com.apps.asuransikapalku.api.repository.irevolution.BindingTransactionSupplierMapRepository;
import com.apps.asuransikapalku.api.repository.irevolution.ChatRepository;
import com.apps.asuransikapalku.api.repository.ship.ShipMasterRepository;
import com.apps.asuransikapalku.api.repository.user.SupplierInsuranceMapRepository;
import com.apps.asuransikapalku.api.repository.user.SupplierRepository;
import com.apps.asuransikapalku.api.repository.view.ViewProjectRepository;
import com.apps.asuransikapalku.api.repository.view.ViewProjectTcRepository;
import com.apps.asuransikapalku.api.service.BindingProcessService;
import com.apps.asuransikapalku.api.service.EmailService;
import com.apps.asuransikapalku.api.utils.FileUtil;
import com.apps.asuransikapalku.api.utils.FormatDateUtil;
import com.apps.asuransikapalku.api.utils.PopulateUtil;

@Transactional
@Service
public class BindingProcessServiceImpl implements BindingProcessService {
	private static final Logger logger = LoggerFactory.getLogger(BindingProcessServiceImpl.class);
	private static final String CURRENT_PAGE = "current-page";
	private static final String TOTAL_ITEM = "total-items";
	private static final String TOTAL_PAGE = "total-pages";
	private static final String FAILED_UPLOAD_TC = "GAGAL UPLOAD DOKUMEN TC";
	private static final String BINDING_DIR = "/Binding";
	private static final String CLAIM_PROJECT = "CLAIM_PROJECT";

	private BindingTransactionRepository bindingTransactionRepository;
	private BindingTransactionDocumentRepository bindingTransactionDocumentRepository;
	private BindingTransactionHistoryRepository bindingTransactionHistoryRepository;
	private BindingTransactionSupplierMapRepository bindingTransactionSupplierMapRepository;

	private InsuranceMasterRepository insuranceMasterRepository;
	private ShipMasterRepository shipMasterRepository;
	private SupplierRepository supplierRepository;
	private SupplierInsuranceMapRepository supplierInsuranceMapRepository;

	private ViewProjectRepository viewProjectRepository;
	private ViewProjectTcRepository viewProjectTcRepository;
	private BindingTransactionQuotationRepository bindingTransactionQuotationRepository;
	private BindingTransactionDocumentDemandRepository bindingTransactionDocumentDemandRepository;

	private BindingTransactionDocumentTcDemandRepository bindingTransactionDocumentTcDemandRepository;
	private BindingTransactionClaimRepository bindingTransactionClaimRepository;
	private ChatRepository chatRepository;

	private EmailService emailService;

	private ModelMapper modelMapper = new ModelMapper();

	private String user;

	@Value("${asuransikapalku.url}")
	private String url;

	@Value("${path.root}")
	private String pathRoot;

	@Value("${dir.file.upload}")
	private String dirUploadFile;

	@Autowired
	public void service(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void repository(BindingTransactionRepository bindingTransactionRepository,
			BindingTransactionDocumentRepository bindingTransactionDocumentRepository,
			BindingTransactionHistoryRepository bindingTransactionHistoryRepository,
			BindingTransactionSupplierMapRepository bindingTransactionSupplierMapRepository) {
		this.bindingTransactionRepository = bindingTransactionRepository;
		this.bindingTransactionDocumentRepository = bindingTransactionDocumentRepository;
		this.bindingTransactionHistoryRepository = bindingTransactionHistoryRepository;
		this.bindingTransactionSupplierMapRepository = bindingTransactionSupplierMapRepository;
	}

	@Autowired
	public void repository(InsuranceMasterRepository insuranceMasterRepository,
			ShipMasterRepository shipMasterRepository, SupplierRepository supplierRepository,
			SupplierInsuranceMapRepository supplierInsuranceMapRepository) {
		this.insuranceMasterRepository = insuranceMasterRepository;
		this.shipMasterRepository = shipMasterRepository;
		this.supplierRepository = supplierRepository;
		this.supplierInsuranceMapRepository = supplierInsuranceMapRepository;
	}

	@Autowired
	public void repository(ViewProjectRepository viewProjectRepository, ViewProjectTcRepository viewProjectTcRepository,
			BindingTransactionQuotationRepository bindingTransactionQuotationRepository,
			BindingTransactionDocumentDemandRepository bindingTransactionDocumentDemandRepository) {
		this.viewProjectRepository = viewProjectRepository;
		this.viewProjectTcRepository = viewProjectTcRepository;
		this.bindingTransactionQuotationRepository = bindingTransactionQuotationRepository;
		this.bindingTransactionDocumentDemandRepository = bindingTransactionDocumentDemandRepository;
	}

	@Autowired
	public void repository(BindingTransactionDocumentTcDemandRepository bindingTransactionDocumentTcDemandRepository,
			BindingTransactionClaimRepository bindingTransactionClaimRepository, ChatRepository chatRepository) {
		this.bindingTransactionDocumentTcDemandRepository = bindingTransactionDocumentTcDemandRepository;
		this.bindingTransactionClaimRepository = bindingTransactionClaimRepository;
		this.chatRepository = chatRepository;
	}

	@Override
	public APIResponse<String> requestReplacement(ReplacementDto replacementDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			this.user = user;
			this.validationRequestPlacement(replacementDto);
			Date currentDate = new Date();

			BindingTransaction bindingTransaction = new BindingTransaction();
			bindingTransaction.setCode("PRJ" + FormatDateUtil.dateToStr(new Date(), "ddMMyyyyHHMMSS"));
			bindingTransaction.setCreatedBy(user);
			bindingTransaction.setCreatedDate(currentDate);

			ShipMaster shipMaster = shipMasterRepository.findById(replacementDto.getId()).orElse(null);
			List<BindingTransactionSupplierMap> bindingTransactionSupplierMaps = this.populateSupplierMap(shipMaster,
					replacementDto, bindingTransaction);
//			if (Boolean.TRUE.equals(replacementDto.getIsPublic())) {
//				for (ShipInsuranceMap sim : shipMaster.getShipInsuranceMaps()) {
//					for (Long l : replacementDto.getIdInsurances()) {
//						if (l.equals(sim.getInsuranceMaster().getId())) {
//							sim.setIsCheck(Boolean.TRUE);
//
//							List<SupplierInsuranceMap> supplierInsuranceMaps = supplierInsuranceMapRepository
//									.supplierInsuranceMaps(sim.getInsuranceMaster().getId(),
//											replacementDto.getIdInsurances());
//							for (SupplierInsuranceMap supSim : supplierInsuranceMaps) {
//								BindingTransactionSupplierMap bindingTransactionSupplierMap = new BindingTransactionSupplierMap();
//								bindingTransactionSupplierMap.setSupplier(supSim.getSupplier());
//								bindingTransactionSupplierMap.setInsuranceMaster(sim.getInsuranceMaster());
//								bindingTransactionSupplierMap.setBindingTransaction(bindingTransaction);
//								bindingTransactionSupplierMaps.add(bindingTransactionSupplierMap);
//							}
//						}
//					}
//				}
//			}

			bindingTransaction.setBindingTransactionSupplierMaps(bindingTransactionSupplierMaps);
			bindingTransaction.setShipMaster(shipMaster);
			bindingTransaction.setStatus(StatusBindingProcessEnum.OUTSTANDING_PLACEMENT.code);
			bindingTransaction.setUsername(user);
			bindingTransaction
					.setIsPublic(null == replacementDto.getIsPublic() ? Boolean.FALSE : replacementDto.getIsPublic());
			bindingTransaction.setIsSelected(
					null == replacementDto.getIsSelected() ? Boolean.FALSE : replacementDto.getIsSelected());

			BindingTransactionHistory bindingTransactionHistory = new BindingTransactionHistory();
			bindingTransactionHistory.setBindingTransaction(bindingTransaction);
			bindingTransactionHistory.setHistoryDate(currentDate);
			bindingTransactionHistory.setStatus(StatusBindingProcessEnum.OUTSTANDING_PLACEMENT.code);
			bindingTransactionHistory.setDescription("Demand melakukan permintaan penempatan kapal.");

			bindingTransactionHistoryRepository.save(bindingTransactionHistory);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal meminta penempatan kapal.");
		}
		return respon;
	}

	private List<BindingTransactionSupplierMap> populateSupplierMap(ShipMaster shipMaster,
			ReplacementDto replacementDto, BindingTransaction bindingTransaction) {
		List<BindingTransactionSupplierMap> bindingTransactionSupplierMaps = new ArrayList<>();
		if (null != replacementDto.getIdInsurances() && !replacementDto.getIdInsurances().isEmpty()) {
			return bindingTransactionSupplierMaps;
		}

		for (ShipInsuranceMap sim : shipMaster.getShipInsuranceMaps()) {
			for (Long l : replacementDto.getIdInsurances()) {
				if (l.equals(sim.getInsuranceMaster().getId())) {
					sim.setIsCheck(Boolean.TRUE);

					List<SupplierInsuranceMap> supplierInsuranceMaps = supplierInsuranceMapRepository
							.supplierInsuranceMaps(sim.getInsuranceMaster().getId(), replacementDto.getIdInsurances());
					for (SupplierInsuranceMap supSim : supplierInsuranceMaps) {
						BindingTransactionSupplierMap bindingTransactionSupplierMap = new BindingTransactionSupplierMap();
						bindingTransactionSupplierMap.setSupplier(supSim.getSupplier());
						bindingTransactionSupplierMap.setInsuranceMaster(sim.getInsuranceMaster());
						bindingTransactionSupplierMap.setBindingTransaction(bindingTransaction);
						bindingTransactionSupplierMaps.add(bindingTransactionSupplierMap);
					}
				}
			}
		}
		return bindingTransactionSupplierMaps;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public APIResponse<String> updateStatus(BindingProcessDto bindingProcessDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		BindingTransactionHistory bindingTransactionHistory = new BindingTransactionHistory();
		boolean isError = false;
		try {
			String msg = this.validation(bindingProcessDto);

			if (msg != null) {
				logger.info("GAGAL MENYIMPAN DATA [UPDATE STATUS PROSES BINDING] : {}", msg);
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), msg);
			}

			BindingTransaction bindingTransaction = bindingTransactionRepository.findById(bindingProcessDto.getId())
					.orElse(null);

			if (null == bindingTransaction) {
				logger.info("DATA BINDING TIDAK ADA : {}", msg);
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data binding tidak ditemukan.");
			}

			Date currentDate = new Date();
			String description = "";
			StatusBindingProcessEnum statusBindingProcessEnum = null;
			for (StatusBindingProcessEnum sbpe : StatusBindingProcessEnum.values()) {
				if (sbpe.code.equalsIgnoreCase(bindingProcessDto.getStatus())) {
					description = sbpe.desc;
					statusBindingProcessEnum = sbpe;
					break;
				}
			}

			bindingTransaction.setStatus(bindingProcessDto.getStatus());

			bindingTransactionHistory.setBindingTransaction(bindingTransaction);
			bindingTransactionHistory.setHistoryDate(currentDate);
			bindingTransactionHistory.setStatus(bindingProcessDto.getStatus());
			bindingTransactionHistory.setDescription(description);

			StringBuilder sb = new StringBuilder();
			boolean isEmpty = false;
			if (StatusBindingProcessEnum.BINDING.code.equalsIgnoreCase(bindingProcessDto.getStatus())) {
				BindingTransactionQuotation bindingTransactionQuotation = bindingTransaction
						.getBindingTransactionQuotations().stream()
						.filter(btq -> btq.getIsAccepted().equals(Boolean.TRUE)).findAny().orElse(null);
				Integer size = bindingTransactionQuotation.getQuoatationMaps().size();
				if (size > 0) {
					for (QuoatationMap map : bindingTransactionQuotation.getQuoatationMaps()) {
						sb.append(map.getSupplier().getEmail());
						if (size > 1) {
							sb.append(",");
							size = size - 1;
						}
					}
				} else {
					isEmpty = true;
				}

			} else if (StatusBindingProcessEnum.OUTSTANDING_QUOTATION_SUPPLIER.code
					.equalsIgnoreCase(bindingProcessDto.getStatus())) {
				List<BindingTransactionSupplierMap> supplierMaps = bindingTransaction
						.getBindingTransactionSupplierMaps();
				Integer size = supplierMaps.size();
				if (size > 0) {
					for (BindingTransactionSupplierMap btsm : supplierMaps) {
						sb.append(btsm.getSupplier().getEmail());
						if (size > 1) {
							sb.append(",");
							size = size - 1;
						}
					}
				} else {
					isEmpty = true;
				}
			} else {
				List<BindingTransactionSupplierMap> supplierMaps = bindingTransaction
						.getBindingTransactionSupplierMaps().stream()
						.filter(btsmp -> Boolean.TRUE.equals(btsmp.getBid())).collect(Collectors.toList());
				Integer size = supplierMaps.size();
				if (size > 0) {
					for (BindingTransactionSupplierMap btsm : supplierMaps) {
						sb.append(btsm.getSupplier().getEmail());
						if (size > 1) {
							sb.append(",");
							size = size - 1;
						}
					}
				} else {
					isEmpty = true;
				}

			}

			if (StatusBindingProcessEnum.OUTSTANDING_QUOTATION_SUPPLIER.code
					.equalsIgnoreCase(bindingProcessDto.getStatus())
					|| StatusBindingProcessEnum.OUTSTANDING_CONFIRMATION_SUPPLIER.code
							.equalsIgnoreCase(bindingProcessDto.getStatus())
					|| StatusBindingProcessEnum.BINDING.code.equalsIgnoreCase(bindingProcessDto.getStatus())) {
				Map<String, String> paramsDemand = new HashMap<>();
				Map<String, String> paramsSupplier = new HashMap<>();
				paramsDemand.put(AsuransiKapalkuConstant.Email.SUBJECT, "NEW UPDATES from AsuransiKapalku.com");
				paramsDemand.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME,
						bindingTransaction.getShipMaster().getDemand().getName());
				paramsDemand.put(AsuransiKapalkuConstant.Email.STATUS_BINDING, statusBindingProcessEnum.secondLabel);
				paramsDemand.put(AsuransiKapalkuConstant.Email.TO,
						bindingTransaction.getShipMaster().getDemand().getEmail());
				paramsDemand.put(AsuransiKapalkuConstant.Email.TEMPLATE, "templates/email/update-status-binding.html");

				if (!isEmpty) {
					logger.info("email to : {}", sb.toString());
					paramsSupplier.put(AsuransiKapalkuConstant.Email.SUBJECT, "NEW UPDATES from AsuransiKapalku.com");
					paramsSupplier.put(AsuransiKapalkuConstant.Email.CUSTOMER_NAME, "All");
					paramsSupplier.put(AsuransiKapalkuConstant.Email.STATUS_BINDING,
							statusBindingProcessEnum.secondLabel);
					paramsSupplier.put(AsuransiKapalkuConstant.Email.TO, sb.toString());
					paramsSupplier.put(AsuransiKapalkuConstant.Email.TEMPLATE,
							"templates/email/update-status-binding.html");
				}

				emailService.sendMail(paramsDemand);
				emailService.sendMail(paramsSupplier);
			}

		} catch (Exception e) {
			e.printStackTrace();
			isError = true;
			logger.error("GAGAL MENYIMPAN DATA [UPDATE STATUS PROSES BINDING] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal menyimpan data.");
		} finally {
			if (!isError) {
				bindingTransactionHistoryRepository.save(bindingTransactionHistory);
			}
		}
		return respon;
	}

	@Override
	public APIResponse<String> uploadTC(Map<String, Object> map, User user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			if (user.getDemand() != null) {
				logger.error(FAILED_UPLOAD_TC);
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Tidak berhak untuk upload dokumen TC.");
			}

			Date currentDate = new Date();
			Long bindingId = (Long) map.get("bindingId");
			String status = (String) map.get("status");
			String dirFile = pathRoot + dirUploadFile + BINDING_DIR + "/" + bindingId;
			Boolean isBid = Boolean.FALSE;
			if (null != user.getSupplier()) {
				isBid = bindingTransactionSupplierMapRepository.isBid(bindingId, user.getSupplier().getId());
			}
			if (null == isBid) {
				logger.error(FAILED_UPLOAD_TC);
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Mohon bid terlebih dahulu.");
			}

			if (Boolean.FALSE.equals(isBid)) {
				logger.error(FAILED_UPLOAD_TC);
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Mohon bid terlebih dahulu.");
			}

			BindingTransaction bindingTransaction = bindingTransactionRepository.findById(bindingId).orElse(null);

			if (null == bindingTransaction) {
				logger.info("DATA BINDING TIDAK ADA : {}", bindingTransaction);
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data binding tidak ditemukan.");
			}

			List<BindingTransactionDocument> bindingTransactionDocuments = new ArrayList<>();
			List<BindingTransactionSupplierMap> bindingTransactionSupplierMaps = bindingTransaction
					.getBindingTransactionSupplierMaps();
			if (user.getAdmin() == null) {
				bindingTransactionSupplierMaps = bindingTransaction.getBindingTransactionSupplierMaps().stream()
						.filter(b -> b.getSupplier().getId().equals(user.getSupplier().getId()))
						.collect(Collectors.toList());
			}

			for (BindingTransactionSupplierMap btsm : bindingTransactionSupplierMaps) {
				BindingTransactionDocument bindingTransactionDocument = new BindingTransactionDocument();
				bindingTransactionDocument.setUploader(user.getUsername());
				bindingTransactionDocument.setUploadDate(currentDate);
				bindingTransactionDocument.setType("BINDING_PROCESS");
				bindingTransactionDocument.setStatus(status);
				bindingTransactionDocument.setBindingTransactionSupplierMap(btsm);
				if (null != map.get("file")) {
					MultipartFile file = (MultipartFile) map.get("file");
					String originalFilename = file.getOriginalFilename();
					String[] str = originalFilename.split("\\.");
					String ext = "." + str[str.length - 1];

					String fileLoc = FileUtil.saveDocument(file,
							"Binding" + bindingId + "_" + btsm.getSupplier().getId() + "_"
									+ btsm.getInsuranceMaster().getId(),
							dirFile, ext, url + dirFile + BINDING_DIR + "/" + bindingId);

					String[] fileLocs = fileLoc.split("/");

					bindingTransactionDocument.setFile(fileLoc);
					bindingTransactionDocument.setFileName(fileLocs[fileLocs.length - 1]);
				}
				bindingTransactionDocuments.add(bindingTransactionDocument);
			}
			bindingTransactionDocumentRepository.saveAll(bindingTransactionDocuments);

		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA [UPLOAD TC]: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), FAILED_UPLOAD_TC);
		}
		return respon;

	}

	@Override
	public APIResponse<String> updateStatusTc(UpdateStatusTCDto updateStatusTCDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			BindingTransactionDocument bindingTransactionDocument = bindingTransactionDocumentRepository
					.findById(updateStatusTCDto.getId()).orElse(null);
			if (null == bindingTransactionDocument) {
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Status TC gagal diubah.");
			}
			bindingTransactionDocument.setStatus(updateStatusTCDto.getStatus());
			bindingTransactionDocumentRepository.save(bindingTransactionDocument);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA [UPDATE STATUS TC]: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Status TC gagal diubah.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> deleteTc(Long id) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			BindingTransactionDocument bindingTransactionDocument = bindingTransactionDocumentRepository.findById(id)
					.orElse(null);
			if (null == bindingTransactionDocument) {
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Status TC gagal diubah.");
			}
			bindingTransactionDocument.setBindingTransactionSupplierMap(null);
			bindingTransactionDocumentRepository.save(bindingTransactionDocument);
			bindingTransactionDocumentRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA [DELETE TC]: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal upload dokumen TC.");
		}
		return respon;
	}

	private void uploadTcForDemand(UploadTcDto uploadTcDto, User user, String dirFile,
			BindingTransaction bindingTransaction) throws CommonException {
		String fileName = uploadTcDto.getFileName() == null ? "Binding" + uploadTcDto.getId()
				: uploadTcDto.getFileName();
		String fileLoc = FileUtil.fileDecodeToDir(uploadTcDto.getFile(), fileName, dirFile,
				AsuransiKapalkuConstant.FileTypeExtension.PDF, url + dirUploadFile + BINDING_DIR);
		String[] fileLocs = fileLoc.split("/");

		BindingTransactionDocumentTcDemand bindingTransactionDocumentTcDemand = new BindingTransactionDocumentTcDemand();
		bindingTransactionDocumentTcDemand.setUploader(user.getUsername());
		bindingTransactionDocumentTcDemand.setUploadDate(new Date());
		bindingTransactionDocumentTcDemand.setType("BINDING_PROCESS");
		bindingTransactionDocumentTcDemand.setStatus(uploadTcDto.getStatus());
		bindingTransactionDocumentTcDemand.setBindingTransaction(bindingTransaction);
		bindingTransactionDocumentTcDemand.setTarget(uploadTcDto.getTo());
		bindingTransactionDocumentTcDemand.setFile(fileLoc);
		bindingTransactionDocumentTcDemand.setFileName(
				uploadTcDto.getFileName() == null ? fileLocs[fileLocs.length - 1] : uploadTcDto.getFileName());

		bindingTransactionDocumentTcDemandRepository.save(bindingTransactionDocumentTcDemand);
	}

	@Override
	public APIResponse<String> uploadTC1(UploadTcDto uploadTcDto, User user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			String msg = validationUploadTc(uploadTcDto);
			if (null != msg) {
				logger.error(FAILED_UPLOAD_TC + " : {}", msg);
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), msg);
			}

			Date currentDate = new Date();
			String dirFile = pathRoot + dirUploadFile + BINDING_DIR;
			BindingTransaction bindingTransaction = bindingTransactionRepository.findById(uploadTcDto.getId())
					.orElse(null);

			if (!Boolean.TRUE.equals(uploadTcDto.getIsSupply())) {
				this.uploadTcForDemand(uploadTcDto, user, dirFile, bindingTransaction);
				return respon;
			}

			List<BindingTransactionDocument> bindingTransactionDocuments = new ArrayList<>();
			List<BindingTransactionSupplierMap> bindingTransactionSupplierMaps = bindingTransaction
					.getBindingTransactionSupplierMaps();
			if (user.getAdmin() == null) {
				bindingTransactionSupplierMaps = bindingTransaction.getBindingTransactionSupplierMaps().stream()
						.filter(b -> b.getSupplier().getId().equals(user.getSupplier().getId()))
						.collect(Collectors.toList());
			}
			if (user.getAdmin() != null) {
				bindingTransactionSupplierMaps = bindingTransaction.getBindingTransactionSupplierMaps().stream()
						.filter(b -> b.getSupplier().getUser().getUsername().equalsIgnoreCase(uploadTcDto.getTo()))
						.collect(Collectors.toList());
			}

			String fileLoc = null;
			if (null != uploadTcDto.getFile()) {
				String fileName = uploadTcDto.getFileName() == null ? "Binding" + uploadTcDto.getId()
						: uploadTcDto.getFileName();
				fileLoc = FileUtil.fileDecodeToDir(uploadTcDto.getFile(), fileName, dirFile,
						AsuransiKapalkuConstant.FileTypeExtension.PDF, url + dirUploadFile + BINDING_DIR);
			}

			for (BindingTransactionSupplierMap btsm : bindingTransactionSupplierMaps) {
				BindingTransactionDocument bindingTransactionDocument = new BindingTransactionDocument();
				bindingTransactionDocument.setUploader(user.getUsername());
				bindingTransactionDocument.setUploadDate(currentDate);
				bindingTransactionDocument.setType("BINDING_PROCESS");
				bindingTransactionDocument.setStatus(uploadTcDto.getStatus());
				bindingTransactionDocument.setBindingTransactionSupplierMap(btsm);
				bindingTransactionDocument.setTarget(uploadTcDto.getTo());
				if (null != fileLoc) {
					String[] fileLocs = fileLoc.split("/");

					bindingTransactionDocument.setFile(fileLoc);
					bindingTransactionDocument
							.setFileName(uploadTcDto.getFileName() == null ? fileLocs[fileLocs.length - 1]
									: uploadTcDto.getFileName());
				}
				bindingTransactionDocuments.add(bindingTransactionDocument);
			}
			bindingTransactionDocumentRepository.saveAll(bindingTransactionDocuments);

			if (user.getAdmin() != null) {
				if (StatusBindingProcessEnum.OUTSTANDING_QUOTATION_SUPPLIER.code
						.equalsIgnoreCase(bindingTransaction.getStatus())) {
					bindingTransaction.setStatus(StatusBindingProcessEnum.OUTSTANDING_NEGOTIATION.code);
					bindingTransaction.setUpdatedBy(user.getUsername());
					bindingTransaction.setUpdatedDate(currentDate);

					BindingTransactionHistory bindingTransactionHistory = new BindingTransactionHistory();
					bindingTransactionHistory.setBindingTransaction(bindingTransaction);
					bindingTransactionHistory.setHistoryDate(currentDate);
					bindingTransactionHistory.setStatus(StatusBindingProcessEnum.OUTSTANDING_NEGOTIATION.code);
					bindingTransactionHistory.setDescription(StatusBindingProcessEnum.OUTSTANDING_NEGOTIATION.label);

					bindingTransactionHistoryRepository.save(bindingTransactionHistory);
				}

				if ("Final".equalsIgnoreCase(uploadTcDto.getStatus())) {
					bindingTransaction.setStatus(StatusBindingProcessEnum.OUTSTANDING_FINAL_OFFERING.code);
					bindingTransaction.setUpdatedBy(user.getUsername());
					bindingTransaction.setUpdatedDate(currentDate);

					BindingTransactionHistory bindingTransactionHistory = new BindingTransactionHistory();
					bindingTransactionHistory.setBindingTransaction(bindingTransaction);
					bindingTransactionHistory.setHistoryDate(currentDate);
					bindingTransactionHistory.setStatus(StatusBindingProcessEnum.OUTSTANDING_FINAL_OFFERING.code);
					bindingTransactionHistory.setDescription(StatusBindingProcessEnum.OUTSTANDING_FINAL_OFFERING.label);

					bindingTransactionHistoryRepository.save(bindingTransactionHistory);
				}

			}

			if (user.getAdmin() == null) {
				if ("Final".equalsIgnoreCase(uploadTcDto.getStatus())) {
					bindingTransaction.setStatus(StatusBindingProcessEnum.OUTSTANDING_CONFIRMATION_SUPPLIER.code);
					bindingTransaction.setUpdatedBy(user.getUsername());
					bindingTransaction.setUpdatedDate(currentDate);

					BindingTransactionHistory bindingTransactionHistory = new BindingTransactionHistory();
					bindingTransactionHistory.setBindingTransaction(bindingTransaction);
					bindingTransactionHistory.setHistoryDate(currentDate);
					bindingTransactionHistory
							.setStatus(StatusBindingProcessEnum.OUTSTANDING_CONFIRMATION_SUPPLIER.code);
					bindingTransactionHistory
							.setDescription(StatusBindingProcessEnum.OUTSTANDING_CONFIRMATION_SUPPLIER.label);

					bindingTransactionHistoryRepository.save(bindingTransactionHistory);
				}
			}
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA [UPLOAD TC]: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal upload dokumen TC.");
		}
		return respon;

	}

	@Override
	public APIResponse<String> addQuotation(AddQuotationDto addQuotationDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		logger.info("QUOTATIONS : {}", addQuotationDto);
		try {
			List<BindingTransactionQuotationDto> bindingTransactionQuotationDtos = new ArrayList<>();
			bindingTransactionQuotationDtos.addAll(addQuotationDto.getBindingTransactionQuotationDtos());
			String msg = this.quotationValidation(bindingTransactionQuotationDtos);
			if (null != msg) {
				logger.error("GAGAL UPLOAD QUOTATION");
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), msg);
			}

			Date currentDate = new Date();
			BindingTransaction bindingTransaction = bindingTransactionRepository
					.findById(bindingTransactionQuotationDtos.get(0).getIdBinding()).orElse(null);
			bindingTransaction.setUpdatedBy(user);
			bindingTransaction.setUpdatedDate(currentDate);
			String dirFile = pathRoot + dirUploadFile + BINDING_DIR;
			List<BindingTransactionQuotation> bindingTransactionQuotations = new ArrayList<>();
			for (BindingTransactionQuotationDto dto : bindingTransactionQuotationDtos) {
				BindingTransactionQuotation bindingTransactionQuotation = modelMapper.map(dto,
						BindingTransactionQuotation.class);
				if (dto.getId() != null) {
					BindingTransactionQuotation bindingTransactionQuotationOld = bindingTransactionQuotationRepository
							.findById(dto.getId()).orElse(null);

					if (dto.getFile() != null && !dto.getFile().contains(pathRoot)) {
						bindingTransactionQuotation.setFile(FileUtil.fileDecodeToDir(dto.getFile(), dto.getName(),
								dirFile, AsuransiKapalkuConstant.FileTypeExtension.PDF,
								url + dirUploadFile + BINDING_DIR));
					} else {
						if (null != bindingTransactionQuotationOld) {
							bindingTransactionQuotation.setFile(bindingTransactionQuotationOld.getFile());
						}
					}
				} else {
					bindingTransactionQuotation.setFile(FileUtil.fileDecodeToDir(dto.getFile(), dto.getName(), dirFile,
							AsuransiKapalkuConstant.FileTypeExtension.PDF, url + dirUploadFile + BINDING_DIR));
				}

				if (!dto.getQuoatationMaps().isEmpty()) {
					bindingTransactionQuotation.setQuoatationMaps(
							populateQuotationMap(dto.getQuoatationMaps(), bindingTransactionQuotation));
				}
				bindingTransactionQuotation.setBindingTransaction(bindingTransaction);
				bindingTransactionQuotations.add(bindingTransactionQuotation);
			}
			bindingTransactionQuotationRepository.saveAll(bindingTransactionQuotations);

			if (StatusBindingProcessEnum.OUTSTANDING_CONFIRMATION_SUPPLIER.code
					.equalsIgnoreCase(bindingTransaction.getStatus())) {
				bindingTransaction.setStatus(StatusBindingProcessEnum.OUTSTANDING_QUOTATION_DEMAND.code);
				bindingTransaction.setUpdatedBy(user);
				bindingTransaction.setUpdatedDate(currentDate);

				BindingTransactionHistory bindingTransactionHistory = new BindingTransactionHistory();
				bindingTransactionHistory.setBindingTransaction(bindingTransaction);
				bindingTransactionHistory.setHistoryDate(currentDate);
				bindingTransactionHistory.setStatus(StatusBindingProcessEnum.OUTSTANDING_QUOTATION_DEMAND.code);
				bindingTransactionHistory.setDescription(StatusBindingProcessEnum.OUTSTANDING_QUOTATION_DEMAND.label);

				bindingTransactionHistoryRepository.save(bindingTransactionHistory);
			}
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA [QUOTATION]: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal menambahkan quotation.");
		}
		return respon;
	}

	private List<QuoatationMap> populateQuotationMap(List<QuotationMapDto> quotationMapDtos,
			BindingTransactionQuotation bindingTransactionQuotation) {
		List<QuoatationMap> quoatationMaps = new ArrayList<>();
		for (QuotationMapDto dto : quotationMapDtos) {
			QuoatationMap quoatationMap = new QuoatationMap();
			quoatationMap.setBindingTransactionQuotation(bindingTransactionQuotation);
			quoatationMap.setInsuranceMaster(
					insuranceMasterRepository.findById(dto.getInsuranceMaster().getId()).orElse(null));
			quoatationMap.setSupplier(supplierRepository.findById(dto.getSupplier().getId()).orElse(null));
			quoatationMap.setCommission(dto.getCommission());
			if (!dto.getQuotationMapSecurityLists().isEmpty()) {
				quoatationMap.setQuotationMapSecurityLists(
						populateQuotationMapSecurityList(dto.getQuotationMapSecurityLists(), quoatationMap));
			}
			quoatationMaps.add(quoatationMap);
		}
		return quoatationMaps;
	}

	private List<QuotationMapSecurityList> populateQuotationMapSecurityList(
			List<QuotationMapSecurityListDto> quotationMapSecurityListDtos, QuoatationMap quoatationMap) {
		List<QuotationMapSecurityList> quotationMapSecurityLists = new ArrayList<>();
		for (QuotationMapSecurityListDto dto : quotationMapSecurityListDtos) {
			QuotationMapSecurityList quotationMapSecurityList = new QuotationMapSecurityList();
			quotationMapSecurityList.setQuoatationMap(quoatationMap);
			quotationMapSecurityList.setInsuranceMaster(
					insuranceMasterRepository.findById(dto.getInsuranceMaster().getId()).orElse(null));
			quotationMapSecurityList.setType(dto.getType());
			quotationMapSecurityList.setPercentage(dto.getPercentage());
			quotationMapSecurityLists.add(quotationMapSecurityList);
		}
		return quotationMapSecurityLists;
	}

	@Override
	public APIResponse<String> deleteQuotation(Long id) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			BindingTransactionQuotation bindingTransactionQuotation = bindingTransactionQuotationRepository.findById(id)
					.orElse(null);
			if (null == bindingTransactionQuotation) {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Quotation tidak ditemukan.");
			}
			bindingTransactionQuotationRepository.save(bindingTransactionQuotation);
			bindingTransactionQuotationRepository.deleteById(id);
		} catch (Exception e) {
			logger.error("GAGAL MENGHAPUS QUOTATION: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal dihapus.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> addDocumentDemand(
			BindingTransactionDocumentDemandDto bindingTransactionDocumentDemandDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			logger.info("DOCUMENT DEMAND : {}", bindingTransactionDocumentDemandDto);
			Date currentDate = new Date();
			BindingTransaction bindingTransaction = bindingTransactionRepository
					.findById(bindingTransactionDocumentDemandDto.getIdBinding()).orElse(null);
			if (null == bindingTransaction) {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data projek tidak ditemukan.");
			}
			bindingTransaction.setUpdatedBy(user);
			bindingTransaction.setUpdatedDate(currentDate);
			String dirFile = pathRoot + dirUploadFile + BINDING_DIR;

			BindingTransactionDocumentDemand bindingTransactionDocument = modelMapper
					.map(bindingTransactionDocumentDemandDto, BindingTransactionDocumentDemand.class);
			if (bindingTransactionDocumentDemandDto.getId() != null) {
				BindingTransactionDocumentDemand bindingTransactionDocumentOld = bindingTransactionDocumentDemandRepository
						.findById(bindingTransactionDocumentDemandDto.getId()).orElse(null);
				if (bindingTransactionDocumentDemandDto.getFile() != null
						&& !bindingTransactionDocumentDemandDto.getFile().contains(pathRoot)) {
					bindingTransactionDocument
							.setFile(FileUtil.fileDecodeToDir(bindingTransactionDocumentDemandDto.getFile(),
									bindingTransactionDocumentDemandDto.getName(), dirFile,
									AsuransiKapalkuConstant.FileTypeExtension.PDF, url + dirUploadFile + BINDING_DIR));
				} else {
					if (null != bindingTransactionDocumentOld) {
						bindingTransactionDocument.setFile(bindingTransactionDocumentOld.getFile());
					}
				}
			} else {
				bindingTransactionDocument.setFile(FileUtil.fileDecodeToDir(
						bindingTransactionDocumentDemandDto.getFile(), bindingTransactionDocumentDemandDto.getName(),
						dirFile, AsuransiKapalkuConstant.FileTypeExtension.PDF, url + dirUploadFile + BINDING_DIR));
			}
			bindingTransactionDocument.setBindingTransaction(bindingTransaction);
			bindingTransactionDocumentDemandRepository.save(bindingTransactionDocument);
		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA [DOCUMENT DEMAND]: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal menambahkan dokumen.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> bidingShip(BidingDto bidingDto, String user) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			List<BindingTransactionSupplierMap> bindingTransactionSupplierMaps = bindingTransactionSupplierMapRepository
					.findByIdBindingTransactionAndSupplier(bidingDto.getId(), bidingDto.getIdSupplier());
			bindingTransactionSupplierMaps.stream().forEach(btsm -> updateStatusBid(bidingDto, btsm));
			bindingTransactionSupplierMapRepository.saveAll(bindingTransactionSupplierMaps);

//			BindingTransaction bindingTransaction = bindingTransactionSupplierMaps.get(0).getBindingTransaction();
//			bindingTransaction.setStatus(StatusBindingProcessEnum.OUTSTANDING_NEGOTIATION.code);
//			
//			BindingTransactionHistory bindingTransactionHistory = new BindingTransactionHistory();
//			bindingTransactionHistory.setBindingTransaction(bindingTransaction);
//			bindingTransactionHistory.setHistoryDate(new Date());
//			bindingTransactionHistory.setStatus(StatusBindingProcessEnum.OUTSTANDING_NEGOTIATION.code);
//			bindingTransactionHistory.setDescription(StatusBindingProcessEnum.OUTSTANDING_NEGOTIATION.label);
//			
//			bindingTransactionHistoryRepository.save(bindingTransactionHistory);

		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA [BIDING KAPAL]: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal bid kapal.");
		}
		return respon;
	}

	private void updateStatusBid(BidingDto bidingDto, BindingTransactionSupplierMap bindingTransactionSupplierMap) {
		bindingTransactionSupplierMap.setBid(bidingDto.getBid());
		bindingTransactionSupplierMap.getBindingTransaction().setUpdatedBy(user);
		bindingTransactionSupplierMap.getBindingTransaction().setUpdatedDate(new Date());
	}

	@Override
	public APIResponse<Map<String, Object>> findBindingProcessById(Long id) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			BindingTransaction bindingTransaction = bindingTransactionRepository.findById(id).orElse(null);

			List<ViewProjectTc> viewProjectTcs = viewProjectTcRepository.findByIdBinding(id);

			String status = "";
			for (StatusBindingProcessEnum sbpe : StatusBindingProcessEnum.values()) {
				if (sbpe.code.equalsIgnoreCase(bindingTransaction.getStatus())) {
					status = sbpe.label;
					break;
				}
			}

			DemandDto demandDto = modelMapper.map(bindingTransaction.getShipMaster().getDemand(), DemandDto.class);
			ShipMasterDto shipMasterDto = modelMapper.map(bindingTransaction.getShipMaster(), ShipMasterDto.class);
			shipMasterDto.setInsuranceKindDto(
					modelMapper.map(bindingTransaction.getShipMaster().getInsuranceKind(), InsuranceKindDto.class));
			if (!bindingTransaction.getShipMaster().getInsuranceOnGoings().isEmpty()) {
				shipMasterDto.setInsuranceOnGoings(bindingTransaction.getShipMaster().getInsuranceOnGoings().stream()
						.map(this::convertToInsuranceOnGoingDto).collect(Collectors.toList()));
			}
			if (!bindingTransaction.getShipMaster().getLossRecords().isEmpty()) {
				shipMasterDto.setLossRecords(bindingTransaction.getShipMaster().getLossRecords().stream()
						.map(lr -> modelMapper.map(lr, LossRecordDto.class)).collect(Collectors.toList()));
			}
			if (!bindingTransaction.getShipMaster().getShipInsuranceMaps().isEmpty()) {
				shipMasterDto.setInsuranceMapDtos(bindingTransaction.getShipMaster().getShipInsuranceMaps().stream()
						.map(this::convertToInsuranceMapDto).collect(Collectors.toList()));
			}
			List<BindingTransactionSupplierMapDto> transactionSupplierMapDtos = new ArrayList<>();
			if (!bindingTransaction.getBindingTransactionSupplierMaps().isEmpty()) {
				transactionSupplierMapDtos = bindingTransaction.getBindingTransactionSupplierMaps().stream()
						.map(this::convertToBindingTransactionSupplierMapDto).collect(Collectors.toList());
			}
			List<BindingTransactionClaimDto> claims = new ArrayList<>();
			if (!bindingTransaction.getBindingTransactionClaims().isEmpty()) {
				claims = bindingTransaction.getBindingTransactionClaims().stream()
						.map(q -> modelMapper.map(q, BindingTransactionClaimDto.class)).collect(Collectors.toList());
			}
			List<BindingTransactionDocumentTcDemandDto> tcDemands = new ArrayList<>();
			if (!bindingTransaction.getBindingTransactionDocumentTcDemands().isEmpty()) {
				tcDemands = bindingTransaction.getBindingTransactionDocumentTcDemands().stream()
						.map(q -> modelMapper.map(q, BindingTransactionDocumentTcDemandDto.class))
						.collect(Collectors.toList());
			}

			dataRes.put("status-code", bindingTransaction.getStatus());
			dataRes.put("status-description", status);
			dataRes.put("assignee", transactionSupplierMapDtos);
			dataRes.put("vessel", shipMasterDto);
			dataRes.put("claims", claims);
			dataRes.put("demand", demandDto);
			dataRes.put("tab-supply", viewProjectTcs.stream().map(v -> modelMapper.map(v, ViewProjectTcDto.class))
					.collect(Collectors.toList()));

			List<BindingTransactionQuotationDto> listQuotation = bindingTransaction.getBindingTransactionQuotations()
					.stream().map(this::convertToQuotationDto).collect(Collectors.toList());

			Comparator<BindingTransactionQuotationDto> comparator = new Comparator<BindingTransactionQuotationDto>() {

				@Override
				public int compare(BindingTransactionQuotationDto o1, BindingTransactionQuotationDto o2) {
					return o1.getId().compareTo(o2.getId());
				}
			};

			Collections.sort(listQuotation, comparator);

			dataRes.put("tab-demand-quotations", listQuotation);
			dataRes.put("tab-demand-documents",
					bindingTransaction.getTransactionDocumentDemands().stream()
							.map(tdd -> modelMapper.map(tdd, BindingTransactionDocumentDemandDto.class))
							.collect(Collectors.toList()));
			dataRes.put("tab-demand-chat",
					chatRepository.findCodeByIdReferenceAndIsProjectAndIsSupply(id, true, false));
			dataRes.put("tab-demand-tc",
					tcDemands.stream()
							.sorted(Comparator.comparingLong(BindingTransactionDocumentTcDemandDto::getId))
							.collect(Collectors.toList()));
			respon.setData(dataRes);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("DATA TIDAK DITEMUKAN: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data projek tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<List<SupplierDto>> getSupplierForTc(Long id) {
		APIResponse<List<SupplierDto>> respon = new APIResponse<>();
		try {
			List<Long> supplierIds = bindingTransactionSupplierMapRepository.getListSupplierIdByBindingId(id);
			if (!supplierIds.isEmpty()) {
				List<Supplier> suppliers = supplierRepository.findByIdIn(supplierIds);
				respon.setData(suppliers.stream().map(this::convertEntityToSupplierDto).collect(Collectors.toList()));
			}
		} catch (Exception e) {
			logger.error("DATA TIDAK DITEMUKAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<List<SupplierDto>> getSupplierForQuotation(Long id) {
		APIResponse<List<SupplierDto>> respon = new APIResponse<>();
		try {
			List<Long> supplierIds = bindingTransactionSupplierMapRepository.getListSupplierIdByBindingId(id);
			if (!supplierIds.isEmpty()) {
				List<Supplier> suppliers = supplierRepository.findByIdIn(supplierIds);
				respon.setData(suppliers.stream().map(this::convertEntityToSupplierDto).collect(Collectors.toList()));
			}
		} catch (Exception e) {
			logger.error("DATA TIDAK DITEMUKAN : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Data tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> addClaim(BindingTransactionClaimDto bindingTransactionClaimDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			BindingTransactionClaim bindingTransactionClaim = modelMapper.map(bindingTransactionClaimDto,
					BindingTransactionClaim.class);
			bindingTransactionClaim.setBindingTransaction(
					bindingTransactionRepository.findById(bindingTransactionClaimDto.getBindingId()).orElse(null));
			String dirFile = pathRoot + dirUploadFile + BINDING_DIR;
			if (null != bindingTransactionClaimDto.getFile()) {
				if (bindingTransactionClaimDto.getFile().contains("data:image/jpeg;base64")
						|| bindingTransactionClaim.getFile().contains("data:image/png;base64")) {
					logger.info("CLAIM IMAGE");
					bindingTransactionClaim.setFile(
							FileUtil.imgDecodeToDir(bindingTransactionClaimDto.getFile(), CLAIM_PROJECT, dirFile,
									AsuransiKapalkuConstant.FileTypeExtension.PNG, url + dirUploadFile + BINDING_DIR));
				}
				if (bindingTransactionClaimDto.getFile().contains("data:application/pdf;base64")) {
					logger.info("CLAIM PDF");
					bindingTransactionClaim.setFile(
							FileUtil.fileDecodeToDir(bindingTransactionClaimDto.getFile(), CLAIM_PROJECT, dirFile,
									AsuransiKapalkuConstant.FileTypeExtension.PDF, url + dirUploadFile + BINDING_DIR));
				}
				if (bindingTransactionClaimDto.getFile().contains("zip;base64")) {
					logger.info("CLAIM ZIP");
					bindingTransactionClaim.setFile(
							FileUtil.fileDecodeToDir(bindingTransactionClaimDto.getFile(), CLAIM_PROJECT, dirFile,
									AsuransiKapalkuConstant.FileTypeExtension.ZIP, url + dirUploadFile + BINDING_DIR));
				}
				if (bindingTransactionClaimDto.getFile().contains("rar;base64")) {
					logger.info("CLAIM ZIP");
					bindingTransactionClaim.setFile(
							FileUtil.fileDecodeToDir(bindingTransactionClaimDto.getFile(), CLAIM_PROJECT, dirFile,
									AsuransiKapalkuConstant.FileTypeExtension.RAR, url + dirUploadFile + BINDING_DIR));
				}
			}
			bindingTransactionClaimRepository.save(bindingTransactionClaim);
		} catch (Exception e) {
			logger.error("GAGAL MEMBUAT KLAIM : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_ACCEPTABLE.value(), "Gagal membuat kalim.");
		}
		return respon;
	}

	private SupplierDto convertEntityToSupplierDto(Supplier supplier) {
		SupplierDto supplierDto = modelMapper.map(supplier, SupplierDto.class);
		supplierDto.setUsername(supplier.getUser().getUsername());
		return supplierDto;
	}

	private BindingTransactionSupplierMapDto convertToBindingTransactionSupplierMapDto(
			BindingTransactionSupplierMap bindingTransactionSupplierMap) {
		BindingTransactionSupplierMapDto bindingTransactionSupplierMapDto = modelMapper
				.map(bindingTransactionSupplierMap, BindingTransactionSupplierMapDto.class);
		bindingTransactionSupplierMapDto
				.setSupplierDto(modelMapper.map(bindingTransactionSupplierMap.getSupplier(), SupplierDto.class));
		return bindingTransactionSupplierMapDto;
	}

	private InsuranceOnGoingDto convertToInsuranceOnGoingDto(InsuranceOnGoing insuranceOnGoing) {
		InsuranceOnGoingDto insuranceOnGoingDto = modelMapper.map(insuranceOnGoing, InsuranceOnGoingDto.class);
		insuranceOnGoingDto
				.setInsuranceKindDto(modelMapper.map(insuranceOnGoing.getInsuranceKind(), InsuranceKindDto.class));
		insuranceOnGoingDto.getInsuranceKindDto().setInsuranceType(
				modelMapper.map(insuranceOnGoing.getInsuranceKind().getInsuranceType(), InsuranceTypeDto.class));
		return insuranceOnGoingDto;
	}

	private ShipInsuranceMapDto convertToInsuranceMapDto(ShipInsuranceMap shipInsuranceMap) {
		ShipInsuranceMapDto shipInsuranceMapDto = modelMapper.map(shipInsuranceMap, ShipInsuranceMapDto.class);
		shipInsuranceMapDto.setInsuranceMasterDto(
				modelMapper.map(shipInsuranceMap.getInsuranceMaster(), InsuranceMasterDto.class));
		shipInsuranceMapDto.getInsuranceMasterDto().setInsuranceKindDto(
				modelMapper.map(shipInsuranceMap.getInsuranceMaster().getInsuranceKind(), InsuranceKindDto.class));
		shipInsuranceMapDto.getInsuranceMasterDto().getInsuranceKindDto().setInsuranceType(modelMapper.map(
				shipInsuranceMap.getInsuranceMaster().getInsuranceKind().getInsuranceType(), InsuranceTypeDto.class));
		return shipInsuranceMapDto;
	}

	private BindingTransactionQuotationDto convertToQuotationDto(
			BindingTransactionQuotation bindingTransactionQuotation) {
		BindingTransactionQuotationDto bindingTransactionQuotationDto = modelMapper.map(bindingTransactionQuotation,
				BindingTransactionQuotationDto.class);
		if (!bindingTransactionQuotation.getQuoatationMaps().isEmpty()) {
			bindingTransactionQuotationDto.setQuoatationMaps(bindingTransactionQuotation.getQuoatationMaps().stream()
					.map(this::convertToQuotationMapDto).collect(Collectors.toList()));
		}

		return bindingTransactionQuotationDto;
	}

	private QuotationMapDto convertToQuotationMapDto(QuoatationMap quotationMap) {
		QuotationMapDto quotationMapDto = modelMapper.map(quotationMap, QuotationMapDto.class);
		quotationMapDto.setSupplier(modelMapper.map(quotationMap.getSupplier(), SupplierDto.class));
		quotationMapDto
				.setInsuranceMaster(modelMapper.map(quotationMap.getInsuranceMaster(), InsuranceMasterDto.class));
		if (!quotationMap.getQuotationMapSecurityLists().isEmpty()) {
			quotationMapDto.setQuotationMapSecurityLists(quotationMap.getQuotationMapSecurityLists().stream()
					.map(this::convertToQuotationMapSecurityListDto).collect(Collectors.toList()));
		}
		return quotationMapDto;
	}

	private QuotationMapSecurityListDto convertToQuotationMapSecurityListDto(
			QuotationMapSecurityList quotationMapSecurityList) {
		QuotationMapSecurityListDto quotationMapSecurityListDto = modelMapper.map(quotationMapSecurityList,
				QuotationMapSecurityListDto.class);
		quotationMapSecurityListDto.setInsuranceMaster(
				modelMapper.map(quotationMapSecurityList.getInsuranceMaster(), InsuranceMasterDto.class));
		return quotationMapSecurityListDto;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllBindingProcess(FilterDto filterDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();

			if (null == filterDto.getSorting()) {
				List<Sorting> sortings = new ArrayList<>();
				Sorting sorting = new Sorting();
				sorting.setColumnName("id");
				sorting.setIsAscending(false);
				sortings.add(sorting);
				filterDto.setSorting(sortings);
			}

			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewProject> page = viewProjectRepository.findAll(new Specification<ViewProject>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ViewProject> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					if (filterDto.getStatus() != null && !filterDto.getStatus().equalsIgnoreCase("")) {
						predicates.add(criteriaBuilder.equal(criteriaBuilder.upper(root.<String>get("status")),
								filterDto.getStatus()));
					}

					if (null != filterDto.getSearchBy()) {
						Predicate vesselName = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("vesselName")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate status = criteriaBuilder.like(criteriaBuilder.upper(root.<String>get("status")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate statusDesc = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("statusDescription")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						Predicate insurancetype = criteriaBuilder.like(
								criteriaBuilder.upper(root.<String>get("insurancetype")),
								"%" + filterDto.getSearchBy().toUpperCase() + "%");
						predicates.add(criteriaBuilder.or(vesselName, status, statusDesc, insurancetype));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<ViewProjectDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(p -> modelMapper.map(p, ViewProjectDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			dataRes.put("projects", list);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			respon.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST PROJEK: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "List projek tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<List<Map<String, Object>>> findStatus() {
		APIResponse<List<Map<String, Object>>> respon = new APIResponse<>();
		List<Map<String, Object>> maps = new ArrayList<>();
		for (StatusBindingProcessEnum sse : StatusBindingProcessEnum.values()) {
			Map<String, Object> map = new HashMap<>();
			map.put("code", sse.code);
			map.put("name", sse.label);
			maps.add(map);
		}
		respon.setData(maps);
		return respon;
	}

	@Override
	public APIResponse<List<Map<String, Object>>> findStatusType() {
		APIResponse<List<Map<String, Object>>> respon = new APIResponse<>();
		List<Map<String, Object>> maps = new ArrayList<>();
		for (StatusTypeEnum sse : StatusTypeEnum.values()) {
			Map<String, Object> map = new HashMap<>();
			map.put("code", sse.code);
			map.put("name", sse.label);
			maps.add(map);
		}
		respon.setData(maps);
		return respon;
	}

	@Override
	public APIResponse<String> updateAssignee(AssignSupplierDto assignSupplierDto) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			logger.info("Object : {}", assignSupplierDto);
			if (!assignSupplierDto.getSuppliers().isEmpty()) {
				bindingTransactionDocumentRepository.deleteMapBySupplierIds(assignSupplierDto.getSuppliers(),
						assignSupplierDto.getBindingId());
				bindingTransactionSupplierMapRepository.deleteMapBySupplierIds(assignSupplierDto.getSuppliers(),
						assignSupplierDto.getBindingId());
				logger.info("SUKSES CLEAR");
			}

			BindingTransaction bindingTransaction = bindingTransactionRepository
					.findById(assignSupplierDto.getBindingId()).orElse(null);
			if (null == bindingTransaction) {
				return new APIResponse<>(HttpStatus.NOT_FOUND.value(), "Data projek tidak ditemukan.");
			}
			List<Long> newSupplier = new ArrayList<>();
			if (!bindingTransaction.getBindingTransactionSupplierMaps().isEmpty()) {
				for (Long l : assignSupplierDto.getSuppliers()) {
					boolean exists = false;
					for (BindingTransactionSupplierMap btsm : bindingTransaction.getBindingTransactionSupplierMaps()) {
						if (l.equals(btsm.getSupplier().getId())) {
							exists = true;
							break;
						}
					}

					if (!exists) {
						newSupplier.add(l);
					}
				}
			} else {
				bindingTransaction.setBindingTransactionSupplierMaps(new ArrayList<>());
				newSupplier.addAll(assignSupplierDto.getSuppliers());
			}

			List<BindingTransactionSupplierMap> bindingTransactionSupplierMaps = new ArrayList<>();
			for (Long ll : newSupplier) {
				BindingTransactionSupplierMap bindingTransactionSupplierMap = new BindingTransactionSupplierMap();
				bindingTransactionSupplierMap.setSupplier(supplierRepository.findById(ll).orElse(null));
				bindingTransactionSupplierMap.setBindingTransaction(bindingTransaction);
				bindingTransactionSupplierMaps.add(bindingTransactionSupplierMap);
			}
			bindingTransaction.getBindingTransactionSupplierMaps().addAll(bindingTransactionSupplierMaps);

			bindingTransaction.setStatus(StatusBindingProcessEnum.OUTSTANDING_QUOTATION_SUPPLIER.code);

			BindingTransactionHistory bindingTransactionHistory = new BindingTransactionHistory();
			bindingTransactionHistory.setBindingTransaction(bindingTransaction);
			bindingTransactionHistory.setHistoryDate(new Date());
			bindingTransactionHistory.setStatus(StatusBindingProcessEnum.OUTSTANDING_QUOTATION_SUPPLIER.code);
			bindingTransactionHistory.setDescription(StatusBindingProcessEnum.OUTSTANDING_QUOTATION_SUPPLIER.label);

			bindingTransactionHistoryRepository.save(bindingTransactionHistory);

		} catch (Exception e) {
			logger.error("GAGAL MENYIMPAN DATA [UPDATE ASSIGNEE PROSES BINDING] : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal menyimpan data.");
		}
		return respon;
	}

	@Override
	public APIResponse<Map<String, Object>> findAllDocumentTc(FilterDto filterDto) {
		APIResponse<Map<String, Object>> respon = new APIResponse<>();
		try {
			Map<String, Object> dataRes = new HashMap<>();
			if (null == filterDto.getSorting()) {
				List<Sorting> sortings = new ArrayList<>();
				Sorting sorting = new Sorting();
				sorting.setColumnName("id");
				sorting.setIsAscending(false);
				sortings.add(sorting);
				filterDto.setSorting(sortings);
			}
			Pageable pageable = PopulateUtil.populatePageable(filterDto);
			Page<ViewProjectTc> page = viewProjectTcRepository.findAll(new Specification<ViewProjectTc>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				@Override
				public Predicate toPredicate(Root<ViewProjectTc> root, CriteriaQuery<?> query,
						CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();

					if (filterDto.getUploaderName() != null && !filterDto.getUploaderName().equalsIgnoreCase("")) {
						predicates.add(criteriaBuilder.equal(criteriaBuilder.upper(root.<String>get("uploaderName")),
								filterDto.getUploaderName()));
					}

					if (filterDto.getStatus() != null && !filterDto.getStatus().equalsIgnoreCase("")) {
						predicates.add(criteriaBuilder.equal(criteriaBuilder.upper(root.<String>get("status")),
								filterDto.getStatus()));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);
			List<ViewProjectTcDto> list = new ArrayList<>();
			int currentPage = 0;
			int totalPage = 0;
			long totalElement = 0L;
			if (null != page) {
				list = page.getContent().stream().map(p -> modelMapper.map(p, ViewProjectTcDto.class))
						.collect(Collectors.toList());
				currentPage = page.getNumber();
				totalElement = page.getTotalElements();
				totalPage = page.getTotalPages();
			}

			dataRes.put("documents", list);
			dataRes.put(CURRENT_PAGE, currentPage);
			dataRes.put(TOTAL_ITEM, totalElement);
			dataRes.put(TOTAL_PAGE, totalPage);
			respon.setData(dataRes);
		} catch (Exception e) {
			logger.error("GAGAL MEMUAT LIST DOKUMEN TC: {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "List dokumen tc tidak ditemukan.");
		}
		return respon;
	}

	@Override
	public APIResponse<String> selectedQuotation(Map<String, Object> map) {
		APIResponse<String> respon = new APIResponse<>();
		try {
			Date currentDate = new Date();
			Integer id = (Integer) map.get("id");
			BindingTransactionQuotation bindingTransactionQuotation = bindingTransactionQuotationRepository
					.findById(Long.valueOf(id)).orElse(null);
			if (null == bindingTransactionQuotation) {
				logger.error("DATA TIDAK DITEMUKAN : {}", bindingTransactionQuotation);
				return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal memilih quotation.");
			}
			bindingTransactionQuotation.getBindingTransaction().setStatus(StatusBindingProcessEnum.BINDING.code);
			bindingTransactionQuotation.getBindingTransaction().setUpdatedBy(user);
			bindingTransactionQuotation.getBindingTransaction().setUpdatedDate(currentDate);

			BindingTransactionHistory bindingTransactionHistory = new BindingTransactionHistory();
			bindingTransactionHistory.setBindingTransaction(bindingTransactionQuotation.getBindingTransaction());
			bindingTransactionHistory.setHistoryDate(currentDate);
			bindingTransactionHistory.setStatus(StatusBindingProcessEnum.BINDING.code);
			bindingTransactionHistory.setDescription(StatusBindingProcessEnum.BINDING.label);

			bindingTransactionQuotation.setIsAccepted(Boolean.TRUE);
			bindingTransactionQuotationRepository.save(bindingTransactionQuotation);

			bindingTransactionHistoryRepository.save(bindingTransactionHistory);
		} catch (Exception e) {
			logger.error("GAGAL MEMILIH QUOTATION : {}", e.getMessage());
			return new APIResponse<>(HttpStatus.NOT_MODIFIED.value(), "Gagal memilih quotation.");
		}
		return respon;
	}

	private String validationUploadTc(UploadTcDto uploadTcDto) {
		if (null == uploadTcDto.getFile()) {
			return "Mohon pilih file.";
		}

		if (null == uploadTcDto.getFileName()) {
			return "Mohon isi nama file.";
		}

		if (null == uploadTcDto.getTo()) {
			return "Mohon isi tujuan file upload.";
		}

		return null;
	}

	private void validationRequestPlacement(ReplacementDto replacementDto) throws ValidateException {
		if (null == replacementDto.getId()) {
			throw new ValidateException("Mohon isi id kapal.");
		}

		if (replacementDto.getIdInsurances().isEmpty() && Boolean.FALSE.equals(replacementDto.getIsPublic())) {
			throw new ValidateException("Mohon pilih salah satu asuransi.");
		}
	}

	private String validation(BindingProcessDto bindingProcessDto) {
		if (bindingProcessDto.getId() == null) {
			return "Binding id harus diisi.";
		}

		if (bindingProcessDto.getStatus() == null) {
			return "Status harus diisi.";
		}

		return null;
	}

	private String quotationValidation(List<BindingTransactionQuotationDto> bindingTransactionQuotationDtos) {
		for (BindingTransactionQuotationDto dto : bindingTransactionQuotationDtos) {
			if (dto.getIdBinding() == null) {
				return "Binding id harus diisi.";
			}
			if (dto.getName() == null) {
				return "Nama file harus diisi.";
			}
		}

		return null;
	}
}
