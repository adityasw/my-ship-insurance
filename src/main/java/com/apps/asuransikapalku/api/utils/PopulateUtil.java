package com.apps.asuransikapalku.api.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.apps.asuransikapalku.api.model.dto.FilterDto;
import com.apps.asuransikapalku.api.model.dto.FilterDto.Sorting;

public class PopulateUtil {
	private PopulateUtil() {
		throw new IllegalStateException("Util Class");
	}
	
	public static PageRequest populatePageable(FilterDto filterDto) {
		List<Order> orders = new ArrayList<>();
		PageRequest pageRequest;
		if (null != filterDto.getSorting() && !filterDto.getSorting().isEmpty()) {
			for(Sorting s : filterDto.getSorting()) {
				if (s.getIsAscending()) {
					orders.add(new Order(Direction.ASC, s.getColumnName()));
				} else {
					orders.add(new Order(Direction.DESC, s.getColumnName()));
				}
			}
			pageRequest = PageRequest.of(filterDto.getPaging().getPage(), filterDto.getPaging().getSize(), Sort.by(orders));
		} else {
			pageRequest = PageRequest.of(filterDto.getPaging().getPage(), filterDto.getPaging().getSize());
		}
		
		return pageRequest;
		
	}
}
