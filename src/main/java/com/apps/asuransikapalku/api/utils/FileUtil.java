package com.apps.asuransikapalku.api.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Base64;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.apps.asuransikapalku.api.constant.AsuransiKapalkuConstant;
import com.apps.asuransikapalku.api.exception.CommonException;

public class FileUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);
	private static final String SLICE = "/";
	
	private FileUtil() {
		throw new IllegalStateException("Utility class");
	}
	
	public static String imgToDir(MultipartFile file, String name, String directory, String ext) throws CommonException {
		String fileName = name.toUpperCase()+"_"+FormatDateUtil.dateToStr(new Date(), "ddMMyyyyHHMMSS");
		File dir = createDirectory(directory);
		String pathFileName =  dir.getAbsolutePath()+SLICE+fileName+ext;
		try(BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(pathFileName))) {
			 byte[] bytes = file.getBytes();
			 stream.write(bytes);
		     stream.flush();
		} catch (IOException e) {
			throw new CommonException(e.getMessage());
		}
		return pathFileName;
	}
	
	public static String imgDecodeToDir(String imgEncode, String name, String directory, String ext, String url) throws CommonException {
		LOGGER.info("> START SAVE IMAGE");
		name = name.replaceAll("\\s+","");
		name = name.replaceAll("\\p{Punct}", "_");
		String fileName = name.toUpperCase()+"_"+FormatDateUtil.dateToStr(new Date(), "ddMMyyyyHHMMSS");
		File file = new File(directory);
		final Path path = file.toPath();
		
		if (!path.toFile().exists() || !path.toFile().isDirectory()) {
			file.mkdir();
		}
		Path pathOutput = Paths.get(path.toString(), fileName+ext);
		String pathFileName =  pathOutput.toAbsolutePath().toString();
		try {
			imgEncode = imgEncode.replace("data:image/jpeg;base64,", "");
			imgEncode = imgEncode.replace("data:image/png;base64,", "");
			File serverFile = new File(pathFileName);

            serverFile.setWritable(true, false);
            serverFile.setExecutable(true, false);
            serverFile.setReadable(true, false);

            BufferedOutputStream stream = null;
            stream = new BufferedOutputStream(new FileOutputStream(serverFile));

            stream.write(Base64.getMimeDecoder().decode(imgEncode));
            stream.close();
            
            Set<PosixFilePermission> perms = new HashSet<>();
            perms.add(PosixFilePermission.OWNER_READ);
            perms.add(PosixFilePermission.OWNER_WRITE);
            perms.add(PosixFilePermission.GROUP_READ);
            perms.add(PosixFilePermission.OTHERS_READ);

            Files.setPosixFilePermissions(serverFile.toPath(), perms);
		} catch (Exception e) {
			LOGGER.error("> FAILED TO SAVE IMAGE WITH ERROR {}", e.getMessage());
			throw new CommonException(e.getMessage());
		}
		LOGGER.info("> IMAGE FILE HAS BEEN SAVED IN {}", pathFileName);
        return url+"/"+fileName+ext;
	}
	
	public static String fileDecodeToDir(String fileEncode, String name, String directory, String ext, String url) throws CommonException {
		name = name.replaceAll("\\s+","");
		name = name.replaceAll("\\p{Punct}", "_");
		String fileName = name.toUpperCase()+"_"+FormatDateUtil.dateToStr(new Date(), "ddMMyyyyHHMMSS");
		File file = new File(directory);
		final Path path = file.toPath();
		
		if (!path.toFile().exists() || !path.toFile().isDirectory()) {
			file.mkdir();
		}
		Path pathOutput = Paths.get(path.toString(), fileName+ext);
		String pathFileName =  pathOutput.toAbsolutePath().toString();
		try {
			fileEncode = fileEncode.replace("data:application/pdf;base64,", "");
			File serverFile = new File(pathFileName);

            serverFile.setWritable(true, false);
            serverFile.setExecutable(true, false);
            serverFile.setReadable(true, false);

            BufferedOutputStream stream = null;
            stream = new BufferedOutputStream(new FileOutputStream(serverFile));

            stream.write(Base64.getMimeDecoder().decode(fileEncode));
            stream.close();
            
            Set<PosixFilePermission> perms = new HashSet<>();
            perms.add(PosixFilePermission.OWNER_READ);
            perms.add(PosixFilePermission.OWNER_WRITE);
            perms.add(PosixFilePermission.GROUP_READ);
            perms.add(PosixFilePermission.OTHERS_READ);

            Files.setPosixFilePermissions(serverFile.toPath(), perms);
		} catch (Exception e) {
			LOGGER.error("> FAILED TO SAVE FILE WITH ERROR {}", e.getMessage());
			throw new CommonException(e.getMessage());
		}
		LOGGER.info("> FILE HAS BEEN SAVED IN {}", pathFileName);
        return url+"/"+fileName+ext;
	}
	
	private static File createDirectory(String path) {
		File dir = new File(path);
		if(!dir.exists()) {
			dir.mkdirs();
		}
		return dir;
	}
	
	public static String encode(String path) throws CommonException {
		File file = new File(path);
	      try (FileInputStream imageInFile = new FileInputStream(file)) {
	          // Reading a Image file from file system
	        String base64Image = "";
	          byte imageData[] = new byte[(int) file.length()];
	          imageInFile.read(imageData);
	          base64Image = Base64.getEncoder().encodeToString(imageData);
	          return base64Image;
	      } catch (FileNotFoundException e) {
	    	  throw new CommonException(e.getMessage());
	      } catch (IOException ioe) {
	    	  throw new CommonException(ioe.getMessage());
	      }
	}
	
	public static void decode(String imgStr, String path, String ext) throws CommonException {
		String fileName = FormatDateUtil.dateToStr(new Date(), "dd-MM-yyyy");
		String pathFileName =  path+fileName+ext;
		try(FileOutputStream imageOutFile = new FileOutputStream(pathFileName)) {
			byte[] imageByteArray = Base64.getDecoder().decode(imgStr);
			imageOutFile.write(imageByteArray);		
			imageOutFile.flush();
		} catch (Exception e) {
			throw new CommonException(e.getMessage());
		}
	}
	
	public static String saveDocument(MultipartFile file, String name, String directory, String ext, String url) throws CommonException {
		String fileName = name.toUpperCase()+"_"+FormatDateUtil.dateToStr(new Date(), "ddMMyyyyHHMMSS");
		try {
			byte[] bytes = file.getBytes();
            File dir = new File(directory + File.separator + "name");
            if (!dir.exists()) {
                dir.mkdirs();
            }

            File serverFile = new File(dir.getAbsolutePath()+File.separator+fileName+ext);
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
            
            serverFile.setWritable(true, false);
            serverFile.setExecutable(true, false);
            serverFile.setReadable(true, false);
            
            stream.write(bytes);
            stream.close();
            
            Set<PosixFilePermission> perms = new HashSet<>();
            perms.add(PosixFilePermission.OWNER_READ);
            perms.add(PosixFilePermission.OWNER_WRITE);
            perms.add(PosixFilePermission.GROUP_READ);
            perms.add(PosixFilePermission.OTHERS_READ);

            Files.setPosixFilePermissions(serverFile.toPath(), perms);

            System.out.println("Server File Location=" + serverFile.getAbsolutePath());

        } catch (Exception e) {
        	LOGGER.error("> FAILED TO SAVE DOCUMENT WITH ERROR {}", e.getMessage());
			throw new CommonException(e.getMessage());
        }
		return url+"/"+fileName+ext;
	}
	
	public static String saveFileDocumentSingle(String fileName, String file, String dirFile, String locationFile) throws CommonException {
		if (file.contains("/pdf;base64")) {
			return fileDecodeToDir(file, fileName,
					dirFile, AsuransiKapalkuConstant.FileTypeExtension.PDF, locationFile);
		}

		if (file.contains("/doc;base64")) {
			return fileDecodeToDir(file, fileName,
					dirFile, AsuransiKapalkuConstant.FileTypeExtension.DOC, locationFile);
		}

		if (file.contains("/xls;base64")) {
			return fileDecodeToDir(file, fileName,
					dirFile, AsuransiKapalkuConstant.FileTypeExtension.XLS, locationFile);
		}

		if (file.contains("/xlsx;base64")) {
			return fileDecodeToDir(file, fileName,
					dirFile, AsuransiKapalkuConstant.FileTypeExtension.XLSX, locationFile);
		}

		if (file.contains("/png;base64")) {
			return fileDecodeToDir(file, fileName,
					dirFile, AsuransiKapalkuConstant.FileTypeExtension.PNG, locationFile);
		}

		if (file.contains("/jpeg;base64")
				|| file.contains("/jpg;base64")) {
			return fileDecodeToDir(file, fileName,
					dirFile, AsuransiKapalkuConstant.FileTypeExtension.JPG, locationFile);
		}
		return file;
	}
}
