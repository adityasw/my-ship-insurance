package com.apps.asuransikapalku.api.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class FormatDateUtil {
	private FormatDateUtil() {
		throw new IllegalStateException("Utility class");
	}
	
	public static Date strToDate(String dateStr, String format) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		return sdf.parse(dateStr);
	}
	
	public static String dateToStr(Date date, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);  
		return dateFormat.format(date);  
	}
	
	public static String dateToStr(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int year = cal.get(Calendar.YEAR);
		return day+" "+getMonth(month)+" "+year+" "+sdf.format(date);  
	}
	
	public static Date additionDateTime(int field, int amount, Date date) {
		Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.add(field, amount);
	    return calendar.getTime();
	}
	
	private static String getMonth(int id) {
		String str = "";
		if (id == 1) {
			str = "Januari";
		}
		if (id == 2) {
			str = "Februari";
		}
		if (id == 3) {
			str = "Maret";
		}
		if (id == 4) {
			str = "April";
		}
		if (id == 5) {
			str = "Mei";
		}
		if (id == 6) {
			str = "Juni";
		}
		if (id == 7) {
			str = "Juli";
		}
		if (id == 8) {
			str = "Agustus";
		}
		if (id == 9) {
			str = "September";
		}
		if (id == 10) {
			str = "Oktober";
		}
		if (id == 11) {
			str = "November";
		}
		if (id == 12) {
			str = "Desember";
		}
		return str;
	}
}
