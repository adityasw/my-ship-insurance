package com.apps.asuransikapalku.api.exception;

public class ValidateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3488054905342852790L;
	
	public ValidateException(String message) {
		super(message);
	}

}
