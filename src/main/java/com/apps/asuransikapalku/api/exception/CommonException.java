package com.apps.asuransikapalku.api.exception;

public class CommonException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8983450550248672234L;
	public CommonException(String message) {
		super(message);
	}

}
