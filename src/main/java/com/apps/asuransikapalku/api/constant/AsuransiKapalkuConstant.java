package com.apps.asuransikapalku.api.constant;

public class AsuransiKapalkuConstant {
	private AsuransiKapalkuConstant() {
		throw new IllegalStateException("Constant class");
	}

	public static final class ControllerMapping {
		public static final String API = "/api";
		public static final String ADMIN = API + "/admin";
		public static final String BINDING_PROCESS = API + "/binding-process";
		public static final String CHAT = API + "/chat";
		public static final String CLAIM_FINANCE = API + "/claim-finance";
		public static final String CLASSIFICATION_BUREAU = API + "/classification-bureau";
		public static final String DEMAND = API + "/demand";
		public static final String INSURANCE_KIND = API + "/insurance-kind";
		public static final String INSURANCE_MASTER = API + "/insurance-master";
		public static final String INSURANCE_TYPE = API + "/insurance-type";
		public static final String MARINE_CONNECT = API + "/marine-connect";
		public static final String OCCUPATION = API + "/occupation";
		public static final String OCCUPATION_TYPE = API + "/occupation-type";
		public static final String REGION = API + "/region";
		public static final String SHIP = API + "/ship";
		public static final String SHIP_TYPE = API + "/ship-type";
		public static final String SUPPLIER = API + "/supplier";
		public static final String ROLE = API + "/role";
		public static final String USER = API + "/user";
	}

	public static final class Email {
		public static final String NO_REPLY_SENDER = "noReplySender";
		public static final String NO_REPLY_SENDER_VALUE = "no-reply@asuransikapalku.com";
		public static final String TEMPLATE = "TEMPLATE";
		public static final String SUBJECT = "SUBJECT";
		public static final String TO = "TO";
		public static final String CC = "CC";
		public static final String BCC = "BCC";

		public static final String LINK = "LINK";
		public static final String USERNAME = "USERNAME";
		public static final String PASSWORD = "PASSWORD";
		public static final String CUSTOMER_NAME = "CUSTOMER_NAME";
		public static final String ZOOM_LINK = "ZOOM_LINK";
		public static final String SCHEDULE_MEET = "SCHEDULE_MEET";
		public static final String STATUS_BINDING = "STATUS_BINDING";

		private Email() {

		}
	}

	public static final class FileTypeExtension {
		public static final String CSV = ".csv";
		public static final String DOC = ".docx";
		public static final String PDF = ".pdf";
		public static final String XLS = ".xls";
		public static final String XLSX = ".xlsx";
		public static final String PGP = ".pgp";
		public static final String JPG = ".jpg";
		public static final String SVG = ".svg";
		public static final String PNG = ".png";
		public static final String ZIP = ".zip";
		public static final String RAR = ".rar";

		private FileTypeExtension() {

		}
	}
}
