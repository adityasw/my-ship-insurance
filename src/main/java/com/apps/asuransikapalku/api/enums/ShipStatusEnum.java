package com.apps.asuransikapalku.api.enums;

public enum ShipStatusEnum {
	WAITING_FOR_APPROVAL("WAIT-APP", "Waiting For Approval"),
	APPROVED("APP", "Approved"),
	REJECTED("REJ", "Rejected");

	public final String code;
	public final String label;

	private ShipStatusEnum(String code, String label) {
		this.code = code;
		this.label = label;
	}
}
