package com.apps.asuransikapalku.api.enums;

public enum StatusEnum {
	PENDING_ASSESSMENT("PND-ASS", "Pending Assessment"), PENDING_VERIFICATION("PND-VER", "Pending Verification"),
	VERIFIED("VER", "Verified"), REJECTED("REJ", "Rejected");

	public final String code;
	public final String label;

	private StatusEnum(String code, String label) {
		this.code = code;
		this.label = label;
	}
}
