package com.apps.asuransikapalku.api.enums;

public enum StatusBindingProcessEnum {
	OUTSTANDING_PLACEMENT("OUT-PLC", "Outstanding Placement", "Demand melakukan permintaan penempatan kapal.", "In Progress"), 
	OUTSTANDING_QUOTATION_SUPPLIER("OUT-QUOT-SUP", "Outstanding Quotation From Supplier", "Admin meneruskan permintaan ke supplier.", "In Progress"),
	OUTSTANDING_NEGOTIATION("OUT-NEGO", "Outstanding Negotiation", "Admin melakukan negosiasi.", "In Progress"), 
	OUTSTANDING_FINAL_OFFERING("OUT-FINOFF", "Outstanding Final Offering", "Admin telah melakukan negosiasi.", "In Progress"),
	OUTSTANDING_CONFIRMATION_SUPPLIER("OUT-CONF", "Outstanding Binding Confirmation", "Menunggu konfirmasi dari Supply.", "Waiting For Confirmation"),
	OUTSTANDING_QUOTATION_DEMAND("OUT-QUOT-DEM", "Outstanding Quotation From Demand", "Admin mengirimkan quotation ke demand.", "Binding"),
	BINDING("BIND", "Bind", "Proses binding", "Binding"),
	CANCELLED("CAN", "Cancelled", "Projek dibatalkan", "Binding");

	public final String code;
	public final String label;
	public final String desc;
	public final String secondLabel;

	private StatusBindingProcessEnum(String code, String label, String desc, String secondLabel) {
		this.code = code;
		this.label = label;
		this.desc = desc;
		this.secondLabel = secondLabel;
	}
}
