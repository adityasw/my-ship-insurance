package com.apps.asuransikapalku.api.enums;

public enum StatusFileEnum {
	REVIEW("REVIEW", "Review"), 
	ALMOST_FINAL("ALMOST-FINAL", "Almost Final"),
	FINAL("FINAL", "Final");

	public final String code;
	public final String label;

	private StatusFileEnum(String code, String label) {
		this.code = code;
		this.label = label;
	}
}
