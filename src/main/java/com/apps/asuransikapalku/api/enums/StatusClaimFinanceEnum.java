package com.apps.asuransikapalku.api.enums;

public enum StatusClaimFinanceEnum {
	WAITING_FOR_AK_REVIEW("WAIT_AK_REVIEW", "Waiting For AK Review"),
	AK_OFFERING("AK_OFFERING", "AK Offering"),
	WAITING_FOR_FINANCE_OFFERING("WAIT_FIN_OFFERING", "Waiting for Financing Offering"),
	CASE_CLOSED_BY_SHIPOWNER("CASE_CLOSED_BY_SHIPOWNER", "Case Closed By Shipowner"),
	QUOTATION_IS_CLOSED_BY_AK("QUOTATION_IS_CLOSED_BY_AK", "Quotation is Closed By AK"),
	AVAILABLE("AVAILABLE", "Available"),
	CLAIM_IS_IN_PROGRESS("CLAIM_IS_IN_PROGRESS","Clain In Progress"),
	IN_PROGRESS("INPROGRESS", "In Progress"),
	APPROVED("APP", "Approved"),
	REJECTED("REJ", "Rejected");

	public final String code;
	public final String label;

	private StatusClaimFinanceEnum(String code, String label) {
		this.code = code;
		this.label = label;
	}
}
