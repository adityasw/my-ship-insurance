package com.apps.asuransikapalku.api.enums;

public enum StatusMarineConnectEnum {
	
	WAITING_QUOTATION("WAIT-QUO", "Waiting Quotation"),
	INPROGRESS("INPROGRESS", "In Progress"),
	NEGOTIATE("NEGOTIATE", "Negotiation"),
	UNPAID("UNPAID", "Unpaid"),
	ACCEPT("ACCEPT", "Accept"),
	REJECT("REJECT", "Reject"),
	PAID("PAID", "Paid"),
	CANCEL("CANCEL", "Cancel"),
	FINISH("FINISH", "Finish");
	
	public final String code;
	public final String label;

	private StatusMarineConnectEnum(String code, String label) {
		this.code = code;
		this.label = label;
	}
}
