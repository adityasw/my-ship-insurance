package com.apps.asuransikapalku.api.enums;

public enum StatusTypeEnum {
	LEADER("LEADER", "Leader"), 
	MEMBER("MEMBER", "Member");

	public final String code;
	public final String label;

	private StatusTypeEnum(String code, String label) {
		this.code = code;
		this.label = label;
	}
}
