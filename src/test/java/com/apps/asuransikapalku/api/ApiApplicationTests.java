package com.apps.asuransikapalku.api;

import org.junit.jupiter.api.Test;

//@SpringBootTest
class ApiApplicationTests {

	//@Test
	void contextLoads() {
		String[] strs = new String[] {"saras.sadanti@gmail.com","weddingpath.id@gmail.com","sridhanipamungkas@gmail.com"};
		int size = strs.length;
		StringBuilder sb = new StringBuilder();
		for(int i=0; i < strs.length; i++) {
			sb.append(strs[i]);
			if (size > 1) {
				sb.append(",");
				size = size-1;
			}
		}
		System.out.println(sb.toString());
		
	}
	
	@Test
	public void splitTest() {
		String url = "https://api-staging.asuransikapalku.com/file/claimfinancing/TEST.pdf";
		String[] strSplit = url.split("/");
		System.out.println(strSplit[strSplit.length-1]);
	}
}
